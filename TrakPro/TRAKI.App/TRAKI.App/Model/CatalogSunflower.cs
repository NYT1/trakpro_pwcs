﻿using System;
using System.ComponentModel;
using TRAKI.App.Common;


namespace TRAKI.App.Model
{
    public class CatalogSunflower : INotifyPropertyChanged, IDataErrorInfo
    {
        private string _catkey;
        private string _orgmfgcode;
        private string _mfgname;
        private string _model;
        private string _modelname;
        private string _offname;
        private DateTime? _cdate;
        private DateTime? _mdate;
        private DateTime? _sdate;
        private DateTime? _edate;
        private string _flag;
        private string _mfgcode;
        private int _lineNo;


        public CatalogSunflower()
        {

        }

        #region Properties

        public int LineNo
        {
            get { return _lineNo; }
            set
            {
                _lineNo = value;
                NotifyPropertyChanged("LineNo");
            }
        }

        public string Catkey
        {
            get { return _catkey; }
            set
            {
                _catkey = value;
                NotifyPropertyChanged("Catkey");
            }
        }

        public string orgmfgcode
        {
            get { return _orgmfgcode; }
            set
            {
                _orgmfgcode = value;
                NotifyPropertyChanged("orgmfgcode");
            }
        }

        public string MfgName
        {
            get { return _mfgname; }
            set
            {
                _mfgname = value;
                NotifyPropertyChanged("MfgName");
            }
        }

        public string Model
        {
            get { return _model; }
            set
            {
                _model = value;
                NotifyPropertyChanged("Model");
            }
        }

        public string ModelName
        {
            get { return _modelname; }
            set
            {
                _modelname = value;
                NotifyPropertyChanged("ModelName");
            }
        }

        public string OffName
        {
            get { return _offname; }
            set
            {
                _offname = value;
                NotifyPropertyChanged("OffName");
            }
        }

        public DateTime? CDate
        {
            get { return _cdate; }
            set
            {
                _cdate = value;
                NotifyPropertyChanged("CDate");
            }
        }

        public DateTime? MDate
        {
            get { return _mdate; }
            set
            {
                _mdate = value;
                NotifyPropertyChanged("MDate");
            }
        }
        public DateTime? SDate
        {
            get { return _sdate; }
            set
            {
                _sdate = value;
                NotifyPropertyChanged("SDate");
            }
        }
        public DateTime? EDate
        {
            get { return _edate; }
            set
            {
                _edate = value;
                NotifyPropertyChanged("EDate");
            }
        }


        public string Flag
        {
            get { return _flag; }
            set
            {
                _flag = value;
                NotifyPropertyChanged("Flag");
            }
        }

        public string MfgCode
        {
            get { return _mfgcode; }
            set
            {
                _mfgcode = value;
                NotifyPropertyChanged("MfgCode");
            }
        }


        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Private Helpers

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion



        #region IDataErrorInfo Members

        string IDataErrorInfo.Error
        {
            get { return null; }
        }

        string IDataErrorInfo.this[string columnName]
        {
            get
            {
                this.IsValid = true;

                if (columnName == "Catkey")
                {
                    return string.IsNullOrEmpty(this._catkey) ? "Catkey is Required value" : null;
                }
                if (columnName == "Model")
                {
                    return string.IsNullOrEmpty(this._model) ? "Model is Required value" : null;
                }
                if (columnName == "ModelName")
                {
                    return string.IsNullOrEmpty(this._modelname) ? "ModelName is Required value" : null;
                }
                if (columnName == "orgmfgcode")
                {
                    return string.IsNullOrEmpty(this._orgmfgcode) ? "orgmfgcode is Required value" : null;
                }
                if (columnName == "OffName")
                {
                    return string.IsNullOrEmpty(this._offname) ? "OffName is Required value" : null;
                }
                if (columnName == "MfgName")
                {
                    return string.IsNullOrEmpty(this._mfgname) ? "MfgName is Required value" : null;
                }



                return null;
            }
        }
        #endregion

        #region validation

        static readonly string[] ValidatedProperties =
        {
            "Catkey",
            "Model",
            "ModelName",
            "orgmfgcode",
            "OffName",
             "MfgName",
             "CDate",
             "MDate",
             "SDate",
             "EDate",
              
        };

        private bool _isValid;

        public bool IsValid
        {
            get { return _isValid; }
            set
            {
                foreach (string property in ValidatedProperties)
                    if (GetValidationErrors(property) != null)
                    {
                        _isValid = false;
                        NotifyPropertyChanged("IsValid");
                        return;
                    }
                _isValid = true;
                NotifyPropertyChanged("IsValid");
            }
        }

        string GetValidationErrors(string PropertyName)
        {
            string Error = null;
            switch (PropertyName)
            {

                case "Catkey":
                    Error = ValidateCatkey();
                    break;
                case "Model":
                    Error = ValidateModel();
                    break;
                case "ModelName":
                    Error = ValidateModelName();
                    break;
                case "orgmfgcode":
                    Error = Validateorgmfgcode();
                    break;
                case "MfgName":
                    Error = ValidateMfgName();
                    break;
                case "OffName":
                    Error = ValidateOffName();
                    break;

            }
            return Error;
        }


        private string ValidateModelName()
        {
            if (string.IsNullOrWhiteSpace(ModelName))
            {
                return "Model Name is required!";
            }
            else
            {
                return null;
            }
        }

        private string Validateorgmfgcode()
        {
            if (string.IsNullOrWhiteSpace(orgmfgcode))
            {
                return "Org MfgCode is required!";
            }
            else
            {
                return null;
            }
        }
        private string ValidateOffName()
        {
            if (string.IsNullOrWhiteSpace(OffName))
            {
                return "Official Name is required!";
            }
            else
            {
                return null;
            }
        }

        private string ValidateMfgName()
        {
            if (string.IsNullOrWhiteSpace(MfgName))
            {
                return "Mfg Name is required!";
            }
            else
            {
                return null;
            }
        }

        private string ValidateCatkey()
        {
            if (string.IsNullOrWhiteSpace(Catkey))
            {
                return "Catkey is required!";
            }
            else
            {
                return null;
            }
        }

        private string ValidateModel()
        {
            if (string.IsNullOrWhiteSpace(Model))
            {
                return "Model is required!";
            }
            else
            {
                return null;
            }
        }




        #endregion


    }
}
