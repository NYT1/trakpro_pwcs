﻿using System;
namespace TRAKI.App.Model
{
    class Report
    {
        private string _assetNumber;
        private string _buildingNumber;
        private string _buildingDescription;
        private string _buildingType;
        private DateTime? _dateSigned;
        private string _currentStatus;
        private decimal _totalCost;
        private decimal _missingCost;
        private decimal _missingCostPercentage;
        private decimal _cost;
        private string _pONumber;
        private string _serialNumber;
        private string _roomNumber;
        private string _catalogNumber;
        private string _catalogDescription;
        private DateTime? _inventoryDate;
        private string _manufacturerName;
        private string _modelNumber;
        private DateTime? _dateAcquired;
        private DateTime? _dateReceived;
        private string _scanBuildingNumber;
        private string _scanRoomNumber;
        private string _status;
        private string _dispositiondescription;


        public string DispositionDescription
        {
            get { return _dispositiondescription; }
            set { _dispositiondescription = value; }
        }


        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }

       

        public string AssetNumber
        {
            get { return _assetNumber; }
            set
            {
                _assetNumber = value;
            }
        }
        
        public string BuildingNumber
        {
            get { return _buildingNumber; }
            set
            {
                _buildingNumber = value;
            }
        }
        public string BuildingDescription
        {
            get { return _buildingDescription; }
            set
            {
                _buildingDescription = value;
            }
        }
        public string BuildingType
        {
            get { return _buildingType; }
            set
            {
                _buildingType = value;
            }
        }
        public decimal TotalCost
        {
            get { return _totalCost; }
            set
            {
                _totalCost = value;
            }
        }
        public decimal MissingCost
        {
            get { return _missingCost; }
            set
            {
                _missingCost = value;
            }
        }
        public decimal MissingCostPercentage
        {
            get { return _missingCostPercentage; }
            set
            {
                _missingCostPercentage = value;
            }
        }
        public DateTime? DateSigned
        {
            get { return _dateSigned; }
            set
            {
                _dateSigned = value;
            }
        }
        public string CurrentStatus
        {
            get { return _currentStatus; }
            set
            {
                _currentStatus = value;
            }
        }
        public decimal Cost
        {
            get { return _cost; }
            set
            {
                _cost = value;
            }
        }
        public string PONumber
        {
            get { return _pONumber; }
            set
            {
                _pONumber = value;
            }
        }
        public string CatalogNumber
        {
            get { return _catalogNumber; }
            set
            {
                _catalogNumber = value;
            }
        }
        public string CatalogDescription
        {
            get { return _catalogDescription; }
            set
            {
                _catalogDescription = value;
            }
        }
        public string SerialNumber
        {
            get { return _serialNumber; }
            set
            {
                _serialNumber = value;
            }
        }
        public string RoomNumber
        {
            get { return _roomNumber; }
            set
            {
                _roomNumber = value;
            }
        }
        public DateTime? InventoryDate
        {
            get { return _inventoryDate; }
            set
            {
                _inventoryDate = value;
            }
        }
        public string ManufacturerName
        {
            get { return _manufacturerName; }
            set
            {
                _manufacturerName = value;
            }
        }
        public string ModelNumber
        {
            get { return _modelNumber; }
            set
            {
                _modelNumber = value;
            }
        }
        public DateTime? DateAcquired
        {
            get { return _dateAcquired; }
            set
            {
                _dateAcquired = value;
            }
        }
        public DateTime? DateReceived
        {
            get { return _dateReceived; }
            set
            {
                _dateReceived = value;
            }
        }

        public string ScanBuildingNumber
        {
            get { return _scanBuildingNumber; }
            set { _scanBuildingNumber = value; }
        }

        public string ScanRoomNumber
        {
            get { return _scanRoomNumber; }
            set { _scanRoomNumber = value; }
        }

    }
}
