﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TRAKI.App.Common;
namespace TRAKI.App.Model
{
    public class Disposition : INotifyPropertyChanged, IDataErrorInfo
    {
        private int _dispositionId;
        private string _DispositionCode;
        private string _dispositionDescription;
        private BooleanValue _isActive;

        public Disposition()
        {
            this.IsActive = BooleanValue.True;
        }

        #region Properties

        public int DispositionId
        {
            get { return _dispositionId; }
            set
            {
                _dispositionId = value;
                NotifyPropertyChanged("DispositionId");
            }
        }

        public string DispositionCode
        {
            get { return _DispositionCode; }
            set
            {
                _DispositionCode = value;
                NotifyPropertyChanged("DispositionCode");
            }
        }

        public string DispositionDescription
        {
            get { return _dispositionDescription; }
            set
            {
                _dispositionDescription = value;
                NotifyPropertyChanged("DispositionDescription");
            }
        }
         
        public BooleanValue IsActive
        {
            get { return _isActive; }
            set
            {
                _isActive = value;
                NotifyPropertyChanged("IsActive");
            }
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Private Helpers

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IDataErrorInfo Members

        string IDataErrorInfo.Error
        {
            get { return null; }
        }


        string IDataErrorInfo.this[string PropertyName]
        {
            get
            {
                this.IsValid = true;
                return GetValidationErrors(PropertyName);
            }
        }


        #endregion
        
        #region validation

        static readonly string[] ValidatedProperties =
        {
            "DispositionCode",
            "DispositionDescription",
        };

        private bool _isValid;

        public bool IsValid
        {
            get { return _isValid; }
            set
            {
                foreach (string property in ValidatedProperties)
                    if (GetValidationErrors(property) != null)
                    {
                        _isValid = false;
                        NotifyPropertyChanged("IsValid");
                        return;
                    }
                        _isValid = true;
                NotifyPropertyChanged("IsValid");
            }
        }

        string GetValidationErrors(string PropertyName)
        {
            string Error = null;
            switch (PropertyName)
            {
                case "DispositionCode":
                    Error = ValidateDispositionCode();
                    break;
                case "DispositionDescription":
                    Error = ValidateDispositionDescription();
                    break;
                
            }
            return Error;
        }

        private string ValidateDispositionCode()
        {
            if (string.IsNullOrWhiteSpace(DispositionCode))
            {
                return "Disposition Code is required!";
            }
            else
            {
                return null;
            }
        }
        private string ValidateDispositionDescription()
        {
            if (string.IsNullOrWhiteSpace(DispositionDescription))
            {
                return "Description is required!";
            }
            else
            {
                return null;
            }
        }

        #endregion
    }
}
