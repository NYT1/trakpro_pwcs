﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TRAKI.App.Common;
namespace TRAKI.App.Model
{
    public class SystemControl : INotifyPropertyChanged, IDataErrorInfo
    {
        // private readonly Regex nameEx = new Regex(@"^[A-Za-z ]+$");
        private int _SystemControlId;
        private string _ControlName;
        private string _Path;
        private string _Password;
        private string _cValue;
        private BooleanValue _AssetFlag;


        public int SystemControlId
        {
            get { return _SystemControlId; }
            set
            {
                _SystemControlId = value;
                NotifyPropertyChanged("SystemControlId");
            }
        }

        public string Path
        {
            get { return _Path; }
            set
            {
                _Path = value;
                NotifyPropertyChanged("Path");
            }
        }
        public string ControlName
        {
            get { return _ControlName; }
            set
            {
                _ControlName = value;
                NotifyPropertyChanged("ControlName");
            }
        }
        public string cValue
        {
            get { return _cValue; }
            set
            {
                _cValue = value;
                NotifyPropertyChanged("cValue");
            }
        }

        public string Password
        {
            get { return _Password; }
            set
            {
                _Password = value;
                NotifyPropertyChanged("Password");
            }
        }

      
        public BooleanValue AssetFlag
        {
            get { return _AssetFlag; }
            set
            {
                _AssetFlag = value;
                NotifyPropertyChanged("AssetFlag");
            }
        }
      
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Private Helpers

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IDataErrorInfo Members

        public string Error
        {
            get { return null; }
        }

        public string this[string columnName]
        {
            get
            {
                if (columnName == "Path")
                {
                    return string.IsNullOrEmpty(this._Path) ? "Required value" : null;
                }
                if (columnName == "Password")
                {
                    return string.IsNullOrEmpty(this._Password) ? "Required value" : null;
                }
                //if (columnName == "Label")
                //{
                //    return string.IsNullOrEmpty(this._Label) ? "Required value" : null;
                //}
               
                return null;
            }
        }
        #endregion
    }
}
