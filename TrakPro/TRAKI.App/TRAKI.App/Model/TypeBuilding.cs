﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TRAKI.App.Model
{
   public class TypeBuilding : INotifyPropertyChanged, IDataErrorInfo
    {
        private int _TypeId;
        private string _TypeName;

        public int TypeId
        {
            get { return _TypeId; }
            set
            {
                _TypeId = value;
                NotifyPropertyChanged("TypeId");
            }
        }

        public string TypeName
        {
            get { return _TypeName; }
            set
            {
                _TypeName = value;
                NotifyPropertyChanged("TypeName");
            }
        }
         

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Private Helpers

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IDataErrorInfo Members

        public string Error
        {
            get { return null; }
        }

        public string this[string columnName]
        {
            get
            {
                if (columnName == "BuildingTypeDescription")
                {
                    return string.IsNullOrEmpty(this._TypeName) ? "Required value" : null;
                }
                return null;
            }
        }
        #endregion
    }
}
