﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TRAKI.App.Model
{
    public class UserRole : INotifyPropertyChanged, IDataErrorInfo
    {
        private int _roleId;
        private string _name;

        #region Properties

        public int RoleId
        {
            get { return _roleId; }
            set
            {
                _roleId = value;
                NotifyPropertyChanged("RoleId");
            }
        }

        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                NotifyPropertyChanged("Name");
            }
        }
      
        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Private Helpers

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IDataErrorInfo Members

        public string Error
        {
            get { return null; }
        }

        public string this[string columnName]
        {
            get
            {
                if (columnName == "Name")
                {
                    return string.IsNullOrEmpty(this._name) ? "Required value" : null;
                }
                return null;
            }
        }
        #endregion
    }
}
