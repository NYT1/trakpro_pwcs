﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TRAKI.App.Account;
using TRAKI.App.Common;

namespace TRAKI.App.Model.Account
{
    class Password : INotifyPropertyChanged, IDataErrorInfo
    {
        private int _userId;
        private string _newPassword;
        private string _confrimNewPassowrd;
        private string _oldPassowrd;
        private BooleanValue _isTempPassword;

        #region Properties


        public int UserId
        {
            get { return _userId; }
            set
            {
                _userId = value;
                NotifyPropertyChanged("UserId");
            }
        }

        public string OldPassword
        {
            get { return _oldPassowrd; }
            set
            {
                _oldPassowrd = value;
                NotifyPropertyChanged("OldPassword");
            }
        }

        public string ConfirmNewPassword
        {
            get { return _confrimNewPassowrd; }
            set
            {
                _confrimNewPassowrd = value;
                NotifyPropertyChanged("ConfirmNewPassword");
            }
        }

        public string NewPassword
        {
            get { return _newPassword; }
            set
            {
                _newPassword = value;
                NotifyPropertyChanged("NewPassword");
            }
        }


        public BooleanValue IsTempPassword
        {
            get { return _isTempPassword; }
            set
            {
                _isTempPassword = value;
                NotifyPropertyChanged("IsTempPassword");
            }
        }



        #endregion


        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Private Helpers

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IDataErrorInfo Members

        string IDataErrorInfo.Error
        {
            get { return null; }
        }


        string IDataErrorInfo.this[string PropertyName]
        {
            get
            {
                this.IsValid = true;
                return GetValidationErrors(PropertyName);
            }
        }


        #endregion

        #region validation

        static readonly string[] ValidatedProperties =
        {
            "OldPassword",
            "NewPassword",
            "ConfirmNewPassword"
        };

        private bool _isValid;

        public bool IsValid
        {
            get { return _isValid; }
            set
            {
                foreach (string property in ValidatedProperties)
                    if (GetValidationErrors(property) != null)
                    {
                        _isValid = false;
                        NotifyPropertyChanged("IsValid");
                        return;
                    }
                _isValid = true;
                NotifyPropertyChanged("IsValid");
            }
        }

        string GetValidationErrors(string PropertyName)
        {
            string Error = null;
            switch (PropertyName)
            {
                case "OldPassword":
                    Error = ValidateOldPassword();
                    break;
                case "NewPassword":
                    Error = ValidateNewPassword();
                    break;
                case "ConfirmNewPassword":
                    Error = ValidateConfirmNewPassword();
                    break;
            }
            return Error;
        }



        private string ValidateOldPassword()
        {
            if (string.IsNullOrWhiteSpace(OldPassword))
                return "Old Password is required!";
            else if (!string.IsNullOrWhiteSpace(OldPassword) && OldPassword.Equals(NewPassword))
                return "Old Password & New Password are same!";
            else if (!string.IsNullOrWhiteSpace(OldPassword) && !(((Thread.CurrentPrincipal as CustomPrincipal).Identity.CurrentUser.Password).Equals(OldPassword)))
                return "Please provide your valid Old Password!";
            else
                return null;
        }
        private string ValidateNewPassword()
        {
            if (string.IsNullOrWhiteSpace(NewPassword))
                return "Password is required!";
            else if (!string.IsNullOrWhiteSpace(ConfirmNewPassword) && !ConfirmNewPassword.Equals(NewPassword))
                return "Password & Confirm Password does not match!";
            else if (!string.IsNullOrWhiteSpace(OldPassword) && OldPassword.Equals(NewPassword))
                return "Old Password & New Password are same!";
            else
                return null;
        }
        private string ValidateConfirmNewPassword()
        {
            if (string.IsNullOrWhiteSpace(ConfirmNewPassword))
                return "Confirm Password is required!";
            else if (!string.IsNullOrWhiteSpace(NewPassword) && !NewPassword.Equals(ConfirmNewPassword))
                return "Password & Confirm Password does not match!";
            else
                return null;
        }


        #endregion
    }
}
