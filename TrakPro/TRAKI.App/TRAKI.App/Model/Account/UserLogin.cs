﻿using System.ComponentModel;
using TRAKI.App.Common;


namespace TRAKI.App.Model.Account
{
    class UserLogin : INotifyPropertyChanged, IDataErrorInfo
    {
        private string _userName;
        private string _password;
        private BooleanValue _isTempPassword;

        #region Properties

        public string UserName
        {
            get { return _userName; }
            set
            {
                _userName = value;
                NotifyPropertyChanged("UserName");
            }
        }
        public string Password
        {
            get { return _password; }
            set
            {
                _password = value;
                NotifyPropertyChanged("Password");
            }
        }
        public BooleanValue IsTempPassword
        {
            get { return _isTempPassword; }
            set
            {
                _isTempPassword = value;
                NotifyPropertyChanged("IsTempPassword");
            }
        }
        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Private Helpers

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IDataErrorInfo Members

        string IDataErrorInfo.Error
        {
            get { return null; }
        }


        string IDataErrorInfo.this[string PropertyName]
        {
            get
            {
                this.IsValid = true;
                return GetValidationErrors(PropertyName);
            }
        }


        #endregion

        #region validation

        static readonly string[] ValidatedProperties =
        {
            "UserName",
            "Password",
        };

        private bool _isValid;

        public bool IsValid
        {
            get { return _isValid; }
            set
            {
                foreach (string property in ValidatedProperties)
                    if (GetValidationErrors(property) != null)
                    {
                        _isValid = false;
                        NotifyPropertyChanged("IsValid");
                        return;
                    }
                _isValid = true;
                NotifyPropertyChanged("IsValid");
            }
        }

        string GetValidationErrors(string PropertyName)
        {
            string Error = null;
            switch (PropertyName)
            {
                case "UserName":
                    Error = ValidateUserName();
                    break;
                case "Password":
                    Error = ValidatePassword();
                    break;
            }
            return Error;
        }

        private string ValidateUserName()
        {
            if (string.IsNullOrWhiteSpace(UserName))
            {
                return "Enter User Name";
            }
            else
            {
                return null;
            }
        }

        private string ValidatePassword()
        {
            if (string.IsNullOrWhiteSpace(Password))
            {
                return "Enter your password!";
            }
            else
            {
                return null;
            }
        }

        #endregion
    }
}
