﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TRAKI.App.Common;

namespace TRAKI.App.Model
{
    public class Department : INotifyPropertyChanged, IDataErrorInfo
    {

        private int _departmentId;
        private string _departmentDescription;
        private BooleanValue _isActive;
        private string _departmentNumber;

        public Department()
        {
            this.IsActive = BooleanValue.True;
        }

        #region Properties
        
        public int DepartmentId
        {
            get { return _departmentId; }
            set
            {
                _departmentId = value;
                NotifyPropertyChanged("DepartmentId");
            }
        }

        public string DepartmentNumber
        {
            get { return _departmentNumber; }
            set
            {
                _departmentNumber = value;
                NotifyPropertyChanged("DepartmentNumber");
            }
        }
         
        public string DepartmentDescription
        {
            get { return _departmentDescription; }
            set
            {
                _departmentDescription = value;
                NotifyPropertyChanged("DepartmentDescription");
            }
        }
         
        public BooleanValue IsActive
        {
            get { return _isActive; }
            set
            {
                _isActive = value;
                NotifyPropertyChanged("IsActive");
            }
        }
        
        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Private Helpers

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IDataErrorInfo Members

        string IDataErrorInfo.Error
        {
            get { return null; }
        }


        string IDataErrorInfo.this[string PropertyName]
        {
            get
            {
                this.IsValid = true;
                return GetValidationErrors(PropertyName);
            }
        }


        #endregion

        #region validation

        static readonly string[] ValidatedProperties =
        {
            "DepartmentNumber",
            "DepartmentDescription",
            "IsActive" 
        };

        private bool _isValid;

        public bool IsValid
        {
            get { return _isValid; }
            set
            {
                foreach (string property in ValidatedProperties)
                    if (GetValidationErrors(property) != null)
                    {
                        _isValid = false;
                        NotifyPropertyChanged("IsValid");
                        return;
                    }
                _isValid = true;
                NotifyPropertyChanged("IsValid");
            }
        }

        string GetValidationErrors(string PropertyName)
        {
            string Error = null;
            switch (PropertyName)
            {
                case "DepartmentNumber":
                    Error = ValidateDepartmentNumber();
                    break;
                case "DepartmentDescription":
                    Error = ValidateDepartmentDescription();
                    break;
            }
            return Error;
        }

        private string ValidateDepartmentNumber()
        {
            if (string.IsNullOrWhiteSpace(DepartmentNumber))
            {
                return "Department Number is required!";
            }
            else
            {
                return null;
            }
        }
        private string ValidateDepartmentDescription()
        {
            if (string.IsNullOrWhiteSpace(DepartmentDescription))
            {
                return "Department Description is required!";
            }
            else
            {
                return null;
            }
        }
        #endregion


    }
}
