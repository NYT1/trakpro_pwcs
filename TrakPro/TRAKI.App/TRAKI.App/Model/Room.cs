﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TRAKI.App.Common;
namespace TRAKI.App.Model
{
    public class Room : INotifyPropertyChanged, IDataErrorInfo
    {
        // private readonly Regex nameEx = new Regex(@"^[A-Za-z ]+$");
        private string _RoomNumber;
        private int _roomId;
        private string _roomdescription;
        private string _buildingdescription;
        private string _buildingNumber;
        private int _buildingId;
        private BooleanValue _isActive;

        public Room()
        {
            IsActive = BooleanValue.True;
        }

        public string BuildingNumber
        {
            get { return _buildingNumber; }
            set
            {
                _buildingNumber = value;
                NotifyPropertyChanged("BuildingNumber");
            }
        }

        public string RoomNumber
        {
            get { return _RoomNumber; }
            set
            {
                _RoomNumber = value;
                NotifyPropertyChanged("RoomNumber");
            }
        }


        public int RoomId
        {
            get { return _roomId; }
            set
            {
                _roomId = value;
                NotifyPropertyChanged("RoomId");
            }
        }


        public string RoomDescription
        {
            get { return _roomdescription; }
            set
            {
                _roomdescription = value;
                NotifyPropertyChanged("RoomDescription");
            }
        }


        public int BuildingId
        {
            get { return _buildingId; }
            set
            {
                _buildingId = value;
                NotifyPropertyChanged("BuildingId");
            }
        }

        public string BuildingDescription
        {
            get { return _buildingdescription; }
            set
            {
                _buildingdescription = value;
                NotifyPropertyChanged("BuildingDescription");
            }
        }


        public BooleanValue IsActive
        {
            get { return _isActive; }
            set
            {
                _isActive = value;
                NotifyPropertyChanged("IsActive");
            }
        }


        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Private Helpers

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IDataErrorInfo Members

        string IDataErrorInfo.Error
        {
            get { return null; }
        }


        string IDataErrorInfo.this[string PropertyName]
        {
            get
            {
                this.IsValid = true;
                return GetValidationErrors(PropertyName);
            }
        }


        #endregion


        #region validation

        static readonly string[] ValidatedProperties =
        {
            "RoomNumber",
            "RoomDescription"
            
        };

        private bool _isValid;

        public bool IsValid
        {
            get { return _isValid; }
            set
            {
                foreach (string property in ValidatedProperties)
                    if (GetValidationErrors(property) != null)
                    {
                        _isValid = false;
                        NotifyPropertyChanged("IsValid");
                        return;
                    }
                _isValid = true;
                NotifyPropertyChanged("IsValid");
            }
        }

        string GetValidationErrors(string PropertyName)
        {
            string Error = null;
            switch (PropertyName)
            {
                case "RoomNumber":
                    Error = ValidateRoomNumber();
                    break;
                case "RoomDescription":
                    Error = ValidateRoomDescription();
                    break;


            }
            return Error;
        }

        private string ValidateRoomNumber()
        {
            if (string.IsNullOrWhiteSpace(RoomNumber))
            {
                return "Room Number is required!";
            }
            else
            {
                return null;
            }
        }


        private string ValidateRoomDescription()
        {
            if (string.IsNullOrWhiteSpace(RoomDescription))
            {
                return "Room Description is required!";
            }
            else
            {
                return null;
            }
        }

        #endregion
    }
}
