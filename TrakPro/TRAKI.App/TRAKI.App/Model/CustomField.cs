﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TRAKI.App.Common;
namespace TRAKI.App.Model
{
    public class CustomField : INotifyPropertyChanged, IDataErrorInfo
    {
        private int _userDefinedFieldId;
        private string _userDefinedField;
        private int _assetId;
        private string _description;
        private BooleanValue _isActive;
        private BooleanValue _published;
        private string _displayOrder;
        private string _dataType;
        private string _pickListValues;
        private int _minNumber;
        private int _maxNumber;
        private DateTime? _minDate;
        private DateTime? _maxDate;
        private int _minStringLength;
        private int _maxStringLength;
        private BooleanValue _required;
        private string _defaultValue;
        private string _fieldType;
        private string _fieldCaption;
        private string _fieldValue;
        private BooleanValue _isNumeric;

        public CustomField()
        {
            IsActive = BooleanValue.True;
            Published = BooleanValue.True;
            FieldType = "textbox";
            MaxStringLength = Constants.MaximumStringLength;
            MaxNumber = Constants.MaximumNumber;
            Required = BooleanValue.False;
            //  FieldValue = "temp";// For validation purpose in case of Edit
        }

        #region Properties

        public int UserDefinedFieldId
        {
            get { return _userDefinedFieldId; }
            set
            {
                _userDefinedFieldId = value;
                NotifyPropertyChanged("UserDefinedFieldId");
            }
        }

        public string UserDefinedField
        {
            get { return _userDefinedField; }
            set
            {
                _userDefinedField = value;
                NotifyPropertyChanged("UserDefinedField");
            }
        }

        public int AssetId
        {
            get { return _assetId; }
            set
            {
                _assetId = value;
                NotifyPropertyChanged("UserDefinedFieldId");
            }
        }


        public string DisplayOrder
        {
            get { return _displayOrder; }
            set
            {
                _displayOrder = value;
                NotifyPropertyChanged("DisplayOrder");
            }
        }

        public string Description
        {
            get { return _description; }
            set
            {
                _description = value;
                NotifyPropertyChanged("Description");
            }
        }

        public string DataType
        {
            get { return _dataType; }
            set
            {
                _dataType = value;
                NotifyPropertyChanged("DataType");
            }
        }

        public string PickListValues
        {
            get { return _pickListValues; }
            set
            {
                _pickListValues = value;
                NotifyPropertyChanged("PickListValues");
            }
        }

        public int MinNumber
        {
            get { return _minNumber; }
            set
            {
                _minNumber = value;
                NotifyPropertyChanged("MinNumber");
            }
        }

        public int MaxNumber
        {
            get { return _maxNumber; }
            set
            {
                _maxNumber = value;
                NotifyPropertyChanged("MaxNumber");
            }
        }

        public DateTime? MinDate
        {
            get { return _minDate; }
            set
            {
                _minDate = value;
                NotifyPropertyChanged("MinDate");
            }
        }

        public DateTime? MaxDate
        {
            get { return _maxDate; }
            set
            {
                _maxDate = value;
                NotifyPropertyChanged("MaxDate");
            }
        }

        public int MinStringLength
        {
            get { return _minStringLength; }
            set
            {
                _minStringLength = value;
                NotifyPropertyChanged("MinStringLength");
            }
        }

        public int MaxStringLength
        {
            get { return _maxStringLength; }
            set
            {
                _maxStringLength = value;
                NotifyPropertyChanged("MaxStringLength");
            }
        }

        public BooleanValue IsActive
        {
            get { return _isActive; }
            set
            {
                _isActive = value;
                NotifyPropertyChanged("IsActive");
            }
        }

        public BooleanValue Published
        {
            get { return _published; }
            set
            {
                _published = value;
                NotifyPropertyChanged("Published");
            }
        }

        public BooleanValue Required
        {
            get { return _required; }
            set
            {
                _required = value;
                NotifyPropertyChanged("Required");
            }
        }

        public BooleanValue IsNumeric
        {
            get { return _isNumeric; }
            set
            {
                _isNumeric = value;
                NotifyPropertyChanged("IsNumeric");
            }
        }

        public string FieldType
        {
            get { return _fieldType; }
            set
            {
                _fieldType = value;
                NotifyPropertyChanged("FieldType");
                NotifyPropertyChanged("PickListValues");
                //if (this.PropertyChanged != null)
                //{
                //    this.PropertyChanged(this, new PropertyChangedEventArgs("FieldType"));
                //    this.PropertyChanged(this, new PropertyChangedEventArgs("PickListValues"));
                //}
            }
        }

        public string FieldCaption
        {
            get { return _fieldCaption; }
            set
            {
                _fieldCaption = value;
                NotifyPropertyChanged("FieldCaption");
            }
        }

        public string FieldValue
        {
            get { return _fieldValue; }
            set
            {
                _fieldValue = value;
                NotifyPropertyChanged("FieldValue");
            }
        }

        public string DefaultValue
        {
            get { return _defaultValue; }
            set
            {
                _defaultValue = value;
                NotifyPropertyChanged("DefaultValue");

            }
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Private Helpers

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IDataErrorInfo Members

        string IDataErrorInfo.Error
        {
            get { return null; }
        }


        string IDataErrorInfo.this[string PropertyName]
        {
            get
            {
                this.IsValid = true;
                return GetValidationErrors(PropertyName);
            }
        }


        static readonly string[] ValidatedProperties =
        {
            "UserDefinedField", 
            "FieldCaption",
            "DisplayOrder",
            "MinStringLength",
            "MaxStringLength",
            "MinNumber",
            "MaxNumber",
            "MinDate",
            "MaxDate",
            "DefaultValue",
            "PickListValues" ,
            "FieldValue"

        };
        private bool _isValid;

        public bool IsValid
        {
            get { return _isValid; }
            set
            {
                foreach (string property in ValidatedProperties)
                    if (GetValidationErrors(property) != null)
                    {
                        _isValid = false;
                        NotifyPropertyChanged("IsValid");
                        return;
                    }
                _isValid = true;
                NotifyPropertyChanged("IsValid");
            }
        }


        string GetValidationErrors(string PropertyName)
        {
            string Error = null;
            switch (PropertyName)
            {
                case "UserDefinedField":
                    Error = ValidateUserDefinedField();
                    break;
                case "FieldCaption":
                    Error = ValidateFieldCaption();
                    break;
                case "MaxStringLength":
                    Error = ValidateMaxStringLength();
                    break;
                case "DefaultValue":
                    Error = ValidateDefalutValue();
                    break;
                case "MinStringLength":
                    Error = ValidateMinStringLength();
                    break;
                case "DisplayOrder":
                    Error = ValidateDisplayOrder();
                    break;
                case "MinNumber":
                    Error = ValidateMinNumber();
                    break;
                case "MaxNumber":
                    Error = ValidateMaxNumber();
                    break;
                case "MinDate":
                    Error = ValidateMinDate();
                    break;
                case "MaxDate":
                    Error = ValidateMaxDate();
                    break;
                case "PickListValues":
                    Error = ValidatePickListValues();
                    break;
                case "FieldValue":
                    Error = ValidateFieldValue();
                    break;

            }
            return Error;
        }



        private string ValidateFieldValue()
        {

            if (string.IsNullOrWhiteSpace(FieldValue) && UserDefinedFieldId != 0)
            {
                return "Field Value is required!";
            }
            else
            {
                return null;
            }
        }

        private string ValidatePickListValues()
        {
            if (!string.IsNullOrEmpty(FieldType))
                if (FieldType.Equals("dropdownlist") || FieldType.Equals("radiobutton") || FieldType.Equals("checkbox"))// > MaxStringLength || DefaultValue.Length < MinStringLength)
                {
                    if (!string.IsNullOrEmpty(PickListValues))
                    {
                        //if (DefaultValue.Length > MaxStringLength || DefaultValue.Length < MinStringLength)
                        // return " Pickup list values requried for the selected Field type!";
                        return null;
                    }
                    else
                        return " Pickup list values requried for the selected Field type!";
                }
            return null;
        }

        private string ValidateUserDefinedField()
        {
            if (string.IsNullOrWhiteSpace(UserDefinedField))
            {
                return "Field Name is required!";
            }
            else
            {
                return null;
            }
        }

        private string ValidateFieldCaption()
        {
            if (string.IsNullOrWhiteSpace(FieldCaption))
            {
                return "Field Caption is required!";
            }
            else
            {
                return null;
            }
        }

        private string ValidateMaxStringLength()
        {
            if (Convert.ToInt32(MaxStringLength) > 30 && Convert.ToInt32(MaxStringLength) < 0)
            {
                return "String length Not proper!";
            }
            else
            {
                return null;
            }
        }

        private string ValidateMinStringLength()
        {
            if (Convert.ToInt32(MinStringLength) > 30 && Convert.ToInt32(MinStringLength) < 0)
            {
                return "String length Not proper!";
            }
            else
            {
                return null;
            }
        }

        private string ValidateDefalutValue()
        {

            if (!string.IsNullOrEmpty(DefaultValue))
            {
                if (DefaultValue.Length > MaxStringLength || DefaultValue.Length < MinStringLength)
                    return " Default value length does not match the given length limits!";
                else return null;
            }
            return null;
        }

        private string ValidateDisplayOrder()
        {
            if (string.IsNullOrWhiteSpace(DisplayOrder))
            {
                return "Display Order is required!";
            }
            else
            {
                return null;
            }
        }

        private string ValidateMinNumber()
        {
            if (Convert.ToInt32(MinNumber) > 2147483647 && Convert.ToInt32(MaxStringLength) < 0)
            {
                return "Number value Not valid!";
            }
            else
            {
                return null;
            }
        }
        private string ValidateMaxNumber()
        {
            if (Convert.ToInt32(MaxNumber) > 2147483647 && Convert.ToInt32(MaxStringLength) < 0)
            {
                return "Number value Not valid!";
            }
            else
            {
                return null;
            }
        }
        private string ValidateMinDate()
        {
            if (string.IsNullOrWhiteSpace(DisplayOrder))
            {
                return "Minimum Date is required!";
            }
            else
            {
                return null;
            }
        }
        private string ValidateMaxDate()
        {
            if (string.IsNullOrWhiteSpace(DisplayOrder))
            {
                return "Maximum Date is required!";
            }
            else
            {
                return null;
            }
        }
        #endregion

        /*
        #region validation

        static readonly string[] ValidatedProperties =
        {
            "FirstName",
            "LastName",
            "Email",
            "UserName",
            "Password",
             "ConfirmPassword"
        };

        private bool _isValid;

        public bool IsValid
        {
            get { return _isValid; }
            set
            {
                foreach (string property in ValidatedProperties)
                    if (GetValidationErrors(property) != null)
                    {
                        _isValid = false;
                        NotifyPropertyChanged("IsValid");
                        return;
                    }
                _isValid = true;
                NotifyPropertyChanged("IsValid");
            }
        }

        string GetValidationErrors(string PropertyName)
        {
            string Error = null;
            switch (PropertyName)
            {
                case "FirstName":
                    Error = ValidateFirstName();
                    break;
                case "LastName":
                    Error = ValidateLastName();
                    break;
                case "Email":
                    Error = ValidateEmail();
                    break;
                case "UserName":
                    Error = ValidateUserName();
                    break;
                case "Password":
                    Error = ValidatePassword();
                    break;
                case "ConfirmPassword":
                    Error = ValidateConfirmPassword();
                    break;
            }
            return Error;
        }

        private string ValidateFirstName()
        {
            if (string.IsNullOrWhiteSpace(FirstName))
            {
                return "First Name is required!";
            }
            else
            {
                return null;
            }
        }
        private string ValidateLastName()
        {
            if (string.IsNullOrWhiteSpace(LastName))
            {
                return "Last Name is required!";
            }
            else
            {
                return null;
            }
        }
        private string ValidateEmail()
        {

            if (!string.IsNullOrEmpty(Email))
            {
                return (!Regex.IsMatch(Email, @"^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$")) ? " Enter a valid email, eg. user@newyeartech.com!" : null;
            }
            return string.IsNullOrEmpty(Email) ? "Email is Required!" : null;
        }
        private string ValidateUserName()
        {
            if (string.IsNullOrWhiteSpace(UserName))
            {
                return "User Name is required!";
            }
            else
            {
                return null;
            }
        }
        private string ValidatePassword()
        {


            if (string.IsNullOrWhiteSpace(Password))
            {
                return "Password is required!";
            }
            else if (!string.IsNullOrWhiteSpace(ConfirmPassword) && !ConfirmPassword.Equals(Password))
                return "Password & Confirm Password does not match!";

            else
            {
                return null;
            }
        }


        private string ValidateConfirmPassword()
        {
            if (string.IsNullOrWhiteSpace(ConfirmPassword))
            {

                return "Confirm Password is required!";
            }
            else if (!string.IsNullOrWhiteSpace(Password) && !ConfirmPassword.Equals(Password))
                return "Password & Confirm Password does not match!";

            else
            {
                return null;
            }
        }
        #endregion
        */
    }
}
