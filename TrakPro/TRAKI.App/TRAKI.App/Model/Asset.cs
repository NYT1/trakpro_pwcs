﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Common;

namespace TRAKI.App.Model
{
    public class Asset : INotifyPropertyChanged, IDataErrorInfo
    {
        private int _assetId;
        private string _assetNumber;
        private string _catalogNumber;
        private string _catalogDescription;
        private int _buildingId;
        private string _buildingNumber;
        private string _buildingDescription;
        private string _status;
        private string _serialNumber;
        private double _cost;
        private DateTime? _dateAcquired;
        private DateTime? _warrantyExpiryDate;
        private string _manufacturerCode;
        private string _manufacturerName;
        private string _departmentNumber;
        private string _departmentDescription;
        private string _roomNumber;
        private string _roomDescription;
        private string _invoice;
        private string _dispositionCode;
        private string _dispositionDescription;
        private DateTime? _inventoryDate;
        private string _assetLocationId;
        private BooleanValue _assetRelocated;
        private BooleanValue _isMissing;
        private BooleanValue _isActive;
        private string _moveType;
        private DateTime? _moveDate;
        private string _modelNumber;
        private string _modelname;
        private BooleanValue _isBulkAdd;
        private BooleanValue _isDeleted;
        private string _prevBuildingNumber;
        private string _prevRoomNumber;
        private string _pONumber;

      

        #region Properties

        public Asset()
        {
            IsActive = BooleanValue.True;
            IsBulkAdd = BooleanValue.False;
            Status = AssetStatus.New.ToString();
        }

        public int AssetId
        {
            get { return _assetId; }
            set
            {
                _assetId = value;
                NotifyPropertyChanged("AssetId");
            }
        }

        public string AssetNumber
        {
            get { return _assetNumber; }
            set
            {
                _assetNumber = value;
                NotifyPropertyChanged("AssetNumber");
            }
        }

        public string CatalogNumber
        {
            get { return _catalogNumber; }
            set
            {
                _catalogNumber = value;
                NotifyPropertyChanged("CatalogNumber");
            }
        }

        public string CatalogDescription
        {
            get { return _catalogDescription; }
            set
            {
                _catalogDescription = value;
                NotifyPropertyChanged("CatalogDescription");
            }
        }

        public string DepartmentNumber
        {
            get { return _departmentNumber; }
            set
            {
                _departmentNumber = value;
                NotifyPropertyChanged("DepartmentNumber");
            }
        }

        public string DepartmentDescription
        {
            get { return _departmentDescription; }
            set
            {
                _departmentDescription = value;
                NotifyPropertyChanged("DepartmentDescription");
            }
        }

        public string RoomNumber
        {
            get { return _roomNumber; }
            set
            {
                _roomNumber = value;
                NotifyPropertyChanged("RoomNumber");
            }
        }

        public string RoomDescription
        {
            get { return _roomDescription; }
            set
            {
                _roomDescription = value;
                NotifyPropertyChanged("RoomDescription");
            }
        }

        public int BuildingId
        {
            get { return _buildingId; }
            set
            {
                _buildingId = value;
                NotifyPropertyChanged("BuildingId");
            }
        }

        public string BuildingNumber
        {
            get { return _buildingNumber; }
            set
            {
                _buildingNumber = value;
                NotifyPropertyChanged("BuildingNumber");
            }
        }

        public string BuildingDescription
        {
            get { return _buildingDescription; }
            set
            {
                _buildingDescription = value;
                NotifyPropertyChanged("BuildingDescription");
            }
        }

        public string Status
        {
            get { return _status; }
            set
            {
                _status = value;
                NotifyPropertyChanged("Status");
            }
        }

        public string SerialNumber
        {
            get { return _serialNumber; }
            set
            {
                _serialNumber = value;
                NotifyPropertyChanged("SerialNumber");
            }
        }

        public string ModelNumber
        {
            get { return _modelNumber; }
            set
            {
                _modelNumber = value;
                NotifyPropertyChanged("ModelNumber");
            }
        }
       
        public string ModelName
        {
            get { return _modelname; }
            set
            {
                _modelname = value;
                NotifyPropertyChanged("ModelName");
            }
        }

        public double Cost
        {
            get { return _cost; }
            set
            {
                _cost = value;
                NotifyPropertyChanged("Cost");
            }
        }

        public DateTime? DateAcquired
        {
            get { return _dateAcquired; }
            set
            {
                _dateAcquired = value;
                NotifyPropertyChanged("DateAcquired");
            }
        }

        public DateTime? WarrantyExpiryDate
        {
            get { return _warrantyExpiryDate; }
            set
            {
                _warrantyExpiryDate = value;
                NotifyPropertyChanged("WarrantyExpiryDate");
            }
        }

        public DateTime? InventoryDate
        {
            get { return _inventoryDate; }
            set
            {
                _inventoryDate = value;
                NotifyPropertyChanged("InventoryDate");
            }
        }

        public string ManufacturerCode
        {
            get { return _manufacturerCode; }
            set
            {
                _manufacturerCode = value;
                NotifyPropertyChanged("ManufacturerCode");
            }
        }

        public string ManufacturerName
        {
            get { return _manufacturerName; }
            set
            {
                _manufacturerName = value;
                NotifyPropertyChanged("ManufacturerName");
            }
        }

        public string DispositionCode
        {
            get { return _dispositionCode; }
            set
            {
                _dispositionCode = value;
                NotifyPropertyChanged("DispositionCode");
            }
        }

        public string DispositionDescription
        {
            get { return _dispositionDescription; }
            set
            {
                _dispositionDescription = value;
                NotifyPropertyChanged("DispositionDescription");
            }
        }

        public string Invoice
        {
            get { return _invoice; }
            set
            {
                _invoice = value;
                NotifyPropertyChanged("Invoice");
            }
        }

        public string AssetLocationId
        {
            get { return _assetLocationId; }
            set
            {
                _assetLocationId = value;
                NotifyPropertyChanged("AssetLocationId");
            }
        }

        public BooleanValue AssetRelocated
        {
            get { return _assetRelocated; }
            set
            {
                _assetRelocated = value;
                NotifyPropertyChanged("AssetRelocated");
            }
        }

        public BooleanValue IsMissing
        {
            get { return _isMissing; }
            set
            {
                _isMissing = value;
                NotifyPropertyChanged("IsMissing");
            }
        }

        public BooleanValue IsActive
        {
            get { return _isActive; }
            set
            {
                _isActive = value;
                NotifyPropertyChanged("IsActive");
            }
        }

        public string MoveType
        {
            get { return _moveType; }
            set
            {
                _moveType = value;
                NotifyPropertyChanged("MoveType");
            }
        }

        public DateTime? MoveDate
        {
            get { return _moveDate; }
            set
            {
                _moveDate = value;
                NotifyPropertyChanged("MoveDate");
            }
        }

        public BooleanValue IsBulkAdd
        {
            get { return _isBulkAdd; }
            set
            {
                _isBulkAdd = value;
                NotifyPropertyChanged("IsBulkAdd");
            }
        }

        public BooleanValue IsDeleted
        {
            get { return _isDeleted; }
            set { _isDeleted = value; }
        }
        
        public string PrevBuildingNumber
        {
            get { return _prevBuildingNumber; }
            set { _prevBuildingNumber = value; }
        }

        public string PrevRoomNumber
        {
            get { return _prevRoomNumber; }
            set { _prevRoomNumber = value; }
        }

        public string PONumber
        {
            get { return _pONumber; }
            set { _pONumber = value; }
        }

       

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Private Helpers

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IDataErrorInfo Members

        string IDataErrorInfo.Error
        {
            get { return null; }
        }


        string IDataErrorInfo.this[string PropertyName]
        {
            get
            {
                this.IsValid = true;
                return GetValidationErrors(PropertyName);
            }
        }


        #endregion

        #region validation

        static readonly string[] ValidatedProperties =
        {
           "AssetNumber",
           "CatalogNumber",
           "CatalogDescription",
           "RoomNumber",
           "RoomDescription",
           "BuildingNumber",
           "BuildingDescription",
           "DateAcquired",
           "IsMissing",
        };


        private bool _isValid;

        public bool IsValid
        {
            get { return _isValid; }
            set
            {
                foreach (string property in ValidatedProperties)
                    if (GetValidationErrors(property) != null)
                    {
                        _isValid = false;
                        NotifyPropertyChanged("IsValid");
                        return;
                    }
                _isValid = true;
                NotifyPropertyChanged("IsValid");
            }
        }

        string GetValidationErrors(string PropertyName)
        {
            string Error = null;
            switch (PropertyName)
            {

                case "AssetNumber":
                    Error = ValidateAssetNumber();
                    break;
                case "CatalogNumber":
                    Error = ValidateCatalogNumber();
                    break;
                case "CatalogDescription":
                    Error = ValidateCatalogDescription();
                    break;
                case "RoomNumber":
                    Error = ValidateRoomNumber();
                    break;
                case "RoomDescription":
                    Error = ValidateRoomDescription();
                    break;
                case "BuildingNumber":
                    Error = ValidateBuildingNumber();
                    break;
                case "BuildingDescription":
                    Error = ValidateBuildingDescription();
                    break;
                case "DateAcquired":
                    Error = ValidateDateAcquired();
                    break;
            }
            return Error;
        }


        private string ValidateDateAcquired()
        {
            if (DateAcquired == null)
            {
                return "Date Acquired is required!";
            }
            else
            {
                return null;
            }
        }

        private string ValidateAssetNumber()
        {
            if (string.IsNullOrWhiteSpace(AssetNumber))
            {
                return "Asset Number is required!";
            }
            else
            {
                return null;
            }
        }

        private string ValidateCatalogNumber()
        {
            if (string.IsNullOrWhiteSpace(CatalogNumber))
            {
                return "Catalog Number is required!";
            }
            else
            {
                return null;
            }
        }

        private string ValidateCatalogDescription()
        {
            if (string.IsNullOrWhiteSpace(CatalogDescription))
            {
                return "Catalog Description is required!";
            }
            else
            {
                return null;
            }
        }

        private string ValidateRoomNumber()
        {
            if (string.IsNullOrWhiteSpace(RoomNumber))
            {
                return "Room Number is required!";
            }
            else
            {
                return null;
            }
        }

        private string ValidateRoomDescription()
        {
            if (string.IsNullOrWhiteSpace(RoomDescription))
            {
                return "Room Description is required!";
            }
            else
            {
                return null;
            }
        }

        private string ValidateBuildingNumber()
        {
            if (string.IsNullOrWhiteSpace(BuildingNumber))
            {
                return "Building Number is required!";
            }
            else
            {
                return null;
            }
        }

        private string ValidateBuildingDescription()
        {
            if (string.IsNullOrWhiteSpace(BuildingDescription))
            {
                return "Building Description Name is required!";
            }
            else
            {
                return null;
            }
        }

        #endregion



    }
}
