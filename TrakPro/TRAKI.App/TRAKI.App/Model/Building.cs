﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TRAKI.App.Common;
namespace TRAKI.App.Model
{
    public class Building : INotifyPropertyChanged, IDataErrorInfo
    {
        // private readonly Regex nameEx = new Regex(@"^[A-Za-z ]+$");
        private string _BuildingNumber;
        private int _BuildingId;
        private string _BuildingDescription;
        private string _AddressLine1;
        private string _AddressLine2;
        private string _PointOfContact;
        private string _Title;
        private string _Phone;
        private string _Fax;
        private DateTime? _DateSigned;
        private DateTime? _DateReceived;
        private string _CurrentStatus;
        private int _BuildingTypeId;
        private string _BuildingTypeDescription;
        private BooleanValue _IsActive;
        private BooleanValue _BuildingStatus;
        private int _CountOfRooms;

        public Building()
        {
            IsActive = BooleanValue.True;
            BuildingStatus = BooleanValue.True;
        }

        public string BuildingNumber
        {
            get { return _BuildingNumber; }
            set
            {
                _BuildingNumber = value;
                NotifyPropertyChanged("BuildingNumber");
            }
        }


        public int BuildingId
        {
            get { return _BuildingId; }
            set
            {
                _BuildingId = value;
                NotifyPropertyChanged("BuildingId");
            }
        }


        public string BuildingDescription
        {
            get { return _BuildingDescription; }
            set
            {
                _BuildingDescription = value;
                NotifyPropertyChanged("BuildingDescription");
            }
        }

        public string AddressLine1
        {
            get { return _AddressLine1; }
            set
            {
                _AddressLine1 = value;
                NotifyPropertyChanged("AddressLine1");
            }
        }

        public string AddressLine2
        {
            get { return _AddressLine2; }
            set
            {
                _AddressLine2 = value;
                NotifyPropertyChanged("AddressLine2");
            }
        }

        public string PointOfContact
        {
            get { return _PointOfContact; }
            set
            {
                _PointOfContact = value;
                NotifyPropertyChanged("PointOfContact");
            }
        }

        public string Title
        {
            get { return _Title; }
            set
            {
                _Title = value;
                NotifyPropertyChanged("Title");
            }
        }

        public string Phone
        {
            get { return _Phone; }
            set
            {
                _Phone = value;
                NotifyPropertyChanged("Phone");
            }
        }

        public string Fax
        {
            get { return _Fax; }
            set
            {
                _Fax = value;
                NotifyPropertyChanged("Fax");
            }
        }

        public DateTime? DateSigned
        {
            get { return _DateSigned; }
            set
            {
                _DateSigned = value;
                NotifyPropertyChanged("DateSigned");
            }
        }

        public DateTime? DateReceived
        {
            get { return _DateReceived; }
            set
            {
                _DateReceived = value;
                NotifyPropertyChanged("DateReceived");
            }
        }

        public string CurrentStatus
        {
            get { return _CurrentStatus; }
            set
            {
                _CurrentStatus = value;
                NotifyPropertyChanged("CurrentStatus");
            }
        }

        public int BuildingTypeId
        {
            get { return _BuildingTypeId; }
            set
            {
                _BuildingTypeId = value;
                NotifyPropertyChanged("BuildingTypeId");
            }
        }

        public string BuildingTypeDescription
        {
            get { return _BuildingTypeDescription; }
            set
            {
                _BuildingTypeDescription = value;
                NotifyPropertyChanged("BuildingTypeDescription");
            }
        }
                     

        public BooleanValue IsActive
        {
            get { return _IsActive; }
            set
            {
                _IsActive = value;
                NotifyPropertyChanged("IsActive");
            }
        }

        public BooleanValue BuildingStatus
        {
            get { return _BuildingStatus; }
            set
            {
                _BuildingStatus = value;
                NotifyPropertyChanged("BuildingStatus");
            }
        }


        public int CountOfRooms
        {
            get { return _CountOfRooms; }
            set
            {
                _CountOfRooms = value;
                NotifyPropertyChanged("CountOfRooms");
            }
        }
      
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Private Helpers

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IDataErrorInfo Members

        string IDataErrorInfo.Error
        {
            get { return null; }
        }

         string IDataErrorInfo.this[string columnName]
        {
            get
            {
                this.IsValid = true;

                if (columnName == "BuildingNumber")
                {
                    return string.IsNullOrEmpty(this._BuildingNumber) ? "Required value" : null;
                }
                if (columnName == "BuildingDescription")
                {
                    return string.IsNullOrEmpty(this._BuildingDescription) ? "Required value" : null;
                }
             
             
               
                return null;
            }
        }
        #endregion

        #region validation

        static readonly string[] ValidatedProperties =
        {
            "BuildingNumber",
            "BuildingDescription",
            "AddressLine1",
            "AddressLine2",
            "PointOfContact",
             "Title",
             "Phone",
             "Fax",
             "DateSigned",
             "DateReceived",
             "CurrentStatus"
        };

        private bool _isValid;

        public bool IsValid
        {
            get { return _isValid; }
            set
            {
                foreach (string property in ValidatedProperties)
                    if (GetValidationErrors(property) != null)
                    {
                        _isValid = false;
                        NotifyPropertyChanged("IsValid");
                        return;
                    }
                _isValid = true;
                NotifyPropertyChanged("IsValid");
            }
        }

        string GetValidationErrors(string PropertyName)
        {
            string Error = null;
            switch (PropertyName)
            {

                case "BuildingNumber":
                    Error = ValidateBuildingNumber();
                    break;
                case "BuildingDescription":
                    Error = ValidateBuildingDescription();
                    break;
              
                //case "DateSigned":
                //    Error = ValidateDateSigned();
                //    break;
                //case "DateReceived":
                //    Error = ValidateDateReceived();
                //    break;
             
            }
            return Error;
        }


        //private string ValidateDateSigned()
        //{
        //    if (DateSigned==null)
        //    {
        //        return "Date Signed is required!";
        //    }
        //    else
        //    {
        //        return null;
        //    }
        //}

        //private string ValidateDateReceived()
        //{
        //    if (DateReceived == null)
        //    {
        //        return "Date Received is required!";
        //    }
        //    else
        //    {
        //        return null;
        //    }
        //}

        private string ValidateBuildingNumber()
        {
            if (string.IsNullOrWhiteSpace(BuildingNumber))
            {
                return "Building Code is required!";
            }
            else
            {
                return null;
            }
        }

        private string ValidateBuildingDescription()
        {
            if (string.IsNullOrWhiteSpace(BuildingDescription))
            {
                return "Building Description is required!";
            }
            else
            {
                return null;
            }
        }
       
      
        
       
        #endregion
    }
}
