﻿using System;
using System.ComponentModel;
using TRAKI.App.Common;


namespace TRAKI.App.Model
{
    class Inventory : INotifyPropertyChanged, IDataErrorInfo, ICloneable
    {
        private string _userId;  // TO Do check this field for data type 
        private int _uploadDataId;
        private string _assetNumber;
        private string _buildingNumber;
        private string _roomNumber;
        private string _description;
        private string _uploadedBy;
        private DateTime? _inventoryDate;
        private DateTime? _uploadDate;
        private string _status;
        private int _assetId;
        private int _appended=0;
    

        public int AssetId
        {
            get { return _assetId; }
            set { _assetId = value; }
        }
        
        private BooleanValue _QA;

        public Inventory()
        {
            this.QA = BooleanValue.False;
        }

        #region Properties

        public string UserId
        {
            get { return _userId; }
            set
            {
                _userId = value;
                NotifyPropertyChanged("UserId");
            }
        }

        public int UploadDataId
        {
            get { return _uploadDataId; }
            set
            {
                _uploadDataId = value;
                NotifyPropertyChanged("UploadDataId");
            }
        }



        public BooleanValue QA
        {
            get { return _QA; }
            set
            {
                _QA = value;
                NotifyPropertyChanged("QA");
            }
        }


        public string AssetNumber
        {
            get { return _assetNumber; }
            set
            {
                _assetNumber = value;
                NotifyPropertyChanged("AssetNumber");
            }
        }



        public string RoomNumber
        {
            get { return _roomNumber; }
            set
            {
                _roomNumber = value;
                NotifyPropertyChanged("RoomNumber");
            }
        }

        public string BuildingNumber
        {
            get { return _buildingNumber; }
            set
            {
                _buildingNumber = value;
                NotifyPropertyChanged("BuildingNumber");
            }
        }

        public string Description
        {
            get { return _description; }
            set
            {
                _description = value;
                NotifyPropertyChanged("Description");
            }
        }

        public string UploadedBy
        {
            get { return _uploadedBy; }
            set
            {
                _uploadedBy = value;
                NotifyPropertyChanged("UploadedBy");
            }
        }

        public string Status
        {
            get { return _status; }
            set
            {
                _status = value;
                NotifyPropertyChanged("Status");
            }
        }


        public DateTime? InventoryDate
        {
            get { return _inventoryDate; }
            set
            {
                _inventoryDate = value;
                NotifyPropertyChanged("InventoryDate");
            }
        }

        public DateTime? UploadDate
        {
            get { return _uploadDate; }
            set
            {
                _uploadDate = value;
                NotifyPropertyChanged("UploadDate");
            }
        }

        public int AppendIt
        {
            get { return _appended; }
            set { _appended = value; }
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Private Helpers

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IDataErrorInfo Members

        string IDataErrorInfo.Error
        {
            get { return null; }
        }


        string IDataErrorInfo.this[string PropertyName]
        {
            get
            {
                this.IsValid = true;
                return GetValidationErrors(PropertyName);
            }
        }


        #endregion

        #region validation

        static readonly string[] ValidatedProperties =
        {
           "AssetNumber",
           "RoomNumber",
           "BuildingNumber",
           "InventoryDate",
        };


        private bool _isValid;

        public bool IsValid
        {
            get { return _isValid; }
            set
            {
                foreach (string property in ValidatedProperties)
                    if (GetValidationErrors(property) != null)
                    {
                        _isValid = false;
                        NotifyPropertyChanged("IsValid");
                        return;
                    }
                _isValid = true;
                NotifyPropertyChanged("IsValid");
            }
        }

        string GetValidationErrors(string PropertyName)
        {
            string Error = null;
            switch (PropertyName)
            {

                case "AssetNumber":
                    Error = ValidateAssetNumber();
                    break;
                case "RoomNumber":
                    Error = ValidateRoomNumber();
                    break;
                case "BuildingNumber":
                    Error = ValidateBuildingNumber();
                    break;
                case "InventoryDate":
                    Error = ValidateInventoryDate();
                    break;
            }
            return Error;
        }

        private string ValidateInventoryDate()
        {
            if (InventoryDate == null)
            {
                return "Date Acquired is required!";
            }
            else
            {
                return null;
            }
        }

        private string ValidateAssetNumber()
        {
            if (string.IsNullOrWhiteSpace(AssetNumber))
            {
                return "Asset Number is required!";
            }
            else
            {
                return null;
            }
        }

        private string ValidateRoomNumber()
        {
            if (string.IsNullOrWhiteSpace(RoomNumber))
            {
                return "Room Number is required!";
            }
            else
            {
                return null;
            }
        }

        private string ValidateBuildingNumber()
        {
            if (string.IsNullOrWhiteSpace(BuildingNumber))
            {
                return "Building Number is required!";
            }
            else
            {
                return null;
            }
        }

        #endregion




        public Object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
