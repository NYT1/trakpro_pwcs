﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Common;

namespace TRAKI.App.Model
{
    public class Catalog : INotifyPropertyChanged, IDataErrorInfo
    {
        private int _catalogId;
        private string _catalogNumber;
        private string _catalogDescription;
        private BooleanValue _isActive;
        private BooleanValue _isComputer;
       

        public Catalog()
        {
            this.IsActive = BooleanValue.True;
            this.IsComputer = BooleanValue.True;
        }

        #region Properties

        public int CatalogId
        {
            get { return _catalogId; }
            set
            {
                _catalogId = value;
                NotifyPropertyChanged("CatalogId");
            }
        }

        public string CatalogNumber
        {
            get { return _catalogNumber; }
            set
            {
                _catalogNumber = value;
                NotifyPropertyChanged("CatalogNumber");
            }
        }

        public string CatalogDescription
        {
            get { return _catalogDescription; }
            set
            {
                _catalogDescription = value;
                NotifyPropertyChanged("CatalogDescription");
            }
        }

        public BooleanValue IsActive
        {
            get { return _isActive; }
            set
            {
                _isActive = value;
                NotifyPropertyChanged("IsActive");
            }
        }

        public BooleanValue IsComputer
        {
            get { return _isComputer; }
            set
            {
                _isComputer = value;
                NotifyPropertyChanged("IsComputer");
            }
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Private Helpers

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IDataErrorInfo Members

        string IDataErrorInfo.Error
        {
            get { return null; }
        }


        string IDataErrorInfo.this[string PropertyName]
        {
            get
            {
                this.IsValid = true;
                return GetValidationErrors(PropertyName);
            }
        }


        #endregion


        #region validation

        static readonly string[] ValidatedProperties =
        {
            "CatalogNumber",
            "CatalogDescription",
        };

        private bool _isValid;

        public bool IsValid
        {
            get { return _isValid; }
            set
            {
                foreach (string property in ValidatedProperties)
                    if (GetValidationErrors(property) != null)
                    {
                        _isValid = false;
                        NotifyPropertyChanged("IsValid");
                        return;
                    }
                _isValid = true;
                NotifyPropertyChanged("IsValid");
            }
        }

        string GetValidationErrors(string PropertyName)
        {
            string Error = null;
            switch (PropertyName)
            {
                case "CatalogNumber":
                    Error = ValidateCatalogId();
                    break;
                case "CatalogDescription":
                    Error = ValidateCatalogDescription();
                    break;
            }
            return Error;
        }

        private string ValidateCatalogId()
        {
            if (string.IsNullOrWhiteSpace(CatalogNumber))
            {
                return "Catalog Number is required!";
            }
            else
            {
                return null;
            }
        }
        private string ValidateCatalogDescription()
        {
            if (string.IsNullOrWhiteSpace(CatalogDescription))
            {
                return "Catalog Description is required!";
            }
            else
            {
                return null;
            }
        }

        #endregion
    }
}
