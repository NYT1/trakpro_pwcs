﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;
using TRAKI.App.Model;

namespace TRAKI.App.DapperQueries.ComputerCatalogQueries
{
    class CheckComputerCatalog: IQuery<int>
    {
        private string _catalognumber;
        public CheckComputerCatalog(string CatalogNumber)
        {
            this._catalognumber = CatalogNumber;
        }
        public int Execute(System.Data.IDbConnection db)
        {
            try
            {
                return (int)db.ExecuteScalar("SELECT count(1) FROM [Catalog] where (isComputer=-1 and [CatalogNumber]=@CatalogNumber)",
                                new
                                {
                                    @CatalogNumber = this._catalognumber
                                });

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
