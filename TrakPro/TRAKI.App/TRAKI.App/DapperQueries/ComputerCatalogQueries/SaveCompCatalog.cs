﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;
using TRAKI.App.Model;
using System.Data;
using System.Collections;

namespace TRAKI.App.DapperQueries.ComputerCatalogQueries
{
    class SaveCompCatalog: IQuery<int>
    {
        private ComputerCatalog _computercatalog;
        //private ArrayList _compcatalogCollector;
        private IEnumerable<string> _compCatalogCollector;
        /// <summary>
        /// initializes the instance of the <see cref="InsertCatalog"/> class.
        /// </summary>
        public SaveCompCatalog(IEnumerable<string> CompCatalogCollector)
        {
            this._compCatalogCollector = CompCatalogCollector;
        }

        public int Execute(System.Data.IDbConnection db)
        {
            IDbTransaction transaction = null;
            int result=0;
            try
            {

                ///Update Catalog Table
                //using (transaction = db.BeginTransaction())
                //{
                       string command1 = @"Update [Catalog] set isComputer=-1 where [Catalog].CatalogNumber in (" + string.Format("'{0}'", string.Join("','", this._compCatalogCollector)) + ")" ;
                        db.Execute(command1, null, transaction);
                    
                //}
                //transaction.Commit();
                return result = 1;

                
                //    string command1 = @"Update [Catalog] set isComputer=-1 where [Catalog].CatalogNumber='" + this._computercatalog.CatalogNumber + "'";
                //    db.Execute(command1, null);
                
                ////transaction.Commit();
                //return result = 1;
            }
                

            catch (Exception ex)
            {
                if (transaction != null)
                    transaction.Rollback();
                throw ex;
            }
        }
    
    }
}
