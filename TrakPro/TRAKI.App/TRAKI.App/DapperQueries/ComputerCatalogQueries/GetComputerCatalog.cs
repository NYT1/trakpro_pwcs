﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;
using TRAKI.App.Model;

namespace TRAKI.App.DapperQueries.ComputerCatalogQueries
{
    class GetComputerCatalog : IQuery<List<ComputerCatalog>>
    {
        private int? _isActive;

        /// <summary>
        /// initializes the instance of the <see cref="GetComputerCatalog"/> class.
        /// </summary>
        public GetComputerCatalog(int IsActive)
        {
            this._isActive = IsActive;
        }

        public List<ComputerCatalog> Execute(System.Data.IDbConnection db)
        {

            if (this._isActive > 0)
                this._isActive = null;
            string query = @"SELECT CatalogNumber, CatalogDescription from [Catalog] where (isComputer=-1) order by CatalogNumber";
            return (List<ComputerCatalog>)db.Query<ComputerCatalog>(query, new
            {
                @IsActive = this._isActive
            });

        }
    }
}
