﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;
using TRAKI.App.Model;
using System.Data;
using System.Collections;

namespace TRAKI.App.DapperQueries.ComputerCatalogQueries
{
    public class DeleteCompCatalog : IQuery<int>
    {
        private ArrayList _computernumber;

        public DeleteCompCatalog(ArrayList ComputerNumber)
        {
            this._computernumber = ComputerNumber;
        }

        public int Execute(System.Data.IDbConnection db)
        {
            IDbTransaction transaction = null;
            int result = 0;
            
            string conditionquery = string.Join(",", _computernumber.ToArray());
            string[] cNumber = conditionquery.Split(',');

            //string command = string.Empty;
            string command1 = string.Empty;
            try
            {
                using (transaction = db.BeginTransaction())
                {
                    if (conditionquery.Length > 0)
                    {

                        foreach (var cnum in cNumber)
                        {
                            //command = @"Delete * from ComputerCatalog where ComputerCatalog.CatalogNumber in (" + conditionquery + ")";
                            //command = @"Delete * from ComputerCatalog where ComputerCatalog.CatalogNumber='" + cnum + "'";
                            //db.Execute(command, null, transaction);

                            ////Update Catalog Table
                        //command1 = @"Update Catalog set isComputer=0 where Catalog.CatalogNumber in (" + string.Format("'{0}'", string.Join("','", _computernumber)) + ")"; ;

                            command1 = @"Update [Catalog] set isComputer=0 where [Catalog].CatalogNumber='" + cnum + "'";
                            db.Execute(command1, null, transaction);
                        }
                    }

                    transaction.Commit();
                    result = 1;
                }


                return result;
            }
            catch (Exception ex)
            {
                if (transaction != null)
                    transaction.Rollback();
                throw ex;
            }
        }

    }
}
