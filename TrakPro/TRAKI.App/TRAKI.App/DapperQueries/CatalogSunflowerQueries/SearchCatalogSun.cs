﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dapper;
using System.Threading.Tasks;
using TRAKI.App.Model;
using TRAKI.App.Interfaces;

namespace TRAKI.App.DapperQueries.CatalogSunflowerQueries
{
    class SearchCatalogSun : IQuery<List<CatalogSunflower>>
    {
        private string _searchText;


        public SearchCatalogSun(string SearchText)
        {
            this._searchText = SearchText;
        }

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>List of Departments</returns>
        public List<CatalogSunflower> Execute(System.Data.IDbConnection db)
        {

            string query = @"SELECT  catkey, orgmfgcode, mfgname, model,modelname,offname,cdate,sdate,mdate,edate,lineno FROM nvCatalog where (catkey   like '%" + this._searchText + "%' OR mfgname like '%" + this._searchText + "%') order by catkey";
            return (List<CatalogSunflower>)db.Query<CatalogSunflower>(query, new
            {
                @SearchText = this._searchText
            });

        }
    }
}
