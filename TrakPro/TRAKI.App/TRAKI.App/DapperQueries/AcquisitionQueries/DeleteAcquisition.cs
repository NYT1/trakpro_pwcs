﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;

namespace TRAKI.App.DapperQueries.AcquisitionQueries
{
    class DeleteAcquisition : IQuery<int>
    {
        private int _acquisitionId;


        /// <summary>
        /// initializes the instance of the <see cref="SearchAcquisition"/> class.
        /// </summary>
        public DeleteAcquisition(int AcquisitionId)
        {
            this._acquisitionId = AcquisitionId;
        }

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>result of the Operation</returns>
        public int Execute(System.Data.IDbConnection db)
        {
            try
            {
                var query = @"Delete from  [Acquisition]  WHERE [AcquisitionId] = @AcquisitionId";
                int result = db.Execute(query, new { @AcquisitionId = this._acquisitionId });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
