﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;

namespace TRAKI.App.DapperQueries.AcquisitionQueries
{
    class InsertAcquisition : IQuery<int>
    {
        private Acquisition _acquisition;

        /// <summary>
        /// initializes the instance of the <see cref="InsertAcquisition"/> class.
        /// </summary>
        public InsertAcquisition(Acquisition Acquisition)
        {
            this._acquisition = Acquisition;
        }


        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Number of rows Affected</returns>
        public int Execute(System.Data.IDbConnection db)
        {
            try
            {
                int result = db.Execute(@"Insert into [Acquisition] ( AcquisitionCode , AcquisitionDescription , IsActive )
                                         values ( @AcquisitionCode, @AcquisitionDescription,   @IsActive ) ",
                                    new
                                    {
                                        @AcquisitionCode = this._acquisition.AcquisitionCode,
                                        @AcquisitionDescription = this._acquisition.AcquisitionDescription,
                                        @IsActive = (Convert.ToBoolean(this._acquisition.IsActive)) ? 1 : 0,
                                    });
                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}