﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;
using TRAKI.App.Model;

namespace TRAKI.App.DapperQueries.AcquisitionQueries
{
    class UpdateAcquisition: IQuery<int>
    {
        private Acquisition _acquisition;

        /// <summary>
        /// initializes the instance of the <see cref="UpdateAcquisition"/> class.
        /// </summary>
        public UpdateAcquisition(Acquisition Acquisition)
        {
            this._acquisition = Acquisition;
        }

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Number of rows Affected</returns>
        public int Execute(System.Data.IDbConnection db)
        {
            try
            {
                int result = db.Execute(@"UPDATE [Acquisition] 
                                   SET 
                                      AcquisitionCode=@AcquisitionCode,
                                     AcquisitionDescription = @AcquisitionDescription, 
                                      IsActive =  @IsActive
                			          WHERE [AcquisitionId] = @AcquisitionId",
                                    new                      
                                    {
                                        @AcquisitionCode = this._acquisition.AcquisitionCode,
                                        @AcquisitionDescription = this._acquisition.AcquisitionDescription,
                                        @IsActive = (Convert.ToBoolean(this._acquisition.IsActive)) ? 1 : 0,
                                        @AcquisitionId = this._acquisition.AcquisitionId
                                    });
                return result;  
            }

            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}