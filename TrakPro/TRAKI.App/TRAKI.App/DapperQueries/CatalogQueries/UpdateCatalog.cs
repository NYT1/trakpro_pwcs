﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;
using TRAKI.App.Model;

namespace TRAKI.App.DapperQueries.CatalogQueries
{
    class UpdateCatalog: IQuery<int>
    {
        private Catalog _catalog;

        /// <summary>
        /// initializes the instance of the <see cref="UpdateCatalog"/> class.
        /// </summary>
        public UpdateCatalog(Catalog Catalog)
        {
            this._catalog = Catalog;
        }


        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Number of rows Affected</returns>
        public int Execute(System.Data.IDbConnection db)
        {
            try
            {
                int result= db.Execute(@"UPDATE [Catalog] 
                                   SET 
                                      CatalogNumber=@CatalogNumber,
                                      CatalogDescription = @CatalogDescription, 
                                      IsActive =  @IsActive
                			          WHERE [CatalogId] = @CatalogId",
                                    new                      
                                    {
                                        @CatalogNumber = this._catalog.CatalogNumber,
                                        @CatalogDescription = this._catalog.CatalogDescription,
                                        @IsActive = (Convert.ToBoolean(this._catalog.IsActive)) ? 1 : 0 ,
                                        @CatalogId = this._catalog.CatalogId
                                    });
                return result;  
            }

            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}