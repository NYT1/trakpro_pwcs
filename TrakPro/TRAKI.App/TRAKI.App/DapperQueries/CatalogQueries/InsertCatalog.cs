﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;

namespace TRAKI.App.DapperQueries.CatalogQueries
{
    class InsertCatalog: IQuery<int>
    {
        private Catalog _catalog;

        /// <summary>
        /// initializes the instance of the <see cref="InsertCatalog"/> class.
        /// </summary>
        public InsertCatalog(Catalog Catalog)
        {
            this._catalog = Catalog;
        }


        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Number of rows Affected</returns>
        public int Execute(System.Data.IDbConnection db)
        {
            try
            {
                 int result= db.Execute(@"Insert into [Catalog] ( CatalogNumber , CatalogDescription , IsActive )
                                         values ( @CatalogNumber, @CatalogDescription,   @IsActive ) ",
                                    new                      
                                    {
                                        @CatalogNumber = this._catalog.CatalogNumber,
                                        @CatalogDescription = this._catalog.CatalogDescription,
                                        @IsActive = (Convert.ToBoolean(this._catalog.IsActive)) ? 1 : 0 ,
                                    });

                return result;  
            }

            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}