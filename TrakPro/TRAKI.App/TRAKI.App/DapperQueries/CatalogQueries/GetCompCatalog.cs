﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;
using TRAKI.App.Model;

namespace TRAKI.App.DapperQueries.CatalogQueries
{
    class GetCompCatalog : IQuery<List<Catalog>>
    {
        private int? _catalogId;
        private int? _isActive;
        private int? _isComputer;

        public GetCompCatalog(int? CatalogId, int? IsActive, int? IsComputer)
        {
            this._catalogId = CatalogId;
            this._isActive = IsActive;
            this._isComputer = IsComputer;
        }

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>List of Catalogs</returns>
        public List<Catalog> Execute(System.Data.IDbConnection db)
        {

            if (this._isActive > 0)
                this._isActive = null;
            string query = @"SELECT CatalogId, CatalogNumber, CatalogDescription, IsActive from [Catalog] where( (@CatalogId is null or  [Catalog].CatalogId= @CatalogId) and (@IsActive is null or [Catalog].IsActive=@IsActive) and (@IsComputer is null or [Catalog].IsComputer=@IsComputer)) order by CatalogNumber";
            return (List<Catalog>)db.Query<Catalog>(query, new
            {
                @CatalogId = this._catalogId,
                @IsActive = this._isActive,
                @IsComputer = this._isComputer
            });

        }
    }
}
