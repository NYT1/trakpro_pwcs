﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;
using Dapper;

namespace TRAKI.App.DapperQueries.BuildingQueries
{
    class GetBuildingWithRooms: IQuery<List<Building>>
    {

        private string _searchText;

        public GetBuildingWithRooms(string SearchText)
        {
            this._searchText = SearchText;
        } 

        /// <summary>
        /// Executes a query against database
        /// </summary>
        /// <returns>List of buildings</returns>
        public List<Building> Execute(System.Data.IDbConnection db)
        {
            string query = string.Empty;
            query = @"SELECT distinct Building.BuildingNumber, Building.BuildingId, Building.BuildingDescription FROM Building INNER JOIN  room  ON Building.BuildingId = Room.BuildingId where ([Building].[BuildingDescription] like '%" + this._searchText + "%' OR  [Building].[BuildingNumber] like '%" + this._searchText + "%') order by Building.BuildingNumber ";
            return (List<Building>)db.Query<Building>(query);
        }
    }
}