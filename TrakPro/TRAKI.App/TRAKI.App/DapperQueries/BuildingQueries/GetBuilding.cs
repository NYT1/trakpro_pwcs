﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;
using TRAKI.App.Model;

namespace TRAKI.App.DapperQueries.BuildingQueries
{
    public class GetBuilding : IQuery<List<Building>>
    {
        private int? _BuildingId;
        private int? _isActive;

        public GetBuilding(int? BuildingId, int? IsActive)
        {
            this._BuildingId = BuildingId;
            this._isActive = IsActive;
        }
        public List<Building> Execute(System.Data.IDbConnection db)
        {

             if (this._isActive > 0)
                this._isActive = null;
             string query =  @" SELECT Building.BuildingNumber, Building.BuildingId,Building.BuildingDescription,Building.AddressLine1,Building.AddressLine2,Building.PointOfContact,
                                    Building.Title,Building.Phone,Building.Fax,Building.DateSigned,Building.DateReceived,Building.CurrentStatus,Building.IsOpen as BuildingStatus,Building.IsActive,
                                    Building.BuildingTypeId,BuildingType.BuildingTypeDescription, (select count(RoomId) from Room where Room.BuildingId= Building.BuildingId) as CountOfRooms 
                                    FROM Building 
                                                            INNER JOIN
                                                            [BuildingType] on [Building].BuildingTypeId =BuildingType.BuildingTypeId
     
                                    where((@BuildingId is null or  Building.BuildingId  = @BuildingId) and (@IsActive is null or Building.IsActive = @IsActive)) order by Building.BuildingNumber";
             return (List<Building>)db.Query<Building>(query, new
            {
                @BuildingId = this._BuildingId,
                @IsActive = this._isActive
            });
        }
    }
}
