﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;
using TRAKI.App.Model;

namespace TRAKI.App.DapperQueries.BuildingQueries
{
    public class SearchBuilding : IQuery<List<Building>>
    {
        private string _SearchText;
        private int? _isActive;
        public SearchBuilding(string SearchText, int? IsActive)
        {
            this._SearchText = SearchText;
            this._isActive = IsActive;
        }
        public List<Building> Execute(System.Data.IDbConnection db)
        {
            string query = string.Empty;
            if (this._isActive > 0)
                this._isActive = null;
            query = @"SELECT Building.BuildingNumber, Building.BuildingId,Building.BuildingDescription,Building.AddressLine1,Building.AddressLine2,Building.PointOfContact,Building.Title,Building.Phone,Building.Fax,Building.DateSigned,Building.DateReceived,Building.CurrentStatus,Building.IsActive,Building.IsOpen as BuildingStatus,Building.BuildingTypeId,BuildingType.BuildingTypeDescription, (select count(RoomId) from Room where Room.BuildingId= Building.BuildingId) as CountOfRooms FROM Building Left JOIN BuildingType ON Building.BuildingTypeId = BuildingType.BuildingTypeId where((@IsActive is null or [Building].IsActive= @IsActive )and  ( [Building].[BuildingDescription] like '%" + this._SearchText + "%' OR  [Building].[BuildingNumber] like '%" + this._SearchText + "%'))";
            return (List<Building>)db.Query<Building>(query,
                new
                {
                    @IsActive = this._isActive
                });
        }
    }
}
