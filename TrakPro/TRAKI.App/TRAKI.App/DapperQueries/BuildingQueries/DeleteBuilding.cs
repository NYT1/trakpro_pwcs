﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;
using TRAKI.App.Model;
using System.Data;


namespace TRAKI.App.DapperQueries.BuildingQueries
{
  public class DeleteBuilding : IQuery<int>
    {
        private int _BuildingId;

        public DeleteBuilding(int BuildingId)
        {
            this._BuildingId = BuildingId;
        }

        public int Execute(System.Data.IDbConnection db)
        {
            IDbTransaction transaction = null;
            int result = 0;
            try
            {
                using (transaction = db.BeginTransaction())
                {
                    string command = string.Empty;
                    command = @"Update Room set Room.IsActive= 0 Where Room.BuildingId=  @BuildingId";

                    db.Execute(command,
                        new
                        {
                            @BuildingId = this._BuildingId
                        }, transaction);



                    command = @"Update Building set Building.IsActive= 0  Where Building.BuildingId=  @BuildingId";

                    db.Execute(command,
                        new
                        {
                            @BuildingId = this._BuildingId
                        }, transaction);

                    transaction.Commit();
                    result = 1;

                }
                return result;
            }
            catch (Exception ex)
            {
                if (transaction != null)
                    transaction.Rollback();
                throw ex;
            }
        }


    }
}
