﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;
using TRAKI.App.Model;

namespace TRAKI.App.DapperQueries.BuildingQueries
{
    public class GetBuildingbyBuildingStatus : IQuery<List<Building>>
    {
        private bool _BuildingStatus;

        public GetBuildingbyBuildingStatus(bool BuildingStatus)
        {
            this._BuildingStatus = BuildingStatus;
        }
        public List<Building> Execute(System.Data.IDbConnection db)
        {
            string query = string.Empty;

            query = @"SELECT Building.BuildingNumber, Building.BuildingId,(BuildingNumber & '-' & BuildingDescription) as BuildingDescription ,Building.IsOpen as BuildingStatus ,Building.BuildingDescription as BuildingTypeDescription FROM Building INNER JOIN BuildingType ON Building.BuildingTypeId = BuildingType.BuildingTypeId where Building.IsOpen=@Status order by Building.BuildingDescription"; 

            return (List<Building>)db.Query<Building>(query, new { @Status = (this._BuildingStatus) ? "-1" : "0" });
        }
    }
}
