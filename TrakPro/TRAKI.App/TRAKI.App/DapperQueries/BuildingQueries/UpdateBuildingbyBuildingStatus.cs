﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;
using TRAKI.App.Model;
using System.Data;
using System.Collections;


namespace TRAKI.App.DapperQueries.BuildingQueries
{
    public class UpdateBuildingbyBuildingStatus : IQuery<int>
    {
        private ArrayList _OpenBuildingIdCollector;
        private ArrayList _ClosedBuildingIdCollector;

        public UpdateBuildingbyBuildingStatus(ArrayList OpenBuildingIdCollector, ArrayList ClosedBuildingIdCollector)
        {
            this._OpenBuildingIdCollector = OpenBuildingIdCollector;
            this._ClosedBuildingIdCollector = ClosedBuildingIdCollector;
        }

        public int Execute(System.Data.IDbConnection db)
        {
            IDbTransaction transaction = null;
            int result = 0;

            //TO do extract items and make  string like
            //conditionquery+= "1,2,3,4,5,6"
            //update--- where id in (  +conditionquery+

            string conditionquery = string.Join(",", _OpenBuildingIdCollector.ToArray());
            string command = string.Empty;
            try
            {
                using (transaction = db.BeginTransaction())
                {
                    if (conditionquery.Length > 0)
                    {
                        // Opening building
                        //Steve
                        //open and close doesn't relate to active directly
                       // command = @"Update Room set Room.IsActive= -1 Where Room.BuildingId in  (" + conditionquery + ")";
                       // db.Execute(command, null, transaction);
                        //Steve nothing to do with active status. either open or close
                        //command = @"Update Building set Building.IsActive= -1, Building.IsOpen=-1  Where Building.BuildingId in (" + conditionquery + ")";
                        command = @"Update Building set Building.IsOpen=-1  Where Building.BuildingId in (" + conditionquery + ")";
                        db.Execute(command, null, transaction);
                    }
                    conditionquery = string.Join(",", _ClosedBuildingIdCollector.ToArray());
                    if (conditionquery.Length > 0)
                    {
                        //Komal Open/close Building doesnot relate to room Active--08232018
                        //// Closing building
                        //command = @"Update Room set Room.IsActive= 0 Where Room.BuildingId in  (" + conditionquery + ")";
                        //db.Execute(command, null, transaction);
                     //Steve
                        command = @"Update Building set Building.IsOpen=0  Where Building.BuildingId in  (" + conditionquery + ")";
                        db.Execute(command, null, transaction);
                    }
                    transaction.Commit();
                    result = 1;

                }
                return result;
            }
            catch (Exception ex)
            {
                if (transaction != null)
                    transaction.Rollback();
                throw ex;
            }


        }

    }
}
