﻿ 
using Dapper;
using System.Collections.Generic;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;

using TRAKI.App.Common;
namespace TRAKI.App.DapperQueries.BuildingQueries
{
    class GetOpenBuildings: IQuery<List<Building>>
    {

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>List of Open Buildings</returns>
        public List<Building> Execute(System.Data.IDbConnection db)
        {
            //Steve
            string query = null;
            if (Constants.isForPWCS == true)
                query=@" SELECT BuildingNumber,  (BuildingNumber & '-' & BuildingDescription) as BuildingDescription FROM Building where(IsActive =-1 and  IsOpen = -1) order by BuildingNumber";
           else
                if (Constants.isFromMissingReport == true)
                    //query = @" SELECT BuildingNumber,  (BuildingNumber & '-' & BuildingDescription) as BuildingDescription FROM Building where(IsActive =-1 and  IsOpen = -1) order by BuildingNumber";
                    query = @" SELECT BuildingNumber,  (BuildingNumber & '-' & BuildingDescription) as BuildingDescription FROM Building where(IsActive =-1) order by BuildingNumber";
                 else
                query = @" SELECT BuildingNumber,  (BuildingNumber & '-' & BuildingDescription) as BuildingDescription FROM Building where IsActive =-1 order by BuildingNumber";
             return (List<Building>)db.Query<Building>(query);
        }
    }
}

