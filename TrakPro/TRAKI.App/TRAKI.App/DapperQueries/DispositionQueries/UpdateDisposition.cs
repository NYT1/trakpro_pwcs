﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;
using TRAKI.App.Model;

namespace TRAKI.App.DapperQueries.DispositionQueries
{
    class UpdateDisposition : IQuery<int>
    {
        private Disposition _disposition;

        /// <summary>
        /// initializes the instance of the <see cref="UpdateDisposition"/> class.
        /// </summary>
        public UpdateDisposition(Disposition Disposition)
        {
            this._disposition = Disposition;
        }

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Number of rows Affected</returns>
        public int Execute(System.Data.IDbConnection db)
        {
            try
            {
                int result = db.Execute(@"UPDATE [Disposition] 
                                   SET 
                                      DispositionCode=@DispositionCode,
                                      DispositionDescription = @DispositionDescription, 
                                      IsActive =  @IsActive
                			          WHERE [DispositionId] = @DispositionId",
                                    new
                                    {
                                        @DispositionCode = this._disposition.DispositionCode,
                                        @DispositionDescription = this._disposition.DispositionDescription,
                                        @IsActive = (Convert.ToBoolean(this._disposition.IsActive)) ? 1 : 0,
                                        @DispositionId = this._disposition.DispositionId
                                    });
                return result;
            }

            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}