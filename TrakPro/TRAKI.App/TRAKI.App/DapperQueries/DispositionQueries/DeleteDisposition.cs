﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;

namespace TRAKI.App.DapperQueries.DispositionQueries
{
    class DeleteDisposition : IQuery<int>
    {
        private int _dispositionId;

        /// <summary>
        /// initializes the instance of the <see cref="DeleteDisposition"/> class.
        /// </summary>
        public DeleteDisposition(int DispositionId)
        {
            this._dispositionId = DispositionId;
        }

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Number of rows Affected</returns>
        public int Execute(System.Data.IDbConnection db)
        {
            try
            {
                var query = @"Delete from  [Disposition]  WHERE [DispositionId] = @DispositionId";
                int result = db.Execute(query, new { @DispositionId = this._dispositionId });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
