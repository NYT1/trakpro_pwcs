﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;

namespace TRAKI.App.DapperQueries.DispositionQueries
{
    class InsertDisposition : IQuery<int>
    {
        private Disposition _disposition;

        /// <summary>
        /// initializes the instance of the <see cref="InsertDisposition"/> class.
        /// </summary>
        public InsertDisposition(Disposition Disposition)
        {
            this._disposition = Disposition;
        }

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Number of rows Affected</returns>
        public int Execute(System.Data.IDbConnection db)
        {
            try
            {
                int result = db.Execute(@"Insert into [Disposition] ( DispositionCode , DispositionDescription , IsActive )
                                         values ( @DispositionCode, @DispositionDescription,   @IsActive ) ",
                                   new
                                   {
                                       @DispositionCode = this._disposition.DispositionCode,
                                       @DispositionDescription = this._disposition.DispositionDescription,
                                       @IsActive = (Convert.ToBoolean(this._disposition.IsActive)) ? 1 : 0,
                                   });

                return result;
            }

            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}