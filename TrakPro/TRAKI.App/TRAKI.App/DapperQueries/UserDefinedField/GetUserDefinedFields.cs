﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;
using TRAKI.App.Model;

namespace TRAKI.App.DapperQueries.UserDefinedField
{
    public class GetUserDefinedFields : IQuery<List<CustomField>>
    {
        private int? _fieldId;
        private int? _isActive;
        private int? _assetId;

        /// <summary>
        /// initializes the instance of the <see cref="GetDepartments"/> class.
        /// </summary>
        public GetUserDefinedFields(int? FieldId, int? IsActive, int? AssetId)
        {
            this._fieldId = FieldId;
            this._isActive = IsActive;
            this._assetId = AssetId;
        }

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>List of Departments</returns>

        public List<CustomField> Execute(System.Data.IDbConnection db)
        {

            if (this._isActive > 0)
                this._isActive = null;
            string query = @"SELECT  UserDefinedFieldId, UserDefinedField, Description, IsActive,DisplayOrder,
DataType,
PickListValues,
MinNumber,
MaxNumber,
MinDate,
MaxDate,
MinStringLength,
MaxStringLength,
Required,
DefaultValue,
FieldCaption, 
FieldType,
IsNumeric,
[Published],
'temp' as FieldValue
FROM UserDefinedField 
where ((@FieldId is null or UserDefinedFieldId=@FieldId)and (AssetId is null or AssetId=@AssetId) and (@IsActive is null or IsActive=@IsActive)) order by DisplayOrder";
            return (List<CustomField>)db.Query<CustomField>(query,
                new
                {
                    @FieldId = this._fieldId,
                    @AssetId=this._assetId,
                    @IsActive = this._isActive
                }
            );
        }
    }
}