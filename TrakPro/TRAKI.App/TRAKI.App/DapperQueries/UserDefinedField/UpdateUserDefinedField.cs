﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;
using TRAKI.App.Model;
using System.Threading;
using TRAKI.App.Account;
using System.Data;


namespace TRAKI.App.DapperQueries.UserDefinedField
{
    class UpdateUserDefinedField : IQuery<int>
    {
        private CustomField _userDefinedField;

        /// <summary>
        /// initializes the instance of the <see cref="UpdateDepartment"/> class.
        /// </summary>
        public UpdateUserDefinedField(CustomField CustomField)
        {
            this._userDefinedField = CustomField;
        }

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Number of rows Affected</returns>
        public int Execute(System.Data.IDbConnection db)
        {

            IDbTransaction transaction = null;
            try
            {
                int result = 0;
                using (transaction = db.BeginTransaction())
                {

                    db.Execute(@"UPDATE UserDefinedField SET UserDefinedField =@UserDefinedField,
                                           Description=@Description,
                                           FieldType=@FieldType,
                                           DataType=@DataType ,
                                           IsActive= @IsActive,
                                           DisplayOrder=@DisplayOrder,
                                           PickListValues=@PickListValues,
                                           MinNumber =@MinNumber,
                                           MaxNumber=@MaxNumber,
                                           MinDate=@MinDate  ,
                                           MaxDate =@MaxDate ,
                                           Required =@Required ,
                                           MinStringLength=@MinStringLength,
                                           MaxStringLength =@MaxStringLength,
                                           DefaultValue=@DefaultValue,
                                           UpdatedBy =@UpdatedBy,
                                           UpdateDate=Now(),
                                           FieldCaption= @FieldCaption,
                                           IsNumeric=@IsNumeric,
                                           [Published]=@Published
                                             WHERE UserDefinedFieldId = @UserDefinedFieldId",
                                            new
                                            {
                                                @UserDefinedField = this._userDefinedField.UserDefinedField,
                                                @Description = this._userDefinedField.Description,
                                                @FieldType = this._userDefinedField.FieldType,
                                                @DataType = (Convert.ToBoolean(this._userDefinedField.IsNumeric)) ? "number" : this._userDefinedField.DataType,
                                                @IsActive = (Convert.ToBoolean(this._userDefinedField.IsActive)) ? 1 : 0,
                                                @DisplayOrder = this._userDefinedField.DisplayOrder,
                                                @PickListValues = this._userDefinedField.PickListValues,
                                                @MinNumber = this._userDefinedField.MinNumber,
                                                @MaxNumber = this._userDefinedField.MaxNumber,
                                                @MinDate = this._userDefinedField.MinDate,
                                                @MaxDate = this._userDefinedField.MaxDate,
                                                @Required = (Convert.ToBoolean(this._userDefinedField.Required)) ? 1 : 0,
                                                @MinStringLength = this._userDefinedField.MinStringLength,
                                                @MaxStringLength = this._userDefinedField.MaxStringLength,
                                                @DefaultValue = this._userDefinedField.DefaultValue,
                                                @UpdatedBy = (Thread.CurrentPrincipal as CustomPrincipal).Identity.CurrentUser.UserName,
                                                @FieldCaption = this._userDefinedField.FieldCaption,
                                                @IsNumeric = (Convert.ToBoolean(this._userDefinedField.IsNumeric)) ? 1 : 0,
                                                @Published = (Convert.ToBoolean(this._userDefinedField.Published)) ? 1 : 0,
                                                @UserDefinedFieldId = _userDefinedField.UserDefinedFieldId

                                            }, transaction);

                    db.Execute(@"Update [UserDefinedFieldValues] set FieldValue =@FieldValue
                                                            where UserDefinedFieldId=@UserDefinedFieldId",
                                           new
                                           {
                                               @FieldValue = this._userDefinedField.DefaultValue,
                                               @UserDefinedFieldId = _userDefinedField.UserDefinedFieldId,
                                           }, transaction);
                    transaction.Commit();
                    result = 1;
                }

                return result;
            }

            catch (Exception ex)
            {

                if (transaction != null)
                    transaction.Rollback();
                throw ex;
            }
        }
    }
}
