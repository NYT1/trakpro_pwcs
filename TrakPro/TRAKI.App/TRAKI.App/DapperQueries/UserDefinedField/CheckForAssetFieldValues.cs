﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;
using TRAKI.App.Model;
using System.Threading;
using TRAKI.App.Account;


namespace TRAKI.App.DapperQueries.UserDefinedField
{
    class CheckForAssetFieldValues : IQuery<int>
    {
        private int _assetId;

        /// <summary>
        /// initializes the instance of the <see cref="UpdateDepartment"/> class.
        /// </summary>
        public CheckForAssetFieldValues(int AssetId)
        {
            this._assetId = AssetId;
        }

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Number of rows Affected</returns>
        public int Execute(System.Data.IDbConnection db)
        {
            try
            {
                string query = @"select count(1) from UserDefinedFieldValues where assetId=" + this._assetId;
                return (int)db.ExecuteScalar<int>(query);
            }

            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}