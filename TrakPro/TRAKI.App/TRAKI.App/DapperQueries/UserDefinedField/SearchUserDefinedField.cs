﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dapper;
using System.Threading.Tasks;
using TRAKI.App.Model;
using TRAKI.App.Interfaces;

namespace TRAKI.App.DapperQueries.UserDefinedField
{
    class SearchUserDefinedField : IQuery<List<CustomField>>
    {
        private string _searchText;
        private int? _sActive;

        /// <summary>
        /// initializes the instance of the <see cref="SearchDepartment"/> class.
        /// </summary>
        public SearchUserDefinedField(string SearchText, int? IsActive)
        {
            this._searchText = SearchText;
            this._sActive = IsActive;
        }

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>List of Custom Fields</returns>
        public List<CustomField> Execute(System.Data.IDbConnection db)
        {

            if (this._sActive > 0)
                this._sActive = null;

            string query = @"SELECT UDF.UserDefinedFieldId, UDF.UserDefinedField, UDF.Description, UDF.IsActive, UDF.DisplayOrder,
                                UDF.DataType,
                                UDF.PickListValues,
                                UDF.MinNumber,
                                UDF.MaxNumber,
                                UDF.MinDate,
                                UDF.MaxDate,
                                UDF.MinStringLength,
                                UDF.MaxStringLength,
                                UDF.DefaultValue,
                                UDF.FieldCaption,
                                UDF.Published,
                                UDF.FieldType,
                                '' as FieldValue,
                                UDF.Required
                                FROM UserDefinedField UDF  
                                where ((@IsActive is null or UDF.IsActive= @IsActive )and  (UDF.UserDefinedField   like '%" + this._searchText + "%' OR UDF.Description like '%" + this._searchText + "%' )) order by UDF.DisplayOrder";
            return (List<CustomField>)db.Query<CustomField>(query, new { @IsActive = this._sActive });
        }
    }
}
