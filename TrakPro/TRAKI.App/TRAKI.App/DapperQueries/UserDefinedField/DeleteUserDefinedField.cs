﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;
using TRAKI.App.Model;


namespace TRAKI.App.DapperQueries.UserDefinedField
{
    public class DeleteUserDefinedField : IQuery<int>
    {
        private int _fieldId;

        /// <summary>
        /// initializes the instance of the <see cref="DeleteUserDefinedField"/> class.
        /// </summary>
        public DeleteUserDefinedField(int UserDefinedFieldId)
        {
            this._fieldId = UserDefinedFieldId;
        }


        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Number of rows Affected</returns>
        public int Execute(System.Data.IDbConnection db)
        {

            // IDbTransaction transaction = null;
            try
            {
                //  int result = 0;
                //   using (transaction = db.BeginTransaction())
                //  {

                return db.Execute(@"Update UserDefinedField set IsActive=0, Published=0 where (@UserDefinedField is null or UserDefinedFieldId=@UserDefinedField)",
                                           new
                                           {
                                               @UserDefinedField = _fieldId
                                           });

                //  db.Execute(@"Update UserDefinedFieldValues  set IsActive=0 where (@UserDefinedField is null or UserDefinedFieldId=@UserDefinedField)",
                //             new
                //            {
                //             @UserDefinedFieldId = _fieldId,
                //           }, transaction);
                // transaction.Commit();
                //  result = 1;
                //  }

                //return result;
            }

            catch (Exception ex)
            {

                // if (transaction != null)
                //      transaction.Rollback();
                throw ex;
            }

        }
    }
}