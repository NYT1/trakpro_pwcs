﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;
using TRAKI.App.Model;

namespace TRAKI.App.DapperQueries.UserQueries
{
    class InsertUser : IQuery<int>
    {
        private User _user;

        /// <summary>
        /// initializes the instance of the <see cref="InsertUser"/> class.
        /// </summary>
        public InsertUser(User User)
        {
            this._user = User;
        }

        /// <summary>
        /// excecutes db query against db
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Number of rows Affected</returns>
        public int Execute(System.Data.IDbConnection db)
        {
            try
            {
                 int result= db.Execute(@"Insert into [User] ( FirstName ,  LastName ,Email , UserName , [Password] , RoleId , IsActive , Gender )
                                         values ( @FirstName, @LastName, @Email,@UserName, @Password, @RoleId,  @IsActive, @Gender ) ",
                                    new                      
                                    {
                                        @FirstName = this._user.FirstName,
                                        @LastName = this._user.LastName,
                                        @Email = this._user.Email,
                                        @UserName = this._user.UserName,
                                        @Password = this._user.Password,
                                        @RoleId =this._user.RoleId,
                                        @IsActive = (Convert.ToBoolean(this._user.IsActive)) ? 1 : 0 ,
                                        @Gender = this._user.Gender.ToString(),
                                    });

                return result;  
            }

            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}