﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;
using TRAKI.App.Model;


namespace TRAKI.App.DapperQueries.UserQueries
{
  public class DeleteUser : IQuery<int>
    {
        private int? _userID;

        /// <summary>
        /// initializes the instance of the <see cref="DeleteUser"/> class.
        /// </summary>
        public DeleteUser(int? UserId)
        {
            this._userID = UserId;
        }

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Number of rows Affected</returns>
        public int Execute(System.Data.IDbConnection db)
        {
            try
            {
                var query = @"Delete from  [User]  WHERE [UserId] = @UserId";
                int result= db.Execute(query, new { @UserId = this._userID });
                return result;            
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
