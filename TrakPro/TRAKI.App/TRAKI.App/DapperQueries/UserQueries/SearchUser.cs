﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dapper;
using System.Threading.Tasks;
using TRAKI.App.Model;
using TRAKI.App.Interfaces;

namespace TRAKI.App.DapperQueries.UserQueries
{
    class SearchUser : IQuery<List<User>>
    {
        private string _searchText;
        private int? _roleId;

        /// <summary>
        /// initializes the instance of the <see cref="SearchUser"/> class.
        /// </summary>
        public SearchUser(string SearchText, int? RoleId)
        {
            this._searchText = SearchText;
            this._roleId = RoleId;
        }

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>List of the Users</returns>
        public List<User> Execute(System.Data.IDbConnection db)
        {

            if (this._roleId == 0)
                this._roleId = null;

            string query = @"SELECT  User.Userid, User.FirstName,User.LastName, User.Email, User.UserName, User.Password, User.Gender, User.IsActive, Role.RoleId, ROLE.Name as Role FROM [User] INNER JOIN ROLE ON User.ROLEID = ROLE.ROLEID where ((@RoleId is null or User.RoleId = @RoleId) and (User.FirstName like '%" + this._searchText + "%' OR User.LastName like '%" + this._searchText + "%')) order by FirstName";

            return (List<User>)db.Query<User>(query, new
            {
                @RoleId = this._roleId
            });
        }
    }
}
