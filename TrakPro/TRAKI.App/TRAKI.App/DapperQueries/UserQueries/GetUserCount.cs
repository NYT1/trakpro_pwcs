﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;
using TRAKI.App.Model;


namespace TRAKI.App.DapperQueries.UserQueries
{
  public class GetUserCount : IQuery<int>
    {
        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Number of rows Affected</returns>
        public int Execute(System.Data.IDbConnection db)
        {
            try
            {
                var query = @"select count(userid) from  [User]  WHERE [IsActive] = -1";
                int result=(int) db.ExecuteScalar(query);
                return result;            
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
