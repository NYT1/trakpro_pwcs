﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using TRAKI.App.Model;
using TRAKI.App.Interfaces;

namespace TRAKI.App.DapperQueries.ManufacturerQueries
{
    public class SearchManufacturer : IQuery<List<Manufacturer>>
    {
        private string _searchText;
        private int? _isActive;

        /// <summary>
        /// initializes the instance of the <see cref="SearchManufacturer"/> class.
        /// </summary>
        public SearchManufacturer(string SearchText, int? IsActive)
        {
            this._searchText = SearchText;
            this._isActive = IsActive;
        }

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>List of Manufacturers</returns>
        public List<Manufacturer> Execute(System.Data.IDbConnection db)
        {

            if (this._isActive > 0)
                this._isActive = null;
            string query = @"SELECT [Manufacturer].ManufacturerId,  [Manufacturer].ManufacturerCode, [Manufacturer].ManufacturerName ,  [Manufacturer].IsActive from [Manufacturer]  
                        where((@IsActive is null or [Manufacturer].IsActive=@IsActive) and  ([Manufacturer].ManufacturerCode  like '%" + this._searchText + "%' OR [Manufacturer].ManufacturerName like '%" + this._searchText + "%')) order by  [Manufacturer].ManufacturerCode";
            return (List<Manufacturer>)db.Query<Manufacturer>(query,
                new
                {
                    @IsActive = this._isActive

                });
        }
    }
}