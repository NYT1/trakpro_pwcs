﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;
using TRAKI.App.Model;

namespace TRAKI.App.DapperQueries.ManufacturerQueries
{
    public class UpdateManufacturer : IQuery<int>
    {
        private Manufacturer _Manufacturer;

        /// <summary>
        /// initializes the instance of the <see cref="UpdateManufacturer"/> class.
        /// </summary>
        public UpdateManufacturer(Manufacturer Manufacturer)
        {
            this._Manufacturer = Manufacturer;
        }

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Number of rows Affected</returns>
        public int Execute(System.Data.IDbConnection db)
        {
            try
            {
                int result = db.Execute(@"UPDATE [Manufacturer] 
                                   SET 
                                      ManufacturerCode=@ManufacturerCode,
                                      ManufacturerName = @ManufacturerName, 
                                      IsActive =  @IsActive
                			          WHERE [ManufacturerId] = @ManufacturerId",
                                    new
                                    {
                                        @ManufacturerCode = this._Manufacturer.ManufacturerCode,
                                        @ManufacturerName = this._Manufacturer.ManufacturerName,
                                        @IsActive = (Convert.ToBoolean(this._Manufacturer.IsActive)) ? 1 : 0,
                                        @ManufacturerId = this._Manufacturer.ManufacturerId
                                    });
                return result;
            }

            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}