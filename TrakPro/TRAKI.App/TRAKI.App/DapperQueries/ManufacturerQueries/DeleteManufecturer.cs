﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;

namespace TRAKI.App.DapperQueries.ManufacturerQueries
{
  public  class DeleteManufacturer: IQuery<int>
    {
        private int _ManufacturerId;

        /// <summary>
        /// initializes the instance of the <see cref="DeleteManufacturer"/> class.
        /// </summary>
        public DeleteManufacturer(int ManufacturerId)
        {
            this._ManufacturerId = ManufacturerId;
        }

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Number of rows Affected</returns>
        public int Execute(System.Data.IDbConnection db)
        {
            try
            {
                var query = @"Delete from  [Manufacturer]  WHERE [ManufacturerId] = @ManufacturerId";
                int result= db.Execute(query, new { @ManufacturerId = this._ManufacturerId });
                return result;            
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
