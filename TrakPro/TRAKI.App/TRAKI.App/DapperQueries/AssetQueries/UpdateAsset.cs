﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;
using Dapper;
using System.Data;
using TRAKI.App.Common;
using System.Threading;
using TRAKI.App.Account;



namespace TRAKI.App.DapperQueries.AssetQueries
{
    class UpdateAsset : IQuery<int>
    {
        private Asset _asset;
        private bool _isLocationChange;
        private bool _changedInventoryDate;

        /// <summary>
        /// initializes the instance of the <see cref="UpdateAsset"/> class.
        /// </summary>
        public UpdateAsset(Asset Asset, bool IsLocationChanged, bool changedInventoryDate)
        {
            this._asset = Asset;
            this._isLocationChange = IsLocationChanged;
            _changedInventoryDate = changedInventoryDate;
        }


        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Number of rows Affected</returns>
        public int Execute(System.Data.IDbConnection db)
        {
            IDbTransaction transaction = null;
            try
            {
                int result = 0;
                using (transaction = db.BeginTransaction())
                {
                    CommandDefinition cmd = new CommandDefinition(@"Update [Asset] set
                                        CurrentBuilding=@BuildingNumber,
                                        CurrentRoom = @RoomNumber,
                                        CatalogNumber = @CatalogNumber,
                                        SerialNumber = @SerialNumber,
                                        Cost= @Cost,
                                        DateAcquired = @DateAcquired ,
                                        WarrantyExpiryDate = @WarrantyExpiryDate,
                                        ManufacturerCode= @ManufacturerCode,
                                        DepartmentNumber = @DepartmentNumber,
                                        Invoice = @Invoice,
                                        DispositionCode = @DispositionCode,
                                        AssetRelocated = @AssetRelocated, 
                                        ModelNumber = @ModelNumber,
                                        ModelName = @ModelName,
                                        PONumber = @PONumber,
                                        ModifiedBy = @ModifiedBy,
                                        ModifiedDate=@ModifiedDate,
                                        IsMissing = @IsMissing,
                                        IsActive = @IsActive,
                                        Status=@Status,
                                        InventoryDate= @InventoryDate WHERE AssetId=@AssetId",

                                         new
                                         {
                                             @BuildingNumber = this._asset.BuildingNumber,
                                             @RoomNumber = this._asset.RoomNumber,
                                             @CatalogNumber = this._asset.CatalogNumber,
                                             @SerialNumber = this._asset.SerialNumber,
                                             @Cost = this._asset.Cost,
                                             @DateAcquired = this._asset.DateAcquired,
                                             @WarrantyExpiryDate = this._asset.WarrantyExpiryDate,
                                             @ManufacturerCode = this._asset.ManufacturerCode,
                                             @DepartmentNumber = this._asset.DepartmentNumber,
                                             @Invoice = this._asset.Invoice,
                                             @DispositionCode = this._asset.DispositionCode,
                                             @AssetRelocated = (Convert.ToBoolean(this._asset.AssetRelocated)) ? 1 : 0,
                                             @ModelNumber = this._asset.ModelNumber,
                                             @ModelName = this._asset.ModelName,
                                             @PONumber = this._asset.PONumber,
                                             @ModifiedBy = (Thread.CurrentPrincipal as CustomPrincipal).Identity.CurrentUser.UserName,
                                             @ModifiedDate = DateTime.Now.ToString(),
                                             @IsMissing = (Convert.ToBoolean(this._asset.IsMissing)) ? 1 : 0,
                                             @IsActive = (Convert.ToBoolean(this._asset.IsActive)) ? 1 : 0,
                                             @Status = this._asset.Status,
                                             @InventoryDate = this._asset.InventoryDate,
                                             @AssetId = this._asset.AssetId


                                         }, transaction);

                    db.Execute(cmd);

                    //if (Convert.ToBoolean(this._asset.AssetRelocated))
                    if (this._isLocationChange)
                    {

                        db.Execute(@"Insert into [AssetLocation] (
                                        AssetId,
                                        BuildingNumber,
                                        RoomNumber,
                                        MoveType
                                          )
                                   values ( 
                                        @AssetId,
                                        @BuildingNumber,
                                        @RoomNumber,
                                        @MoveType
                                        ) ",
                                           new
                                           {
                                               @AssetId = this._asset.AssetId,
                                               @BuildingNumber = this._asset.PrevBuildingNumber,
                                               @RoomNumber = this._asset.PrevRoomNumber,
                                               @MoveType = MoveType.AssetEdited,
                                           }, transaction);

                    }


                    //Steve
                    //If record is existing in inventory, update it with latest locaiton and inventory information
                    if (this._changedInventoryDate)
                    {
                        string query = "SELECT count(1) from [Inventory] where (AssetNumber =@AssetNumber)";
                        int rtl = (int)db.ExecuteScalar(query, new { @AssetNumber = this._asset.AssetNumber }, transaction);
                        if (rtl > 0)
                        {
                            db.Execute(@" UPDATE [Inventory] 
                                          Set UserID=@UserID,
                                            BuildingNumber=@BuildingNumber,
                                            RoomNumber=@RoomNumber,
                                            InventoryDate=@InventoryDate,
                                            Description= @Description,
                                            Status =@Status 
                                            WHERE AssetNumber=@AssetNumber ",
                                          new
                                          {
                                              @UserID = "PWCS",
                                              @BuildingNumber = this._asset.BuildingNumber,
                                              @RoomNumber = this._asset.RoomNumber,
                                              @InventoryDate = this._asset.InventoryDate,
                                              @Description = "",
                                              @Status = this._asset.Status,
                                              @AssetNumber = this._asset.AssetNumber
                                          }, transaction);
                        }
                        //Steve
                        //If record doesn't exist in inventory, insert new record with latest locaiton and inventory information
                        else
                        {
                            db.Execute(@"Insert into [Inventory] (
                                        QA,
                                        UserID,
                                        AssetNumber,
                                        BuildingNumber,
                                        RoomNumber ,
                                        InventoryDate,
                                        Description ,
                                        Status
                                        )
                                   values ( 
                                        @QA,
                                        @UserID,
                                        @AssetNumber,
                                        @BuildingNumber,
                                        @RoomNumber ,
                                        @InventoryDate,
                                        @Description, 
                                        @Status
                                        ) ",
                                                   new
                                                   {
                                                       @QA = 1,
                                                       @UserID = "PWCS",
                                                       @AssetNumber = this._asset.AssetNumber,
                                                       @BuildingNumber = this._asset.BuildingNumber,
                                                       @RoomNumber = this._asset.RoomNumber,
                                                       @InventoryDate = this._asset.InventoryDate,
                                                       @Description = "",
                                                       @Status = this._asset.Status
                                                   }, transaction);
                        }
                    }
                    transaction.Commit();
                    result = 1;

                }

                return result;
            }

            catch (Exception ex)
            {
                if (transaction != null)
                    transaction.Rollback();
                throw ex;
            }
        }
    }
}

