﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;
using Dapper;
using System.Data;
using TRAKI.App.Common;
using System.Threading;
using TRAKI.App.Account;


namespace TRAKI.App.DapperQueries.AssetQueries
{
    class InsertAsset : IQuery<int>
    {
        private Asset _asset;


        /// <summary>
        /// initializes the instance of the <see cref="InsertAsset"/> class.
        /// </summary>
        public InsertAsset(Asset Asset)
        {
            this._asset = Asset;
        }

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Number of rows Affected</returns>
        public int Execute(System.Data.IDbConnection db)
        {
            IDbTransaction transaction = null;
            try
            {
                    CommandDefinition cmd = new CommandDefinition(@"Insert into [Asset] (
                                        AssetNumber,
                                        CurrentBuilding,
                                        CurrentRoom,
                                        CatalogNumber,
                                        SerialNumber,
                                        Cost,
                                        DateAcquired ,
                                        WarrantyExpiryDate,
                                        ManufacturerCode,
                                        DepartmentNumber,
                                        Invoice,
                                        DispositionCode,
                                        InventoryDate,
                                        AssetRelocated, 
                                        ModelNumber,
                                        ModelName,
                                        PONumber,
                                        CreatedBy,
                                        IsMissing ,
                                        IsActive,
                                        Status
                                        )
                                   values ( 
                                        @AssetNumber,
                                        @CurrentBuilding,
                                        @CurrentRoom,
                                        @CatalogNumber,
                                        @SerialNumber,
                                        @Cost,
                                        @DateAcquired ,
                                        @WarrantyExpiryDate,
                                        @ManufacturerCode,
                                        @DepartmentNumber,
                                        @Invoice,
                                        @DispositionCode,
                                        @InventoryDate,
                                        @AssetRelocated,
                                        @ModelNumber,
                                        @ModelName,
                                        @PONumber,
                                        @CreatedBy,
                                        @IsMissing ,
                                        @IsActive ,
                                        @Status
                                        ) ",
                                         new
                                         {
                                             @AssetNumber = this._asset.AssetNumber,
                                             @CurrentBuilding= this._asset.BuildingNumber ,
                                             @CurrentRoom= this._asset.RoomNumber ,
                                             @CatalogNumber = this._asset.CatalogNumber,
                                             @SerialNumber = this._asset.SerialNumber,
                                             @Cost = this._asset.Cost,
                                             @DateAcquired = this._asset.DateAcquired,
                                             @WarrantyExpiryDate = this._asset.WarrantyExpiryDate,
                                             @ManufacturerCode = this._asset.ManufacturerCode,
                                             @DepartmentNumber = this._asset.DepartmentNumber,
                                             @Invoice = this._asset.Invoice,
                                             @DispositionCode = this._asset.DispositionCode,

                                             @InventoryDate=this._asset.InventoryDate,
                                             // @AssetLocationId = 0,//this._asset.AssetLocationId,
                                             @AssetRelocated = 0,
                                             @ModelNumber = this._asset.ModelNumber,
                                             @ModelName = this._asset.ModelName,
                                             @PONumber = this._asset.PONumber,
                                             @CreatedBy = (Thread.CurrentPrincipal as CustomPrincipal).Identity.CurrentUser.UserName,
                                             @IsMissing = (Convert.ToBoolean(this._asset.IsMissing)) ? 1 : 0,
                                             @IsActive = (Convert.ToBoolean(this._asset.IsActive)) ? 1 : 0,
                                             @Status = this._asset.Status

                                         }, transaction);

                    db.Execute(cmd);

                return 1;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}