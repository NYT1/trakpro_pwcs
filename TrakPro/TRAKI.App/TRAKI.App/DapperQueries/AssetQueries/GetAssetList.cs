﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dapper;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;
using TRAKI.App.Common;
using System.Collections;

namespace TRAKI.App.DapperQueries.AssetQueries
{
    class GetAssetList : IQuery<List<Asset>>
    {
        private long? _minIdValue;
        private long? _maxIdValue;
        private string _operatorValue;
        private string _status;
        private string _searchText;
        private int _numberOfRecords;
        private int _isArchived;

        /// <summary>
        /// initializes the instance of the <see cref="GetAssetList"/> class.
        /// </summary>
        public GetAssetList(long? MinIdValue, long? MaxIdValue, string OperatorValue, string Status, string SearchText, int NumberOfRecords, int IsArchived)
        {
            this._minIdValue = MinIdValue;
            this._maxIdValue = MaxIdValue;
            this._operatorValue = OperatorValue;
            this._status = Status;
            this._searchText = SearchText;
            this._numberOfRecords = NumberOfRecords;
            this._isArchived = IsArchived;
        }

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>List of Assets that matches the Criteria</returns>
        public List<Asset> Execute(System.Data.IDbConnection db)
        {
            List<Asset> result;
            if (this._status.Equals("All"))
                this._status = null;

            string query = @" SELECT top " + this._numberOfRecords + @"  Asset.AssetId, Asset.AssetNumber,Asset.CurrentRoom as RoomNumber, Asset.CurrentBuilding as BuildingNumber, Asset.SerialNumber, Asset.ModelNumber,   Asset.ModelName,  Asset.IsMissing , Asset.Status  ,
                            (select ManufacturerName from Manufacturer where ManufacturerCode = Asset.ManufacturerCode) as ManufacturerName, 
                            (select CatalogDescription from [Catalog] where  [Catalog].CatalogNumber =Asset.CatalogNumber) as CatalogDescription, 
                            (select DepartmentDescription from Department where DepartmentNumber = Asset.DepartmentNumber) as DepartmentDescription,  
                            (select DispositionCode from Disposition where DispositionCode = Asset.DispositionCode) as DispositionCode 
                        from Asset ";

            if (_maxIdValue == null)
            {
                query = query + "where((@MinIdValue is null or  Asset.AssetId " + this._operatorValue + " @MinIdValue) and (@Status is null or Asset.Status = @Status) and ((Asset.AssetNumber like '%" + this._searchText + "%') or (Asset.SerialNumber like '%" + this._searchText + "%')) and (IsDeleted =@IsArchived)) order by Asset.AssetId ";
                result = (List<Asset>)db.Query<Asset>(query, new { @MinIdValue = this._minIdValue, @Status = this._status, @IsArchived = this._isArchived });
            }
            else
            {
                query = query + "where( (@MinIdValue is null or (Asset.AssetId >= @MinIdValue and Asset.AssetId <= @MaxIdValue)) and (@Status is null or Asset.Status = @Status) and ((Asset.AssetNumber like '%" + this._searchText + "%') or (Asset.SerialNumber like '%" + this._searchText + "%')) and (IsDeleted =@IsArchived)) order by Asset.AssetId ";
                result = (List<Asset>)db.Query<Asset>(query, new { @MinIdValue = this._minIdValue, @MaxIdValue = this._maxIdValue, @Status = this._status, @IsArchived= this._isArchived });
            }
            return result;
        }

    }
}
