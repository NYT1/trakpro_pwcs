﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dapper;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;
using TRAKI.App.Common;
using System.Collections;


namespace TRAKI.App.DapperQueries.AssetQueries
{
    class GetMinAndMaxIdValuesForPager : IQuery<List<Asset>>
    {
        private long? _minIdValue;
        private string _status;
        private string _searchText;
        private bool _getLastRecords;
        private int _numberOfRecords;
        private int _isArchived;

        /// <summary>
        /// initializes the instance of the <see cref="GetMinAndMaxIdValuesForPager"/> class.
        /// </summary>
        public GetMinAndMaxIdValuesForPager(long? MinIdValue, string Status, string SearchText, bool GetLastRecords, int NumberOfRecords, int IsArchived)
        {
            this._minIdValue = MinIdValue;
            this._status = Status;
            this._searchText = SearchText;
            this._getLastRecords = GetLastRecords;
            this._numberOfRecords = NumberOfRecords;
            this._isArchived = IsArchived;
        }

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>List of Id's of the  Assets that matches the criteria</returns>
        public List<Asset> Execute(System.Data.IDbConnection db)
        {
            List<Asset> result;
            if (this._status.Equals("All"))
                this._status = null;
            string query = @" SELECT top " + this._numberOfRecords + @" Asset.AssetId  From Asset ";

            if (!this._getLastRecords)
            {
                query = query + "where((@MinIdValue is null or  Asset.AssetId < @MinIdValue) and (@Status is null or Asset.Status = @Status) and ((Asset.AssetNumber like '%" + this._searchText + "%') or (Asset.SerialNumber like '%" + this._searchText + "%')) and (IsDeleted =@IsArchived))  order by Asset.AssetId desc";
                result = (List<Asset>)db.Query<Asset>(query, new { @MinIdValue = this._minIdValue, @Status = this._status, @IsArchived= this._isArchived });
            }
            else
            {
                query = query + "where((@Status is null or Asset.Status = @Status) and ((Asset.AssetNumber like '%" + this._searchText + "%') or (Asset.SerialNumber like '%" + this._searchText + "%')) and (IsDeleted =@IsArchived)) order by Asset.AssetId desc";
                result = (List<Asset>)db.Query<Asset>(query, new { @Status = this._status, @IsArchived=this._isArchived });

            }
            return result;
        }
    }
}
