﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;
using TRAKI.App.Model;

namespace TRAKI.App.DapperQueries.AssetQueries
{
    class GetAssetForEdit : IQuery<List<Asset>>
    {
        private int _assetId;


        /// <summary>
        /// initializes the instance of the <see cref="GetAssetForEdit"/> class.
        /// </summary>
        public GetAssetForEdit(int AssetId)
        {
            this._assetId = AssetId;
        }


        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Return the Asset datail for Edit/Update</returns>
        public List<Asset> Execute(System.Data.IDbConnection db)
        {
            string query = @" SELECT  Asset.AssetNumber, Asset.CatalogNumber , Asset.SerialNumber , Asset.PONumber, Asset.Cost , Asset.Status, Asset.DateAcquired , Asset.WarrantyExpiryDate , Asset.ManufacturerCode , Asset.DepartmentNumber , Asset.Invoice , Asset.DispositionCode , Asset.InventoryDate , Asset.IsMissing , Asset.AssetRelocated , Asset.AssetBarcodeId , Asset.AssetId , Asset.IsActive , Asset.ModelNumber, Asset.ModelName,  
                                (select ManufacturerName from Manufacturer where ManufacturerCode = Asset.ManufacturerCode) as ManufacturerName, 
                                (select CatalogDescription from [Catalog] where  [Catalog].CatalogNumber =Asset.CatalogNumber) as CatalogDescription, 
                                (select DepartmentDescription from Department where DepartmentNumber = Asset.DepartmentNumber) as DepartmentDescription,  
                                (select DispositionDescription from Disposition where DispositionCode = Asset.DispositionCode) as DispositionDescription,  
                                Asset.IsDeleted,    Building.BuildingId,  Building.BuildingDescription,  Room.RoomDescription  , Asset.CurrentRoom as PrevRoomNumber,  Asset.CurrentRoom as RoomNumber,Asset.CurrentBuilding as PrevBuildingNumber, Asset.CurrentBuilding as BuildingNumber
                                                        from (Asset 
                                INNER JOIN
                                Building on Building.BuildingNumber =Asset.CurrentBuilding) 
                                INNER JOIN
                                Room on Room.RoomNumber =Asset.CurrentRoom  where(Room.BuildingId= Building.BUildingId and (Asset.AssetId =@AssetId))";

            return (List<Asset>)db.Query<Asset>(query, new { @AssetId= this._assetId});
        }
    }
}
