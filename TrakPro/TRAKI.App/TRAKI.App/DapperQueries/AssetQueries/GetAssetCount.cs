﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;

namespace TRAKI.App.DapperQueries.AssetQueries
{
    class GetAssetCount : IQuery<int>
    {
        private string _searchText;
        private string _status;
        private int _isArchived;


        /// <summary>
        /// initializes the instance of the <see cref="GetAssetCount"/> class.
        /// </summary>
        public GetAssetCount(string SearchText, string Status, int IsArchived)
        {
            this._searchText = SearchText;
            this._status = Status;
            this._isArchived = IsArchived;
        }

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>return the count of the records thats matches the criteria</returns>
        public int Execute(System.Data.IDbConnection db)
        {


            if (this._status.Equals("All"))
                this._status = null;

            string query = @"SELECT count(1) from Asset where((@Status is null or Asset.Status= @Status) and  ((Asset.AssetNumber like '%" + this._searchText + "%') or (Asset.SerialNumber like '%" + this._searchText + "%')) and Asset.IsDeleted=@IsArchived)";
            return (int)db.ExecuteScalar(query, new { @Status = this._status, @IsArchived = this._isArchived });
        }
    }
}
