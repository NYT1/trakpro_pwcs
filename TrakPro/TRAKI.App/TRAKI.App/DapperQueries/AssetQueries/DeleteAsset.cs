﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;
using System.Threading;
using TRAKI.App.Account;

namespace TRAKI.App.DapperQueries.AssetQueries
{
    class DeleteAsset : IQuery<int>
    {
        private int? _AssetId;


        /// <summary>
        /// initializes the instance of the <see cref="DeleteAsset"/> class.
        /// </summary>
        public DeleteAsset(int? AssetId)
        {
            this._AssetId = AssetId;
        }


        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Number of rows Affected</returns>
        public int Execute(System.Data.IDbConnection db)
        {
            try
            {
                var query = @"Update Asset set IsDeleted = -1, IsActive=0, ModifiedBy=@ModifiedBy, ModifiedDate =@ModifiedDate WHERE [AssetId] = @AssetId";
                int result = db.Execute(query,
                    new
                    {
                        @ModifiedBy = (Thread.CurrentPrincipal as CustomPrincipal).Identity.CurrentUser.UserName,
                        @ModifiedDate = DateTime.Now.ToString(),
                        @AssetId = this._AssetId
                    });
             
                return result;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
