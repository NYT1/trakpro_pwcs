﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;
using Dapper;

namespace TRAKI.App.DapperQueries.AssetQueries
{
    class GetAssetBarcode : IQuery<List<AssetBarcode>>
    {
        private int _assetId;

        /// <summary>
        /// initializes the instance of the <see cref="GetAssetBarcode"/> class.
        /// </summary>
        public GetAssetBarcode(int AssetId)
        {
            this._assetId = AssetId;

        }


        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>List of Barcodes of a particular Asset</returns>
        public List<AssetBarcode> Execute(System.Data.IDbConnection db)
        {
            string query = string.Empty;
            query = @"SELECT distinct Asset.AssetId, Asset.AssetNumber, AssetBarcode.RetagDate, AssetBarcode.PreviousAssetNumber
                         from 
                         (Asset 
                         LEFT JOIN
                         AssetBarcode on Asset.AssetId =AssetBarcode.AssetId)
                            where Asset.AssetId =@AssetId order by AssetBarcode.RetagDate desc";
            return (List<AssetBarcode>)db.Query<AssetBarcode>(query, new
            {
                @AssetId = this._assetId
            });
        }
    }
}



