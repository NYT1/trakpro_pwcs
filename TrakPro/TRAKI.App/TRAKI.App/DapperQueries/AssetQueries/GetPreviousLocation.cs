﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;
using TRAKI.App.Model;

namespace TRAKI.App.DapperQueries.AssetQueries
{
    public class GetPreviousLocation : IQuery<List<Asset>>
    {
        private int _assetId;


        /// <summary>
        /// initializes the instance of the <see cref="GetPreviousLocation"/> class.
        /// </summary>
        public GetPreviousLocation(int AssetId)
        {
            this._assetId = AssetId;

        }


        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Number of rows Affected</returns>
        public List<Asset> Execute(System.Data.IDbConnection db)
        {
            string query = string.Empty;
            query = @"SELECT Asset.AssetNumber, Building.BuildingNumber, Building.BuildingDescription , Room.RoomNumber, Room.RoomDescription, AssetLocation.MoveDate, AssetLocation.MoveType
                         from 
                         ((AssetLocation 
                         INNER JOIN
                         Asset on Asset.AssetId =AssetLocation.AssetId)
                         INNER JOIN
                         Building on Building.BuildingNumber =AssetLocation.BuildingNumber)
                         INNER JOIN
                         Room on Room.RoomNumber =AssetLocation.RoomNumber  where Room.BuildingId= Building.BuildingId  and AssetLocation.AssetId =@AssetId order by AssetLocation.MoveDate desc ";
            return (List<Asset>)db.Query<Asset>(query, new
            {
                @AssetId = this._assetId
            });
        }
    }
}



