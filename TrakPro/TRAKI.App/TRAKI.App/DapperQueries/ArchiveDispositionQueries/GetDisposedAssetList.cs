﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dapper;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;
using TRAKI.App.Common;
using System.Collections;

namespace TRAKI.App.DapperQueries.ArchiveDispositionQueries
{
    class GetDisposedAssetList : IQuery<List<Asset>>
    {
        private long? _minIdValue;
        private long? _maxIdValue;
        private string _operatorValue;
        private string _status;
        private string _searchText;
        private int _numberOfRecords;
        private int _isArchived;
        private string _DisType;

        /// <summary>
        /// initializes the instance of the <see cref="GetAssetList"/> class.
        /// </summary>
        public GetDisposedAssetList(long? MinIdValue, long? MaxIdValue, string OperatorValue, string Status, string SearchText, int NumberOfRecords, int IsArchived, string DisType)
        {
            this._minIdValue = MinIdValue;
            this._maxIdValue = MaxIdValue;
            this._operatorValue = OperatorValue;
            this._status = Status;
            this._searchText = SearchText;
            this._numberOfRecords = NumberOfRecords;
            this._isArchived = IsArchived;
            this._DisType = DisType;
        }

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>List of Assets that matches the Criteria</returns>
        public List<Asset> Execute(System.Data.IDbConnection db)
        {
            List<Asset> result;
            
            if (this._status.Equals("ALL"))
                this._status = null;
//            string query = @" SELECT top " + this._numberOfRecords + @" Asset.AssetId, Asset.AssetNumber,  Asset.ModelNumber,Asset.SerialNumber, Asset.IsMissing , Asset.Status , Manufacturer.ManufacturerName, 
//                       [Catalog].CatalogDescription,  Building.BuildingNumber,  Department.DepartmentDescription , Room.RoomNumber, Disposition.DispositionCode, Disposition.DispositionDescription
//                        from ((((((Asset 
//                        INNER JOIN
//                        [Catalog] on [Catalog].CatalogNumber =Asset.CatalogNumber)
//                        LEFT JOIN
//                         Manufacturer on Manufacturer.ManufacturerCode =Asset.ManufacturerCode)
//                        LEFT JOIN
//                         Disposition on Disposition.DispositionCode =Asset.DispositionCode)
//                        INNER JOIN
//                        AssetLocation on AssetLocation.AssetLocationId= Asset.AssetLocationId)
//                        INNER JOIN
//                        Building on Building.BuildingNumber =AssetLocation.BuildingNumber)
//                        LEFT JOIN
//                        Department on Department.DepartmentNumber =Asset.DepartmentNumber)
//                        INNER JOIN
//                        Room on Room.RoomNumber =AssetLocation.RoomNumber ";


            string query = @" SELECT top " + this._numberOfRecords + @" Asset.AssetId, Asset.AssetNumber,  Asset.ModelNumber,Asset.SerialNumber , Asset.Status , Manufacturer.ManufacturerName, 
                       [Catalog].CatalogDescription,     Department.DepartmentDescription ,  Disposition.DispositionCode, Disposition.DispositionDescription
                        from (((Asset 
                        INNER JOIN
                        [Catalog] on [Catalog].CatalogNumber =Asset.CatalogNumber)
                        LEFT JOIN
                         Manufacturer on Manufacturer.ManufacturerCode =Asset.ManufacturerCode)
                        Inner JOIN
                         Disposition on Disposition.DispositionCode =Asset.DispositionCode) 
                        LEFT JOIN
                        Department on Department.DepartmentNumber =Asset.DepartmentNumber ";

            //Steve
            //either "All" or 0, or something else
            if (_maxIdValue == null)
            {
                //Steve
                //either "All" or 0, or something else
                if (this._DisType == "ALL" || this._DisType == "0")
                  query = query + "where( (@MinIdValue is null or  Asset.AssetId " + this._operatorValue + " @MinIdValue) and (@Status is null or Asset.Status = @Status) and ((Asset.AssetNumber like '%" + this._searchText + "%') or (Asset.SerialNumber like '%" + this._searchText + "%')) and (IsDeleted =@IsArchived and Asset.DispositionCode is not null)) order by Asset.AssetId";
                else
                    query = query + "where( (@MinIdValue is null or  Asset.AssetId " + this._operatorValue + " @MinIdValue) and (@Status is null or Asset.Status = @Status) and ((Asset.AssetNumber like '%" + this._searchText + "%') or (Asset.SerialNumber like '%" + this._searchText + "%')) and (IsDeleted =@IsArchived and Asset.DispositionCode is not null and Disposition.DispositionCode = '" + this._DisType + "' )) order by Asset.AssetId";
                
                result = (List<Asset>)db.Query<Asset>(query, new { @MinIdValue = this._minIdValue, @Status = this._status, @IsArchived = this._isArchived });
            }
            else
            {
                //Steve
                //either "All" or 0, or something else
                if (this._DisType == "ALL" || this._DisType == "0" )
                  query = query + "where(  (@MinIdValue is null or (Asset.AssetId >= @MinIdValue and Asset.AssetId <= @MaxIdValue)) and (@Status is null or Asset.Status = @Status) and ((Asset.AssetNumber like '%" + this._searchText + "%') or (Asset.SerialNumber like '%" + this._searchText + "%')) and (IsDeleted =@IsArchived and Asset.DispositionCode is not null)) order by Asset.AssetId";
                else
                    query = query + "where( (@MinIdValue is null or  Asset.AssetId " + this._operatorValue + " @MinIdValue) and (@Status is null or Asset.Status = @Status) and ((Asset.AssetNumber like '%" + this._searchText + "%') or (Asset.SerialNumber like '%" + this._searchText + "%')) and (IsDeleted =@IsArchived and Asset.DispositionCode is not null and Disposition.DispositionCode = '" + this._DisType + "' )) order by Asset.AssetId";
                result = (List<Asset>)db.Query<Asset>(query, new { @MinIdValue = this._minIdValue, @MaxIdValue = this._maxIdValue, @Status = this._status, @IsArchived= this._isArchived });
            }
            return result;
        }

    }
}
