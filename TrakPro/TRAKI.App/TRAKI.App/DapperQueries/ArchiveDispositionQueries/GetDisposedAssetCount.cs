﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;

namespace TRAKI.App.DapperQueries.ArchiveDispositionQueries
{
    class GetDisposedAssetCount : IQuery<int>
    {
        private string _searchText;
        private string _status;
        private int _isArchived;
        private string _disType;

        /// <summary>
        /// initializes the instance of the <see cref="GetAssetCount"/> class.
        /// </summary>
        public GetDisposedAssetCount(string SearchText, string Status, int IsArchived,string DisType)
        {
            this._searchText = SearchText;
            this._status = Status;
            this._isArchived = IsArchived;
            this._disType = DisType;
        }

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>return the count of the records thats matches the criteria</returns>
        public int Execute(System.Data.IDbConnection db)
        {


            if (this._status.Equals("ALL"))
                this._status = null;

//            string query = @"SELECT count(1) from ((((((Asset 
//                        INNER JOIN
//                        [Catalog] on [Catalog].CatalogNumber =Asset.CatalogNumber)
//                        LEFT JOIN
//                         Manufacturer on Manufacturer.ManufacturerCode =Asset.ManufacturerCode)
//                        LEFT JOIN
//                         Disposition on Disposition.DispositionCode =Asset.DispositionCode)
//                        INNER JOIN
//                      AssetLocation on AssetLocation.AssetLocationId= Asset.AssetLocationId)
//                        INNER JOIN
//                        Building on Building.BuildingNumber =AssetLocation.BuildingNumber) 
//                        LEFT JOIN
//                        Department on Department.DepartmentNumber =Asset.DepartmentNumber)
//                        INNER JOIN
//                        Room on Room.RoomNumber =AssetLocation.RoomNumber where((@Status is null or Asset.Status= @Status) and  ((Asset.AssetNumber like '%" + this._searchText + "%') or (Asset.SerialNumber like '%" + this._searchText + "%')) and ( Asset.IsDeleted=@IsArchived and Disposition.DispositionCode is not null))";
            string query="";
            //Steve
            //either "All" or 0, or something else
            if (this._disType =="ALL" || this._disType =="0")
                query = @"SELECT count(1) from Asset inner join Disposition on Disposition.DispositionCode =Asset.DispositionCode where ((@Status is null or Asset.Status= @Status) and  ((Asset.AssetNumber like '%" + this._searchText + "%') or (Asset.SerialNumber like '%" + this._searchText + "%')) and ( Asset.IsDeleted=@IsArchived and Asset.DispositionCode is not null))";
            else
                query = @"SELECT count(1) from  Asset inner join  Disposition on Disposition.DispositionCode =Asset.DispositionCode where ((@Status is null or Asset.Status= @Status) and  ((Asset.AssetNumber like '%" + this._searchText + "%') or (Asset.SerialNumber like '%" + this._searchText + "%')) and ( Asset.IsDeleted=@IsArchived and (Asset.DispositionCode is not null) and Asset.DispositionCode ='" + this._disType + "'))";
          
            
            return (int)db.ExecuteScalar(query, new { @Status = this._status, @IsArchived = this._isArchived });
        }
    }
}
