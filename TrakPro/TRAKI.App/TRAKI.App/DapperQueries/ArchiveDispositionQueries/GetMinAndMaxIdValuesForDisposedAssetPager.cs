﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dapper;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;
using TRAKI.App.Common;
using System.Collections;


namespace TRAKI.App.DapperQueries.ArchiveDispositionQueries
{
    class GetMinAndMaxIdValuesForDisposedAssetPager : IQuery<List<Asset>>
    {
        private long? _minIdValue;
        private string _status;
        private string _searchText;
        private bool _getLastRecords;
        private int _numberOfRecords;
        private int _isArchived;
        private string _disType;

        /// <summary>
        /// initializes the instance of the <see cref="GetMinAndMaxIdValuesForPager"/> class.
        /// </summary>
        public GetMinAndMaxIdValuesForDisposedAssetPager(long? MinIdValue, string Status, string SearchText, bool GetLastRecords, int NumberOfRecords, int IsArchived,string disType)
        {
            this._minIdValue = MinIdValue;
            this._status = Status;
            this._searchText = SearchText;
            this._getLastRecords = GetLastRecords;
            this._numberOfRecords = NumberOfRecords;
            this._isArchived = IsArchived;
            this._disType = disType;
        }

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>List of Id's of the  Assets that matches the criteria</returns>
        public List<Asset> Execute(System.Data.IDbConnection db)
        {
            List<Asset> result;
            if (this._status.Equals("All"))
                this._status = null;

//            string query = @" SELECT top " + this._numberOfRecords + @" Asset.AssetId 
//                        from ((((((Asset 
//                        INNER JOIN
//                        [Catalog] on [Catalog].CatalogNumber =Asset.CatalogNumber)
//                        LEFT JOIN
//                         Manufacturer on Manufacturer.ManufacturerCode =Asset.ManufacturerCode)
//                            LEFT JOIN
//                         Disposition on Disposition.DispositionCode =Asset.DispositionCode)
//                        INNER JOIN
//                      AssetLocation on AssetLocation.AssetLocationId= Asset.AssetLocationId)
//                        INNER JOIN
//                        Building on Building.BuildingNumber =AssetLocation.BuildingNumber)
//                        LEFT JOIN
//                        Department on Department.DepartmentNumber =Asset.DepartmentNumber)
//                        INNER JOIN
//                        Room on Room.RoomNumber =AssetLocation.RoomNumber ";
            string query = @" SELECT top " + this._numberOfRecords + @" Asset.AssetId from Asset inner join Disposition on Disposition.DispositionCode =Asset.DispositionCode ";

            if (!this._getLastRecords)
            {
                if (_disType == "All" || _disType == "0")
                   query = query + " where((@MinIdValue is null or  Asset.AssetId < @MinIdValue) and (@Status is null or Asset.Status = @Status) and ((Asset.AssetNumber like '%" + this._searchText + "%') or (Asset.SerialNumber like '%" + this._searchText + "%')) and (IsDeleted =@IsArchived and Asset.DispositionCode is not null))  order by Asset.AssetId desc";
                else
                   query = query + " where((@MinIdValue is null or  Asset.AssetId < @MinIdValue) and (@Status is null or Asset.Status = @Status) and ((Asset.AssetNumber like '%" + this._searchText + "%') or (Asset.SerialNumber like '%" + this._searchText + "%')) and (IsDeleted =@IsArchived and Asset.DispositionCode is not null))  order by Asset.AssetId desc";

                result = (List<Asset>)db.Query<Asset>(query, new { @MinIdValue = this._minIdValue, @Status = this._status, @IsArchived= this._isArchived });
            }
            else
            {
                if (_disType == "All" || _disType == "0")
                   query = query + " where((@Status is null or Asset.Status = @Status) and ((Asset.AssetNumber like '%" + this._searchText + "%') or (Asset.SerialNumber like '%" + this._searchText + "%')) and (IsDeleted =@IsArchived and Asset.DispositionCode is not null)) order by Asset.AssetId desc";
                else
                   query = query + " where((@Status is null or Asset.Status = @Status) and ((Asset.AssetNumber like '%" + this._searchText + "%') or (Asset.SerialNumber like '%" + this._searchText + "%')) and (IsDeleted =@IsArchived and Asset.DispositionCode is not null)) order by Asset.AssetId desc";
               
                result = (List<Asset>)db.Query<Asset>(query, new { @Status = this._status, @IsArchived=this._isArchived });

            }
            return result;
        }
    }
}
