﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;
using System.Threading;
using TRAKI.App.Account;


namespace TRAKI.App.DapperQueries.ArchiveDispositionQueries
{
    class ArchiveDisposition: IQuery<int>
    {

        string _dispositionCode;
        string _buildingnum;
        /// <summary>
        /// initializes the instance of the <see cref="DeleteAsset"/> class.
        /// </summary>
        public ArchiveDisposition(string dispositionCode, string buildingnum)
        {
            this._dispositionCode = dispositionCode;
            this._buildingnum = buildingnum;
        }


        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Number of rows Affected</returns>
        public int Execute(System.Data.IDbConnection db)
        {
            int result;
            string ModifyDate= DateTime.Now.ToString();
            string ModifyBy=(Thread.CurrentPrincipal as CustomPrincipal).Identity.CurrentUser.UserName;
            try
            {
                if (this._dispositionCode == "ALL" & _buildingnum == null)
                {
                    ////var query = @"Update Asset set IsDeleted = -1, ModifiedBy = @ModifiedBy,  ModifiedDate=@ModifiedDate where (DispositionCode is not null and IsDeleted = 0)";
                    //var query = @"Update Asset inner join Disposition on Disposition.DispositionCode =Asset.DispositionCode  set Asset.IsDeleted = -1, Asset.ModifiedBy = 'Lisa',  Asset.ModifiedDate='10/9/2018 3:21:50 PM'   where  ( Asset.IsDeleted=0 and Asset.DispositionCode is not null)";
                    //result = db.Execute(query,
                    //    new
                    //    {
                    //        @ModifiedBy = (Thread.CurrentPrincipal as CustomPrincipal).Identity.CurrentUser.UserName,
                    //        @ModifiedDate = DateTime.Now.ToString(),
                    //    });

                    var query = @"Update Asset inner join Disposition on Disposition.DispositionCode =Asset.DispositionCode  set Asset.IsDeleted = -1, Asset.ModifiedBy = '" + ModifyBy +"',  Asset.ModifiedDate='" +ModifyDate +"' where ( Asset.IsDeleted=0 and Asset.DispositionCode is not null)";
                    result = db.Execute(query, new {  });



                }
                else if (this._dispositionCode != "ALL" & _buildingnum == null)
                {

                    var query = @"Update Asset inner join Disposition on Disposition.DispositionCode =Asset.DispositionCode  set Asset.IsDeleted = -1, Asset.ModifiedBy = '" + ModifyBy + "',  Asset.ModifiedDate='" + ModifyDate + "'  where ( Asset.DispositionCode='" + _dispositionCode + "' and Asset.IsDeleted=0)";
                    result = db.Execute(query, new { });

                    //var query = @"Update Asset set IsDeleted = -1, ModifiedBy = @ModifiedBy,  ModifiedDate=@ModifiedDate where (DispositionCode='" + _dispositionCode + "' and IsDeleted = 0)";
                    //result = db.Execute(query,
                    //    new
                    //    {
                    //        @ModifiedBy = (Thread.CurrentPrincipal as CustomPrincipal).Identity.CurrentUser.UserName,
                    //        @ModifiedDate = DateTime.Now.ToString(),
                    //    });
                }
                else if (this._dispositionCode != "ALL" & _buildingnum != "")
                {

                    var query = @"Update Asset inner join Disposition on Disposition.DispositionCode =Asset.DispositionCode  set Asset.IsDeleted = -1, Asset.ModifiedBy = '" + ModifyBy + "', Asset.ModifiedDate='" + ModifyDate + "' where ( Asset.DispositionCode='" + _dispositionCode + "' and Asset.CurrentBuilding='" + _buildingnum + "' and Asset.IsDeleted=0)";
                    result = db.Execute(query, new { });
                    //var query = @"Update Asset set IsDeleted = -1, ModifiedBy = @ModifiedBy,  ModifiedDate=@ModifiedDate where (DispositionCode='" + _dispositionCode + "' and CurrentBuilding='" + _buildingnum + "' and IsDeleted = 0)";
                    //result = db.Execute(query,
                    //    new
                    //    {
                    //        @ModifiedBy = (Thread.CurrentPrincipal as CustomPrincipal).Identity.CurrentUser.UserName,
                    //        @ModifiedDate = DateTime.Now.ToString(),
                    //    });
                }
                else 
                    //if (this._dispositionCode == "ALL" & _buildingnum = "")
                {


                    var query = @"Update Asset inner join Disposition on Disposition.DispositionCode =Asset.DispositionCode  set Asset.IsDeleted = -1, Asset.ModifiedBy = '" + ModifyBy + "', Asset.ModifiedDate='" + ModifyDate + "' where ( Asset.CurrentBuilding='" + _buildingnum + "' and Asset.IsDeleted=0 and Asset.DispositionCode is not null)";
                    result = db.Execute(query, new { });
                    //var query = @"Update Asset set IsDeleted = -1, ModifiedBy = @ModifiedBy,  ModifiedDate=@ModifiedDate where (CurrentBuilding='" + _buildingnum + "' and DispositionCode is not null and IsDeleted = 0)";
                    //result = db.Execute(query,
                    //    new
                    //    {
                    //        @ModifiedBy = (Thread.CurrentPrincipal as CustomPrincipal).Identity.CurrentUser.UserName,
                    //        @ModifiedDate = DateTime.Now.ToString(),
                    //    });
                }
                    return result;
            }
                
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
