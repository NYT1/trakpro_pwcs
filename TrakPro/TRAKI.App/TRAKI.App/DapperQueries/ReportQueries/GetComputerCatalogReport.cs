﻿using Dapper;
using System.Collections.Generic;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;

namespace TRAKI.App.DapperQueries.ReportQueries
{
    class GetComputerCatalogReport : IQuery<List<Report>>
    {
        private string _catalogNumber;
        public GetComputerCatalogReport(string CatalogNumber)
        {
            this._catalogNumber= CatalogNumber;
        }
        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Asset Value Summary</returns>
        public List<Report> Execute(System.Data.IDbConnection db)
        {
            
            string query = null;

            if (this._catalogNumber == null)
            {
//                query = @"SELECT Count(Asset.AssetNumber) AS Cost, [Catalog].CatalogNumber, [Catalog].CatalogDescription as CatalogDescription, Building.BuildingTypeId, 
//                      (SELECT BuildingTypeDescription from BuildingType where Building.BuildingTypeId=BuildingType.BuildingTypeId) AS BuildingType, Building.BuildingNumber +' - '+ Building.BuildingDescription as BuildingDescription
//                       FROM (Asset LEFT JOIN [Catalog] ON Asset.CatalogNumber = [Catalog].CatalogNumber) LEFT JOIN Building ON Asset.CurrentBuilding = Building.BuildingNumber
//                       WHERE (((Asset.IsDeleted)=0 and [Catalog].isComputer=-1))
//                       GROUP BY [Catalog].CatalogNumber, [Catalog].CatalogDescription, Building.BuildingTypeId, Building.BuildingNumber, Building.BuildingDescription
//                       ORDER BY [Catalog].CatalogNumber, Building.BuildingTypeId, Building.BuildingNumber";

                query = @"SELECT Count(Asset.AssetNumber) AS Cost, Building.BuildingTypeId, 
                      (SELECT BuildingTypeDescription from BuildingType where Building.BuildingTypeId=BuildingType.BuildingTypeId) AS BuildingType, Building.BuildingNumber +' - '+ Building.BuildingDescription as BuildingDescription
                       FROM (Asset LEFT JOIN [Catalog] ON Asset.CatalogNumber = [Catalog].CatalogNumber) LEFT JOIN Building ON Asset.CurrentBuilding = Building.BuildingNumber
                       WHERE (((Asset.IsDeleted)=0 and [Catalog].isComputer=-1))
                       GROUP BY Building.BuildingTypeId, Building.BuildingNumber, Building.BuildingDescription
                       ORDER BY Building.BuildingTypeId, Building.BuildingNumber";
                return (List<Report>)db.Query<Report>(query, new {});

            }
            else
            {
//                query = @"SELECT Count(Asset.AssetNumber) AS Cost, [Catalog].CatalogNumber, [Catalog].CatalogDescription as CatalogDescription, Building.BuildingTypeId, 
//                      (SELECT BuildingTypeDescription from BuildingType where Building.BuildingTypeId=BuildingType.BuildingTypeId) AS BuildingType, Building.BuildingNumber +' - '+ Building.BuildingDescription as BuildingDescription
//                       FROM (Asset LEFT JOIN [Catalog] ON Asset.CatalogNumber = [Catalog].CatalogNumber) LEFT JOIN Building ON Asset.CurrentBuilding = Building.BuildingNumber
//                       WHERE (((Asset.IsDeleted)=0 and Asset.CatalogNumber=@CatalogNumber))
//                       GROUP BY [Catalog].CatalogNumber, [Catalog].CatalogDescription, Building.BuildingTypeId, Building.BuildingNumber, Building.BuildingDescription
//                       ORDER BY [Catalog].CatalogNumber, Building.BuildingTypeId, Building.BuildingNumber";

                query = @"SELECT Count(Asset.AssetNumber) AS Cost, Building.BuildingTypeId, 
                      (SELECT BuildingTypeDescription from BuildingType where Building.BuildingTypeId=BuildingType.BuildingTypeId) AS BuildingType, Building.BuildingNumber +' - '+ Building.BuildingDescription as BuildingDescription
                       FROM (Asset LEFT JOIN [Catalog] ON Asset.CatalogNumber = [Catalog].CatalogNumber) LEFT JOIN Building ON Asset.CurrentBuilding = Building.BuildingNumber
                       WHERE (((Asset.IsDeleted)=0 and Asset.CatalogNumber=@CatalogNumber and  [Catalog].isComputer=-1))
                       GROUP BY Building.BuildingTypeId, Building.BuildingNumber, Building.BuildingDescription
                       ORDER BY Building.BuildingTypeId, Building.BuildingNumber";




                return (List<Report>)db.Query<Report>(query, new
                {
                    @CatalogNumber = this._catalogNumber
                });

            
            }

//            //if (string.IsNullOrEmpty(this._startingBuildingNumber) || string.IsNullOrEmpty(this._startingBuildingNumber))
//            query = @" SELECT Count(Asset.AssetNumber) AS Cost, Building.BuildingNumber as BuildingNumber, Building.BuildingDescription as BuildingDescription , 
//                        (SELECT BuildingTypeDescription from BuildingType where Building.BuildingTypeId=BuildingType.BuildingTypeId) AS BuildingType
//                        FROM Asset INNER JOIN Building ON Asset.CurrentBuilding = Building.BuildingNumber
//                        WHERE (Asset.IsDeleted=0 and Asset.[CatalogNumber]=@CatalogNumber)
//                        GROUP BY Building.BuildingTypeId, Building.BuildingNumber, Building.BuildingDescription order by  Building.BuildingTypeId, Building.BuildingNumber";
            


            
        }
    }
}
