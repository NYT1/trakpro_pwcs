﻿using TRAKI.App.Model;
using Dapper;
using TRAKI.App.Interfaces;
using System.Collections.Generic;

namespace TRAKI.App.DapperQueries.ReportQueries
{
    class GetDispositionReport : IQuery<List<Report>>
    {

        private IEnumerable<string> _buildingNumbers;
        private string _dispositionCode;

        /// <summary>
        /// initializes the instance of the <see cref="GetDispositionReport"/> class.
        /// </summary>
        public GetDispositionReport(IEnumerable<string> BuildingNumbers, string DispositionCode)
        {
            this._dispositionCode = DispositionCode;
            this._buildingNumbers = BuildingNumbers;
        }

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Asset Value Summary</returns>
        public List<Report> Execute(System.Data.IDbConnection db)
        {
            //            string query = @"  SELECT Asset.AssetNumber, Asset.Cost, Asset.SerialNumber, Asset.DateAcquired,
            //                                  (select CatalogDescription from  [Catalog] where CatalogNumber = Asset.CatalogNumber) as CatalogDescription,
            //                                  [Asset].CatalogNumber,  Asset.CurrentBuilding as BuildingNumber, '' as [Condition],
            //                                  (select ManufacturerName from Manufacturer where ManufacturerCode = asset.ManufacturerCode) as ManufacturerName,
            //                                  Asset.ModelNumber 
            //                                  from (Asset   INNER JOIN   Disposition on Disposition.DispositionCode =Asset.DispositionCode)
            //                                    INNER JOIN
            //                                 Building on Building.BuildingNumber =Asset.CurrentBuilding where (Asset.IsDeleted=0 and Asset.DispositionCode = @DispositionCode )";
            if (this._dispositionCode == "ALL")
            {
                string query = @"  SELECT Asset.AssetNumber, Asset.Cost, Asset.SerialNumber, Asset.DateAcquired,Asset.DispositionCode,
                                  (select DispositionDescription from  [Disposition] where DispositionCode = Asset.DispositionCode) as DispositionDescription,
                                  (select CatalogDescription from  [Catalog] where CatalogNumber = Asset.CatalogNumber) as CatalogDescription,
                                  [Asset].CatalogNumber,  Asset.CurrentBuilding as BuildingNumber, '' as [Condition],
                                  (select ManufacturerName from Manufacturer where ManufacturerCode = asset.ManufacturerCode) as ManufacturerName,
                                  Asset.ModelNumber 
                                  from (Asset   INNER JOIN   Disposition on Disposition.DispositionCode =Asset.DispositionCode)
                                    INNER JOIN
                                 Building on Building.BuildingNumber =Asset.CurrentBuilding where (Asset.IsDeleted=0 and (Asset.CurrentBuilding in  (" + string.Format("'{0}'", string.Join("','", _buildingNumbers)) + "))) order by Asset.DispositionCode , Asset.CurrentBuilding";
                return (List<Report>)db.Query<Report>(query, new { });////return (List<Report>)db.Query<Report>(query, new{ });
            }
            else
            {
                string query = @"  SELECT Asset.AssetNumber, Asset.Cost, Asset.SerialNumber, Asset.DateAcquired,Asset.DispositionCode,
                                  (select DispositionDescription from  [Disposition] where DispositionCode = Asset.DispositionCode) as DispositionDescription,
                                  (select CatalogDescription from  [Catalog] where CatalogNumber = Asset.CatalogNumber) as CatalogDescription,
                                  [Asset].CatalogNumber,  Asset.CurrentBuilding as BuildingNumber, '' as [Condition],
                                  (select ManufacturerName from Manufacturer where ManufacturerCode = asset.ManufacturerCode) as ManufacturerName,
                                  Asset.ModelNumber 
                                  from (Asset   INNER JOIN   Disposition on Disposition.DispositionCode =Asset.DispositionCode)
                                    INNER JOIN
                                 Building on Building.BuildingNumber =Asset.CurrentBuilding where (Asset.IsDeleted=0 and Asset.DispositionCode = @DispositionCode and (Asset.CurrentBuilding in  (" + string.Format("'{0}'", string.Join("','", _buildingNumbers)) + "))) order by Asset.CurrentBuilding";
                return (List<Report>)db.Query<Report>(query, new
                {
                    @DispositionCode = this._dispositionCode

                });
            }
        }
    }
}


