﻿using System.Collections.Generic;
using TRAKI.App.Interfaces;
using Dapper;
using TRAKI.App.Model;
using System;

namespace TRAKI.App.DapperQueries.ReportQueries
{
    class GetDiscrepancyReport : IQuery<List<Report>>
    {
        private string _discrepancyType;
        private IEnumerable<string> _buildingNumbers;
        /// <summary>
        /// initializes the instance of the <see cref="GetAssetValueSummaryReport"/> class.
        /// </summary>
        public GetDiscrepancyReport(IEnumerable<string> BuildingNumbers, string DiscrepancyType)
        {
            this._buildingNumbers = BuildingNumbers;
            this._discrepancyType = DiscrepancyType;
        }
        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Discrepancy Asset Report List</returns>
        public List<Report> Execute(System.Data.IDbConnection db)
        {

            try
            {
                string query = string.Empty;
                List<Report> reportList = new List<Report>();

                if (this._discrepancyType.Equals("Mismatch") || this._discrepancyType.Equals("All"))
                {
                    //Steve
                    // don't show deleted items
                    // No buidling filter
                    /* 
                    query = @" select   'Mismatch' AS CurrentStatus, 
                             Asset.AssetNumber ,  Asset.InventoryDate, Asset.CurrentBuilding as BuildingNumber , Asset.CurrentRoom as RoomNumber,  1 as Status,
                             Inventory.BuildingNumber as ScanBuildingNumber ,  Inventory.RoomNumber as ScanRoomNumber ,  (select CatalogDescription from [Catalog] where CatalogNumber = Asset.CatalogNumber) as CatalogDescription,
                             (select ManufacturerName from Manufacturer where ManufacturerCode = asset.ManufacturerCode) as ManufacturerName
                      
                              from Asset 
                             Inner join Inventory on
                             Inventory.AssetNumber = Asset.AssetNumber 
                             where (Asset.IsDeleted=0) and ((Inventory.BuildingNumber <>  Asset.CurrentBuilding or  Inventory.RoomNumber <>  Asset.CurrentRoom) and  (Asset.CurrentBuilding in  (" + string.Format("'{0}'", string.Join("','", _buildingNumbers)) + ")))";
                     */
                    query = @" select   'Mismatch' AS CurrentStatus, 
                             Asset.AssetNumber ,  Asset.InventoryDate, Asset.CurrentBuilding as BuildingNumber , Asset.CurrentRoom as RoomNumber,  1 as Status,
                             Inventory.BuildingNumber as ScanBuildingNumber ,  Inventory.RoomNumber as ScanRoomNumber ,  (select CatalogDescription from [Catalog] where CatalogNumber = Asset.CatalogNumber) as CatalogDescription,
                             (select ManufacturerName from Manufacturer where ManufacturerCode = asset.ManufacturerCode) as ManufacturerName
                      
                              from Asset 
                             Inner join Inventory on
                             Inventory.AssetNumber = Asset.AssetNumber 
                             where (Asset.IsDeleted=0) and (Inventory.BuildingNumber <>  Asset.CurrentBuilding or  Inventory.RoomNumber <>  Asset.CurrentRoom) ";


                    reportList.InsertRange(0, db.Query<Report>(query));
                }
                if (this._discrepancyType.Equals("Shortage") || this._discrepancyType.Equals("All"))
                {
                    var _inventoryCount = db.Query<int>(@"SELECT count(1) from Inventory").AsList<int>();
                    string _conQuery = string.Empty;
                    // string dd = _rlt[0].ToString();
                    if (_inventoryCount[0] <= 0)  // TO GET RECORDS when 
                    {
                        _conQuery = "Asset.AssetId=0 and";
                    }
                    //Steve
                    // don't show deleted items
                    //Don't need buildings filter
                    /*
                    query = @" select 'Shortages' AS CurrentStatus, 
                                 Asset.AssetNumber ,  Asset.InventoryDate, Asset.CurrentBuilding as BuildingNumber , Asset.CurrentRoom as RoomNumber,  3 as Status,
                             Inventory.BuildingNumber as ScanBuildingNumber ,  Inventory.RoomNumber as ScanRoomNumber , (select CatalogDescription from [Catalog] where CatalogNumber = Asset.CatalogNumber) as CatalogDescription,
                             (select ManufacturerName from Manufacturer where ManufacturerCode = asset.ManufacturerCode) as ManufacturerName
                                  from Asset   
                                 LEFT JOIN  
                                 Inventory on
                                 Inventory.AssetNumber = Asset.AssetNumber  where (Asset.IsDeleted=0) and  ((" + _conQuery + "  Inventory.BuildingNumber is null) and  (Asset.CurrentBuilding in  (" + string.Format("'{0}'", string.Join("','", _buildingNumbers)) + ")))";
                     */
                    query = @" select 'Shortages' AS CurrentStatus, 
                                 Asset.AssetNumber ,  Asset.InventoryDate, Asset.CurrentBuilding as BuildingNumber , Asset.CurrentRoom as RoomNumber,  3 as Status,
                             Inventory.BuildingNumber as ScanBuildingNumber ,  Inventory.RoomNumber as ScanRoomNumber , (select CatalogDescription from [Catalog] where CatalogNumber = Asset.CatalogNumber) as CatalogDescription,
                             (select ManufacturerName from Manufacturer where ManufacturerCode = asset.ManufacturerCode) as ManufacturerName
                                  from Asset   
                                 LEFT JOIN  
                                 Inventory on
                                 Inventory.AssetNumber = Asset.AssetNumber  where (Asset.IsDeleted=0) and  (Inventory.AssetNumber is null)";
                    reportList.InsertRange(reportList.Count, db.Query<Report>(query));
                }
                if (this._discrepancyType.Equals("Overage") || this._discrepancyType.Equals("All"))
                {
                    //Steve
                    //Don't need buildings.
                  
                    /*
                    query = @"SELECT  'Overages' AS CurrentStatus, 
                  Inventory.AssetNumber ,  Inventory.InventoryDate, Asset.CurrentBuilding as BuildingNumber , Asset.CurrentRoom as RoomNumber,  2 as Status,
                             Inventory.BuildingNumber as ScanBuildingNumber ,  Inventory.RoomNumber as ScanRoomNumber , Inventory.description as CatalogDescription,
                             (select ManufacturerName from Manufacturer where ManufacturerCode = asset.ManufacturerCode) as ManufacturerName
                    from  Inventory
                    left join
                    Asset 
                    on Asset.AssetNumber = Inventory.AssetNumber
                        where  ( Asset.AssetNumber is null and  (Inventory.BuildingNumber in  (" + string.Format("'{0}'", string.Join("','", _buildingNumbers)) + ")))";
                  */
                    query = @"SELECT  'Overages' AS CurrentStatus, 
                  Inventory.AssetNumber ,  Inventory.InventoryDate, Asset.CurrentBuilding as BuildingNumber , Asset.CurrentRoom as RoomNumber,  2 as Status,
                             Inventory.BuildingNumber as ScanBuildingNumber ,  Inventory.RoomNumber as ScanRoomNumber , Inventory.description as CatalogDescription,
                             (select ManufacturerName from Manufacturer where ManufacturerCode = asset.ManufacturerCode) as ManufacturerName
                    from  Inventory
                    left join
                    Asset 
                    on Asset.AssetNumber = Inventory.AssetNumber
                        where  ( Asset.AssetNumber is null )";

                    reportList.InsertRange(reportList.Count, db.Query<Report>(query));
                }


                // return (List<Report>)db.Query<Report>(query);
                return reportList;

            }
            catch (Exception ex) {
                var dd = ex.Message;
                return null;
            }

        }
    }
}


