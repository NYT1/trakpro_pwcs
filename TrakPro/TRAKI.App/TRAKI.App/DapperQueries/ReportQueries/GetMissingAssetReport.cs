﻿using Dapper;
using System.Collections;
using System.Collections.Generic;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;

namespace TRAKI.App.DapperQueries.ReportQueries
{
    class GetMissingAssetReport : IQuery<List<Report>>
    {
        private IEnumerable<string> _buildingNumbers;

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Inventory Summary</returns>
        /// 
        public GetMissingAssetReport(IEnumerable<string> BuildingNumbers)
        {
            this._buildingNumbers = BuildingNumbers;
        }
        public List<Report> Execute(System.Data.IDbConnection db)
        {
            //List<Report> reportList = new List<Report>();

            //****
            //            string query = @"SELECT Asset.AssetNumber, Asset.Cost, Asset.SerialNumber, Asset.CatalogNumber, Asset.CurrentBuilding as BuildingNumber,
            //                                     Building.BuildingDescription,  
            //                                    Asset.CurrentRoom as RoomNumber,
            //                                    (select [Catalog].CatalogDescription from [Catalog]  where CatalogNumber = Asset.CatalogNumber ) as CatalogDescription
            //                                    FROM 
            //							        Asset 
            //									INNER JOIN Building ON Building.BuildingNumber =Asset.CurrentBuilding
            //                                    Where( Asset.IsDeleted=0 and Asset.IsMissing=-1)";
            //***
            // (Inventory.BuildingNumber in  (" + string.Format("'{0}'", string.Join("','", _buildingNumbers)) + "))


            string query = @"SELECT Asset.AssetNumber, Asset.Cost, Asset.SerialNumber, Asset.CatalogNumber, Asset.CurrentBuilding as BuildingNumber,
                                     Building.BuildingDescription,  
                                    Asset.CurrentRoom as RoomNumber,
                                    (select [Catalog].CatalogDescription from [Catalog]  where CatalogNumber = Asset.CatalogNumber ) as CatalogDescription
                                    FROM 
							        Asset 
									INNER JOIN Building ON Building.BuildingNumber =Asset.CurrentBuilding
                                    Where( Asset.IsDeleted=0 and Asset.IsMissing=-1 and (Asset.CurrentBuilding in  (" + string.Format("'{0}'", string.Join("','", _buildingNumbers)) + "))) order by Asset.CurrentBuilding,Asset.AssetNumber ";
            return (List<Report>)db.Query<Report>(query);
        }
    }
}