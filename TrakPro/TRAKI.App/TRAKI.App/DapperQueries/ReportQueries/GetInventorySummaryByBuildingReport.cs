﻿using Dapper;
using System.Collections.Generic;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;

namespace TRAKI.App.DapperQueries.ReportQueries
{
    class GetInventorySummaryByBuildingReport : IQuery<List<Report>>
    {
    
        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Inventory Summary</returns>
        public List<Report> Execute(System.Data.IDbConnection db)
        {
            string query = @"   Select BuildingNumber, BuildingDescription, CurrentStatus , DateSigned,  TotalCost ,BuildingType,
                             (MissingCost /TotalCost) as MissingCostPercentage,
                             (select sum(cost) from asset  
                            where  (Asset.CurrentBuilding= BuildingNumber and Asset.isMissing=-1) group by Asset.currentBuilding)  as MissingCost
                             from 
						(
						SELECT  B.BuildingNumber, B.BuildingDescription , B.DateSigned, B.CurrentStatus, BT.BuildingTypeDescription as BuildingType,
                                         sum(cost)  as TotalCost 

                                       
												from (Asset A 
												INNER JOIN
												Building B on B.BuildingNumber =A.CurrentBuilding)
                                                inner join BuildingType BT on BT.BuildingTypeId = B.BuildingTypeId
                                                     Where (A.IsDeleted=0)            
                                                group by  B.BuildingNumber,  B.BuildingDescription , B.DateSigned, B.CurrentStatus,BT.BuildingTypeDescription

) ";
            return (List<Report>)db.Query<Report>(query);
        }
    }
}