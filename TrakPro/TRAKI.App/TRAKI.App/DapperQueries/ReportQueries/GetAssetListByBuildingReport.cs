﻿using Dapper;
using System.Collections.Generic;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;

namespace TRAKI.App.DapperQueries.ReportQueries
{
    class GetAssetListByBuildingReport : IQuery<List<Report>>
    {
        private string _startingBuildingNumber;
        private string _endingBuildingNumber;
        /// <summary>
        /// initializes the instance of the <see cref="GetAssetValueSummaryReport"/> class.
        /// </summary>
        public GetAssetListByBuildingReport(string StartingBuildingNumber,string EndingBuildingNumber)
        {
            this._endingBuildingNumber= EndingBuildingNumber;
            this._startingBuildingNumber=StartingBuildingNumber;
        }

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Asset Value Summary</returns>
        public List<Report> Execute(System.Data.IDbConnection db)
        {
            /*
            string query = @"  SELECT Asset.AssetNumber,  Asset.Cost,  Asset.SerialNumber ,  Asset.InventoryDate, Building.BuildingNumber,  Building.BuildingDescription, [Catalog].CatalogNumber,
                       [Catalog].CatalogDescription , Asset.CurrentRoom as RoomNumber
                        from (Asset 
                        INNER JOIN
                        [Catalog] on [Catalog].CatalogNumber =Asset.CatalogNumber) 
                        INNER JOIN
                        Building on Building.BuildingNumber =Asset.CurrentBuilding  Where (Asset.IsDeleted=0   
                            and (@StartingBuildingNumber is null or @EndingBuildingNumber is null ) 
                                                        OR  (Asset.CurrentBuilding between @StartingBuildingNumber and @EndingBuildingNumber))"; */
            // Steve
            // Should list all assets as long as its building number is valid. 
            // catalog information is optional. Therefore, it should be left join instead of inner join
            //
            // dataset should be soorted before report paging.

          /*
           string bldgnb1 = (string.IsNullOrEmpty(this._startingBuildingNumber)) ? null : this._startingBuildingNumber;
           string bldgnb2 = (string.IsNullOrEmpty(this._endingBuildingNumber)) ? null : this._endingBuildingNumber;
           */
           //Steve query doesn't work with null value in where cause.
            //Changed to detect text box and then go to correct SQL statement

           string query=null;

           if (string.IsNullOrEmpty(this._startingBuildingNumber) || string.IsNullOrEmpty(this._startingBuildingNumber))
                 query = @"  SELECT Asset.AssetNumber,  Asset.Cost,  Asset.SerialNumber ,  Asset.InventoryDate, Building.BuildingNumber,  Building.BuildingDescription, [Catalog].CatalogNumber,
                       [Catalog].CatalogDescription , Asset.CurrentRoom as RoomNumber
                        from (Asset 
                        Left JOIN
                        [Catalog] on [Catalog].CatalogNumber =Asset.CatalogNumber ) 
                        INNER JOIN
                        Building on Building.BuildingNumber =Asset.CurrentBuilding  Where   Asset.IsDeleted = 0  
                       Order by Asset.CurrentBuilding, Asset.AssetNumber  ";
           else
                query = @"  SELECT Asset.AssetNumber,  Asset.Cost,  Asset.SerialNumber ,  Asset.InventoryDate, Building.BuildingNumber,  Building.BuildingDescription, [Catalog].CatalogNumber,
                       [Catalog].CatalogDescription , Asset.CurrentRoom as RoomNumber
                        from (Asset 
                        Left JOIN
                        [Catalog] on [Catalog].CatalogNumber =Asset.CatalogNumber ) 
                        INNER JOIN
                        Building on Building.BuildingNumber =Asset.CurrentBuilding  Where (  Asset.IsDeleted = 0  and
                             (Asset.CurrentBuilding between @StartingBuildingNumber and @EndingBuildingNumber))  
                       Order by Asset.CurrentBuilding, Asset.AssetNumber  ";
           

            return (List<Report>)db.Query<Report>(query, new
            {
                @StartingBuildingNumber = (string.IsNullOrEmpty(this._startingBuildingNumber))?null:this._startingBuildingNumber,
                @EndingBuildingNumber = (string.IsNullOrEmpty(this._endingBuildingNumber)) ? null : this._endingBuildingNumber
            });

        }
    }
}
        
        
        
        
       