﻿using Dapper;
using System.Collections.Generic;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;

namespace TRAKI.App.DapperQueries.ReportQueries
{
    class GetAssetValueSummaryReport : IQuery<List<Report>>
    {

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Asset Value Summary</returns>
        public List<Report> Execute(System.Data.IDbConnection db)
        {
            string query = @"SELECT   Sum(Asset.Cost) as Cost ,  Building.BuildingNumber,  Building.BuildingDescription 
                        from Asset  
                        INNER JOIN
                        Building on Building.BuildingNumber =Asset.CurrentBuilding 
						Where( Asset.IsDeleted=0)
                        Group By Building.BuildingNumber,  Building.BuildingDescription";
            return (List<Report>)db.Query<Report>(query);

        }
    }
}

