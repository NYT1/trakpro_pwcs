﻿using Dapper;
using System.Collections.Generic;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;

namespace TRAKI.App.DapperQueries.ReportQueries
{
    class GetCafeteriaReport : IQuery<List<Report>>
    {
       
        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Asset Value Summary</returns>
        public List<Report> Execute(System.Data.IDbConnection db)
        {
            /*
             string query = @"Select BuildingNumber, BuildingDescription , DateReceived, CurrentStatus, TotalCost  from 
                         (
                         SELECT B.BuildingNumber, B.BuildingDescription ,B.DateReceived,B.CurrentStatus,       
                                         ( 
                                         Select sum(AST.cost)
                                         from  Asset AST 
                                                 INNER JOIN
                                                 Building BL on BL.BuildingNumber =AST.CurrentBuilding
                                                      Where (   AST.IsDeleted=0 and AST.CurrentBuilding= B.BuildingNumber  and  A.AssetNumber like 'C%' ) )
                                                 group by  BL.BuildingNumber,  BL.BuildingDescription , BL.DateSigned, BL.CurrentStatus

                                                   ) as TotalCost 
                                                 from Asset A 
                                                 INNER JOIN
                                                 Building B on B.BuildingNumber =A.CurrentBuilding where A.AssetNumber like 'C%' )";   */
            string query = @"  Select BuildingNumber, BuildingDescription , DateReceived, CurrentStatus, TotalCost from
                                    (SELECT CurrentBuilding, sum(cost) as totalcost 
                                        FROM Asset
                                            where right(CurrentBuilding,1)='C' and IsDeleted=0
                                                group by CurrentBuilding
                                                    Order by Asset.CurrentBuilding) as ast inner join Building bldg on ast.CurrentBuilding=bldg.BuildingNumber";
            // Steve
            // Fixed to meet requirements
            // 7/22/2018
            return (List<Report>)db.Query<Report>(query);

        }
    }
}
        
        
        
        
       