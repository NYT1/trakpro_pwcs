﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;
using TRAKI.App.Model;


namespace TRAKI.App.DapperQueries.DepartmentQueries
{
    public class DeleteDepartment : IQuery<int>
    {
        private int _departmentId;

        /// <summary>
        /// initializes the instance of the <see cref="UpdateUser"/> class.
        /// </summary>
        public DeleteDepartment(int DepartmentId)
        {
            this._departmentId = DepartmentId;
        }


        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Number of rows Affected</returns>
        public int Execute(System.Data.IDbConnection db)
        {
            try
            {
                var query = @"Delete from  Department  WHERE [DepartmentId] = @DepartmentId";
                int result = db.Execute(query, new { @DepartmentId = this._departmentId });
             
                return result;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}