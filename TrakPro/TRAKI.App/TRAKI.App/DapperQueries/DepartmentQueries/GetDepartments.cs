﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;
using TRAKI.App.Model;

namespace TRAKI.App.DapperQueries.DepartmentQueries
{
    public class GetDepartments : IQuery<List<Department>>
    {
        private int? _departmentId;
        private int? _isActive;

        /// <summary>
        /// initializes the instance of the <see cref="GetDepartments"/> class.
        /// </summary>
        public GetDepartments(int? DepartmentId, int? IsActive)
        {
            this._departmentId = DepartmentId;
            this._isActive = IsActive;
        }

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>List of Departments</returns>

        public List<Department> Execute(System.Data.IDbConnection db)
        {

            if (this._isActive > 0)
                this._isActive = null;
            string query = @"SELECT  DepartmentId, DepartmentDescription, DepartmentNumber, IsActive FROM Department where ((@DepartmentId is null or [Department].DepartmentId=@DepartmentId) and (@IsActive is null or Department.IsActive=@IsActive)) order by DepartmentNumber";
            return (List<Department>)db.Query<Department>(query,
                new
                {
                    @DepartmentId = this._departmentId,
                    @IsActive = this._isActive
                }
            );
        }
    }
}