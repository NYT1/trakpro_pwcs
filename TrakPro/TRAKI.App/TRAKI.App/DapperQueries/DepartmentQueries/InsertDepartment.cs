﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;
using TRAKI.App.Model;


namespace TRAKI.App.DapperQueries.DepartmentQueries
{
    class InsertDepartment : IQuery<int>
    {
        private Department _department;

        /// <summary>
        /// initializes the instance of the <see cref="InsertDepartment"/> class.
        /// </summary>
        public InsertDepartment(Department Department)
        {
            this._department = Department;
        }


        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Number of rows Affected</returns>
        public int Execute(System.Data.IDbConnection db)
        {
            try
            {
                int result = db.Execute(@"Insert into Department (DepartmentNumber,  DepartmentDescription, IsActive)
                                         values (@DepartmentNumber, @DepartmentDescription,  @IsActive ) ",
                                       new
                                       {
                                           @DepartmentNumber = this._department.DepartmentNumber,
                                           @DepartmentDescription = this._department.DepartmentDescription,
                                           @IsActive = (Convert.ToBoolean(this._department.IsActive)) ? 1 : 0,

                                       });

                return result;
            }

            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}