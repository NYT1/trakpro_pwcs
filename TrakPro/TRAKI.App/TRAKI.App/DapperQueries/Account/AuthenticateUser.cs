﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;
using Dapper;
using TRAKI.App.Common;
using System.Data;


namespace TRAKI.App.DapperQueries.UserQueries.Account
{
    class AuthenticateUser : IQuery<User>
    {
        private User _user;

        /// <summary>
        /// initializes the instance of the <see cref="AuthenticateUser"/> class.
        /// </summary>
        public AuthenticateUser(User User)
        {
            this._user = User;
        }


        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Authenticated User Object or null</returns>
        public User Execute(System.Data.IDbConnection db)
        {
            IDbTransaction transaction = null;
            try
            {
                User user;
                int result = 0;

             /*
                var query = @"SELECT  User.Userid, User.FirstName,User.LastName, User.Email, User.UserName, User.Password, User.Gender,   User.IsActive,  User.RoleId, User.IsTempPassword, ROLE.Name as Role  FROM [User] INNER JOIN [Role] ON User.RoleId = ROLE.RoleId 
                            where (((User.UserName=@UserName or User.Email=@UserName ) and
                            (StrComp(User.Password, @Password, 0) = 0 or User.IsTempPassword =-1 ))and (User.IsActive=-1) )";
              */
 
                //Steve
                // In order to have different messages for each type users, need to get a list of user 
                //regardless active or inactive

                var query = @"SELECT  User.Userid, User.FirstName,User.LastName, User.Email, User.UserName, User.Password, User.Gender,   User.IsActive,  User.RoleId, User.IsTempPassword, ROLE.Name as Role  FROM [User] INNER JOIN [Role] ON User.RoleId = ROLE.RoleId 
                            where ((User.UserName=@UserName or User.Email=@UserName ) and
                            (StrComp(User.Password, @Password, 0) = 0 or User.IsTempPassword =-1 )) ";
                user = db.Query<User>(query,

                                new
                                {
                                    @UserName = this._user.UserName,
                                    @Password = this._user.Password
                                }).FirstOrDefault();


                if (user != null)
                {
                    if (user.IsTempPassword == BooleanValue.False)
                       return user;
                    else
                    {
                        
                        using (transaction = db.BeginTransaction())
                        {
                            if (user.Password.Equals(this._user.Password))
                            {
                                result = db.Execute(@"UPDATE [User]
                                   SET 
                                       IsTempPassword = @IsTempPassowrd 
                                        WHERE (( User.UserName=@UserName or User.Email=@UserName ) and User.Password=@Password and User.IsActive=-1 )",
                                        new
                                        {
                                            @IsTempPassowrd = 0,
                                            @UserName = this._user.UserName,
                                            @Password = this._user.Password
                                        }, transaction);

                                result = (int)db.Execute(@"UPDATE [PasswordHistory]
                                                                   SET 
                                                                      [IsValid] = 0
                                                			          WHERE ([Email] = (select Email from [User] where ((User.UserName=@UserName or User.Email=@UserName) and User.IsActive=-1)))",
                                         new
                                         {
                                             @UserName = this._user.UserName,
                                         }, transaction);

                                user.IsTempPassword = BooleanValue.False;
                            }
                            else
                            {

                                result = (int)db.ExecuteScalar(@"SELECT count(1) FROM [User] INNER JOIN [PasswordHistory]  on [User].Email = PasswordHistory.Email
                                                    where ((User.UserName=@UserName or User.Email=@UserName ) and TemporaryPassword =@Password and IsValid=-1)",
                                       new
                                       {
                                           @UserName = this._user.UserName,
                                           @Password = this._user.Password

                                       }, transaction);
                                result = (int)db.Execute(@"UPDATE [PasswordHistory]
                                                                   SET 
                                                                      [IsValid] = 0
                                                			          WHERE ([Email] = (select Email from [User] where ((User.UserName=@UserName or User.Email=@UserName) and User.IsActive=-1))
                                                                        and TemporaryPassword =@Password)",
                                          new
                                          {
                                              @UserName = this._user.UserName,
                                              @Password = this._user.Password
                                          }, transaction);
                               // if (result > 0)
                               //     user.Password = string.Empty;
                            }
                            transaction.Commit();
                            if (result > 0)
                                return user;
                        }
                    }
                }
                return null;
            }

            catch (Exception ex)
            {
                if (transaction != null)
                    transaction.Rollback();
                throw ex;
            }
        }
    }
}
