﻿using System;

using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;
using Dapper;
using TRAKI.App.Common;
using System.Data;
using System.Collections.Generic;
using System.Threading;
using TRAKI.App.Account;


namespace TRAKI.App.DapperQueries.InventoryQueries
{

    // Query to Close all the open buildings.

    class CloseAllBuildings : IQuery<int>
    {
       
        public int Execute(System.Data.IDbConnection db)
        {
            IDbTransaction transaction = null;
            int result = 0;
            try
            {
                using (transaction = db.BeginTransaction())
                {
                    db.Execute("Update Room set Room.IsActive= 0",null, transaction);
                    db.Execute("Update Building set Building.IsActive= 0, Building.IsOpen=0",null, transaction);
                    transaction.Commit();
                    result = 1;
                }
                return result;
            }
            catch (Exception ex)
            {
                if (transaction != null)
                    transaction.Rollback();
                throw ex;
            }
        }


    }

    // Query to Close all the open buildings.

    class CloseSelectedBuildings : IQuery<int>
    {
        private IEnumerable<string> _buildingNumbers;
        public CloseSelectedBuildings(IEnumerable<string> BuildingNumbers)
        {
            this._buildingNumbers = BuildingNumbers;
        }
        public int Execute(System.Data.IDbConnection db)
        {
            IDbTransaction transaction = null;
            int result = 0;
            try
            {
                using (transaction = db.BeginTransaction())
                {
                    //Steve
                    //Open/close buildings doesn't relate to active status directly
                    //db.Execute("Update Room set Room.IsActive= 0 where BuildingId in ( select BuildingId from Building where BuildingNumber in (" + string.Format("'{0}'", string.Join("','", _buildingNumbers)) + "))", null, transaction);
                    //db.Execute("Update Building set Building.IsActive= 0, Building.IsOpen=0 where BuildingNumber in (" + string.Format("'{0}'", string.Join("','", _buildingNumbers)) + ")", null, transaction);
                    db.Execute("Update Building set Building.IsOpen=0 where BuildingNumber in (" + string.Format("'{0}'", string.Join("','", _buildingNumbers)) + ")", null, transaction);
                    transaction.Commit();
                    result = 1;
                }
                return result;
            }
            catch (Exception ex)
            {
                if (transaction != null)
                    transaction.Rollback();
                throw ex;
            }
        }


    }

    public class OpenAllBuilding : IQuery<int>
    {

        public int Execute(System.Data.IDbConnection db)
        {
            IDbTransaction transaction = null;
            int result = 0;
            try
            {
                using (transaction = db.BeginTransaction())
                {

                    db.Execute("Update Building set Building.IsActive= -1, Building.IsOpen=-1", null, transaction);
                    db.Execute("Update Room set Room.IsActive=-1",null, transaction);
                    transaction.Commit();
                    result = 1;

                }
                return result;
            }
            catch (Exception ex)
            {
                if (transaction != null)
                    transaction.Rollback();
                throw ex;
            }
        }


    }

    // Query to Empty the Inventory Table after Update
    class EmptyInventoryTable : IQuery<int>
    {

        public int Execute(System.Data.IDbConnection db)
        {
            try
            {
                int result = db.Execute("Delete from  Inventory");
                if (result > 0)
                    db.Execute("ALTER TABLE [Inventory] ALTER COLUMN InventoryId COUNTER(1,1)");
                return result;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
