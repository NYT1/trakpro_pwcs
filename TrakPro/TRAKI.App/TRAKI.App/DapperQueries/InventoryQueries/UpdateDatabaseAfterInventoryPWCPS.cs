﻿using System;

using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;
using Dapper;
using TRAKI.App.Common;
using System.Data;
using System.Collections.Generic;
using System.Threading;
using TRAKI.App.Account;


namespace TRAKI.App.DapperQueries.InventoryQueries
{

    // Query to get the MIsmatched Assets
    class GetMismatchAssetsPWCPS : IQuery<List<Inventory>>
    {
        private IEnumerable<string> _buildingNumbers;

        public GetMismatchAssetsPWCPS(IEnumerable<string> BuildingNumbers)
        {
            this._buildingNumbers = BuildingNumbers;
        }

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Staus of the AssetNumber</returns>
        public List<Inventory> Execute(System.Data.IDbConnection db)
        {

            string query = @"select   Asset.AssetNumber , Asset.AssetId, Inventory.InventoryDate, Inventory.BuildingNumber , Inventory.RoomNumber 
                              from Asset 
                             Inner join Inventory on
                             Inventory.AssetNumber = Asset.AssetNumber 
                             where ((Inventory.BuildingNumber <>  Asset.CurrentBuilding or  Inventory.RoomNumber <>  Asset.CurrentRoom) and Inventory.BuildingNumber in  (" + string.Format("'{0}'", string.Join("','", _buildingNumbers)) + "))";


            return (List<Inventory>)db.Query<Inventory>(query);
        }
    }

    // Query th Get the List of Overage Assets
    class GetOverageAssetsPWCPS : IQuery<List<Inventory>>
    {

        private IEnumerable<string> _buildingNumbers;

        public GetOverageAssetsPWCPS(IEnumerable<string> BuildingNumbers)
        {
            this._buildingNumbers = BuildingNumbers;
        }

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Staus of the AssetNumber</returns>
        public List<Inventory> Execute(System.Data.IDbConnection db)
        {

            string query = @"SELECT Inventory.AssetNumber, Inventory.InventoryDate, Inventory.BuildingNumber , Inventory.RoomNumber
                    from  Inventory
                    left join
                    Asset 
                    on Asset.AssetNumber = Inventory.AssetNumber
                        where ( Asset.AssetNumber is null and Inventory.BuildingNumber in  (" + string.Format("'{0}'", string.Join("','", _buildingNumbers)) + "))";

            return (List<Inventory>)db.Query<Inventory>(query);
        }

    }

    // Query to Update the locations of the Mismatch Assets
    class UpdateLocationOfMismatchAssetsPWCPS : IQuery<int>
    {
        // private Asset _asset;
        private List<Inventory> _missmatchAssetList;

        public UpdateLocationOfMismatchAssetsPWCPS(List<Inventory> _missmatchAssetList)
        {
            this._missmatchAssetList = _missmatchAssetList;
        }

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Staus of the AssetNumber</returns>
        public int Execute(System.Data.IDbConnection db)
        {

            System.Data.IDbTransaction transaction = null;
            try
            {
                int result = 0;
                using (transaction = db.BeginTransaction())
                {
                    foreach (Inventory _missmatchAsset in this._missmatchAssetList)
                    {

                        dynamic _previousLocInfo = db.Query(@"SELECT CurrentBuilding,CurrentRoom from Asset WHERE AssetId=@AssetId",
                                              new { @AssetId = _missmatchAsset.AssetId, }, transaction).SingleOrDefault();


                        if (_previousLocInfo != null)
                        {
                            var idRow = (IDictionary<string, object>)_previousLocInfo;
                            db.Execute(@"Insert into [AssetLocation] (
                                        AssetId,
                                        BuildingNumber,
                                        RoomNumber,
                                        MoveType
                                          )
                                   values ( 
                                        @AssetId,
                                        @BuildingNumber,
                                        @RoomNumber,
                                        @MoveType
                                        ) ",
                                          new
                                          {
                                              @AssetId = _missmatchAsset.AssetId,
                                              @BuildingNumber = idRow["CurrentBuilding"],
                                              @RoomNumber = idRow["CurrentRoom"],
                                              @MoveType = MoveType.AcceptInventoryData,
                                          }, transaction);
                        }

                        CommandDefinition cmd = new CommandDefinition(@"Update [Asset] set  AssetRelocated = @AssetRelocated, CurrentBuilding = @CurrentBuilding, CurrentRoom =@CurrentRoom, InventoryDate=@InventoryDate,
                                                                         ModifiedBy = @ModifiedBy, ModifiedDate=@ModifiedDate, IsMissing= 0 , Status='Active' WHERE AssetId=@AssetId",
                                            new
                                            {
                                                @AssetRelocated = 1,
                                                @CurrentBuilding = _missmatchAsset.BuildingNumber,
                                                @CurrentRoom = _missmatchAsset.RoomNumber,
                                                @InventoryDate = _missmatchAsset.InventoryDate,
                                                @ModifiedBy = (Thread.CurrentPrincipal as CustomPrincipal).Identity.CurrentUser.UserName,
                                                @ModifiedDate = DateTime.Now.ToString(),
                                                @AssetId = _missmatchAsset.AssetId,
                                            }, transaction);

                        db.Execute(cmd);

                        // TO Do Comment this if you do not want to add the missing location from inventory
                        this.AddMissingLocations(db, transaction, _missmatchAsset);

                    }
                    //}
                    transaction.Commit();
                    result = 1;

                }

                return result;
            }

            catch (Exception ex)
            {
                if (transaction != null)
                    transaction.Rollback();
                throw ex;
            }
        }


        /// <summary>
        /// Method to insert the lcation into the database which is not found in DB
        /// </summary>
        /// <param name="db"></param>
        /// <param name="transaction"></param>
        /// <param name="inventory"></param>
        private void AddMissingLocations(System.Data.IDbConnection db, IDbTransaction transaction, Inventory inventory)
        {

            var _buildingInfo = db.Query<int>(@"SELECT BuildingId from Building  where(Building.BuildingNumber =@BuildingNumber)",
                   new { @BuildingNumber = inventory.BuildingNumber }, transaction).ToArray();



            if (_buildingInfo.Count() == 0)
            {
                db.Execute(@"Insert into Building ( BuildingNumber ,  BuildingDescription ,BuildingTypeId,IsOpen,IsActive)
                                         values ( @BuildingNumber, @BuildingDescription,  @BuildingTypeId,@BuildingStatus, @IsActive ) ",

                            new
                            {
                                @BuildingNumber = inventory.BuildingNumber,
                                @BuildingDescription = inventory.BuildingNumber,
                                @BuildingTypeId = 1,
                                @BuildingStatus = 1,
                                @IsActive = 1

                            }, transaction);


                IDbCommand NewIdCommand = new System.Data.OleDb.OleDbCommand();
                NewIdCommand.Transaction = transaction;
                NewIdCommand.CommandText = "SELECT @@IDENTITY";
                NewIdCommand.Connection = db;
                var newBuildingId = NewIdCommand.ExecuteScalar();


                db.Execute(@"Insert into Room (RoomNumber,RoomDescription,BuildingId,IsActive)
                                         values (@RoomNumber,@RoomDescription, @BuildingId,@IsActive) ",
                           new
                           {
                               @RoomNumber = inventory.RoomNumber,
                               @RoomDescription = inventory.RoomNumber,
                               @BuildingId = newBuildingId,
                               @IsActive = 1
                           }, transaction);

            }
            else
            {
                var _roomInfo = db.Query<string>(@"SELECT RoomNumber from Room  where(RoomNumber = @RoomNumber and BuildingId =@BuildingId)",
                      new { @RoomNumber = inventory.RoomNumber, @BuildingId = _buildingInfo[0] }, transaction).ToArray();

                if (_roomInfo.Count() == 0)
                {

                    db.Execute(@"Insert into Room (RoomNumber,RoomDescription,BuildingId,IsActive)
                                         values (@RoomNumber,@RoomDescription, @BuildingId,@IsActive) ",
                             new
                             {
                                 @RoomNumber = inventory.RoomNumber,
                                 @RoomDescription = inventory.RoomNumber,
                                 @BuildingId = _buildingInfo[0],
                                 @IsActive = 1
                             }, transaction);
                }
            }

        }

    }

    //Query to Update the Records with Valid Location
    class UpdateAssetsWithValidLocationPWCPS : IQuery<int>
    {
        private IEnumerable<string> _buildingNumbers;

        public UpdateAssetsWithValidLocationPWCPS(IEnumerable<string> BuildingNumbers)
        {
            this._buildingNumbers = BuildingNumbers;
        }


        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Staus of the AssetNumber</returns>
        public int Execute(System.Data.IDbConnection db)
        {
            IDbTransaction transaction = null;
            try
            {
                int result = 0;

                db.Execute(@"update Asset Inner join Inventory on
                                Inventory.AssetNumber = Asset.AssetNumber 
                                set IsMissing=0, Asset.Status='Active', Asset.InventoryDate= Inventory.InventoryDate,  ModifiedBy = @ModifiedBy, ModifiedDate=now()
                                where (Inventory.BuildingNumber =  Asset.CurrentBuilding  and  Inventory.RoomNumber =  Asset.CurrentRoom and Inventory.BuildingNumber in (" + string.Format("'{0}'", string.Join("','", _buildingNumbers)) + "))",
                                        new
                                        {
                                            @ModifiedBy = (Thread.CurrentPrincipal as CustomPrincipal).Identity.CurrentUser.UserName,
                                        });

                result = 1;
                return result;
            }

            catch (Exception ex)
            {
                if (transaction != null)
                    transaction.Rollback();
                throw ex;
            }
        }

    }

    //Query to Update the Shortage Assets
    class UpdateShortageAssetsPWCPS : IQuery<int>
    {
        private IEnumerable<string> _buildingNumbers;

        public UpdateShortageAssetsPWCPS(IEnumerable<string> BuildingNumbers)
        {
            this._buildingNumbers = BuildingNumbers;
        }

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Staus of the AssetNumber</returns>
        public int Execute(System.Data.IDbConnection db)
        {
            try
            {

                //Steve
                //correct Building
               /*
                db.Execute(@"update Asset 
                                Left join Inventory on
                                Inventory.AssetNumber = Asset.AssetNumber 
                                set IsMissing=-1, Asset.Status='Missing',  ModifiedBy = @ModifiedBy, ModifiedDate=now()
                                where (Inventory.AssetNumber is null and Asset.BuildingNumber in (" + string.Format("'{0}'", string.Join("','", _buildingNumbers)) + "))",
                                       new
                                       {
                                           @ModifiedBy = (Thread.CurrentPrincipal as CustomPrincipal).Identity.CurrentUser.UserName,
                                       });
                */

                db.Execute(@"update Asset 
                                Left join Inventory on
                                Inventory.AssetNumber = Asset.AssetNumber 
                                set IsMissing=-1, Asset.Status='Missing',  ModifiedBy = @ModifiedBy, ModifiedDate=now()
                                where (Inventory.AssetNumber is null and Asset.CurrentBuilding in (" + string.Format("'{0}'", string.Join("','", _buildingNumbers)) + "))",
                                    new
                                    {
                                        @ModifiedBy = (Thread.CurrentPrincipal as CustomPrincipal).Identity.CurrentUser.UserName,
                                    });

                return 1;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

    }

    // Query to Insert the assets that are overage
    class InsertOverageAssetsPWCPS : IQuery<int>
    {
        private List<Inventory> _overageAssetList;

        public InsertOverageAssetsPWCPS(List<Inventory> _overageAssetList)
        {
            this._overageAssetList = _overageAssetList;
        }


        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Staus of the AssetNumber</returns>
        public int Execute(System.Data.IDbConnection db)
        {
            IDbTransaction transaction = null;
            try
            {
                int result = 0;
                using (transaction = db.BeginTransaction())
                {

                    foreach (Inventory _overageAsset in this._overageAssetList)
                    {
                        CommandDefinition cmd = new CommandDefinition(@"Insert into [Asset] (
                                        AssetNumber,
                                        CurrentBuilding,
                                        CurrentRoom,
                                        CatalogNumber,
                                        AssetRelocated, 
                                        InventoryDate,
                                        CreatedBy,
                                        IsMissing ,
                                        IsActive,
                                        Status
                                        )
                                   values ( 
                                        @AssetNumber,
                                        @CurrentBuilding,
                                        @CurrentRoom,
                                        @CatalogNumber,
                                        @AssetRelocated,
                                        @InventoryDate,
                                        @CreatedBy,
                                        @IsMissing ,
                                        @IsActive ,
                                        @Status
                                        ) ",
                                             new
                                             {
                                                 @AssetNumber = _overageAsset.AssetNumber,
                                                 @CurrentBuilding = _overageAsset.BuildingNumber,
                                                 @CurrentRoom = _overageAsset.RoomNumber,
                                                 @CatalogNumber = "UNKNOWN",
                                                 @AssetRelocated = 0,//_missmatchAsset.AssetLocationId,
                                                 @InventoryDate = _overageAsset.InventoryDate,
                                                 @CreatedBy = (Thread.CurrentPrincipal as CustomPrincipal).Identity.CurrentUser.UserName,
                                                 @IsMissing = 0,
                                                 @IsActive = 1,
                                                 @Status = "Active"  //_missmatchAsset.Status

                                             }, transaction);
                        db.Execute(cmd);

                        //TO DO enable this if you do not want to Add the missing location from the inventory
                        this.AddMissingLocations(db, transaction, _overageAsset);

                    }
                    transaction.Commit();
                    result = 1;
                }
                return result;
            }

            catch (Exception ex)
            {
                if (transaction != null)
                    transaction.Rollback();
                throw ex;
            }
        }


        /// <summary>
        /// Method to insert the lcation into the database which is not found in DB
        /// </summary>
        /// <param name="db"></param>
        /// <param name="transaction"></param>
        /// <param name="inventory"></param>
        private void AddMissingLocations(System.Data.IDbConnection db, IDbTransaction transaction, Inventory inventory)
        {

            var _buildingInfo = db.Query<int>(@"SELECT BuildingId from Building  where(Building.BuildingNumber =@BuildingNumber)",
                   new { @BuildingNumber = inventory.BuildingNumber }, transaction).ToArray();

            // string dd = _rlt[0].ToString();
            if (_buildingInfo.Count() == 0)
            {
                db.Execute(@"Insert into Building ( BuildingNumber ,  BuildingDescription ,BuildingTypeId,IsOpen,IsActive)
                                         values ( @BuildingNumber, @BuildingDescription,  @BuildingTypeId,@BuildingStatus, @IsActive ) ",

                            new
                            {
                                @BuildingNumber = inventory.BuildingNumber,
                                @BuildingDescription = inventory.BuildingNumber,
                                @BuildingTypeId = 1,
                                @BuildingStatus = 1,
                                @IsActive = 1

                            }, transaction);


                IDbCommand NewIdCommand = new System.Data.OleDb.OleDbCommand();
                NewIdCommand.Transaction = transaction;
                NewIdCommand.CommandText = "SELECT @@IDENTITY";
                NewIdCommand.Connection = db;
                var newBuildingId = NewIdCommand.ExecuteScalar();


                db.Execute(@"Insert into Room (RoomNumber,RoomDescription,BuildingId,IsActive)
                                         values (@RoomNumber,@RoomDescription, @BuildingId,@IsActive) ",
                           new
                           {
                               @RoomNumber = inventory.RoomNumber,
                               @RoomDescription = inventory.RoomNumber,
                               @BuildingId = newBuildingId,
                               @IsActive = 1
                           }, transaction);

            }
            else
            {
                var _roomInfo = db.Query<string>(@"SELECT RoomNumber from Room  where(RoomNumber = @RoomNumber and BuildingId =@BuildingId)",
                       new { @RoomNumber = inventory.RoomNumber, @BuildingId = _buildingInfo[0] }, transaction).ToArray();

                if (_roomInfo.Count() == 0)
                {

                    db.Execute(@"Insert into Room (RoomNumber,RoomDescription,BuildingId,IsActive)
                                         values (@RoomNumber,@RoomDescription, @BuildingId,@IsActive) ",
                             new
                             {
                                 @RoomNumber = inventory.RoomNumber,
                                 @RoomDescription = inventory.RoomNumber,
                                 @BuildingId = _buildingInfo[0],
                                 @IsActive = 1
                             }, transaction);
                }
            }

        }

    }

}
