﻿using TRAKI.App.Interfaces;
using Dapper;
using TRAKI.App.Model;

namespace TRAKI.App.DapperQueries.InventoryQueries
{
    class FindDiscrepancies : IQuery<string>
    {
        private Inventory _inventory;

        /// <summary>
        /// initializes the instance of the <see cref="FindDiscrepancies"/> class.
        /// </summary>
        public FindDiscrepancies(Inventory Inventory)
        {
            this._inventory = Inventory;
        }


        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Staus of the AssetNumber</returns>
        public string Execute(System.Data.IDbConnection db)
        {

            string query = @"SELECT IIF((count(Asset.AssetNumber) >0), 'Mismatch', 'Overage') AS Status
                                  FROM Asset
                                       where (Asset.AssetNumber =@AssetNumber and Asset.IsDeleted =0)";

            return (string)db.ExecuteScalar(query, new
            {
                @AssetNumber = this._inventory.AssetNumber,
            });
        }
    }
}



