﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;
using Dapper;
using System.Data;
using TRAKI.App.Common;

namespace TRAKI.App.DapperQueries.InventoryQueries
{
    class SaveInventoryData : IQuery<int>
    {
        private List<Inventory> _inventoryList;
        private List<string> bldgRoomList = null;


        /// <summary>
        /// initializes the instance of the <see cref="InsertAsset"/> class.
        /// </summary>
        public SaveInventoryData(List<Inventory> InventoryList)
        {
            this._inventoryList = InventoryList;
        }

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Number of rows Affected</returns>
        /// New Changes
        public int Execute(System.Data.IDbConnection db)
        {


            string query = @"SELECT trim(Building.BuildingNumber)+' '+ trim(Room.RoomNumber) FROM Building INNER JOIN Room ON Building.BuildingId = Room.BuildingId ";
            bldgRoomList = db.Query<string>(@query).ToList<string>();

            IDbTransaction transaction = null;
            try
            {
                int result = 0;
                using (transaction = db.BeginTransaction())
                {

                    foreach (var inventory in this._inventoryList)
                    {
                        
                        // int _rlt = db.Execute(@" Update [Inventory] set QA=-1 WHERE AssetNumber=@AssetNumber ",
                        //         new { @AssetNumber = inventory.AssetNumber }, transaction);

                        string dr = inventory.BuildingNumber.Trim() + " " + inventory.RoomNumber.Trim();
                        if (bldgRoomList.Contains(dr))
                        {
                            var _rlt = db.Query(@" Select AssetNumber from [Inventory] WHERE AssetNumber=@AssetNumber ",
                                    new { @AssetNumber = inventory.AssetNumber }, transaction);
                            if (_rlt.Count() == 0)
                            {
                                db.Execute(@"Insert into [Inventory] (
                                        QA,
                                        UserID,
                                        AssetNumber,
                                        BuildingNumber,
                                        RoomNumber ,
                                        InventoryDate,
                                        Description ,
                                        Status
                                        )
                                   values ( 
                                        @QA,
                                        @UserID,
                                        @AssetNumber,
                                        @BuildingNumber,
                                        @RoomNumber ,
                                        @InventoryDate,
                                        @Description, 
                                        @Status
                                        ) ",
                                                    new
                                                    {
                                                        @QA = (Convert.ToBoolean(inventory.QA)) ? 1 : 0,
                                                        @UserID = inventory.UserId,
                                                        @AssetNumber = inventory.AssetNumber,
                                                        @BuildingNumber = inventory.BuildingNumber,
                                                        @RoomNumber = inventory.RoomNumber,
                                                        @InventoryDate = inventory.InventoryDate,
                                                        @Description = inventory.Description,
                                                        @Status = inventory.Status
                                                    }, transaction);
                                //Steve
                                this._inventoryList[this._inventoryList.IndexOf(inventory)].AppendIt = 1;
                            }
                            else
                            {
                                // int tl = db.Execute(@" UPDATE [Inventory] 
                                // order of the parameters must macth the order of data fields 
                                db.Execute(@" UPDATE [Inventory] 
                                          Set UserID=@UserID,
                                            BuildingNumber=@BuildingNumber,
                                            RoomNumber=@RoomNumber,
                                            InventoryDate=@InventoryDate,
                                            Description= @Description,
                                            Status =@Status 
                                            WHERE AssetNumber=@AssetNumber and InventoryDate <= @InventoryDate ",
                                           new
                                           {
                                               @UserID = inventory.UserId,
                                               @BuildingNumber = inventory.BuildingNumber,
                                               @RoomNumber = inventory.RoomNumber,
                                               @InventoryDate = inventory.InventoryDate,
                                               @Description = inventory.Description,
                                               @Status = inventory.Status,
                                               @AssetNumber = inventory.AssetNumber
                                           }, transaction);
                                //  tl += 1;
                                //Steve
                                this._inventoryList[this._inventoryList.IndexOf(inventory)].AppendIt = 1;
                            };
                        }
                    }

                    transaction.Commit();
                    result = 1;
                }

                return result;
            }

            catch (Exception ex)
            {
                if (transaction != null)
                    transaction.Rollback();
                throw ex;
            }
        }
    }
}