﻿using System.Collections.Generic;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;
using Dapper;


namespace TRAKI.App.DapperQueries.InventoryQueries
{
    class ValidateAssetLocation : IQuery<int>
    {
        private Inventory _inventory;

        /// <summary>
        /// initializes the instance of the <see cref="GetPreviousLocation"/> class.
        /// </summary>
        public ValidateAssetLocation(Inventory Inventory)
        {
            this._inventory = Inventory;

        }


        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Number of rows Affected</returns>
        public int Execute(System.Data.IDbConnection db)
        {
            string query = "SELECT count(1) from   Asset where (AssetNumber =@AssetNumber and CurrentBuilding = @BuildingNumber and CurrentRoom = @RoomNumber)";
            var result = (int)db.ExecuteScalar(query, new
            {
                @AssetNumber = this._inventory.AssetNumber,
                @BuildingNumber = this._inventory.BuildingNumber,
                @RoomNumber = this._inventory.RoomNumber
            });

            return result;
        }
    }
}



