﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dapper;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;
using TRAKI.App.Common;
using System.Collections;


namespace TRAKI.App.DapperQueries.InventoryQueries.UploadData
{
    class GetMinAndMaxIdValuesForPager : IQuery<List<Inventory>>
    {
        private long? _idValue;
        private string _searchText;
        private bool _getLastRecords;
        private int _numberOfRecords;
        private string _operatorValue;

        /// <summary>
        /// initializes the instance of the <see cref="GetMinAndMaxIdValuesForPager"/> class.
        /// </summary>
        public GetMinAndMaxIdValuesForPager(long? IdValue, string OperatorValue, string SearchText, bool GetLastRecords, int NumberOfRecords)
        {
            this._idValue = IdValue;
            this._searchText = SearchText;
            this._getLastRecords = GetLastRecords;
            this._numberOfRecords = NumberOfRecords;
            this._operatorValue = OperatorValue;
        }

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>List of Id's of the  Assets that matches the criteria</returns>
        public List<Inventory> Execute(System.Data.IDbConnection db)
        {
            List<Inventory> result;
            string orderby = "order by UploadDataId desc";

            if (this._operatorValue.Equals(SqlOperatorValue.GreaterThan))
                orderby = "order by UploadDataId asc";
            string query = @" SELECT top " + this._numberOfRecords + @" UploadDataId 
                        from  UploadData ";

            if (!this._getLastRecords)
            {
                query = query + "where((@IdValue is null or  UploadDataId " + this._operatorValue + " @IdValue) and ((AssetNumber like '%" + this._searchText + "%') or (BuildingNumber like '%" + this._searchText + "%'))) " + orderby;
                result = (List<Inventory>)db.Query<Inventory>(query, new { @IdValue = this._idValue });
            }
            else
            {
                query = query + "where(((AssetNumber like '%" + this._searchText + "%') or (BuildingNumber like '%" + this._searchText + "%'))) " + orderby;
                result = (List<Inventory>)db.Query<Inventory>(query);

            }
            return result;
        }
    }
}
