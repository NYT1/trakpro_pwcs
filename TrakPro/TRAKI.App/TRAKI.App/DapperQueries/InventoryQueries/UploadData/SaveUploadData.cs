﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Model;
using Dapper;
using TRAKI.App.Interfaces;
using TRAKI.App.Account;
using System.Threading;
using System.Data.OleDb;
using TRAKI.App.Common;

namespace TRAKI.App.DapperQueries.InventoryQueries.UploadData
{
    class SaveUploadData : IQuery<int>
    {
        private List<Inventory> _inventoryList;

        private string AssetN = "";
        /// <summary>
        /// initializes the instance of the <see cref="InsertAsset"/> class.
        /// </summary>
        public SaveUploadData(List<Inventory> InventoryList)
        {
            this._inventoryList = InventoryList;
        }

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>Number of rows Affected</returns>
        public int Execute(System.Data.IDbConnection db)
        {

            IDbConnection dbMainConnection;
          
            List<string> bldgList = null;
            try
            {
                //
                //Steve
                //Temp create a valid buidling 
                //
                using (dbMainConnection = new OleDbConnection(Constants.ConnectionString))
                {
                    dbMainConnection = new OleDbConnection(Constants.ConnectionString);
                    dbMainConnection.Open();
                    try
                    {
                        string query = @"SELECT BuildingNumber FROM Building where IsActive <> 0 ";

                        bldgList = dbMainConnection.Query<string>(@query).ToList<string>();
                    }
                    catch (Exception ex)
                    { }
                    finally
                    {
                        dbMainConnection.Close();
                    };


                }
            }
            catch (Exception ex)
            {
                throw ex;
            }


            try
            {
                db.Execute(@"drop table buildings");
            }
            catch (Exception ex)
            { };

            try
            {
                db.Execute(@"create table buildings ( buildingnumber TEXT(10) )");
            }
            catch (Exception ex)
            { };


            foreach (var Item in bldgList)
            {
                db.Execute(@"Insert into buildings (buildingnumber) values ('" + Item.ToString() + "') ");
            };

            


            IDbTransaction transaction = null;
          
            try
            {
                
                int result = 0;
                using (transaction = db.BeginTransaction())
                {

                    foreach (var inventory in this._inventoryList)
                    {
                        //var _rlt = db.Query<int>(@" Select AssetNumber from [UploadData] WHERE AssetNumber=@AssetNumber ",
                        //       new { @AssetNumber = inventory.AssetNumber }, transaction).ToArray();

                        //09/05/2018 "Transaction has not committed until the end of insert or update, asset number is an index field so could not use generic <int> array"

                        var _rlt = db.Query(@" Select AssetNumber from [UploadData] WHERE AssetNumber=@AssetNumber ",
                                    new { @AssetNumber = inventory.AssetNumber }, transaction);


                        if (_rlt.Count() == 0)
                        {
                            AssetN = inventory.AssetNumber;
                            //if (inventory.AssetNumber == "P53162")
                            //{
                            //    AssetN = inventory.AssetNumber;
                            //}
                            result = db.Execute(@"Insert into [UploadData] (
                                        QA,
                                        UserID,
                                        AssetNumber,
                                        BuildingNumber,
                                        RoomNumber ,
                                        InventoryDate,
                                        Description,
                                        UploadedBy 
                                        )
                                   values ( 
                                        @QA,
                                        @UserID,
                                        @AssetNumber,
                                        @BuildingNumber,
                                        @RoomNumber ,
                                        @InventoryDate,
                                        @Description,
                                        @UploadedBy                                      
                                        ) ",
                                                 new
                                                 {
                                                     @QA = inventory.QA,
                                                     @UserID = inventory.UserId,
                                                     @AssetNumber = inventory.AssetNumber,
                                                     @BuildingNumber = inventory.BuildingNumber,
                                                     @RoomNumber = inventory.RoomNumber,
                                                     @InventoryDate = inventory.InventoryDate,
                                                     @Description = inventory.Description,
                                                     @UploadedBy = (Thread.CurrentPrincipal as CustomPrincipal).Identity.CurrentUser.UserName
                                                 }, transaction);
                        }
                        else 
                        {
                            
                            // int tl = db.Execute(@" UPDATE [Inventory] 
                            // order of the parameters must macth the order of data fields 
                            db.Execute(@" UPDATE [UploadData] 
                                          Set UserID=@UserID,
                                            BuildingNumber=@BuildingNumber,
                                            RoomNumber=@RoomNumber,
                                            InventoryDate=@InventoryDate,
                                            Description= @Description,
                                            Status =@Status 
                                            WHERE AssetNumber=@AssetNumber and InventoryDate < @InventoryDate ",
                                       new
                                       {
                                           @UserID = inventory.UserId,
                                           @BuildingNumber = inventory.BuildingNumber,
                                           @RoomNumber = inventory.RoomNumber,
                                           @InventoryDate = inventory.InventoryDate,
                                           @Description = inventory.Description,
                                           @Status = inventory.Status,
                                           @AssetNumber = inventory.AssetNumber
                                       }, transaction);
                            //  tl += 1;
                        };
                    }


                    transaction.Commit();

                    //Steve 
                    //
                    db.Execute(@"UPDATE [UploadData]  set QA=0");
                    db.Execute(@"UPDATE [UploadData] INNER JOIN Buildings ON UploadData.BuildingNumber = Buildings.buildingnumber SET QA = -1");
                    result = 1;
                }

                return result;
            }

            catch (Exception ex)
            {
                ErrorLogging.LogException("Error:" + ex.Message +" "+ AssetN);
                if (transaction != null)
                    transaction.Rollback();
                throw ex;
            }
        }
    }
}

