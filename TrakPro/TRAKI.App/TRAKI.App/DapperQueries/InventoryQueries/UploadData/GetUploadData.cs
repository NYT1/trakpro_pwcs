﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;
using Dapper;


namespace TRAKI.App.DapperQueries.InventoryQueries.UploadData
{
    class GetUploadData : IQuery<List<Inventory>>
    {
        private long? _minIdValue;
        private long? _maxIdValue;
        private string _operatorValue;
        private string _searchText;
        private int _numberOfRecords;

        /// <summary>
        /// initializes the instance of the <see cref="GetUploadData"/> class.
        /// </summary>
        public GetUploadData(long? MinIdValue, long? MaxIdValue, string OperatorValue, string SearchText, int NumberOfRecords)
        {
            this._minIdValue = MinIdValue;
            this._maxIdValue = MaxIdValue;
            this._operatorValue = OperatorValue;
            this._searchText = SearchText;
            this._numberOfRecords = NumberOfRecords;
        }

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>List of Assets that matches the Criteria</returns>
        public List<Inventory> Execute(System.Data.IDbConnection db)
        {
            List<Inventory> result;

            //string query = @"SELECT UserID,AssetNumber,BuildingNumber,RoomNumber,UploadDate as InventoryDate,Description, UploadedBy from UploadData order by UploadDataId desc"
            string query = @" SELECT top " + this._numberOfRecords + @" UploadDataId, QA, UserID,AssetNumber,BuildingNumber,RoomNumber, InventoryDate,Description, UploadedBy, Status from UploadData 
                                where( ( ( @MinIdValue is null or UploadDataId >= @MinIdValue)  and( @MaxIdValue is null or UploadDataId <= @MaxIdValue) ) )  order by UploadDataId desc ";
                              //  where( ( ( @MinIdValue is null or UploadDataId >= @MinIdValue)  and( @MaxIdValue is null or UploadDataId <= @MaxIdValue) ) and  ((AssetNumber like '%" + this._searchText + "%') or (BuildingNumber like '%" + this._searchText + "%')))  order by UploadDataId desc ";

            // To get the last records
            //if (_minIdValue == 0)
            //{
            //    query = query + "where(((AssetNumber like '%" + this._searchText + "%') or (BuildingNumber like '%" + this._searchText + "%'))) order by UploadDataId ";
            //    result = (List<Inventory>)db.Query<Inventory>(query);
            //}
            //else
            //{
         //   query = query + "where( ( ( @MinIdValue is null or UploadDataId >= @MinIdValue)  and( @MaxIdValue is null or UploadDataId <= @MaxIdValue) ) and  ((AssetNumber like '%" + this._searchText + "%') or (BuildingNumber like '%" + this._searchText + "%')))  order by UploadDataId desc ";
            result = (List<Inventory>)db.Query<Inventory>(query, new { @MinIdValue = this._minIdValue, @MaxIdValue = this._maxIdValue });
            // }
            return result;
        }

    }

    class GetAllUploadData : IQuery<List<Inventory>>
    {
        

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>List of Assets that matches the Criteria</returns>
        public List<Inventory> Execute(System.Data.IDbConnection db)
        {
            List<Inventory> result;
            //------------------------------Update uploadData table  Replace "~" with "". 10/02/2018-Komal
            int result1;
            var query1 = @"Update UploadData set AssetNumber = Replace(AssetNumber,'~','')";
            result1 = db.Execute(query1,new{ });

            //-----------------------------------------------------------


            string query = @" SELECT  UploadDataId, QA, UserID,AssetNumber,BuildingNumber,RoomNumber, InventoryDate,Description, UploadedBy, Status from UploadData  ";

            result = (List<Inventory>)db.Query<Inventory>(query);
            // }
            return result;
        }

    }




}


//namespace TRAKI.App.DapperQueries.InventoryQueries
//{
//    class GetUploadData : IQuery<List<Inventory>>
//    {
//        /// <summary>
//        /// initializes the instance of the <see cref="GetManufacturer"/> class.
//        /// </summary>
//        public GetUploadData()
//        {

//        }

//        /// <summary>
//        /// excecutes db query against database
//        /// </summary>
//        /// <param name="db"></param>
//        /// <returns>List of Manufacturers</returns>
//        public List<Inventory> Execute(System.Data.IDbConnection db)
//        {
//            string query = @"SELECT UserID,AssetNumber,BuildingNumber,RoomNumber,UploadDate as InventoryDate,Description, UploadedBy from UploadData order by UploadDate desc";
//            return (List<Inventory>)db.Query<Inventory>(query);
//        }
//    }
//}

