﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;
using TRAKI.App.Model;
using TRAKI.App.Common;
 

namespace TRAKI.App.DapperQueries.InventoryQueries.UploadData
{
    class DeleteAppendedUploadData : IQuery<int>
    {

        public int Execute(System.Data.IDbConnection db )
        {
            try
            {
                if (Constants.TempInventoryList.Count != 0)
                {
                       foreach (var item in Constants.TempInventoryList)
                    {
                    var query = @"Delete from  UploadData where assetnumber='" + item.ToString() + "' ";
                    int result = db.Execute(query);
                };
                
                };

                return 1;            
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}