﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;

namespace TRAKI.App.DapperQueries.InventoryQueries.UploadData
{
    class DeleteAllUploadData : IQuery<int>
    {

        public int Execute(System.Data.IDbConnection db)
        {
            try
            {
                var query = @"Delete from  UploadData";
                int result = db.Execute(query);
             
                return result;            
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}