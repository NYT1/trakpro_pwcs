﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;

namespace TRAKI.App.DapperQueries.InventoryQueries.UploadData
{
    class GetUploadDataCount : IQuery<int>
    {
        private string _searchText;


        /// <summary>
        /// initializes the instance of the <see cref="GetAssetCount"/> class.
        /// </summary>
        public GetUploadDataCount(string SearchText)
        {
            this._searchText = SearchText;
        }

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>return the count of the records thats matches the criteria</returns>
        public int Execute(System.Data.IDbConnection db)
        {

            string query = @"SELECT count(1) from UploadData where((AssetNumber like '%" + this._searchText + "%') or (BuildingNumber like '%" + this._searchText + "%'))";
            return (int)db.ExecuteScalar(query);
        }
    }
}
