﻿using TRAKI.App.Interfaces;
using Dapper;
using System;
 
namespace TRAKI.App.DapperQueries.Common
{
    class CheckEmail : IQuery<int>
    {
        private string _emailId;
        public CheckEmail(string EmailId)
        {
            this._emailId = EmailId;
        }
        public int Execute(System.Data.IDbConnection db)
        {
            try
            {
                return (int)db.ExecuteScalar("SELECT count(1) FROM [User] where [Email]=@Email",
                                  new
                                  {
                                      @Email = this._emailId
                                  });

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
