﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;
using TRAKI.App.Model;

namespace TRAKI.App.DapperQueries.Common
{
    class CheckRoom : IQuery<int>
    {
        private string _roomNumber;
        private string _bldgNumber;
        public CheckRoom(string RoomNumber,string bldgnumber)
        {
            this._roomNumber = RoomNumber;
            this._bldgNumber = bldgnumber;   //Steve
        }
        public int Execute(System.Data.IDbConnection db)
        {
            try
            {
                return (int)db.ExecuteScalar("SELECT count(1) FROM [Room] where [RoomNumber]=@RoomNumber and [BuildingId]=@BldgNumber ",
                                new
                                {
                                    @RoomNumber = this._roomNumber,
                                    @BldgNumber=this._bldgNumber //Steve
                                });

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
