﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;
using TRAKI.App.Model;

namespace TRAKI.App.DapperQueries.Common
{
    public class GetBuildingType : IQuery<List<TypeBuilding>>
    {
        public GetBuildingType()
        {
        }
        public List<TypeBuilding> Execute(System.Data.IDbConnection db)
        {
            string query = string.Empty;
            query = @"SELECT BuildingType.BuildingTypeId as TypeId, BuildingType.BuildingTypeDescription as TypeName, BuildingType.IsActive FROM BuildingType order by BuildingTypeId";

            return (List<TypeBuilding>)db.Query<TypeBuilding>(query);
        }
    }
}
