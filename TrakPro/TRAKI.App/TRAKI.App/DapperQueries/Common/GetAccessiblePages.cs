﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using TRAKI.App.Interfaces;
using System.Collections;

namespace TRAKI.App.DapperQueries.Common
{
    class GetAccessiblePages : IQuery<IEnumerable>
    {
        private int _roleId;

        /// <summary>
        /// initializes the instance of the <see cref="GetCatalog"/> class.
        /// </summary>
        public GetAccessiblePages(int RoleId)
        {
            this._roleId = RoleId;
        }


        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>List of pages to which the use is authorised</returns>
        public IEnumerable Execute(System.Data.IDbConnection db)
        {
            return db.Query(@"SELECT distinct MenuItemName, PageName FROM [PageAccess]  inner join GroupMenu on PageAccess.MenuItemId= GroupMenu.MenuItemId where ( [PageAccess].IsActive=true and ( @RoleId is null or [PageAccess].RoleId= @RoleId))", new { @RoleId = this._roleId }).ToList();
        }

    }
}
