﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;
using TRAKI.App.Model;

namespace TRAKI.App.DapperQueries.Common
{
    public class GetUserRoles : IQuery<List<UserRole>>
    {

        /// <summary>
        /// excecutes db query against database
        /// </summary>
        /// <param name="db"></param>
        /// <returns>List of User Roles</returns>
        public List<UserRole> Execute(System.Data.IDbConnection db)
        {
            string query = string.Empty;// @"SELECT  Users.Userid, USERS.FirstName,USERS.LastName, USERS.Email, USERS.UserName, USERS.Password, Users.Gender,  IIF(Users.IsActive=Yes, 'True', 'False') AS IsActive, Role.RoleId, ROLE.Name as Role FROM Users INNER JOIN ROLE ON USERS.ROLEID = ROLE.ROLEID;";
            query = @"SELECT Role.RoleId, ROLE.Name FROM ROLE where Role.IsActive=true order by Role.RoleId ";

            return (List<UserRole>)db.Query<UserRole>(query);
        }
    }
}
