﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;
using TRAKI.App.Model;

namespace TRAKI.App.DapperQueries.Common
{
    class CheckBuilding : IQuery<int>
    {
        private string _buildingNumber;
        public CheckBuilding(string BuildingNumber)
        {
            this._buildingNumber = BuildingNumber;
        }
        public int Execute(System.Data.IDbConnection db)
        {
            try
            {
                return (int)db.ExecuteScalar("SELECT count(1) FROM [Building] where [BuildingNumber]=@BuildingNumber",
                                new
                                {
                                    @BuildingNumber = this._buildingNumber
                                });

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
