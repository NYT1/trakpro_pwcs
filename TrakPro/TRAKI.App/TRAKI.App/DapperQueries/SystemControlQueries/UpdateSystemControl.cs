﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;
using TRAKI.App.Model;


namespace TRAKI.App.DapperQueries.SystemControlQueries
{
    class UpdateSystemControl : IQuery<int>
    {
        private SystemControl _SystemControl;

        public UpdateSystemControl(SystemControl SystemControl)
        {
            this._SystemControl = SystemControl;
        }

        public int Execute(System.Data.IDbConnection db)
        {
            try
            {

                int result = db.Execute(@"UPDATE SystemControl 
                                   SET 
                                      Path = @Path,
                                      [ControlName] = @ControlName ,
                                      [cValue] = @cValue,  
                                      [Password] = @Password                                 
                			          WHERE [SystemControlId] = @SystemControlId",
                                    new
                                    {
                                       
                                        @Path = this._SystemControl.Path,                                       
                                        @ControlName = this._SystemControl.ControlName,
                                        @cValue = this._SystemControl.cValue,
                                        @Password = this._SystemControl.Password,
                                        @SystemControlId = this._SystemControl.SystemControlId
                                    });

             
                return result;
            }

            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}