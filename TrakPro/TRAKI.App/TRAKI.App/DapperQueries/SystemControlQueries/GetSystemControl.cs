﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;
using TRAKI.App.Model;
using System.Collections;


namespace TRAKI.App.DapperQueries.SystemControlQueries
{
   public class GetSystemControl : IQuery<List<SystemControl>>
    {
        private int _sControlId;

        public GetSystemControl(int SControlId)
        {
            this._sControlId = SControlId;
        }


        public List<SystemControl> Execute(System.Data.IDbConnection db)
        {
            string query = string.Empty;
            //return db.Query(@"SELECT distinct MenuItemName, PageName FROM [PageAccess]  inner join GroupMenu on PageAccess.MenuItemId= GroupMenu.MenuItemId where ( [PageAccess].IsActive=true and ( @RoleId is null or [PageAccess].RoleId= @RoleId))", new { @RoleId = this._roleId }).ToList();
            query = @"SELECT [SystemControl].[SystemControlId],ControlName, [SystemControl].[Path], cValue ,[SystemControl].[Password] FROM [SystemControl] where ([SystemControlId] is null or [SystemControl].[SystemControlId]=@SystemControlId)";

                return (List<SystemControl>)db.Query<SystemControl>(query, new
            {
                @SystemControlId = this._sControlId
            });
            
        }
    }
}
