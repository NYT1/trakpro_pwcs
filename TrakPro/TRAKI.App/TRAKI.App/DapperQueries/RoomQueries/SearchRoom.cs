﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using TRAKI.App.Model;
using TRAKI.App.Interfaces;

namespace TRAKI.App.DapperQueries.RoomQueries
{
    public class SearchRoom : IQuery<List<Room>>
    {
        private string _searchText;
        private int _buildingId;
        private int? _isActive;


        public SearchRoom(string SearchText, int BuildingId, int? IsActive)
        {
            this._searchText = SearchText;
             this._buildingId = BuildingId;
             this._isActive = IsActive;
        }
        public List<Room> Execute(System.Data.IDbConnection db)
        {
            if (this._isActive > 0)
                this._isActive = null;

            string query = @"SELECT  Room.RoomNumber, Room.RoomId,Room.RoomDescription, Room.BuildingId, Room.IsActive, Building.BuildingDescription FROM Room INNER JOIN Building ON Room.BuildingId = Building.BuildingId
               where((@IsActive is null or [Room].IsActive= @IsActive ) and  
                     ([Room].RoomNumber  like '%" + this._searchText + "%' OR [Room].RoomDescription like '%" + this._searchText + "%' ) and Room.BuildingId=" + this._buildingId + ") order by Room.RoomNumber";

            return (List<Room>)db.Query<Room>(query,
                new
                {
                    @IsActive = this._isActive
                });
        }
    }
}