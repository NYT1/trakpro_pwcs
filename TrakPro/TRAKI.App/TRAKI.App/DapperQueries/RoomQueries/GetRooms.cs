﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;
using TRAKI.App.Model;

namespace TRAKI.App.DapperQueries.RoomQueries
{
    public class GetRooms : IQuery<List<Room>>
    {
        private int _roomID;

        public GetRooms(int RoomId)
        {
            this._roomID = RoomId;
        }

        public List<Room> Execute(System.Data.IDbConnection db)
        {
            string query = string.Empty;// @"SELECT  Users.Userid, USERS.FirstName,USERS.LastName, USERS.Email, USERS.UserName, USERS.Password, Users.Gender,  IIF(Users.IsActive=Yes, 'True', 'False') AS IsActive, Role.RoleId, ROLE.Name as Role FROM Users INNER JOIN ROLE ON USERS.ROLEID = ROLE.ROLEID;";
            if (this._roomID == 0)
            {
                query = @"SELECT Room.RoomNumber, Room.RoomId,Room.RoomDescription, Room.BuildingId, Room.IsActive,Building.BuildingNumber, Building.BuildingDescription FROM Room INNER JOIN Building ON Room.BuildingId = Building.BuildingId ORDER BY Room.RoomDescription order by  Room.RoomNumber;";

            }
            else
            {
                //query = @"SELECT  Room.RoomId,Room.RoomDescription, Room.BuildingId, Room.IsActive, Building.BuildingDescription FROM Room INNER JOIN Building ON Room.BuildingId = Building.BuildingId where Room.RoomId='" + this._roomID + "'";
                query = @"SELECT  Room.RoomNumber, Room.RoomId,Room.RoomDescription, Room.BuildingId, Room.IsActive,Building.BuildingNumber, Building.BuildingDescription FROM Room INNER JOIN Building ON Room.BuildingId = Building.BuildingId where Room.RoomId=" + this._roomID;
            }


            return (List<Room>)db.Query<Room>(query);
        }
    }
}
