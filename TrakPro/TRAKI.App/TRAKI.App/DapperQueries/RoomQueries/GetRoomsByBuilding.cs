﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;
using TRAKI.App.Model;

namespace TRAKI.App.DapperQueries.RoomQueries
{
    public class GetRoomsByBuilding : IQuery<List<Room>>
    {
        private int? _buildingId;
        private int? _isActive;

        public GetRoomsByBuilding(int? BuildingId, int? IsActive)
        {
            this._buildingId = BuildingId;
            this._isActive = IsActive;
        }

        public List<Room> Execute(System.Data.IDbConnection db)
        {

            if (this._isActive > 0)
                this._isActive = null;
            string query = @" SELECT Room.RoomNumber, Room.RoomId,Room.RoomDescription, Room.BuildingId,
                                Room.IsActive,Building.BuildingNumber, Building.BuildingDescription 
                            FROM Room  
                            INNER JOIN
                            Building ON Room.BuildingId = Building.BuildingId 
                            where((@BuildingId is null or  Room.BuildingId  = @BuildingId) and (@IsActive is null or Room.IsActive = @IsActive))";
            return (List<Room>)db.Query<Room>(query, new
            {
                @BuildingId = this._buildingId,
                @IsActive = this._isActive
            });
        }
    }
}
