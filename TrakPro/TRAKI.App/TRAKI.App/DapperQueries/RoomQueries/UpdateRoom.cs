﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using Dapper;
using TRAKI.App.Model;

namespace TRAKI.App.DapperQueries.RoomQueries
{
    class UpdateRoom : IQuery<int>
    {
        private Room _room;

        public UpdateRoom(Room Room)
        {
            this._room = Room;
        }

        public int Execute(System.Data.IDbConnection db)
        {
            try
            {
                int result = db.Execute(@"UPDATE [Room] 
                                   SET 
                                      RoomNumber=@RoomNumber,
                                      RoomDescription = @RoomDescription, 
                                      BuildingId  = @BuildingId ,
                                      IsActive =  @IsActive
                			          WHERE [RoomId] = @RoomId",
                                    new
                                    {
                                        @RoomNumber = this._room.RoomNumber,
                                        @RoomDescription = this._room.RoomDescription,
                                        @BuildingId = this._room.BuildingId,
                                        @IsActive = (Convert.ToBoolean(this._room.IsActive)) ? 1 : 0,
                                        @RoomId = this._room.RoomId
                                    });
                return result;
            }

            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}