﻿#pragma checksum "..\..\..\..\Content\CommonContent\PreviousLocationPopUpDialog.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "349CAF9C1E28F3B19A9C8ACC3CF203B2AA0C5C13"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using FirstFloor.ModernUI.Presentation;
using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Controls;
using FirstFloor.ModernUI.Windows.Converters;
using FirstFloor.ModernUI.Windows.Navigation;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using TRAKI.App.Common;
using TRAKI.App.Common.Converters;


namespace TRAKI.App.Content.CommonContent {
    
    
    /// <summary>
    /// PreviousLocationPopUpDialog
    /// </summary>
    public partial class PreviousLocationPopUpDialog : FirstFloor.ModernUI.Windows.Controls.ModernDialog, System.Windows.Markup.IComponentConnector {
        
        
        #line 23 "..\..\..\..\Content\CommonContent\PreviousLocationPopUpDialog.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid PreviousLocationDataGrid;
        
        #line default
        #line hidden
        
        
        #line 35 "..\..\..\..\Content\CommonContent\PreviousLocationPopUpDialog.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock EmptyMessageBlock;
        
        #line default
        #line hidden
        
        
        #line 70 "..\..\..\..\Content\CommonContent\PreviousLocationPopUpDialog.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnFirst;
        
        #line default
        #line hidden
        
        
        #line 76 "..\..\..\..\Content\CommonContent\PreviousLocationPopUpDialog.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnPrev;
        
        #line default
        #line hidden
        
        
        #line 82 "..\..\..\..\Content\CommonContent\PreviousLocationPopUpDialog.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Documents.Run TextBlockPages;
        
        #line default
        #line hidden
        
        
        #line 83 "..\..\..\..\Content\CommonContent\PreviousLocationPopUpDialog.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock TextBlockRecordCount;
        
        #line default
        #line hidden
        
        
        #line 87 "..\..\..\..\Content\CommonContent\PreviousLocationPopUpDialog.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnNext;
        
        #line default
        #line hidden
        
        
        #line 92 "..\..\..\..\Content\CommonContent\PreviousLocationPopUpDialog.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnLast;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Track1.App;component/content/commoncontent/previouslocationpopupdialog.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\Content\CommonContent\PreviousLocationPopUpDialog.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.PreviousLocationDataGrid = ((System.Windows.Controls.DataGrid)(target));
            return;
            case 2:
            this.EmptyMessageBlock = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 3:
            this.btnFirst = ((System.Windows.Controls.Button)(target));
            
            #line 70 "..\..\..\..\Content\CommonContent\PreviousLocationPopUpDialog.xaml"
            this.btnFirst.Click += new System.Windows.RoutedEventHandler(this.btnFirst_Click);
            
            #line default
            #line hidden
            return;
            case 4:
            this.btnPrev = ((System.Windows.Controls.Button)(target));
            
            #line 76 "..\..\..\..\Content\CommonContent\PreviousLocationPopUpDialog.xaml"
            this.btnPrev.Click += new System.Windows.RoutedEventHandler(this.btnPrev_Click);
            
            #line default
            #line hidden
            return;
            case 5:
            this.TextBlockPages = ((System.Windows.Documents.Run)(target));
            return;
            case 6:
            this.TextBlockRecordCount = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 7:
            this.btnNext = ((System.Windows.Controls.Button)(target));
            
            #line 87 "..\..\..\..\Content\CommonContent\PreviousLocationPopUpDialog.xaml"
            this.btnNext.Click += new System.Windows.RoutedEventHandler(this.btnNext_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.btnLast = ((System.Windows.Controls.Button)(target));
            
            #line 92 "..\..\..\..\Content\CommonContent\PreviousLocationPopUpDialog.xaml"
            this.btnLast.Click += new System.Windows.RoutedEventHandler(this.btnLast_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

