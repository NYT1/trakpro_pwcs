﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TRAKI.App.Interfaces
{
    public interface IDbContext  : IDisposable
    {
        IDbConnection Connection { get; }

        T Execute<T>(IQuery<T> query);
        void Execute(IQuery command);
    }

    

}
