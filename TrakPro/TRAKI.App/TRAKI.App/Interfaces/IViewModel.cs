﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TRAKI.App.Interfaces
{
    public interface IViewModel
    {
          int LoggedInUserRoleId { get; }
          bool EmptyMessageVisibility { get; set; }
          string LoggedInUserName { get; }

          
    }
}