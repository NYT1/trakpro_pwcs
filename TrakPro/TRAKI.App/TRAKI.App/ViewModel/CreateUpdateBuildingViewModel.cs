﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Collections.ObjectModel;
using TRAKI.App.Model;
using TRAKI.App.Interfaces;
using TRAKI.App.Common;
using TRAKI.App.DapperQueries.Common;
using TRAKI.App.DapperQueries.BuildingQueries;

namespace TRAKI.App.ViewModel
{
    public class CreateUpdateBuildingViewModel : INotifyPropertyChanged
    {

        public ObservableCollection<Building> BuildingList { get; private set; }

        protected IDbContext dbContext;//cu

        public CreateUpdateBuildingViewModel()
        {
        }

        public ObservableCollection<Building> GetBuilding(int BuildingID)
        {
            try
            {
                if (BuildingID == 0)
                {
                    BuildingList = new ObservableCollection<Building>();
                    BuildingList.Add(new Building());
                }
                else
                {
                    GetConnection getConnection = new GetConnection();
                    dbContext = getConnection.InitializeConnection();
                    var buildingQuery = new GetBuilding(BuildingID,null);
                    BuildingList = new ObservableCollection<Building>(dbContext.Execute(buildingQuery));
                }
                return BuildingList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public int CloseBuilding(int BuildingId)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                var buildingQuery = new CloseBuilding(BuildingId);
               return dbContext.Execute(buildingQuery);
                

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public int InsertNewBuilding(Building building)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                int result = 0;
                dynamic buildingQuery = null;
                if (building.BuildingId <=0)
                {
                    var resultQuery = new CheckBuilding(building.BuildingNumber.ToString());
                    result = dbContext.Execute(resultQuery);
                    if (result > 0)
                    {
                        return -1; //buildingid already exists
                    }
                  
                    if (result >= 0)
                    {
                        buildingQuery = new InsertBuilding(building);
                        result = dbContext.Execute(buildingQuery);
                        result = 1;  //success insert operation
                    }
                }
                else
                {
                    buildingQuery = new UpdateBuilding(building);
                    result = dbContext.Execute(buildingQuery);
                    if (result > 0)
                    {
                        result = 2;  // success update operation
                    }
                }
                return result;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Private Helpers

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion
    }
}



