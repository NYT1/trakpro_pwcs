﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TRAKI.App.Account;
using TRAKI.App.Common;
using TRAKI.App.DapperQueries.CatalogQueries;
using TRAKI.App.DapperQueries.Common;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;

namespace TRAKI.App.ViewModel
{
    class CatalogViewModel : INotifyPropertyChanged, IViewModel
    {
        private ObservableCollection<Catalog> CatalogList;
        protected IDbContext dbContext;
        private bool _emptyMessageVisibility = false;
        private long _recordCount = 0;

        public CatalogViewModel()
        {
        }

        #region Private

        /// <summary>
        /// Private method to return the list of Catalogs
        /// </summary>
        /// <param name="CatalogId"></param>
        /// <param name="IsActive"></param>
        /// <returns>List of Catalogs</returns>
        /// 
        private ObservableCollection<Catalog> GetCatalogList(int? CatalogId, int? IsActive)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                CatalogList = new ObservableCollection<Catalog>(dbContext.Execute(new GetCatalog(CatalogId, IsActive)));
                return CatalogList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Get Computer catalog List

        private ObservableCollection<Catalog> GetCompCatalogList(int? CatalogId, int? IsActive, int? IsComputer)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                CatalogList = new ObservableCollection<Catalog>(dbContext.Execute(new GetCompCatalog(CatalogId, IsActive, IsComputer)));
                return CatalogList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private ObservableCollection<Catalog> GetCompCatalogcheckboxList(int? CatalogId, int? IsActive, int? IsComputer)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                CatalogList = new ObservableCollection<Catalog>(dbContext.Execute(new GetCompCatalogListBox(CatalogId, IsActive, IsComputer)));
                return CatalogList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Internal

        /// <summary>
        /// Method to return the list of Catalogs
        /// </summary>
        /// <param name="CatalogId"></param>
        /// <param name="IsActive"></param>
        /// <returns>List of Catalogs</returns>
        /// 
        internal ObservableCollection<Catalog> GetCatalog(int? CatalogId, int? IsActive)
        {
            try
            {
                if (CatalogId == -1)
                {
                    CatalogList = new ObservableCollection<Catalog>();
                    CatalogList.Add(new Catalog());
                }
                else
                {
                    CatalogList = this.GetCatalogList(CatalogId, IsActive);
                    this.RecordCount = CatalogList.Count;
                    if (CatalogList == null || CatalogList.Count <= 0)
                    {
                        this.EmptyMessageVisibility = true;
                    }
                }
                return CatalogList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        ///// Get Computer Catalog List
        internal ObservableCollection<Catalog> GetCompCatalogCheckboxList(int? CatalogId, int? IsActive, int? IsComputer)
        {
            try
            {
                if (CatalogId == -1)
                {
                    CatalogList = new ObservableCollection<Catalog>();
                    CatalogList.Add(new Catalog());
                }
                else
                {
                    CatalogList = this.GetCompCatalogcheckboxList(CatalogId, IsActive, IsComputer);
                    this.RecordCount = CatalogList.Count;
                    if (CatalogList == null || CatalogList.Count <= 0)
                    {
                        this.EmptyMessageVisibility = true;
                    }
                }
                return CatalogList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        ///// Get Computer Catalog List
        internal ObservableCollection<Catalog> GetCompCatalog(int? CatalogId, int? IsActive, int? IsComputer)
        {
            try
            {
                if (CatalogId == -1)
                {
                    CatalogList = new ObservableCollection<Catalog>();
                    CatalogList.Add(new Catalog());
                }
                else
                {
                    CatalogList = this.GetCompCatalogList(CatalogId, IsActive, IsComputer);
                    this.RecordCount = CatalogList.Count;
                    if (CatalogList == null || CatalogList.Count <= 0)
                    {
                        this.EmptyMessageVisibility = true;
                    }
                }
                return CatalogList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        /// <summary>
        /// Method to return the list of Catalogs that matches the search criteria
        /// </summary>
        /// <param name="SearchText"></param>
        /// <param name="IsActive"></param>
        /// <returns>List of Catalogs</returns>
        /// 
        internal ObservableCollection<Catalog> SearchCatalog(string SearchText, int? IsActive)
        {
            try
            {

                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                var catalogQuery = new SearchCatalog(SearchText, IsActive);
                CatalogList = new ObservableCollection<Catalog>(dbContext.Execute(catalogQuery));
                this.RecordCount = CatalogList.Count;
                if (CatalogList == null || CatalogList.Count <= 0)
                {
                    this.EmptyMessageVisibility = true;
                }

                return CatalogList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Method to delete the  Acquisition
        /// </summary>
        /// <param name="AcquisitionId"></param>
        /// <returns>returns the operation result</returns>
        /// 
        internal int DeleteExistingCatalog(int CatalogId)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                var catalogQuery = new DeleteCatalog(CatalogId);
                return dbContext.Execute(catalogQuery);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        /// <summary>
        /// Method to Insert a new Catalog or to update the existing one.
        /// </summary>
        /// <param name="Catalog Object"></param>
        /// <returns>returns the operation result</returns>
        /// 
        internal int InsertUpdateCatalog(Catalog catalog)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                int result = 0;
                dynamic catalogQuery = null;
                if (catalog.CatalogId == 0)
                {
                    catalogQuery = new InsertCatalog(catalog);
                    result = dbContext.Execute(catalogQuery);
                    if (result > 0)
                    {
                        result = 1;  //success insert operation
                    }
                }
                else
                {
                    catalogQuery = new UpdateCatalog(catalog);
                    result = dbContext.Execute(catalogQuery);
                    if (result > 0)
                    {
                        result = 2;  // success update operation
                    }
                }
                return result;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Private Helpers

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));

            }
        }

        #endregion

        #region Properties


        public long RecordCount
        {
            get { return _recordCount; }
            set
            {
                _recordCount = value;
                NotifyPropertyChanged("RecordCount");
            }
        }


        #region IViewModel Properties

        public bool EmptyMessageVisibility
        {
            get { return _emptyMessageVisibility; }
            set
            {
                _emptyMessageVisibility = value;
                NotifyPropertyChanged("EmptyMessageVisibility");
            }
        }

        public int LoggedInUserRoleId
        {
            get
            {
                return (Thread.CurrentPrincipal as CustomPrincipal).Identity.CurrentUser.RoleId;
            }
        }

        public string LoggedInUserName
        {
            get { throw new NotImplementedException(); }
        }

        #endregion
        #endregion

    }
}

