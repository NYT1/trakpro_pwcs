﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TRAKI.App.Account;
using TRAKI.App.Common;
using TRAKI.App.DapperQueries.AssetQueries;
using TRAKI.App.DapperQueries.ArchiveDispositionQueries;
using TRAKI.App.DapperQueries.Common;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;

namespace TRAKI.App.ViewModel
{
    public class ArchiveDispositionViewModel : INotifyPropertyChanged, IViewModel
    {

        private ObservableCollection<Asset> AssetList;//
        private long _recordCount = 0;
        protected IDbContext dbContext;
        private bool _emptyMessageVisibility = false;


        #region Internal


        /// <summary>
        /// Archive the Asset that are disposed
        /// </summary>
        /// <returns>returns the result of the Operation</returns>
        /// 
        internal int ArchiveDisposition(string dispositionCode, string buildingnum)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                var userQuery = new ArchiveDisposition(dispositionCode, buildingnum);
                return dbContext.Execute(userQuery);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Method to return the list of Assets
        /// </summary>
        /// <param name="MinIdValue"></param>
        /// <param name="MaxIdValue"></param>
        /// <param name="OperatorValue"></param>
        /// <param name="Status"></param>
        /// <param name="SearchText"></param>
        /// <param name="NumberOfRecords"></param>
        /// <param name="IsArchived"></param>
        /// <returns>List of Disposed Assets</returns>
        /// 
        internal ObservableCollection<Asset> GetDisposedAssetList(long? MinIdValue, long? MaxIdValue, string OperatorValue, string Status, string SearchText, int NumberOfRecords, int IsArchived,string DisType)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                var assetQuery = new GetDisposedAssetList(MinIdValue, MaxIdValue, OperatorValue, Status, SearchText, NumberOfRecords, IsArchived, DisType);
                AssetList = new ObservableCollection<Asset>(dbContext.Execute(assetQuery));
                if (AssetList == null || AssetList.Count <= 0)
                {
                    this.EmptyMessageVisibility = true;
                }
                return AssetList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Method to get the Minimum and maximum of the Id's based on MinIdValue of Disposed Assets
        /// </summary>
        /// <param name="MinIdValue"></param>
        /// <param name="GetLastRecords"></param>
        /// <param name="Status"></param>
        /// <param name="SearchText"></param>
        /// <param name="NumberOfRecords"></param>
        /// <param name="IsArchived"></param>
        /// <returns>List of Disposed Assets</returns>
        /// 
        internal List<Asset> GetMinAndMaxIdValuesForDisposedAssetPager(long? MinIdValue, string Status, string SearchText, bool GetLastRecords, int NumberOfRecords, int IsArchived, string DisType)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                var assetQuery = new GetMinAndMaxIdValuesForDisposedAssetPager(MinIdValue, Status, SearchText, GetLastRecords, NumberOfRecords, IsArchived,DisType);
                return dbContext.Execute(assetQuery);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Method to Fetch the Disposed Asset Record count based on search criteria and Status
        /// </summary>
        /// <param name="SearchText"></param>
        /// <param name="Status"></param>
        /// <returns>returns the operation result</returns>
        /// 
        internal long GetDisposedAssetRecordCount(string SearchText, string Status, int IsArchived, string DisCode)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                var countQuery = new GetDisposedAssetCount(SearchText, Status, IsArchived,DisCode);
                this.RecordCount = dbContext.Execute(countQuery);
                return this.RecordCount;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Private Helpers

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));

            }
        }

        #endregion

        #region Properties

        public long RecordCount
        {
            get { return _recordCount; }
            set
            {
                _recordCount = value;
                NotifyPropertyChanged("RecordCount");
            }
        }


        #region IViewModel Properties

        public bool EmptyMessageVisibility
        {
            get { return _emptyMessageVisibility; }
            set
            {
                _emptyMessageVisibility = value;
                NotifyPropertyChanged("EmptyMessageVisibility");
            }
        }

        public int LoggedInUserRoleId
        {
            get
            {
                return (Thread.CurrentPrincipal as CustomPrincipal).Identity.CurrentUser.RoleId;
            }
        }

        public string LoggedInUserName
        {
            get { throw new NotImplementedException(); }
        }

        #endregion

        #endregion

    }
}



