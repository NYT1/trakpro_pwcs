﻿using System;
using System.ComponentModel;
using System.Threading;
using System.Windows.Controls;
using System.Security;
using TRAKI.App.Account;
using TRAKI.App.Common;
using TRAKI.App.Model;
using TRAKI.App.Interfaces;
using System.Collections.ObjectModel;
using System.Windows;
using TRAKI.App.Content.AccountContent;
using TRAKI.App.Model.Account;
using TRAKI.App.ViewModel.Common;

namespace TRAKI.App.ViewModel
{
    public class LoginViewModel : IViewModel, INotifyPropertyChanged
    {
        private readonly IAuthenticationService _authenticationService = new AuthenticationService();
        private string _username;
        private string _status;

        public LoginViewModel()
        {

        }

        #region Ineternal Methods

        /// <summary>
        /// Method to Login
        /// </summary>
        /// <param name="User"></param>
        internal void Login(UserLogin User)
        {
            try
            {
                User user = new User();
                user.UserName = User.UserName;
                user.Password = User.Password;
                SettingsViewModel settingViewModel = new SettingsViewModel();
                //  komal changed 11\15\2018..

                //Constants.ConnectionString = settingViewModel.GetConnectionString(@"AppData/AppInfo.txt");
                string fileLocation = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + @"\NYT Inc\Asset TRAK Pro\AppInfo.txt";
                Constants.ConnectionString = settingViewModel.GetConnectionString(fileLocation);
                ///Komal
                if (user.UserName.Equals("nyt") && user.Password.Equals("nyttyn"))
                {
                    user.FirstName="NYT Admin";
                    user.Role="Administrator";
                    user.RoleId=1;
                }
                else
                {
                    user = _authenticationService.AuthenticateUser(user);
                }

                CustomPrincipal customPrincipal = Thread.CurrentPrincipal as CustomPrincipal;
                if (customPrincipal == null)
                    throw new ArgumentException("The application's default thread principal must be set to a CustomPrincipal object on startup.");

                customPrincipal.Identity = new CustomIdentity(user);
                Username = string.Empty; //reset
                User.Password = string.Empty; //reset
                Status = string.Empty;
               // this.ShowView(false);
            }
            catch (UnauthorizedAccessException)
            {
                //Steve
                //0-- origanl  1--else
                if (Constants.InnerErrorType == 0)
                    Status = "Login failed! Please provide your valid credentials.";
                else
                    Status = "This user account is inactive.  Please contact your administrator to re-activate your account.";
            }
            catch (Exception ex)
            {
                Status = ex.Message;
            }
        }

        /// <summary>
        /// Method to Logout
        /// </summary>
        internal void Logout()
        {
            CustomPrincipal customPrincipal = Thread.CurrentPrincipal as CustomPrincipal;
            if (customPrincipal != null)
            {
                customPrincipal.Identity = new AnonymousIdentity();
                Status = string.Empty;
                Constants.AppSettings = null;
            }
        }

        /// <summary>
        /// Method to show the view
        /// </summary>
        /// <param name="IsLogout"></param>
        internal void ShowView(bool IsLogout)
        {
            try
            {
                Status = string.Empty;
                IView view;
                if (IsLogout)
                    view = new UserLoginWindow();
                else
                {
                    view = new MainWindow();
                }

                view.Show();
                var count = Application.Current.Windows.Count;
                if (IsLogout)
                    Application.Current.Windows[0].Close();

            }
            catch (SecurityException)
            {
                Status = "You are not authorized!";
            }
        }

        #endregion

        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        #region Properties

        public string Username
        {
            get { return _username; }
            set { _username = value; NotifyPropertyChanged("Username"); }
        }
        public string Status
        {
            get { return _status; }
            set { _status = value; NotifyPropertyChanged("Status"); }
        }

        #region IViewModel Properties

        public int LoggedInUserRoleId
        {
            get { throw new NotImplementedException(); }
        }

        public bool EmptyMessageVisibility
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string LoggedInUserName
        {
            get { throw new NotImplementedException(); }
        }

        #endregion

        #endregion


    }
}
