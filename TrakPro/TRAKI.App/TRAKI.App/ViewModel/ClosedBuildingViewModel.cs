﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Collections.ObjectModel;
using TRAKI.App.Model;
using TRAKI.App.Interfaces;
using TRAKI.App.Common;
using TRAKI.App.DapperQueries.Common;
using TRAKI.App.DapperQueries.BuildingQueries;

namespace TRAKI.App.ViewModel
{
    public class ClosedBuildingViewModel : INotifyPropertyChanged
    {

        public List<Building> BuildingList { get; private set; }

        protected IDbContext dbContext;//cu

        /// <summary>
        ///     Method to get the open or closed buildings
        /// </summary>
        /// <param name="BuildingStatus">Open or Closed</param>
        /// <returns>List of the buildings( open or Closed)</returns>

        public List<Building> GetBuildingbyBuildingStatus(bool BuildingStatus)
        {
            try
            {

                // if (BuildingStatus == true)
                //  {
                //  BuildingList = new ObservableCollection<Building>();
                //   BuildingList.Add(new Building());
                // }
                // else
                // {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                var buildingQuery = new GetBuildingbyBuildingStatus(BuildingStatus);
                BuildingList = new List<Building>(dbContext.Execute(buildingQuery));
                // }
                return BuildingList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        ///     Method to update the building 
        /// </summary>
        /// <param name="OpenIdCollector">Id's of the buildings to be opened</param>
        /// <param name="ClosedIdCollector">Id's of the buildings to be closed</param>
        /// <returns></returns>
        public int UpdateBuildingbyBuildingStatus(System.Collections.ArrayList OpenIdCollector, System.Collections.ArrayList ClosedIdCollector)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                dynamic  buildingQuery = new UpdateBuildingbyBuildingStatus(OpenIdCollector, ClosedIdCollector);
                return dbContext.Execute(buildingQuery);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Private Helpers

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion
    }
}



