﻿using FirstFloor.ModernUI.Presentation;
using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using TRAKI.App.Account;
using TRAKI.App.Common;
using TRAKI.App.Interfaces;
using TRAKI.App.ViewModel.Common;

namespace TRAKI.App.ViewModel
{
    class MainWindowViewModel : NotifyPropertyChanged, IViewModel
    {
        SettingsViewModel settingViewModel;

        #region private

        private void GetSettings()
        {

            settingViewModel = new SettingsViewModel();
            Constants.AppSettings = settingViewModel.GetSettings();
        }

        #endregion

        #region Internal

        /// <summary>
        /// Method to return the Links that are accessible i.e. to which the user is authorized of the specified role
        /// </summary>
        /// <param name="linkGroupCollection"></param>
        /// <param name="RoleId"></param>
        /// <returns> The Groups of the accessible Links</returns>
        internal LinkGroupCollection GetAccessibleLinks(LinkGroupCollection linkGroupCollection, int RoleId)
        {
            settingViewModel = new SettingsViewModel();
            this.GetSettings();
            var accessiblePageList = settingViewModel.GetAccessiblePages(RoleId);

            List<string> GroupItems = new List<string>();
            List<string> linkItems = new List<string>();

            foreach (var accessiblePage in accessiblePageList)
            {
                var keys = ((IDictionary<string, object>)accessiblePage).Keys.ToList();
                var data = (IDictionary<string, object>)accessiblePage;


                if (!GroupItems.Contains(data[keys[0].ToString()].ToString()))
                    GroupItems.Add(data[keys[0].ToString()].ToString());

                if (!linkItems.Contains(data[keys[1].ToString()].ToString()))
                    linkItems.Add(data[keys[1].ToString()].ToString());
            }

            LicenseOperations.Operations operation = new LicenseOperations.Operations();
            Constants.AccessibleModules = operation.GetAccessibleModulesOfLicense(LicenseOperations.Utilities.IoHelper.BasePath + @"\License.licx");

            foreach (var item in Constants.AccessibleModules.ToArray())
            {
                if (!GroupItems.Contains(item))
                    Constants.AccessibleModules.Remove(item);
            }

            GroupItems = Constants.AccessibleModules;

            var linkGroupCol = linkGroupCollection as LinkGroupCollection;

            foreach (var linkGroupItem in linkGroupCol.ToList())
            {
                var linkGroup = linkGroupItem as LinkGroup;
                var linkGroupName = linkGroup.DisplayName;
                var links = linkGroup.Links;

                if (!GroupItems.Contains(linkGroup.DisplayName))
                {
                    linkGroupCollection.Remove(linkGroup);
                    continue;
                }

                foreach (var link in links.ToList())
                {
                    if (!linkItems.Contains(link.DisplayName))
                        links.Remove(link);
                }
            }

            return linkGroupCollection;

        }

        #endregion

        #region Properties

        public int LoggedInUserRoleId
        {
            get { return (Thread.CurrentPrincipal as CustomPrincipal).Identity.CurrentUser.RoleId; }
        }

        public string LoggedInUserName
        {
            get { return (Thread.CurrentPrincipal as CustomPrincipal).Identity.CurrentUser.FirstName + " " + (Thread.CurrentPrincipal as CustomPrincipal).Identity.CurrentUser.LastName; }
        }

        public bool EmptyMessageVisibility
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion


    }
}