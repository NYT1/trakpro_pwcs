﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Common;
using TRAKI.App.DapperQueries.Common;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;

namespace TRAKI.App.ViewModel.Common
{
    class BuildingTypeViewModel
    {
        public ObservableCollection<TypeBuilding> BuildingTypeList { get; private set; }
    

        protected IDbContext dbContext;

        public BuildingTypeViewModel()
        {
            this.GetBuildingTypeList();
        }


        public void GetBuildingTypeList()
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                var query = dbContext.Execute(new GetBuildingType());
                BuildingTypeList = new ObservableCollection<TypeBuilding>(dbContext.Execute(new GetBuildingType()));
               // TypeBuilding buildingType = new TypeBuilding();
               // buildingType.TypeId = 0;
              //  buildingType.TypeName = "-Select Type-";
               // BuildingTypeList.Insert(0, buildingType);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }
}

