﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Common;
using TRAKI.App.DapperQueries.Common;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;

namespace TRAKI.App.ViewModel.Common
{
    class RoleViewModel
    {
        public ObservableCollection<UserRole> RoleList { get; private set; }

        protected IDbContext dbContext;

        public RoleViewModel()
        {
            this.GetRoleList();
        }


        /// <summary>
        /// Method to get Role List
        /// </summary>
        /// <returns>List of Roles</returns>
        /// 
        public void GetRoleList()
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                RoleList = new ObservableCollection<UserRole>(dbContext.Execute(new GetUserRoles())); 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }
}

