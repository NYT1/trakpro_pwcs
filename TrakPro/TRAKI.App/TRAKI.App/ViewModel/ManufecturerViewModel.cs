﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TRAKI.App.Account;
using TRAKI.App.Common;
using TRAKI.App.DapperQueries.Common;
using TRAKI.App.DapperQueries.ManufacturerQueries;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;

namespace TRAKI.App.ViewModel
{
    class ManufacturerViewModel : INotifyPropertyChanged, IViewModel
    {
        private ObservableCollection<Manufacturer> ManufacturerList;
        protected IDbContext dbContext;
        private bool _emptyMessageVisibility = false;
        private long _recordCount = 0;

        public ManufacturerViewModel()
        {
        }

        #region Private

        /// <summary>
        /// Private method to return the list of Manufacturers
        /// </summary>
        /// <param name="ManufacturerId"></param>
        /// <param name="IsActive"></param>
        /// <returns>List of Manufacturers</returns>
        /// 
        private ObservableCollection<Manufacturer> GetManufacturerList(int? ManufacturerId, int? IsActive)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                ManufacturerList = new ObservableCollection<Manufacturer>(dbContext.Execute(new GetManufacturer(ManufacturerId, IsActive)));
                return ManufacturerList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #endregion

        #region Internal

        /// <summary>
        /// Method to return the list of Manufacturer
        /// </summary>
        /// <param name="ManufacturerId"></param>
        /// <param name="IsActive"></param>
        /// <returns>List of Manufacturers</returns>
        /// 
        internal ObservableCollection<Manufacturer> GetManufacturer(int? ManufacturerId, int? IsActive)
        {
            try
            {
                if (ManufacturerId == -1)
                {
                    ManufacturerList = new ObservableCollection<Manufacturer>();
                    ManufacturerList.Add(new Manufacturer());
                }
                else
                {
                    ManufacturerList = this.GetManufacturerList(ManufacturerId, IsActive);
                    this.RecordCount = ManufacturerList.Count;
                    if (ManufacturerList == null || ManufacturerList.Count <= 0)
                    {
                        this.EmptyMessageVisibility = true;
                    }
                }
                return ManufacturerList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Method to return the list of Manufacturers that matches the search criteria
        /// </summary>
        /// <param name="SearchText"></param>
        /// <param name="IsActive"></param>
        /// <returns>List of Manufacturers</returns>
        /// 
        internal ObservableCollection<Manufacturer> SearchManufacturer(string SearchText, int? IsActive)
        {
            try
            {

                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                var mfgQuery = new SearchManufacturer(SearchText, IsActive);
                ManufacturerList = new ObservableCollection<Manufacturer>(dbContext.Execute(mfgQuery));
                this.RecordCount = ManufacturerList.Count;
                if (ManufacturerList == null || ManufacturerList.Count <= 0)
                {
                    this.EmptyMessageVisibility = true;
                }
                return ManufacturerList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Method to delete the  Manufacturer
        /// </summary>
        /// <param name="ManufacturerId"></param>
        /// <returns>returns the operation result</returns>
        /// 
        internal int DeleteExistingManufacturer(int ManufacturerId)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                var mfgQuery = new DeleteManufacturer(ManufacturerId);
                return dbContext.Execute(mfgQuery);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Method to Insert a new Manufacturer or to update the existing one.
        /// </summary>
        /// <param name="Manufacturer Object"></param>
        /// <returns>returns the operation result</returns>
        /// 
        internal int InsertUpdateManufacturer(Manufacturer Manufacturer)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                int result = 0;
                dynamic mfgQuery = null;
                if (Manufacturer.ManufacturerId == 0)
                {
                    mfgQuery = new InsertManufacturer(Manufacturer);
                    result = dbContext.Execute(mfgQuery);
                    if (result > 0)
                    {
                        result = 1;  //success insert operation
                    }
                }
                else
                {
                    mfgQuery = new UpdateManufacturer(Manufacturer);
                    result = dbContext.Execute(mfgQuery);
                    if (result > 0)
                    {
                        result = 2;  // success update operation
                    }
                }
                return result;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Private Helpers

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));

            }
        }

        #endregion

        #region Properties
         
        public long RecordCount
        {
            get { return _recordCount; }
            set
            {
                _recordCount = value;
                NotifyPropertyChanged("RecordCount");
            }
        }

        #region IViewModel Properties

        public bool EmptyMessageVisibility
        {
            get { return _emptyMessageVisibility; }
            set
            {
                _emptyMessageVisibility = value;
                NotifyPropertyChanged("EmptyMessageVisibility");
            }
        }

        public int LoggedInUserRoleId
        {
            get
            {
                return (Thread.CurrentPrincipal as CustomPrincipal).Identity.CurrentUser.RoleId;
            }
        }

        public string LoggedInUserName
        {
            get { throw new NotImplementedException(); }
        }

        #endregion
        #endregion
    }
}

