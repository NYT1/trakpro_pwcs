﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TRAKI.App.Account;
using TRAKI.App.Common;
using TRAKI.App.DapperQueries.AssetQueries;
using TRAKI.App.DapperQueries.ArchiveDispositionQueries;
using TRAKI.App.DapperQueries.Common;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;

namespace TRAKI.App.ViewModel
{
    public class AssetViewModel : INotifyPropertyChanged, IViewModel
    {

        //public ObservableCollection<Asset> AssetList { get; private set; }
        private ObservableCollection<Asset> AssetList;//
        private long _recordCount = 0;

        protected IDbContext dbContext;
        private bool _emptyMessageVisibility = false;

        public AssetViewModel()
        {
            //  AssetList = this.GetAsset();
        }

        #region Private
        /// <summary>
        /// Private method to return the list of Disposed Assets
        /// </summary>
        /// <param name="MinIdValue"></param>
        /// <param name="MaxIdValue"></param>
        /// <param name="OperatorValue"></param>
        /// <param name="Status"></param>
        /// <param name="SearchText"></param>
        /// <param name="NumberOfRecords"></param>
        /// <param name="IsArchived"></param>
        /// <returns>List of Assets</returns>
        /// 
        private ObservableCollection<Asset> GetAssetList(long? MinIdValue, long? MaxIdValue, string OperatorValue, string Status, string SearchText, int NumberOfRecords, int IsArchived)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                var assetQuery = new GetAssetList(MinIdValue, MaxIdValue, OperatorValue, Status, SearchText, NumberOfRecords, IsArchived);
                return new ObservableCollection<Asset>(dbContext.Execute(assetQuery));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Internal


        #region Asset Disposition

        /// <summary>
        /// Archive the Asset that are disposed
        /// </summary>
        /// <returns>returns the result of the Operation</returns>
        /// 
        internal int ArchiveDisposition()
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                var userQuery = new ArchiveDisposition(null,null);
                return dbContext.Execute(userQuery);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Method to return the list of Assets
        /// </summary>
        /// <param name="MinIdValue"></param>
        /// <param name="MaxIdValue"></param>
        /// <param name="OperatorValue"></param>
        /// <param name="Status"></param>
        /// <param name="SearchText"></param>
        /// <param name="NumberOfRecords"></param>
        /// <param name="IsArchived"></param>
        /// <returns>List of Disposed Assets</returns>
        /// 
        internal ObservableCollection<Asset> GetDisposedAssetList(long? MinIdValue, long? MaxIdValue, string OperatorValue, string Status, string SearchText, int NumberOfRecords, int IsArchived, string DisType)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                var assetQuery = new GetDisposedAssetList(MinIdValue, MaxIdValue, OperatorValue, Status, SearchText, NumberOfRecords, IsArchived, DisType);
                AssetList = new ObservableCollection<Asset>(dbContext.Execute(assetQuery));
                if (AssetList == null || AssetList.Count <= 0)
                {
                    this.EmptyMessageVisibility = true;
                }
                return AssetList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Method to get the Minimum and maximum of the Id's based on MinIdValue of Disposed Assets
        /// </summary>
        /// <param name="MinIdValue"></param>
        /// <param name="GetLastRecords"></param>
        /// <param name="Status"></param>
        /// <param name="SearchText"></param>
        /// <param name="NumberOfRecords"></param>
        /// <param name="IsArchived"></param>
        /// <returns>List of Disposed Assets</returns>
        /// 
        internal List<Asset> GetMinAndMaxIdValuesForDisposedAssetPager(long? MinIdValue, string Status, string SearchText, bool GetLastRecords, int NumberOfRecords, int IsArchived,string DisType)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                var assetQuery = new GetMinAndMaxIdValuesForDisposedAssetPager(MinIdValue, Status, SearchText, GetLastRecords, NumberOfRecords, IsArchived,DisType);
                return dbContext.Execute(assetQuery);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Method to Fetch the Disposed Asset Record count based on search criteria and Status
        /// </summary>
        /// <param name="SearchText"></param>
        /// <param name="Status"></param>
        /// <returns>returns the operation result</returns>
        /// 
        internal long GetDisposedAssetRecordCount(string SearchText, string Status, int IsArchived,string DisCode)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                var countQuery = new GetDisposedAssetCount(SearchText, Status, IsArchived,DisCode);
                this.RecordCount = dbContext.Execute(countQuery);
                return this.RecordCount;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Asset

        /// <summary>
        /// Method to update the existing one.
        /// </summary>
        /// <param name="Asset Object"></param>
        /// <returns>returns the operation result</returns>
        /// 
        internal int UpdateAsset(Asset asset, bool IsLocatationChanged, bool changedInventoryDate)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                var userQuery = new UpdateAsset(asset, IsLocatationChanged, changedInventoryDate);
                return dbContext.Execute(userQuery);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Method to Insert a new Asset.
        /// </summary>
        /// <param name="Asset Object"></param>
        /// <returns>returns the operation result</returns>
        /// 
        internal int InsertAsset(Asset asset)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();

                // TO DO REMOVE THIS;; -- for asset bulk insert
                //    string astN = asset.AssetNumber+"_";
                //for (int i = 1; i < 12; i++)
                //{
                //    asset.AssetNumber = astN + i; ;
                //    // asset.CatalogNumber = "C" + i;
                //    // asset.ManufacturerCode = "M" + 1;
                //    // asset.BuildingNumber ="B"+i;
                //    // asset.RoomNumber = "R" + i;
                //    var userQuery = new InsertAsset(asset);
                //    dbContext.Execute(userQuery);
                //}
                //return 1;

                 var userQuery = new InsertAsset(asset);
                 return dbContext.Execute(userQuery);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }




        /// <summary>
        /// Method to Check wheter asset number already exists.
        /// </summary>
        /// <param name="Asset Object"></param>
        /// <returns>returns the operation result</returns>
        /// 
        internal int CheckAssetNumber(string AssetNumber)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                var resultQuery = new CheckAssetNumber(AssetNumber); // TO do check existing asset
                return dbContext.Execute(resultQuery);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Method to return the list of Assets
        /// </summary>
        /// <param name="MinIdValue"></param>
        /// <param name="MaxIdValue"></param>
        /// <param name="OperatorValue"></param>
        /// <param name="Status"></param>
        /// <param name="SearchText"></param>
        /// <param name="NumberOfRecords"></param>
        /// <returns>List of Assets</returns>
        /// 
        internal ObservableCollection<Asset> GetAsset(long? MinIdValue, long? MaxIdValue, string OperatorValue, string Status, string SearchText, int NumberOfRecords, int IsArchived)
        {
            try
            {
                AssetList = this.GetAssetList(MinIdValue, MaxIdValue, OperatorValue, Status, SearchText, NumberOfRecords, IsArchived);
                if (AssetList == null || AssetList.Count <= 0)
                {
                    this.EmptyMessageVisibility = true;
                }
                return AssetList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Method to Get the Asset for Edit.
        /// </summary>
        /// <param name="AssetID"></param>
        /// <returns>returns the operation result</returns>
        /// 
        internal ObservableCollection<Asset> GetAssetForEdit(int AssetId)
        {

            try
            {
                if (AssetId == 0)
                {
                    AssetList = new ObservableCollection<Asset>();
                    AssetList.Add(new Asset());
                }
                else
                {

                    GetConnection getConnection = new GetConnection();
                    dbContext = getConnection.InitializeConnection();
                    var assetQuery = new GetAssetForEdit(AssetId);
                    AssetList = new ObservableCollection<Asset>(dbContext.Execute(assetQuery));
                }
                return AssetList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Method to get the Minimum and maximum of the Id's based on MinIdValue
        /// </summary>
        /// <param name="MinIdValue"></param>
        /// <param name="GetLastRecords"></param>
        /// <param name="Status"></param>
        /// <param name="SearchText"></param>
        /// <param name="NumberOfRecords"></param>
        /// <returns>List of Assets</returns>
        /// 
        internal List<Asset> GetMinAndMaxIdValuesForPager(long? MinIdValue, string Status, string SearchText, bool GetLastRecords, int NumberOfRecords, int IsArchived)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                var assetQuery = new GetMinAndMaxIdValuesForPager(MinIdValue, Status, SearchText, GetLastRecords, NumberOfRecords, IsArchived);//(">", "> 0");
                return dbContext.Execute(assetQuery);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Method to delete the  Asset
        /// </summary>
        /// <param name="AssetId"></param>
        /// <returns>returns the operation result</returns>
        /// 
        internal int DeleteAsset(int AssetId)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                var userQuery = new DeleteAsset(AssetId);
                return dbContext.Execute(userQuery);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Method to Asset Record count based on search criteria and Status
        /// </summary>
        /// <param name="SearchText"></param>
        /// <param name="Status"></param>
        /// <returns>returns the operation result</returns>
        /// 
        internal long GetAssetRecordCount(string SearchText, string Status, int IsArchived)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                var countQuery = new GetAssetCount(SearchText, Status, IsArchived);//(">", "> 0");
                this.RecordCount = dbContext.Execute(countQuery);
                return this.RecordCount;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Method to Asset Previsous location details
        /// </summary>
        /// <param name="AssetId"></param>
        /// <returns>returns the location detail as list</returns>
        /// 
        internal ObservableCollection<Asset> GetAssetPreviousLocation(int AssetId)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                var assetQuery = new GetPreviousLocation(AssetId);
                AssetList = new ObservableCollection<Asset>(dbContext.Execute(assetQuery));
                this.RecordCount = AssetList.Count();
                return AssetList;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Method to Get Asset Barcode Details
        /// </summary>
        /// <param name="AssetId"></param>
        /// <returns>returns the Asset Barcode detail as list</returns>
        /// 
        internal List<AssetBarcode> GetAssetBarcodeDetail(int AssetId)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                this.EmptyMessageVisibility = false;
                var assetQuery = new GetAssetBarcode(AssetId);
                var list = new List<AssetBarcode>(dbContext.Execute(assetQuery));
                this.RecordCount = list.Count;
                if (list.Count <= 1 && list[0].RetagDate == null)
                {
                    this.EmptyMessageVisibility = true;
                    list[0].PreviousAssetNumber = list[0].AssetNumber;
                }
                return list;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Update the barcode of an existing Asset
        /// </summary>
        /// <param name="AssetBarcode Object"></param>
        /// <returns>returns the result of the Operation</returns>
        /// 
        internal int InsertUpdateBarCode(AssetBarcode assetBarcode)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                int result = 0;
                dynamic barcodeQuery = null;
                barcodeQuery = new UpdateAssetBarcode(assetBarcode);
                result = dbContext.Execute(barcodeQuery);
                this.EmptyMessageVisibility = false;
                return result;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion


        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Private Helpers

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));

            }
        }

        #endregion

        #region Properties

        public long RecordCount
        {
            get { return _recordCount; }
            set
            {
                _recordCount = value;
                NotifyPropertyChanged("RecordCount");
            }
        }


        #region IViewModel Properties

        public bool EmptyMessageVisibility
        {
            get { return _emptyMessageVisibility; }
            set
            {
                _emptyMessageVisibility = value;
                NotifyPropertyChanged("EmptyMessageVisibility");
            }
        }
        public int LoggedInUserRoleId
        {
            get
            {
                return (Thread.CurrentPrincipal as CustomPrincipal).Identity.CurrentUser.RoleId;
            }
        }

        public string LoggedInUserName
        {
            get { throw new NotImplementedException(); }
        }

        #endregion

        #endregion



    }
}



