﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Common;
using TRAKI.App.DapperQueries.RoomQueries;
using TRAKI.App.DapperQueries.Common;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;
using System.ComponentModel;
using System.Threading;
using TRAKI.App.Account;

namespace TRAKI.App.ViewModel
{
    public class RoomViewModel : INotifyPropertyChanged, IViewModel
    {
        public ObservableCollection<Room> RoomList { get; private set; }
        protected IDbContext dbContext;
        private bool _emptyMessageVisibility = false;
        private long _recordCount = 0;

        public RoomViewModel()
        {
        }

        private ObservableCollection<Room> GetRoomList(int? BuildingId, int? IsActive)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                RoomList = new ObservableCollection<Room>(dbContext.Execute(new GetRoomsByBuilding(BuildingId, IsActive)));
                this.RecordCount = RoomList.Count;
                if (RoomList == null || RoomList.Count <= 0)
                {
                    this.EmptyMessageVisibility = true;
                }
                return RoomList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ObservableCollection<Room> GetRoomsByBuilding(int? BuildingId, int? IsActive)
        {
            try
            {

                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                var roomQuery = new GetRoomsByBuilding(BuildingId, IsActive);
                RoomList = new ObservableCollection<Room>(dbContext.Execute(roomQuery));
                this.RecordCount = RoomList.Count;
                if (RoomList == null || RoomList.Count <= 0)
                {
                    this.EmptyMessageVisibility = true;
                }
                return RoomList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ObservableCollection<Room> GetRooms(int RoomId)
        {
            try
            {
                if (RoomId == -1)
                {
                    RoomList = new ObservableCollection<Room>();
                    RoomList.Add(new Room());
                }
                else
                {

                    GetConnection getConnection = new GetConnection();
                    dbContext = getConnection.InitializeConnection();
                    var roomQuery = new GetRooms(RoomId);
                    RoomList = new ObservableCollection<Room>(dbContext.Execute(roomQuery));
                    this.RecordCount = RoomList.Count;
                    if (RoomList == null || RoomList.Count <= 0)
                    {
                        this.EmptyMessageVisibility = true;
                    }
                }
                return RoomList;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ObservableCollection<Room> SearchRoom(string SearchText, int buildingId, int? IsActive)
        {
            try
            {

                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                var roomQuery = new SearchRoom(SearchText, buildingId, IsActive);// toh parmaterte
                RoomList = new ObservableCollection<Room>(dbContext.Execute(roomQuery));
                this.RecordCount = RoomList.Count;
                if (RoomList == null || RoomList.Count <= 0)
                {
                    this.EmptyMessageVisibility = true;
                }
                return RoomList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int DeleteExistingRoom(int RoomId)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                var roomQuery = new DeleteRoom(RoomId);
                return dbContext.Execute(roomQuery);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int InsertNewRoom(Room room,string bldgID)   //Steve
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                int result = 0;
                dynamic roomQuery = null;
                if (room.RoomId == 0)
                {
                    var resultQuery = new CheckRoom(room.RoomNumber,bldgID);  //Steve
                    result = dbContext.Execute(resultQuery);
                    if (result > 0)
                    {
                        return -1; //room already exists
                    }
                    roomQuery = new InsertRoom(room);
                    result = dbContext.Execute(roomQuery);
                    if (result > 0)
                    {
                        result = 1;  //success insert operation
                    }
                }
                else
                {
                    roomQuery = new UpdateRoom(room);
                    result = dbContext.Execute(roomQuery);
                    if (result > 0)
                    {
                        result = 2;  // success update operation
                    }
                }
                return result;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string PropertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
        }


        #region Properties


        public long RecordCount
        {
            get { return _recordCount; }
            set
            {
                _recordCount = value;
                NotifyPropertyChanged("RecordCount");
            }
        }


        #region IViewModel Properties

        public bool EmptyMessageVisibility
        {
            get { return _emptyMessageVisibility; }
            set
            {
                _emptyMessageVisibility = value;
                NotifyPropertyChanged("EmptyMessageVisibility");
            }
        }

        public int LoggedInUserRoleId
        {
            get
            {
                return (Thread.CurrentPrincipal as CustomPrincipal).Identity.CurrentUser.RoleId;
            }
        }

        public string LoggedInUserName
        {
            get { throw new NotImplementedException(); }
        }

        #endregion
        #endregion


    }
}

