﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Common;
using TRAKI.App.DapperQueries.Common;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;
using System.ComponentModel;
using TRAKI.App.DapperQueries;
using System.Threading;
using TRAKI.App.Account;
using TRAKI.App.DapperQueries.UserDefinedField;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Data;


namespace TRAKI.App.ViewModel
{
    class UserDefinedFieldViewModel : INotifyPropertyChanged, IViewModel
    {
        private ObservableCollection<CustomField> UserDefinedFieldList;
        protected IDbContext dbContext;
        private bool _emptyMessageVisibility = false;
        private long _recordCount = 0;
        private bool _isValid = true;

        public UserDefinedFieldViewModel()
        {
        }

        #region Private

        /// <summary>
        /// Private method to return the list of CustomFields
        /// </summary>
        /// <param name="CustomFieldId"></param>
        /// <param name="IsActive"></param>
        /// <returns>List of CustomFields</returns>
        /// 
        private ObservableCollection<CustomField> GetCustomFieldList(int? FieldId, int? IsActive, int? AssetId)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                UserDefinedFieldList = new ObservableCollection<CustomField>(dbContext.Execute(new GetUserDefinedFields(FieldId, IsActive, AssetId)));
                return UserDefinedFieldList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Internal
        /// <summary>
        /// Method to return the list of CustomFields
        /// </summary>
        /// <param name="CustomFieldId"></param>
        /// <param name="IsActive"></param>
        /// <returns>List of CustomFields</returns>
        /// 
        internal ObservableCollection<CustomField> GetCustomFields(int? CustomFieldId, int? IsActive, int? AssetId)
        {
            try
            {
                if (CustomFieldId == 0)
                {
                    UserDefinedFieldList = new ObservableCollection<CustomField>();
                    UserDefinedFieldList.Add(new CustomField());
                }
                else
                {
                    UserDefinedFieldList = this.GetCustomFieldList(CustomFieldId, IsActive, AssetId);
                    this.RecordCount = UserDefinedFieldList.Count;
                    if (UserDefinedFieldList == null || UserDefinedFieldList.Count <= 0)
                    {
                        this.EmptyMessageVisibility = true;
                    }
                }
                return UserDefinedFieldList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Method to return the list of CustomFields that matches the search criteria
        /// </summary>
        /// <param name="SearchText"></param>
        /// <param name="IsActive"></param>
        /// <returns>List of CustomFields</returns>
        /// 
        internal ObservableCollection<CustomField> SearchCustomField(string SearchText, int? IsActive)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                var CustomFieldQuery = new SearchUserDefinedField(SearchText, IsActive);
                UserDefinedFieldList = new ObservableCollection<CustomField>(dbContext.Execute(CustomFieldQuery));
                this.RecordCount = UserDefinedFieldList.Count;
                if (UserDefinedFieldList == null || UserDefinedFieldList.Count <= 0)
                {
                    this.EmptyMessageVisibility = true;
                }
                return UserDefinedFieldList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Method to return the list of custom field values of an asset
        /// </summary>
        /// <param name="SearchText"></param>
        /// <param name="IsActive"></param>
        /// <returns>List of CustomFields</returns>
        /// 
        internal ObservableCollection<CustomField> GetUserDefinedFieldValues(string SearchText, int? IsActive, int? AssetNumber)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                var CustomFieldQuery = new GetUserDefinedFieldValues(SearchText, IsActive, AssetNumber);
                UserDefinedFieldList = new ObservableCollection<CustomField>(dbContext.Execute(CustomFieldQuery));
                this.RecordCount = UserDefinedFieldList.Count;
                if (UserDefinedFieldList == null || UserDefinedFieldList.Count <= 0)
                {
                    this.EmptyMessageVisibility = true;
                }
                return UserDefinedFieldList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Method to delete the  CustomField
        /// </summary>
        /// <param name="CustomFieldId"></param>
        /// <returns>returns the operation result</returns>
        /// 
        internal int DeleteExistingCustomField(int CustomFieldId)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                var CustomFieldQuery = new DeleteUserDefinedField(CustomFieldId);
                return dbContext.Execute(CustomFieldQuery);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Method to Insert a update the custom field values
        /// </summary>
        /// <param name="CustomField Object"></param>
        /// <returns>returns the operation result</returns>
        /// 

        internal int InsertUpdateUserDefinedFieldValues(List<CustomField> customFieldList, int AssetId)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                int result = 0;
                dynamic CustomFieldQuery = null;
                CustomFieldQuery = new CheckForAssetFieldValues(AssetId);
                int opr = dbContext.Execute(CustomFieldQuery);

                foreach (CustomField customField in customFieldList)
                {
                    customField.AssetId = AssetId;
                    CustomFieldQuery = new InsertUpdateUserDefinedFieldValues(customField, opr);
                    result = dbContext.Execute(CustomFieldQuery);
                }
                return result;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Method to Insert a new User defined Field or to update the existing one.
        /// </summary>
        /// <param name="CustomField Object"></param>
        /// <returns>returns the operation result</returns>
        /// 
        internal int InsertUpdateUserDefinedField(CustomField customField)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                int result = 0;
                dynamic customFieldQuery = null;
                this.SetDataType(customField);
                if (customField.UserDefinedFieldId == 0)
                {
                    customFieldQuery = new InsertUserDefinedField(customField);
                    result = dbContext.Execute(customFieldQuery);
                    if (result > 0)
                    {
                        result = 1;  //success insert operation
                    }
                }
                else
                {
                    customFieldQuery = new UpdateUserDefinedField(customField);
                    result = dbContext.Execute(customFieldQuery);
                    if (result > 0)
                    {
                        result = 2;  // success update operation
                    }
                }
                return result;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Method to set the datatype of the Custom field based on the control type
        /// </summary>
        /// <param name="CustomField Object"></param>
        /// <returns>returns updated custom field</returns>
        /// 
        private CustomField SetDataType(CustomField customField)
        {
            if (customField.FieldType.Equals("radiobutton"))
            {
                customField.DataType = "boolean";
            }
            else if (customField.FieldType.Equals("datepicker"))
            {
                customField.DataType = "date";
            }
            else
            {
                customField.DataType = "string";
            }
            return customField;
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Private Helpers

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));

            }
        }

        #endregion

        #region Properties

        public long RecordCount
        {
            get { return _recordCount; }
            set
            {
                _recordCount = value;
                NotifyPropertyChanged("RecordCount");
            }
        }

        public bool IsValid
        {
            get { return _isValid; }
            set
            {
                _isValid = value;
                NotifyPropertyChanged("IsValid");
            }
        }


        #region IViewModel Properties

        public bool EmptyMessageVisibility
        {
            get { return _emptyMessageVisibility; }
            set
            {
                _emptyMessageVisibility = value;
                NotifyPropertyChanged("EmptyMessageVisibility");
            }
        }

        public int LoggedInUserRoleId
        {
            get
            {
                return (Thread.CurrentPrincipal as CustomPrincipal).Identity.CurrentUser.RoleId;
            }
        }

        public string LoggedInUserName
        {
            get { throw new NotImplementedException(); }
        }

        #endregion
        #endregion

    }
}

