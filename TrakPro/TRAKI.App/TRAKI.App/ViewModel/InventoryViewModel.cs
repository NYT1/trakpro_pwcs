﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TRAKI.App.Account;
using TRAKI.App.Common;
using TRAKI.App.DapperQueries.InventoryQueries;
using TRAKI.App.DapperQueries.InventoryQueries.UploadData;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;
using System.Data;
using System.Data.OleDb;
using TRAKI.App.ViewModel.Common;

namespace TRAKI.App.ViewModel
{
    class InventoryViewModel : INotifyPropertyChanged, IViewModel
    {
        private ObservableCollection<Inventory> InventoryList;
        protected IDbContext dbContext;
        SettingsViewModel settingViewModel = new SettingsViewModel();
        protected DbContext idbContext;
        private ObservableCollection<SystemControl> SCList;
        private bool _emptyMessageVisibility = false;
        // private bool _saveAndCloseButtonVisibility = false;
        private long _recordCount = 0;

        public InventoryViewModel()
        {
            this.EmptyMessageVisibility = true;
            InitializeLocalDbConnectionString.SetLocalConnectionString();
        }


        #region Private
        /// <summary>
        /// Method to preform the QA ie if Location is valid it sets QA to true, false otherwise
        /// </summary>
        /// <param name="Inventory Object"></param>
        /// <returns>List of Inventory Objects</returns>
        /// 
        /*
        private void PerformQA(Inventory inventory)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                var locationQuery = new ValidateAssetLocation(inventory);
                if (dbContext.Execute(locationQuery) > 0)
                {
                    inventory.QA = BooleanValue.True;
                    inventory.Status = "Valid";
                }
                else
                {
                    inventory.QA = BooleanValue.False;
                    inventory.Status = "Mismatch";
                }

                // return inventory;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        */
        /// <summary>
        /// Method to find Discrepancies in the Inventory Data 
        /// </summary>
        /// <param name="Inventory Object"></param>
        /// <returns>List of Inventory Objects</returns>
        /// 
        private void FindDiscrepancy(Inventory inventory)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                var query = new FindDiscrepancies(inventory);
                // if (dbContext.Execute(locationQuery) > 0)
                inventory.Status = dbContext.Execute(query);
                // else
                //  inventory.QA = BooleanValue.False;

                //  return inventory;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }




        /// <summary>
        /// Method to move the upload data file to the spcified destination.
        /// </summary>
        /// <param name="Source file path"></param>
        /// <returns>True if filed moved successfully , false otherwise</returns>
        /// 
        public bool MoveFile(string SrcPath)
        {
            try
            {
               
                string FileName = Path.GetFileNameWithoutExtension(SrcPath);
                //FileName = FileName + "_" + (Thread.CurrentPrincipal as CustomPrincipal).Identity.CurrentUser.UserName + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".TXT";
                FileName = FileName + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".TXT";

                //Komal- Upload folder User define--09072018

               
                ///GetFolder Details

                //SCList = settingViewModel.GetSystemControl(1);

                SCList = settingViewModel.GetSystemControl(1);

                var DestDir = ((ObservableCollection<SystemControl>)this.SCList)[0].Path.ToString();

                //Get  Delete folder Details
                SCList = settingViewModel.GetSystemControl(2);

                //this.IDataContext = SCList;
                var iVal = ((ObservableCollection<SystemControl>)this.SCList)[0].cValue.ToString();
               
                //string DestDir = System.Configuration.ConfigurationManager.AppSettings["Path"];

                //// Check whether user has specifed a path/ otherwise use the default application path
                //if (string.IsNullOrEmpty(DestDir))
                //    DestDir = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "TRAKI_UploadDataFiles");

                if (DestDir==null )
                {
                    DestDir = "C:\\Users\\Default\\AppData\\Local\\NYT\\Asset Trak Pro\\InventoryDataArchive";
                    //Update the folder path
                    SystemControl systemControl = new SystemControl();
                    systemControl.SystemControlId = 1;
                    systemControl.ControlName = "ArchiveFolderPath ";
                    systemControl.Path = DestDir;
                   
                    systemControl.Password = "";
                    systemControl.cValue = "0";
                    var result = settingViewModel.UpdateSystemControl(systemControl);  //Steve

                }
                if (!Directory.Exists(DestDir))
                    Directory.CreateDirectory(DestDir);

                //DirectorySecurity sec = Directory.GetAccessControl(DestDir);
                //SecurityIdentifier everyone = new SecurityIdentifier(WellKnownSidType.WorldSid, null);
                //sec.AddAccessRule(new FileSystemAccessRule(everyone, FileSystemRights.Modify | FileSystemRights.Synchronize, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
                //Directory.SetAccessControl(DestDir, sec);

                string DestPath = DestDir + @"\" + FileName;
                if (File.Exists(DestPath))
                    File.Delete(DestPath);

                File.SetAttributes(SrcPath, FileAttributes.Normal);
                if (iVal == "1")
                {
                    File.Move(SrcPath, DestPath);
                }
                else
                {
                    File.Copy(SrcPath, DestPath);
                }
                if (File.Exists(DestPath))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        #endregion

        #region Internal

        /// <summary>
        /// Method to UPload and read The File Contents
        /// </summary>
        /// <param name="SearchText"></param>
        /// <param name="IsActive"></param>
        /// <returns>List of Inventory Objects</returns>
        /// 
        internal ObservableCollection<Inventory> UploadAndGetFileData(string FilePath)
        {
            // int counter = 0;
            string line = string.Empty;
            List<Inventory> _inventoryList = new List<Inventory>();
            GetConnection getConnection = new GetConnection();
            dbContext = getConnection.InitializeLocalDbConnection();

            try
            {   // Open the text file using a stream reader.
                using (StreamReader sr = new StreamReader(FilePath))
                {
                    // Read the stream to a string, and write the string to the console.
                    while ((!string.IsNullOrEmpty(line = sr.ReadLine())))
                    {
                        Inventory inventory = new Inventory();
                        // line = sr.ReadLine();
                        //Steve
                        // new room size 10, new bldg size is 10
                        inventory.UserId = line.Substring(0, 4).Trim();
                        inventory.BuildingNumber = line.Substring(4, 10).Trim();
                        inventory.RoomNumber = line.Substring(14, 10).Trim();
                        inventory.AssetNumber = line.Substring(24, 15).Trim();
                        inventory.Description = line.Substring(39, 2).Trim();
                        string date = line.Substring(41, 11).Trim();
                        date = date + " " + line.Substring(52, 8).Trim();
                        inventory.InventoryDate = DateTime.ParseExact(date, "MM/dd/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                        // inventory.InventoryDate = Convert.ToDateTime(date);

                        //this.PerformQA(inventory); // TO DO check it usage otherwise remove this
                        _inventoryList.Add(inventory);
                    }
                }

                //this.SaveUploadData(_inventoryList);

                //var query = ;
                int result = dbContext.Execute(new SaveUploadData(_inventoryList));
                InventoryList = new ObservableCollection<Inventory>(dbContext.Execute(new GetAllUploadData()));

                //int result = inventoryViewModel.SaveUploadData(this._inventoryList.ToList());
                //if (result > 0)
                //{
                //    string message = "Data sucessfully Uploaded";
                //    if (!inventoryViewModel.MoveFile(SrcFilePath))
                //        message = message + ", But there is an error while moving the UploadData File!";
                //    ModernDialog.ShowMessage(message, "Success", MessageBoxButton.OK);
                //    this.InventoryGrid.ItemsSource = null;
                //    this.InventoryGrid.Items.Refresh();
                //    //  SrcFilePath = string.Empty;
                //}


                this.RecordCount = InventoryList.Count;
                if (InventoryList.Count > 0)
                {
                    this.EmptyMessageVisibility = false;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return InventoryList;
        }

        ///// <summary>
        ///// Method to Save the Uploaded Data 
        ///// </summary>
        ///// <param name="List of Inventory objects"></param>
        ///// <returns>Result of the Operation</returns>
        ///// 
        //private int SaveUploadData(List<Inventory> InventoryList)
        //{
        //    try
        //    {
        //        GetConnection getConnection = new GetConnection();
        //        dbContext = getConnection.InitializeLocalDbConnection();
        //        var query = new SaveUploadData(InventoryList);
        //        int result = dbContext.Execute(query);
        //        if (result > 0)
        //        {
        //            this.RecordCount = 0;
        //            this.EmptyMessageVisibility = true;
        //        }

        //        return result;

        //    }
        //    catch (Exception e)
        //    {
        //        throw e;
        //    }
        //}


        /// <summary>
        /// Method to Save the Uploaded Data 
        /// </summary>
        /// <param name="List of Inventory objects"></param>
        /// <returns>Result of the Operation</returns>
        /// 
        internal int UpdateUploadData(List<Inventory> InventoryList)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                var query = new UpdateUploadData(InventoryList);
                int result = dbContext.Execute(query);
                if (result > 0)
                {
                    this.RecordCount = 0;
                    this.EmptyMessageVisibility = true;
                    //  this.SaveAndCloseButtonVisibility = false;
                }
                return result;

            }
            catch (Exception e)
            {
                throw e;
            }
        }



        /// <summary>
        /// Method to Save the Data to Inventory
        /// </summary>
        /// <param name="List of Inventory objects"></param>
        /// <returns>Result of the Operation</returns>
        /// 
        internal int SaveInventoryData(List<Inventory> inventoryList)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                var query = new SaveInventoryData(inventoryList);
                int result = dbContext.Execute(query);
                if (result > 0)
                {
                    this.RecordCount = 0;
                    this.EmptyMessageVisibility = true;
                    //  this.SaveAndCloseButtonVisibility = true;
                   //Steve
                    //Only delete records that has been appended to inventory table
                    //this.DeleteAllUploadData();
                    Constants.TempInventoryList.Clear();
                    foreach (var inventory in inventoryList)
                    {
                        if (inventory.AppendIt==1)
                        {
                            Constants.TempInventoryList.Add(inventory.AssetNumber);
                        }
                    };

                    this.DeleteUploadDataHasBeenAppended();
                }

                return result;

            }
            catch (Exception e)
            {
                throw e;
            }
        }


        /// <summary>
        /// Method to Upadate the Trak Pro database after inventory 
        /// </summary>
        /// <param name="List of Inventory objects"></param>
        /// <returns>Result of the Operation</returns>
        /// 
        internal int UpdateDatabaseAfterInventory()
        {
            int result = 0; // Fial
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();

                if (dbContext.Execute(new CheckInventoryDataAvailability()) <= 0)
                {
                    return 1;// 
                }

                // Get Missmatch records and Update  their Location and Inventory Information.
                List<Inventory> _inventoryAssetList = new List<Inventory>();
                _inventoryAssetList = dbContext.Execute(new GetMismatchAssets());
                if (_inventoryAssetList != null && _inventoryAssetList.Count > 0)
                {
                    var _query = new UpdateLocationOfMismatchAssets(_inventoryAssetList);
                    dbContext.Execute(_query);
                }


                //Update Assets with Valid Location and Update their InventoryDate and Set IsMmissing to false.
                dbContext.Execute(new UpdateAssetsWithValidLocation());

                // Get Overages and Insert them into trakpro Database
                _inventoryAssetList = dbContext.Execute(new GetOverageAssets());
                if (_inventoryAssetList != null && _inventoryAssetList.Count > 0)
                {
                    dbContext.Execute(new InsertOverageAssets(_inventoryAssetList));
                }

                //Update the shortages
                dbContext.Execute(new UpdateShortageAssets());
                result = 2;

                if (result == 2)
                {
                    dbContext.Execute(new EmptyInventoryTable()); // TO Do Uncomment this to empty inventory table
                }

                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }



        /// <summary>
        /// Method to Upadate the Trak Pro database after inventory  PWCPS
        /// </summary>
        /// <param name="List of Inventory objects"></param>
        /// <returns>Result of the Operation</returns>
        /// 
        internal int UpdateDatabaseAfterInventoryPWCPS(IEnumerable<string> BuildNumbers)
        {
            int result = 0; // Fial
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();

                if (dbContext.Execute(new CheckInventoryDataAvailability()) <= 0)
                {
                    return 1;// 
                }

                // Get Missmatch records and Update  their Location and Inventory Information.

                List<Inventory> _inventoryAssetList = new List<Inventory>();
                _inventoryAssetList = dbContext.Execute(new GetMismatchAssetsPWCPS(BuildNumbers));
                if (_inventoryAssetList != null && _inventoryAssetList.Count > 0)
                {
                    var _query = new UpdateLocationOfMismatchAssetsPWCPS(_inventoryAssetList);
                    dbContext.Execute(_query);
                }

                //Update Assets with Valid Location and Update their InventoryDate and Set IsMmissing to false.
                dbContext.Execute(new UpdateAssetsWithValidLocation());

                // Get Overages and Insert them into trakpro Database
                _inventoryAssetList = dbContext.Execute(new GetOverageAssetsPWCPS(BuildNumbers));
                if (_inventoryAssetList != null && _inventoryAssetList.Count > 0)
                {
                    dbContext.Execute(new InsertOverageAssetsPWCPS(_inventoryAssetList));
                }

                //Update the shortages
                dbContext.Execute(new UpdateShortageAssetsPWCPS(BuildNumbers));

                // CLose all open buildings
                dbContext.Execute(new CloseSelectedBuildings(BuildNumbers));
                result = 2;

                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }



        #region Reset Baseline


        /// <summary>
        /// Method to Close all the buildings
        /// </summary>
        /// <returns>Result of the Operation</returns>
        /// 
        internal int CloseAllBuildings()
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                var query = new CloseAllBuildings();
                return dbContext.Execute(query);

            }
            catch (Exception e)
            {
                throw e;
            }
        }


        /// <summary>
        /// Method to Open all the buildings
        /// </summary>
        /// <returns>Result of the Operation</returns>
        /// 
        internal int OpenAllBuilding()
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                var query = new OpenAllBuilding();
                return dbContext.Execute(query);

            }
            catch (Exception e)
            {
                throw e;
            }
        }



        /// <summary>
        /// Method to Clear the data of the inventory Table
        /// </summary>
        /// <returns>Result of the Operation</returns>
        /// 
        internal int ClearInventoryData()
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                var query = new EmptyInventoryTable();
                return dbContext.Execute(query);
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        #endregion
        /// <summary>
        /// Method to Delete all upload data that is in local storage.
        /// </summary>
        /// <returns>Result of the Operation</returns>
        /// 
        internal int DeleteAllUploadData()
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeLocalDbConnection();
                var query = new DeleteAllUploadData();
                int result = dbContext.Execute(query);
                if (result > 0)
                {
                    this.RecordCount = 0;
                    this.EmptyMessageVisibility = true;
                    // this.SaveAndCloseButtonVisibility = false;
                }

                return result;

            }
            catch (Exception e)
            {
                throw e;
            }
        }

        internal int DeleteUploadDataHasBeenAppended()
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeLocalDbConnection();
                var query = new DeleteAppendedUploadData();
                
                int result = dbContext.Execute(query);
                if (result > 0)
                {
                    this.RecordCount = 0;
                    this.EmptyMessageVisibility = true;
                    // this.SaveAndCloseButtonVisibility = false;
                }

                return result;

            }
            catch (Exception e)
            {
                throw e;
            }

            return 1;
        }
           
       

        #region Upload Data





        /// <summary>
        /// Method to return the list of Assets
        /// </summary>
        /// <param name="MinIdValue"></param>
        /// <param name="MaxIdValue"></param>
        /// <param name="OperatorValue"></param>
        /// <param name="SearchText"></param>
        /// <param name="NumberOfRecords"></param>
        /// <returns>List of Upload Data</returns>
        /// 
        internal ObservableCollection<Inventory> GetUploadData()
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeLocalDbConnection();
                var uploadQuery = new GetAllUploadData();
                InventoryList = new ObservableCollection<Inventory>(dbContext.Execute(uploadQuery));
                dbContext = getConnection.InitializeConnection();
                if (InventoryList != null || InventoryList.Count > 0)
                {
                    foreach (Inventory inventory in InventoryList)
                    {
                        if (inventory.QA == BooleanValue.False)
                        {
                            //this.FindDiscrepancy(inventory);
                            // if (inventory.Status == "Mismatch")
                            // {
                            var locationQuery = new ValidateAssetLocation(inventory);
                            if (dbContext.Execute(locationQuery) > 0)
                            {
                                inventory.QA = BooleanValue.True;
                                inventory.Status = "Valid";
                            }
                            // }
                        }
                    }

                    this.RecordCount = InventoryList.Count;
                    if (InventoryList.Count > 0)
                    {
                        this.EmptyMessageVisibility = false;
                    }

                }

                return InventoryList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Method to return the list of Assets
        /// </summary>
        /// <param name="MinIdValue"></param>
        /// <param name="MaxIdValue"></param>
        /// <param name="OperatorValue"></param>
        /// <param name="SearchText"></param>
        /// <param name="NumberOfRecords"></param>
        /// <returns>List of Upload Data</returns>
        /// 
        internal ObservableCollection<Inventory> GetUploadData(long? MinIdValue, long? MaxIdValue, string OperatorValue, string SearchText, int NumberOfRecords)
        {
            try
            {
                ObservableCollection<Inventory> InventoryList = new ObservableCollection<Inventory>();
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeLocalDbConnection();
                var uploadQuery = new GetUploadData(MinIdValue, MaxIdValue, OperatorValue, SearchText, NumberOfRecords);
                InventoryList = new ObservableCollection<Inventory>(dbContext.Execute(uploadQuery));
                dbContext = getConnection.InitializeConnection();
                if (InventoryList != null || InventoryList.Count > 0)
                {
                    foreach (Inventory inventory in InventoryList)
                    {
                        if (inventory.QA == BooleanValue.False)
                        {
                            //this.FindDiscrepancy(inventory);
                            // if (inventory.Status == "Mismatch")
                            // {
                            var locationQuery = new ValidateAssetLocation(inventory);
                            if (dbContext.Execute(locationQuery) > 0)
                            {
                                inventory.QA = BooleanValue.True;
                                inventory.Status = "Valid";
                            }
                            // }
                        }
                    }
                    this.EmptyMessageVisibility = false;
                    //  this.SaveAndCloseButtonVisibility = true;

                }

                return InventoryList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        ///// <summary>
        ///// Method to return the list of Assets
        ///// </summary>
        ///// <param name="MinIdValue"></param>
        ///// <param name="MaxIdValue"></param>
        ///// <param name="OperatorValue"></param>
        ///// <param name="SearchText"></param>
        ///// <param name="NumberOfRecords"></param>
        ///// <returns>List of Upload Data</returns>
        ///// 
        //internal ObservableCollection<Inventory> GetUploadData(long? MinIdValue, long? MaxIdValue, string OperatorValue, string SearchText, int NumberOfRecords)
        //{
        //    try
        //    {
        //        GetConnection getConnection = new GetConnection();
        //        dbContext = getConnection.InitializeLocalDbConnection();
        //        var uploadQuery = new GetUploadData(MinIdValue, MaxIdValue, OperatorValue, SearchText, NumberOfRecords);
        //        InventoryList = new ObservableCollection<Inventory>(dbContext.Execute(uploadQuery));
        //        dbContext = getConnection.InitializeConnection();
        //        if (InventoryList != null || InventoryList.Count > 0)
        //        {
        //            foreach (Inventory inventory in InventoryList)
        //            {
        //                if (inventory.QA == BooleanValue.False)
        //                {
        //                    //this.FindDiscrepancy(inventory);
        //                    // if (inventory.Status == "Mismatch")
        //                    // {
        //                    var locationQuery = new ValidateAssetLocation(inventory);
        //                    if (dbContext.Execute(locationQuery) > 0)
        //                    {
        //                        inventory.QA = BooleanValue.True;
        //                        inventory.Status = "Valid";
        //                    }
        //                    // }
        //                }
        //            }
        //            this.EmptyMessageVisibility = false;
        //            //  this.SaveAndCloseButtonVisibility = true;

        //        }

        //        return InventoryList;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}


        /// <summary>
        /// Method to get the Minimum and maximum of the Id's based on MinIdValue
        /// </summary>
        /// <param name="MinIdValue"></param>
        /// <param name="GetLastRecords"></param>
        /// <param name="SearchText"></param>
        /// <param name="NumberOfRecords"></param>
        /// <returns>List of Upload Data</returns>
        /// 
        internal List<Inventory> GetMinAndMaxIdValuesForPager(long? IdValue, string OperatorValue, string SearchText, bool GetLastRecords, int NumberOfRecords, bool isLocalDb = false)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                if (isLocalDb)
                    dbContext = getConnection.InitializeLocalDbConnection();
                else
                    dbContext = getConnection.InitializeConnection();
                var assetQuery = new GetMinAndMaxIdValuesForPager(IdValue, OperatorValue, SearchText, GetLastRecords, NumberOfRecords);//(">", "> 0");
                return dbContext.Execute(assetQuery);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Method to Asset Record count based on search criteria and Status
        /// </summary>
        /// <param name="SearchText"></param>
        /// <param name="Status"></param>
        /// <returns>returns the operation result</returns>
        /// 
        internal long GetUploadDataRecordCount(string SearchText)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeLocalDbConnection();
                var countQuery = new GetUploadDataCount(SearchText);//(">", "> 0");
                this.RecordCount = dbContext.Execute(countQuery);
                return this.RecordCount;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion


        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Private Helpers

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));

            }
        }

        #endregion

        #region Properties


        public long RecordCount
        {
            get { return _recordCount; }
            set
            {
                _recordCount = value;
                NotifyPropertyChanged("RecordCount");
            }
        }


        #region IViewModel Properties

        public bool EmptyMessageVisibility
        {
            get { return _emptyMessageVisibility; }
            set
            {
                _emptyMessageVisibility = value;
                NotifyPropertyChanged("EmptyMessageVisibility");
            }
        }
        public int LoggedInUserRoleId
        {
            get
            {
                return (Thread.CurrentPrincipal as CustomPrincipal).Identity.CurrentUser.RoleId;
            }
        }

        public string LoggedInUserName
        {
            get { throw new NotImplementedException(); }
        }

        #endregion
        #endregion

    }
}


