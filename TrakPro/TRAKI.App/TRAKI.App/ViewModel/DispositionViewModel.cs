﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Common;
using TRAKI.App.DapperQueries.DispositionQueries;
using TRAKI.App.DapperQueries.Common;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;
using System.ComponentModel;
using System.Threading;
using TRAKI.App.Account;

namespace TRAKI.App.ViewModel
{
    class DispositionViewModel : INotifyPropertyChanged, IViewModel
    {
        private ObservableCollection<Disposition> DispositionList;
        protected IDbContext dbContext;
        private bool _emptyMessageVisibility = false;
        private long _recordCount = 0;

        public DispositionViewModel()
        {
        }

        #region Private

        /// <summary>
        /// Private method to return the list of Dispositions
        /// </summary>
        /// <param name="DispositionId"></param>
        /// <param name="IsActive"></param>
        /// <returns>List of Dispositions</returns>
        /// 
        private ObservableCollection<Disposition> GetDispositionList(int? DispositionId, int? IsActive)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                DispositionList = new ObservableCollection<Disposition>(dbContext.Execute(new GetDisposition(DispositionId, IsActive)));
                return DispositionList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Internal

        /// <summary>
        /// Method to return the list of Dispositions
        /// </summary>
        /// <param name="DispositionId"></param>
        /// <param name="IsActive"></param>
        /// <returns>List of Dispositions</returns>
        /// 
        internal ObservableCollection<Disposition> GetDisposition(int? DispositionId, int? IsActive)
        {
            try
            {
                if (DispositionId == -1)
                {
                    DispositionList = new ObservableCollection<Disposition>();
                    DispositionList.Add(new Disposition());
                }
                else
                {

                    //DispositionList.Add(new Disposition() { DispositionId = 0, DispositionCode = "BLANK", DispositionDescription = "BLANK", IsActive = 0 });
                    DispositionList = this.GetDispositionList(DispositionId, IsActive);
                    if (Constants.isforDispcode == "DispAsset")
                    {
                       DispositionList.Insert(0, new Disposition() { DispositionId = 0, DispositionCode = "CLEAR DISPOSITION CODE", DispositionDescription = "CLEAR DISPOSITION CODE", IsActive = 0 });
                    }
                    else
                    {
                        DispositionList.Insert(0, new Disposition() { DispositionId = 0, DispositionCode = "ALL", DispositionDescription = "ALL", IsActive = 0 });
                    }
                    
                    //this.DispositionList.Add(new Disposition() {"Blank");
                    this.RecordCount = this.DispositionList.Count;
                    if (DispositionList == null || DispositionList.Count <= 0)
                    {
                        this.EmptyMessageVisibility = true;
                    }
                }
                return DispositionList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Method to return the list of Dispositions that matches the search criteria
        /// </summary>
        /// <param name="SearchText"></param>
        /// <param name="IsActive"></param>
        /// <returns>List of Dispositions</returns>
        /// 
        internal ObservableCollection<Disposition> SearchDisposition(string SearchText, int? IsActive)
        {
            try
            {

                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                var dispositionQuery = new SearchDisposition(SearchText, IsActive);
                DispositionList = new ObservableCollection<Disposition>(dbContext.Execute(dispositionQuery));
                this.RecordCount = this.DispositionList.Count;
                if (DispositionList == null || DispositionList.Count <= 0)
                {
                    this.EmptyMessageVisibility = true;
                }
                return DispositionList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Method to delete the existing Disposition
        /// </summary>
        /// <param name="DispositionId"></param>
        /// <returns>returns the operation result</returns>
        /// 
        internal int DeleteExistingDisposition(int DispositionId)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                var dispositionQuery = new DeleteDisposition(DispositionId);
                return dbContext.Execute(dispositionQuery);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Method to Insert a new Disposition or to update the existing one.
        /// </summary>
        /// <param name="Disposition Object"></param>
        /// <returns>returns the operation result</returns>
        /// 
        internal int InsertUpdateDisposition(Disposition disposition)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                int result = 0;
                dynamic dispositionQuery = null;
                if (disposition.DispositionId == 0)
                {
                    dispositionQuery = new InsertDisposition(disposition);
                    result = dbContext.Execute(dispositionQuery);
                    if (result > 0)
                    {
                        result = 1;  //success insert operation
                    }
                }
                else
                {
                    dispositionQuery = new UpdateDisposition(disposition);
                    result = dbContext.Execute(dispositionQuery);
                    if (result > 0)
                    {
                        result = 2;  // success update operation
                    }
                }
                return result;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Private Helpers

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));

            }
        }

        #endregion

        #region Properties

        public long RecordCount
        {
            get { return _recordCount; }
            set
            {
                _recordCount = value;
                NotifyPropertyChanged("RecordCount");
            }
        }


        #region IViewModel Properties

        public bool EmptyMessageVisibility
        {
            get { return _emptyMessageVisibility; }
            set
            {
                _emptyMessageVisibility = value;
                NotifyPropertyChanged("EmptyMessageVisibility");
            }
        }

        public int LoggedInUserRoleId
        {
            get
            {
                return (Thread.CurrentPrincipal as CustomPrincipal).Identity.CurrentUser.RoleId;
            }
        }

        public string LoggedInUserName
        {
            get { throw new NotImplementedException(); }
        }

        #endregion

        #endregion

    }
}

