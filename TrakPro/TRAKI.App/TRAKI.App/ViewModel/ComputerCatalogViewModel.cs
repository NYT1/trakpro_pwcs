﻿using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TRAKI.App.Account;
using TRAKI.App.Common;
using TRAKI.App.DapperQueries.ComputerCatalogQueries;
using TRAKI.App.DapperQueries.Common;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;

namespace TRAKI.App.ViewModel
{

    class ComputerCatalogViewModel : INotifyPropertyChanged, IViewModel
    {
        public List<ComputerCatalog> ComputerCatalogList { get; private set; }
        protected IDbContext dbContext;
        private bool _emptyMessageVisibility = false;
        private long _recordCount = 0;

        //public ComputerCatalogViewModel()
        //{ 
        //}

        #region Private

        public List<ComputerCatalog> GetComputerCatalog(int isActive)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                var ComputerQuery = new GetComputerCatalog(isActive);
                ComputerCatalogList = new List<ComputerCatalog>(dbContext.Execute(ComputerQuery));
                return ComputerCatalogList;
            }

            catch (Exception ex)
            {
                throw ex;
            }

        }
        //Delete
        public int DeleteCompCatalogList(System.Collections.ArrayList CompCatalogIdlist)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                dynamic CatalogQuery = new DeleteCompCatalog(CompCatalogIdlist);
                return dbContext.Execute(CatalogQuery);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Save
        internal int SaveCompCatalog(IEnumerable<string> CatalogCollector)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                int result = 0;

                dynamic CompSaveQuery = null;

                //var resultQuery = new CheckComputerCatalog(ComputerCatalog.CatalogNumber.ToString());
                //result= dbContext.Execute(resultQuery);


                //if (result > 0)
                //{
                //    return -1; //Computer Catalog already exists
                //}

                if (result >= 0)
                {
                    CompSaveQuery = new SaveCompCatalog(CatalogCollector);
                    result = dbContext.Execute(CompSaveQuery);
                    result = 1;  //success save operation
                }


                return result;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Private Helpers

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));

            }
        }

        #endregion

        #region Properties


        public long RecordCount
        {
            get { return _recordCount; }
            set
            {
                _recordCount = value;
                NotifyPropertyChanged("RecordCount");
            }
        }


        #region IViewModel Properties

        public bool EmptyMessageVisibility
        {
            get { return _emptyMessageVisibility; }
            set
            {
                _emptyMessageVisibility = value;
                NotifyPropertyChanged("EmptyMessageVisibility");
            }
        }

        public int LoggedInUserRoleId
        {
            get
            {
                return (Thread.CurrentPrincipal as CustomPrincipal).Identity.CurrentUser.RoleId;
            }
        }

        public string LoggedInUserName
        {
            get { throw new NotImplementedException(); }
        }

        #endregion
        #endregion

    }
}
