﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Common;
using TRAKI.App.DapperQueries.Common;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;
using System.ComponentModel;
using TRAKI.App.DapperQueries.DepartmentQueries;
using System.Threading;
using TRAKI.App.Account;

namespace TRAKI.App.ViewModel
{
    class DepartmentViewModel : INotifyPropertyChanged, IViewModel
    {
        private ObservableCollection<Department> DepartmentList;
        protected IDbContext dbContext;
        private bool _emptyMessageVisibility = false;
        private long _recordCount = 0;


        public DepartmentViewModel()
        {
        }

        #region Private

        /// <summary>
        /// Private method to return the list of Departments
        /// </summary>
        /// <param name="DepartmentId"></param>
        /// <param name="IsActive"></param>
        /// <returns>List of Departments</returns>
        /// 
        private ObservableCollection<Department> GetDepartmentList(int? DepartmentId, int? IsActive)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                DepartmentList = new ObservableCollection<Department>(dbContext.Execute(new GetDepartments(DepartmentId, IsActive)));
                return DepartmentList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Internal
        /// <summary>
        /// Method to return the list of Departments
        /// </summary>
        /// <param name="DepartmentId"></param>
        /// <param name="IsActive"></param>
        /// <returns>List of Departments</returns>
        /// 
        internal ObservableCollection<Department> GetDepartments(int? DepartmentId, int? IsActive)
        {
            try
            {
                if (DepartmentId == -1)
                {
                    DepartmentList = new ObservableCollection<Department>();
                    DepartmentList.Add(new Department());
                }
                else
                {
                    DepartmentList = this.GetDepartmentList(DepartmentId, IsActive);
                    this.RecordCount = DepartmentList.Count;
                    if (DepartmentList == null || DepartmentList.Count <= 0)
                    {
                        this.EmptyMessageVisibility = true;
                    }
                }
                return DepartmentList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Method to return the list of Departments that matches the search criteria
        /// </summary>
        /// <param name="SearchText"></param>
        /// <param name="IsActive"></param>
        /// <returns>List of Departments</returns>
        /// 
        internal ObservableCollection<Department> SearchDepartment(string SearchText, int? IsActive)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                var DepartmentQuery = new SearchDepartment(SearchText, IsActive);
                DepartmentList = new ObservableCollection<Department>(dbContext.Execute(DepartmentQuery));
                this.RecordCount = DepartmentList.Count;
                if (DepartmentList == null || DepartmentList.Count <= 0)
                {
                    this.EmptyMessageVisibility = true;
                }
                return DepartmentList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Method to delete the  Department
        /// </summary>
        /// <param name="DepartmentId"></param>
        /// <returns>returns the operation result</returns>
        /// 
        internal int DeleteExistingDepartment(int DepartmentId)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                var DepartmentQuery = new DeleteDepartment(DepartmentId);
                return dbContext.Execute(DepartmentQuery);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Method to Insert a new Department or to update the existing one.
        /// </summary>
        /// <param name="Department Object"></param>
        /// <returns>returns the operation result</returns>
        /// 
        internal int InsertUpdateDepartment(Department Department)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                int result = 0;
                dynamic departmentQuery = null;
                if (Department.DepartmentId == 0)
                { 
                    departmentQuery = new InsertDepartment(Department);
                    result = dbContext.Execute(departmentQuery);
                    if (result > 0)
                    {
                        result = 1;  //success insert operation
                    }
                }
                else
                {
                    departmentQuery = new UpdateDepartment(Department);
                    result = dbContext.Execute(departmentQuery);
                    if (result > 0)
                    {
                        result = 2;  // success update operation
                    }
                }
                return result;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Private Helpers

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));

            }
        }

        #endregion

        #region Properties
         
        public long RecordCount
        {
            get { return _recordCount; }
            set
            {
                _recordCount = value;
                NotifyPropertyChanged("RecordCount");
            }
        }

        #region IViewModel Properties

        public bool EmptyMessageVisibility
        {
            get { return _emptyMessageVisibility; }
            set
            {
                _emptyMessageVisibility = value;
                NotifyPropertyChanged("EmptyMessageVisibility");
            }
        }

        public int LoggedInUserRoleId
        {
            get
            {
                return (Thread.CurrentPrincipal as CustomPrincipal).Identity.CurrentUser.RoleId;
            }
        }

        public string LoggedInUserName
        {
            get { throw new NotImplementedException(); }
        }

        #endregion
        #endregion

    }
}

