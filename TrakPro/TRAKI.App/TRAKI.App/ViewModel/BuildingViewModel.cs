﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Windows.Data;
using System.Collections.ObjectModel;
using TRAKI.App.Interfaces;
using TRAKI.App.Common;
using TRAKI.App.Model;
using TRAKI.App.DapperQueries.BuildingQueries;
using System.Threading;
using TRAKI.App.Account;

namespace TRAKI.App.ViewModel
{
    public class BuildingViewModel : INotifyPropertyChanged, IViewModel
    {

        private ObservableCollection<Building> BuildingList;// { get; private set; }
        protected IDbContext dbContext;

        private bool _emptyMessageVisibility = false;
        private long _recordCount = 0;

        public BuildingViewModel()
        {
        }


        private ObservableCollection<Building> GetBuildingList(int? BuildingId, int? IsActive)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                BuildingList = new ObservableCollection<Building>(dbContext.Execute(new GetBuilding(BuildingId, IsActive)));
                this.RecordCount = BuildingList.Count;
                if (BuildingList == null || BuildingList.Count <= 0)
                {
                    this.EmptyMessageVisibility = true;
                }
                return BuildingList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ObservableCollection<Building> GetBuilding(int? BuildingId, int? IsActive)
        {
            try
            {
                if (BuildingId == -1)
                {
                    BuildingList = new ObservableCollection<Building>();
                    BuildingList.Add(new Building());
                }
                else
                {
                    BuildingList = this.GetBuildingList(BuildingId, IsActive);
                    this.RecordCount = BuildingList.Count;
                    if (BuildingList == null || BuildingList.Count <= 0)
                    {
                        this.EmptyMessageVisibility = true;
                    }
                }
                return BuildingList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int DeleteBuilding(int BuildingId)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                var buildingQuery = new DeleteBuilding(BuildingId);
                return dbContext.Execute(buildingQuery);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Method to get Building List that has atleast One Room
        /// </summary>
        /// <returns>List of buildings</returns>
        public ObservableCollection<Building> GetBuildingWithRooms(string SearchText)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                var BuildingQuery = new GetBuildingWithRooms(SearchText);
                ObservableCollection<Building> _Building = new ObservableCollection<Building>(dbContext.Execute(BuildingQuery));
                BuildingList = new ObservableCollection<Building>(dbContext.Execute(BuildingQuery));
                this.RecordCount = BuildingList.Count;
                if (BuildingList == null || BuildingList.Count <= 0)
                {
                    this.EmptyMessageVisibility = true;
                }
                return BuildingList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ObservableCollection<Building> SearchBuilding(string SearchText, int? IsActive)
        {
            GetConnection getConnection = new GetConnection();
            dbContext = getConnection.InitializeConnection();
            var buildingQuery = new SearchBuilding(SearchText, IsActive);
            BuildingList = new ObservableCollection<Building>(dbContext.Execute(buildingQuery));
            this.RecordCount = BuildingList.Count;
            if (BuildingList == null || BuildingList.Count <= 0)
            {
                this.EmptyMessageVisibility = true;
            }

            return BuildingList;
        }


        /// <summary>
        /// Method to get the list of Open Buildings
        /// </summary>
        /// <returns>List of buildings</returns>
        public ObservableCollection<Building> GetOpenBuildings()
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                var BuildingQuery = new GetOpenBuildings();
                BuildingList = new ObservableCollection<Building>(dbContext.Execute(BuildingQuery));
                return BuildingList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }




      


        #region Properties


        public long RecordCount
        {
            get { return _recordCount; }
            set
            {
                _recordCount = value;
                NotifyPropertyChanged("RecordCount");
            }
        }


        #region IViewModel Properties

        public bool EmptyMessageVisibility
        {
            get { return _emptyMessageVisibility; }
            set
            {
                _emptyMessageVisibility = value;
                NotifyPropertyChanged("EmptyMessageVisibility");
            }
        }

        public int LoggedInUserRoleId
        {
            get
            {
                return (Thread.CurrentPrincipal as CustomPrincipal).Identity.CurrentUser.RoleId;
            }
        }

        public string LoggedInUserName
        {
            get { throw new NotImplementedException(); }
        }

        #endregion

        #endregion



        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string PropertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
        }


    }
}


