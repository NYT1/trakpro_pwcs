﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using TRAKI.App.Account;
using TRAKI.App.Common;
using TRAKI.App.DapperQueries.ReportQueries;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;

namespace TRAKI.App.ViewModel
{
    class ReportViewModel : INotifyPropertyChanged
    {
        private long _recordCount = 0;

        protected IDbContext dbContext;

        #region Internal
        /// <summary>
        /// Method to return the Asset Value Report Data 
        /// </summary>
        /// <returns>Report Data</returns>
        internal IEnumerable GetAssetValueSummaryReport()
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                return dbContext.Execute(new GetAssetValueSummaryReport());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Method to return the Perkins Report Data 
        /// </summary>
        /// <returns>Report Data</returns>
        internal IEnumerable GetPerkinsReport()
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                return dbContext.Execute(new GetPerkinsReport());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Method to return the Disposition Report Data 
        /// </summary>
        /// <returns>Report Data</returns>
        internal IEnumerable GetDispositionReport(IEnumerable<string> BuildingNumbers, string DispositionCode)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                return dbContext.Execute(new GetDispositionReport(BuildingNumbers, DispositionCode));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Method to return the Cafeteria Report
        /// </summary>
        /// <returns>Report Data</returns>
        internal IEnumerable GetCafeteriaReport()
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                return dbContext.Execute(new GetCafeteriaReport());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Method to return the Inventory Summary Report Data 
        /// </summary>
        /// <returns>Report Data</returns>
        internal IEnumerable GetInventorySummaryByBuildingReport()
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                return dbContext.Execute(new GetInventorySummaryByBuildingReport());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Method to return the Asset List By Building Report Data 
        /// </summary>
        /// <returns>Report Data</returns>
        internal List<Report> GetComputerCatalogReport(string CatalogNumber)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                List<Report> result = dbContext.Execute(new GetComputerCatalogReport(CatalogNumber));
                this.RecordCount = result.Count;
                return result;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Method to return the Asset List By Building Report Data 
        /// </summary>
        /// <returns>Report Data</returns>
        internal List<Report> GetAssetListByBuildingReport(string StartingBuildingNumber, string EndingBuildingNumber)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                List<Report> result = dbContext.Execute(new GetAssetListByBuildingReport(StartingBuildingNumber, EndingBuildingNumber));
                this.RecordCount = result.Count;
                return result;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Method to return the Asset List By Missing Asset Report Data 
        /// </summary>
        /// <returns>Report Data</returns>
        internal List<Report> GetMissingAssetReport(IEnumerable<string> BuildingNumbers)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();

                List<Report> result = dbContext.Execute(new GetMissingAssetReport(BuildingNumbers));
                this.RecordCount = result.Count;
                return result;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Method to move the upload data file to the spcified destination.
        /// </summary>
        /// <param name="Source file path"></param>
        /// <returns>True if filed moved successfully , false otherwise</returns>
        /// 
        internal string GetReportName(string Extension, string ReportName)
        {
            try
            {
                string FileName = System.IO.Path.GetFileNameWithoutExtension(ReportName);
                //FileName = FileName + "_" + (Thread.CurrentPrincipal as CustomPrincipal).Identity.CurrentUser.UserName + "_" + DateTime.Now.ToString("yyyyMMddHHmm") + "." + Extension;
                return (FileName + "." + Extension);
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        /// <summary>
        /// Method to return the list of Catalogs
        /// </summary>
        /// <param name="CatalogId"></param>
        /// <param name="IsActive"></param>
        /// <returns>List of Catalogs</returns>
        /// 
        internal IEnumerable GetCatalogListReport()
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                return dbContext.Execute(new GetCatalogListReport());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Method to Get Discrepancy Report Data
        /// </summary>
        /// <returns>Report Data</returns>
        internal List<Report> GetDiscrepancyReport(IEnumerable<string> BuildingNumbers, string DiscrepancyType)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                List<Report> result = dbContext.Execute(new GetDiscrepancyReport(BuildingNumbers, DiscrepancyType));
                this.RecordCount = result.Count;
                return result;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Method to Get Discrepancy Report Data
        /// </summary>
        /// <returns>Report Data</returns>
        internal List<Report> GetDiscrepancyReportPWCPS(IEnumerable<string> BuildingNumbers, string DiscrepancyType)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                List<Report> result = dbContext.Execute(new GetDiscrepancyReportPWCPS(BuildingNumbers, DiscrepancyType));
                this.RecordCount = result.Count;
                return result;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        internal ContextMenu PrepareContextMenu(GrapeCity.ActiveReports.Document.PageDocument pageDocument)
        {
            double opacity = 1;
            bool isEnabled = true;
            if (pageDocument == null)
            {
                opacity = 0.5;
                isEnabled = false;
            }
            var contextMenu = new ContextMenu();
            MenuItem menuItemPDF = new MenuItem { Name = ExportMenuItemName.ExportToPDF.ToString(), Header = "Export To PDF" };
            // menuItemPDF.Click += ExportToPDFItem_Click;
            menuItemPDF.Icon = new Image
            {
                Source = new BitmapImage(new Uri("pack://application:,,,/Images/pdf.png")),
                VerticalAlignment = VerticalAlignment.Center,
                Width = 16,
                Height = 16,
                Margin = new Thickness(2, 3, 2, 2),
                Opacity = opacity
            };
            menuItemPDF.IsEnabled = isEnabled;
            menuItemPDF.Opacity = opacity;
            contextMenu.Items.Add(menuItemPDF);
            contextMenu.Items.Add(new Separator());
            MenuItem menuItemExcel = new MenuItem { Name = ExportMenuItemName.ExportToExcel.ToString(), Header = "Export To Excel" };
            // menuItemExcel.Click += ExportToExcelItem_Click;
            menuItemExcel.Icon = new Image
            {
                Source = new BitmapImage(new Uri("pack://application:,,,/Images/excel.png")),
                VerticalAlignment = VerticalAlignment.Center,
                Width = 16,
                Height = 16,
                Margin = new Thickness(2, 3, 2, 2),
                Opacity = opacity
            };
            menuItemExcel.IsEnabled = isEnabled;
            menuItemExcel.Opacity = opacity;
            contextMenu.Items.Add(menuItemExcel);
            contextMenu.Items.Add(new Separator());
            MenuItem menuItemTextFile = new MenuItem { Name = ExportMenuItemName.ExportToFile.ToString(), Header = "Export To Text File" };
            // menuItemTextFile.Click += ExportToTextFileItem_Click;
            menuItemTextFile.Icon = new Image
            {
                Source = new BitmapImage(new Uri("pack://application:,,,/Images/file.png")),
                VerticalAlignment = VerticalAlignment.Center,
                Width = 16,
                Height = 16,
                Margin = new Thickness(2, 3, 2, 2),
                Opacity = opacity
            };
            menuItemTextFile.IsEnabled = isEnabled;
            menuItemTextFile.Opacity = opacity;
            contextMenu.Items.Add(menuItemTextFile);
            return contextMenu;
        }





        #endregion



        public long RecordCount
        {
            get { return _recordCount; }
            set
            {
                _recordCount = value;
                NotifyPropertyChanged("RecordCount");
            }
        }

        #region Private Helpers

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion


        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion
    }
}

//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.ComponentModel;
//using System.IO;
//using System.Linq;
//using System.Text;
//using System.Threading;
//using System.Threading.Tasks;
//using System.Windows;
//using System.Windows.Controls;
//using System.Windows.Media.Imaging;
//using TRAKI.App.Account;
//using TRAKI.App.Common;
//using TRAKI.App.DapperQueries.ReportQueries;
//using TRAKI.App.Interfaces;
//using TRAKI.App.Model;

//namespace TRAKI.App.ViewModel
//{
//    class ReportViewModel : INotifyPropertyChanged
//    {
//        private long _recordCount = 0;

//        protected IDbContext dbContext;

//        #region Internal
//        /// <summary>
//        /// Method to return the Asset Value Report Data 
//        /// </summary>
//        /// <returns>Report Data</returns>
//        internal IEnumerable GetAssetValueSummaryReport()
//        {
//            try
//            {
//                GetConnection getConnection = new GetConnection();
//                dbContext = getConnection.InitializeConnection();
//                return dbContext.Execute(new GetAssetValueSummaryReport());
//            }
//            catch (Exception ex)
//            {
//                throw ex;
//            }
//        }


//        /// <summary>
//        /// Method to return the Perkins Report Data 
//        /// </summary>
//        /// <returns>Report Data</returns>
//        internal IEnumerable GetPerkinsReport()
//        {
//            try
//            {
//                GetConnection getConnection = new GetConnection();
//                dbContext = getConnection.InitializeConnection();
//                return dbContext.Execute(new GetPerkinsReport());
//            }
//            catch (Exception ex)
//            {
//                throw ex;
//            }
//        }


//        /// <summary>
//        /// Method to return the Disposition Report Data 
//        /// </summary>
//        /// <returns>Report Data</returns>
//        internal IEnumerable GetDispositionReport(string DispositionCode)
//        {
//            try
//            {
//                GetConnection getConnection = new GetConnection();
//                dbContext = getConnection.InitializeConnection();
//                return dbContext.Execute(new GetDispositionReport(DispositionCode));
//            }
//            catch (Exception ex)
//            {
//                throw ex;
//            }
//        }


//        /// <summary>
//        /// Method to return the Cafeteria Report
//        /// </summary>
//        /// <returns>Report Data</returns>
//        internal IEnumerable GetCafeteriaReport()
//        {
//            try
//            {
//                GetConnection getConnection = new GetConnection();
//                dbContext = getConnection.InitializeConnection();
//                return dbContext.Execute(new GetCafeteriaReport());
//            }
//            catch (Exception ex)
//            {
//                throw ex;
//            }
//        }

//        /// <summary>
//        /// Method to return the Inventory Summary Report Data 
//        /// </summary>
//        /// <returns>Report Data</returns>
//        internal IEnumerable GetInventorySummaryByBuildingReport()
//        {
//            try
//            {
//                GetConnection getConnection = new GetConnection();
//                dbContext = getConnection.InitializeConnection();
//                return dbContext.Execute(new GetInventorySummaryByBuildingReport());
//            }
//            catch (Exception ex)
//            {
//                throw ex;
//            }
//        }



//        /// <summary>
//        /// Method to return the Asset List By Building Report Data 
//        /// </summary>
//        /// <returns>Report Data</returns>
//        internal List<Report> GetAssetListByBuildingReport(string StartingBuildingNumber, string EndingBuildingNumber)
//        {
//            try
//            {
//                GetConnection getConnection = new GetConnection();
//                dbContext = getConnection.InitializeConnection();
//                List<Report> result = dbContext.Execute(new GetAssetListByBuildingReport(StartingBuildingNumber, EndingBuildingNumber));
//                this.RecordCount = result.Count;
//                return result;

//            }
//            catch (Exception ex)
//            {
//                throw ex;
//            }
//        }


//        /// <summary>
//        /// Method to return the Asset List By Missing Asset Report Data 
//        /// </summary>
//        /// <returns>Report Data</returns>
//        internal List<Report> GetMissingAssetReport()
//        {
//            try
//            {
//                GetConnection getConnection = new GetConnection();
//                dbContext = getConnection.InitializeConnection();

//                List<Report> result = dbContext.Execute(new GetMissingAssetReport());
//                this.RecordCount = result.Count;
//                return result;

//            }
//            catch (Exception ex)
//            {
//                throw ex;
//            }
//        }







//        /// <summary>
//        /// Method to move the upload data file to the spcified destination.
//        /// </summary>
//        /// <param name="Source file path"></param>
//        /// <returns>True if filed moved successfully , false otherwise</returns>
//        /// 
//        internal string GetReportName(string Extension, string ReportName)
//        {
//            try
//            {
//                string FileName = System.IO.Path.GetFileNameWithoutExtension(ReportName);
//                //FileName = FileName + "_" + (Thread.CurrentPrincipal as CustomPrincipal).Identity.CurrentUser.UserName + "_" + DateTime.Now.ToString("yyyyMMddHHmm") + "." + Extension;
//                return (FileName + "." + Extension);
//            }
//            catch (Exception e)
//            {
//                throw e;
//            }
//        }


//        /// <summary>
//        /// Method to return the list of Catalogs
//        /// </summary>
//        /// <param name="CatalogId"></param>
//        /// <param name="IsActive"></param>
//        /// <returns>List of Catalogs</returns>
//        /// 
//        internal IEnumerable GetCatalogListReport()
//        {
//            try
//            {
//                GetConnection getConnection = new GetConnection();
//                dbContext = getConnection.InitializeConnection();
//                return dbContext.Execute(new GetCatalogListReport());
//            }
//            catch (Exception ex)
//            {
//                throw ex;
//            }
//        }

//        /// <summary>
//        /// Method to Get Discrepancy Report Data
//        /// </summary>
//        /// <returns>Report Data</returns>
//        internal List<Report> GetDiscrepancyReport(IEnumerable<string> BuildingNumbers, string DiscrepancyType)
//        {
//            try
//            {
//                GetConnection getConnection = new GetConnection();
//                dbContext = getConnection.InitializeConnection();
//                List<Report> result = dbContext.Execute(new GetDiscrepancyReport(BuildingNumbers, DiscrepancyType));
//                this.RecordCount = result.Count;
//                return result;

//            }
//            catch (Exception ex)
//            {
//                throw ex;
//            }
//        }


//        /// <summary>
//        /// Method to Get Discrepancy Report Data
//        /// </summary>
//        /// <returns>Report Data</returns>
//        internal List<Report> GetDiscrepancyReportPWCPS(IEnumerable<string> BuildingNumbers, string DiscrepancyType)
//        {
//            try
//            {
//                GetConnection getConnection = new GetConnection();
//                dbContext = getConnection.InitializeConnection();
//                List<Report> result = dbContext.Execute(new GetDiscrepancyReportPWCPS(BuildingNumbers, DiscrepancyType));
//                this.RecordCount = result.Count;
//                return result;

//            }
//            catch (Exception ex)
//            {
//                throw ex;
//            }
//        }



//        internal ContextMenu PrepareContextMenu(GrapeCity.ActiveReports.Document.PageDocument pageDocument)
//        {
//            double opacity = 1;
//            bool isEnabled = true;
//            if (pageDocument == null)
//            {
//                opacity = 0.5;
//                isEnabled = false;
//            }
//            var contextMenu = new ContextMenu();
//            MenuItem menuItemPDF = new MenuItem { Name = ExportMenuItemName.ExportToPDF.ToString(), Header = "Export To PDF" };
//            // menuItemPDF.Click += ExportToPDFItem_Click;
//            menuItemPDF.Icon = new Image
//            {
//                Source = new BitmapImage(new Uri("pack://application:,,,/Images/pdf.png")),
//                VerticalAlignment = VerticalAlignment.Center,
//                Width = 16,
//                Height = 16,
//                Margin = new Thickness(2, 3, 2, 2),
//                Opacity = opacity
//            };
//            menuItemPDF.IsEnabled = isEnabled;
//            menuItemPDF.Opacity = opacity;
//            contextMenu.Items.Add(menuItemPDF);
//            contextMenu.Items.Add(new Separator());
//            MenuItem menuItemExcel = new MenuItem { Name = ExportMenuItemName.ExportToExcel.ToString(), Header = "Export To Excel" };
//            // menuItemExcel.Click += ExportToExcelItem_Click;
//            menuItemExcel.Icon = new Image
//            {
//                Source = new BitmapImage(new Uri("pack://application:,,,/Images/excel.png")),
//                VerticalAlignment = VerticalAlignment.Center,
//                Width = 16,
//                Height = 16,
//                Margin = new Thickness(2, 3, 2, 2),
//                Opacity = opacity
//            };
//            menuItemExcel.IsEnabled = isEnabled;
//            menuItemExcel.Opacity = opacity;
//            contextMenu.Items.Add(menuItemExcel);
//            contextMenu.Items.Add(new Separator());
//            MenuItem menuItemTextFile = new MenuItem { Name = ExportMenuItemName.ExportToFile.ToString(), Header = "Export To Text File" };
//            // menuItemTextFile.Click += ExportToTextFileItem_Click;
//            menuItemTextFile.Icon = new Image
//            {
//                Source = new BitmapImage(new Uri("pack://application:,,,/Images/file.png")),
//                VerticalAlignment = VerticalAlignment.Center,
//                Width = 16,
//                Height = 16,
//                Margin = new Thickness(2, 3, 2, 2),
//                Opacity = opacity
//            };
//            menuItemTextFile.IsEnabled = isEnabled;
//            menuItemTextFile.Opacity = opacity;
//            contextMenu.Items.Add(menuItemTextFile);
//            return contextMenu;
//        }





//        #endregion



//        public long RecordCount
//        {
//            get { return _recordCount; }
//            set
//            {
//                _recordCount = value;
//                NotifyPropertyChanged("RecordCount");
//            }
//        }

//        #region Private Helpers

//        private void NotifyPropertyChanged(string propertyName)
//        {
//            if (PropertyChanged != null)
//            {
//                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
//            }
//        }

//        #endregion


//        #region INotifyPropertyChanged Members

//        public event PropertyChangedEventHandler PropertyChanged;

//        #endregion
//    }
//}
