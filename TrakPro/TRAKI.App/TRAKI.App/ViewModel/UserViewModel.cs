﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Threading;
using TRAKI.App.Account;
using TRAKI.App.Common;
using TRAKI.App.DapperQueries.UserQueries;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;
using TRAKI.App.Model.Account;
using TRAKI.App.ViewModel.Common;


namespace TRAKI.App.ViewModel
{
    public class UserViewModel : INotifyPropertyChanged, IViewModel
    {

        private ObservableCollection<User> UserList;
        protected IDbContext dbContext;

        private bool _emptyMessageVisibility = false;
        private long _recordCount = 0;

        #region Private

        /// <summary>
        /// Private method to return the list of Users
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="RoleId"></param>
        /// <returns>List of Users</returns>
        /// 
        private ObservableCollection<User> GetUserList(int? UserId, int? RoleId)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                var userQuery = new GetUsers(UserId, RoleId);
                return new ObservableCollection<User>(dbContext.Execute(userQuery));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Internal


        /// <summary>
        /// Method to return the list of Users
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="RoleId"></param>
        /// <returns>List of Users</returns>
        /// 
        internal ObservableCollection<User> GetUser(int? UserId, int? RoleId)
        {
            try
            {
                if (UserId == -1)
                {
                    UserList = new ObservableCollection<User>();
                    UserList.Add(new User());
                }
                else
                {
                    UserList = this.GetUserList(UserId, RoleId);
                    this.RecordCount = UserList.Count;
                    if (UserList == null || UserList.Count <= 0)
                    {
                        this.EmptyMessageVisibility = true;
                    }
                }
                return UserList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Method to return the Login User Object for User To Login
        /// </summary>
        /// <returns>Login User Object</returns>
        /// 
        internal ObservableCollection<UserLogin> GetLogInUser()
        {
            try
            {
                var user = new ObservableCollection<UserLogin>();
                user.Add(new UserLogin());
                return user;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Method to return the list of Users that matches the search criteria
        /// </summary>
        /// <param name="SearchText"></param>
        /// <param name="RoleId"></param>
        /// <returns>List of Users</returns>
        /// 
        internal ObservableCollection<User> SearchUser(string SearchText, int? RoleId)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                var userQuery = new SearchUser(SearchText, RoleId);
                UserList = new ObservableCollection<User>(dbContext.Execute(userQuery));
                this.RecordCount = UserList.Count;
                if (UserList == null || UserList.Count <= 0)
                {
                    this.EmptyMessageVisibility = true;
                }
                return UserList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Method to delete the  Acquisition
        /// </summary>
        /// <param name="AcquisitionId"></param>
        /// <returns>returns the operation result</returns>
        /// 
        internal int DeleteExistingUser(int UserId)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                var userQuery = new DeleteUser(UserId);
                return dbContext.Execute(userQuery);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Method to get the count of active users.
        /// </summary>
        /// <returns>returns the operation result</returns>
        /// 
        internal int GetUserCount()
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                var userQuery = new GetUserCount();
                return dbContext.Execute(userQuery);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        /// <summary>
        /// Method to Insert a new User or to update the existing one.
        /// </summary>
        /// <param name="User Object"></param>
        /// <returns>returns the operation result</returns>
        /// 
        internal int InsertUpdateUser(User user)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                int result = 0;
                dynamic userQuery = null;
                if (user.UserId == 0)
                {
                    userQuery = new InsertUser(user);
                    result = dbContext.Execute(userQuery);
                    if (result > 0)
                    {
                        result = 1;  //success insert operation
                    }
                }
                else
                {
                    userQuery = new UpdateUser(user);
                    result = dbContext.Execute(userQuery);
                    if (result > 0)
                    {
                        result = 2;  // success update operation
                    }
                }
                return result;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Private Helpers

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));

            }
        }

        #endregion

        #region Properties

        public long RecordCount
        {
            get { return _recordCount; }
            set
            {
                _recordCount = value;
                NotifyPropertyChanged("RecordCount");
            }
        }

        #region IViewModel Properties

        public bool EmptyMessageVisibility
        {
            get { return _emptyMessageVisibility; }
            set
            {
                _emptyMessageVisibility = value;
                NotifyPropertyChanged("EmptyMessageVisibility");
            }
        }

        public int LoggedInUserRoleId
        {
            get
            {
                return (Thread.CurrentPrincipal as CustomPrincipal).Identity.CurrentUser.RoleId;
            }
        }

        public string LoggedInUserName
        {
            get { throw new NotImplementedException(); }
        }

        #endregion


        #endregion
    }
}



