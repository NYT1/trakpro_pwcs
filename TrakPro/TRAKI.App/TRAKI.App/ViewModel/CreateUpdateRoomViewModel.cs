﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Collections.ObjectModel;
using TRAKI.App.Model;
using TRAKI.App.Interfaces;
using TRAKI.App.Common;
using TRAKI.App.DapperQueries.Common;
using TRAKI.App.DapperQueries.RoomQueries;

namespace TRAKI.App.ViewModel
{
    public class CreateUpdateRoomViewModel : INotifyPropertyChanged
    {

        public ObservableCollection<Room> RoomList { get; private set; }

        protected IDbContext dbContext;

        public CreateUpdateRoomViewModel()
        {
        }

        public ObservableCollection<Room> GetRoom(int BuildingID)
        {
            try
            {
                if (BuildingID<0)
                {
                    RoomList = new ObservableCollection<Room>();
                    RoomList.Add(new Room());
                }
                else
                {
                    GetConnection getConnection = new GetConnection();
                    dbContext = getConnection.InitializeConnection();
                    var roomQuery = new GetRoomsByBuilding(BuildingID,null);
                    RoomList = new ObservableCollection<Room>(dbContext.Execute(roomQuery));
                }
                return RoomList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
         
        public int InsertNewRoom(Room room, string bldgID)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                int result = 0;
                dynamic roomQuery = null;
                if (room.RoomId <= 0)
                {
                    var resultQuery = new CheckRoom(room.RoomNumber.ToString(),bldgID);
                    result = dbContext.Execute(resultQuery);
                    if (result > 0)
                    {
                        return -1; //roomid already exists
                    }

                    if (result >= 0)
                    {
                        roomQuery = new InsertRoom(room);
                        result = dbContext.Execute(roomQuery);
                        result = 1;  //success insert operation
                    }
                }


              
                else
                {
                    roomQuery = new UpdateRoom(room);
                    result = dbContext.Execute(roomQuery);
                    if (result > 0)
                    {
                        result = 2;  // success update operation
                    }
                }
                return result;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int DeleteExistingRoom(int RoomId)
        {
            try
            {
                GetConnection getConnection = new GetConnection();
                dbContext = getConnection.InitializeConnection();
                var roomQuery = new DeleteRoom(RoomId);
                return dbContext.Execute(roomQuery);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Private Helpers

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion
    }
}



