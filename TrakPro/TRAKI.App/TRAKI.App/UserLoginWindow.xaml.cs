﻿using FirstFloor.ModernUI.Presentation;
using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TRAKI.App.Account;
using TRAKI.App.Common;
using TRAKI.App.Content.AccountContent;
using TRAKI.App.Content.CommonContent;
using TRAKI.App.Content.UsersContent;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;
using TRAKI.App.Model.Account;
using TRAKI.App.ViewModel;
using TRAKI.App.ViewModel.Account;

namespace TRAKI.App
{
    /// <summary>
    /// Interaction logic for UserLogin.xaml
    /// </summary>
    public partial class UserLoginWindow : ModernWindow, IView
    {

        private ObservableCollection<UserLogin> user;
        public UserLoginWindow()
        {

            InitializeComponent();
            //Style = (Style)App.Current.Resources["BlankWindow"];
            UserViewModel userViewModel = new UserViewModel();
            user = userViewModel.GetLogInUser();
            InitializeConnectionString.SetConnectionString();
            this.DataContext = user;
           
            if (!string.IsNullOrEmpty(Constants.TrialStatus))
            {
                DateTime ExpirationDate = DateTime.ParseExact(Constants.LicenseDetail["expirationdate"], "dd/MM/yyyy", null);
              //  DateTime ExpirationDate = DateTime.ParseExact(Constants.LicenseDetail["expirationdate"], "dd-MM-yyyy", CultureInfo.InvariantCulture);//"dd/MM/yyyy", null);
                var days = (ExpirationDate - DateTime.Now.Date).TotalDays;
                //string daysleft = string.Empty;
                if (days == 0)
                    this.Title = "Asset TRAK Pro" + " - " + Constants.TrialStatus + ", License will expire today";
                else if (days == 1)
                    this.Title = "Asset TRAK Pro" + " - " + Constants.TrialStatus + ", License will expire tomorrow";
                else
                    this.Title = "Asset TRAK Pro" + " - " + Constants.TrialStatus + " " + (ExpirationDate - DateTime.Now.Date).TotalDays + " days left";
            }
        }




        #region Private

        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                LoginViewModel loginViewModel = new LoginViewModel();
                UserLogin _user = ((ObservableCollection<UserLogin>)(sender as Button).DataContext)[0];
                loginViewModel.Login(_user);
                if ((Thread.CurrentPrincipal as CustomPrincipal).Identity.CurrentUser.IsTempPassword == BooleanValue.True)
                {
                    this.loginPanel.Visibility = Visibility.Collapsed;
                    this.changePasswordPanel.Visibility = Visibility.Visible;
                    ChangePasswordViewModel changePasswordViewModel = new ChangePasswordViewModel();
                    this.DataContext = changePasswordViewModel.BindPassword();
                }
                else if (Thread.CurrentPrincipal.Identity.IsAuthenticated)
                {
                    loginViewModel.ShowView(false);
                    this.Close();
                }
                this.loginstatus.DataContext = loginViewModel;
                this.loginstatus.Visibility = Visibility.Visible;
            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        private void linkForgotPassword_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var content = new ForgotPassword();
                var dialog = new ModernDialog
                {
                    Title = "Forgot Password",
                    Content = content,//new AssetBarcodeDialog(this.assetId),
                    Buttons = new Button[] { },
                };
                dialog.ShowDialog();

                if (dialog.DialogResult.Value == false)
                {
                    //this.LoadAssetDetail();
                }
            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        private void TextLoginUserName_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = IsMatching.Check(e.Text);
        }

        private void TextPassword_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = IsMatching.Check(e.Text);
        }

        #region Password change

        private void btnChange_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ChangePasswordViewModel changePasswordViewModel = new ChangePasswordViewModel();
                Password passwordInfo = ((ObservableCollection<Password>)(sender as Button).DataContext)[0];
                passwordInfo.IsTempPassword = BooleanValue.False;
                if (changePasswordViewModel.ChangePassword(passwordInfo) > 0)
                {
                    ModernDialog.ShowMessage("Password successfully changed!", "Success!", MessageBoxButton.OK);
                    if ((Thread.CurrentPrincipal as CustomPrincipal).Identity.CurrentUser.IsTempPassword == BooleanValue.True)
                    {
                        LoginViewModel loginViewModel = new LoginViewModel();
                        loginViewModel.Logout();
                        loginViewModel.ShowView(true);
                    }
                }
                else
                {
                    ModernDialog.ShowMessage("Unable to change password!", "Error!", MessageBoxButton.OK);
                }
            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);

            }
        }

        private void TextConfirmPassword_PreviewExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            if (e.Command == ApplicationCommands.Copy ||
                          e.Command == ApplicationCommands.Cut ||
                          e.Command == ApplicationCommands.Paste)
            {
                e.Handled = true;
            }
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            LoginViewModel loginViewModel = new LoginViewModel();
            loginViewModel.Logout();
            loginViewModel.ShowView(true);
        }

        private void TextNewPassword_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = IsMatching.Check(e.Text);
        }

        private void TextConfirmNewPassword_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = IsMatching.Check(e.Text);
        }

        #endregion

        private void btnExit_Click(object sender, RoutedEventArgs e)
        {
            App.Current.Shutdown();
        }

        #endregion

        #region IView Members

        public IViewModel ViewModel
        {
            get
            {
                return DataContext as IViewModel;
            }
            set
            {
                DataContext = value;
            }
        }

        #endregion

        private void linkSystemControl_Click(object sender, RoutedEventArgs e)
        {
            //NavigationCommands.GoToPage.Execute("/Content/CommonContent/UCSharedAndLocalDbPath.xaml#" + null, this);

            //try
            //{
            //    var content = new UCSharedAndLocalDbPath();
            //    var dialog = new ModernDialog
            //    {
            //        Title = "System Control",
            //        Content = content,//new AssetBarcodeDialog(this.assetId),
            //        //Buttons = new Button[] { },
            //    };
            //    dialog.ShowDialog();

            //    if (dialog.DialogResult.Value == false)
            //    {
            //        //this.LoadAssetDetail();
            //    }
            //}
            //catch (Exception ex)
            //{
            //    ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            //}
        }

        private void btnSC_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var content = new UCSharedAndLocalDbPath();
                var dialog = new ModernDialog
                {
                    Title = "System Control",
                    Content = content,//new AssetBarcodeDialog(this.assetId),
                    //Buttons = new Button[] { },
                };
                dialog.ShowDialog();

                if (dialog.DialogResult.Value == false)
                {
                    //this.LoadAssetDetail();
                }
            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

    }

}
