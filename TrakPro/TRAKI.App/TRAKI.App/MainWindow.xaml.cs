﻿using FirstFloor.ModernUI.Presentation;
using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TRAKI.App.Account;
using TRAKI.App.Common;
using TRAKI.App.Interfaces;
using TRAKI.App.ViewModel;

namespace TRAKI.App
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 
    [PrincipalPermission(SecurityAction.Demand)]
    public partial class MainWindow : ModernWindow, IView
    {
        public MainWindow()
        {
            InitializeComponent();
            MainWindowViewModel mainWindowViewModel = new MainWindowViewModel();
            this.MenuLinkGroups = mainWindowViewModel.GetAccessibleLinks(this.MenuLinkGroups, (Thread.CurrentPrincipal as CustomPrincipal).Identity.CurrentUser.RoleId);
            this.lblUserName.Text = mainWindowViewModel.LoggedInUserName;
            //test
            //Constants.TrialStatus = "Yes";
            if (!string.IsNullOrEmpty(Constants.TrialStatus))
            {
                DateTime ExpirationDate = DateTime.ParseExact(Constants.LicenseDetail["expirationdate"], "dd/MM/yyyy", null);
                //DateTime ExpirationDate = DateTime.ParseExact(Constants.LicenseDetail["expirationdate"], "dd-MM-yyyy", CultureInfo.InvariantCulture);
                var days = (ExpirationDate - DateTime.Now.Date).TotalDays;
                //string daysleft = string.Empty;
                if (days == 0)
                    this.Title = "Asset TRAK Pro" + " - " + Constants.TrialStatus + " Version 2.7.68 (01/23/2019) License will expire today";
                else if (days==1)
                    this.Title = "Asset TRAK Pro" + " - " + Constants.TrialStatus + " Version 2.7.68 (01/23/2019) License will expire tomorrow";
                else
                    this.Title = "Asset TRAK Pro" + " - " + Constants.TrialStatus + " Version 2.7.68 (01/23/2019) " + (ExpirationDate - DateTime.Now.Date).TotalDays + " days left";
            }

            ContentSource = MenuLinkGroups.First().Links.First().Source;
            if ((Thread.CurrentPrincipal as CustomPrincipal).Identity.CurrentUser.RoleId == 1)
            {
                Link link = new Link();
                link.DisplayName = "License";
                link.Source = new Uri("/Pages/LicensePage.xaml", UriKind.Relative);
                this.TitleLinks.Add(link);

            }

        }

        #region IView Members

        public IViewModel ViewModel
        {
            get
            {
                return DataContext as IViewModel;
            }
            set
            {
                DataContext = value;
            }
        }

        #endregion
    }
}
