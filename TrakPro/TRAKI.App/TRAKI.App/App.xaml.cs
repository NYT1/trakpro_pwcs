﻿using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using TRAKI.App.Account;
using TRAKI.App.Common;
using TRAKI.App.Content.CommonContent;
using TRAKI.App.Content.SystemControlContent;
using TRAKI.App.Content.UsersContent;
using TRAKI.App.ViewModel;
using TRAKI.App.ViewModel.Common;


namespace TRAKI.App
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private static Mutex _mutex = null;

        private void Application_StartUp(object sender, StartupEventArgs e)
        {

            //Code to create just one instance of the applciation
            const string appName = "AssetTrakPro";
            bool newAppInstance;
            _mutex = new Mutex(true, appName, out newAppInstance);
            if (!newAppInstance)
            {
                //app is already running! Exiting the application  
                Application.Current.Shutdown();
            }


            SettingsViewModel settingsViewModel = new SettingsViewModel();
            try
            {
                 //LicenseDialog dialog1 = new LicenseDialog();
                 //dialog1.ShowDialog();
                 // return;
                this.CheckLicense();
                DateTime ExpirationDate = DateTime.ParseExact(Constants.LicenseDetail["expirationdate"], "dd/MM/yyyy", null);
               // DateTime ExpirationDate = DateTime.ParseExact(Constants.LicenseDetail["expirationdate"], "dd-MM-yyyy", CultureInfo.InvariantCulture);
                if (ExpirationDate < DateTime.Now.Date)
                {
                    ModernDialog.ShowMessage("Your copy of Trak pro license has expired, Please renew the License in order to use the application", "Information", MessageBoxButton.OK);
                    LicenseDialog dialog = new LicenseDialog();
                    dialog.ShowDialog();
                    App.Current.Shutdown();
                }
                if (Convert.ToBoolean(Constants.LicenseDetail["istrial"]))
                    Constants.TrialStatus = "Trial Version";
                CustomPrincipal customPrincipal = new CustomPrincipal();
                AppDomain.CurrentDomain.SetThreadPrincipal(customPrincipal);
               // string fileLocation = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, @"AppData\\AppInfo.txt");
                string fileLocation = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + @"\NYT Inc\Asset TRAK Pro\AppInfo.txt";
                string fileLocation1 = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + @"\NYT Inc\Asset TRAK Pro\LocalDbPath.txt";

                bool f1 = File.Exists(fileLocation);
                bool f2 = File.Exists(fileLocation1);
                if (File.Exists(fileLocation)==false || File.Exists(fileLocation1)==false)
                {
                    this.ShowSystemControlDialog(string.Empty);
                }
                string connectionString = settingsViewModel.GetConnectionString(fileLocation);

                

                //fileLocation = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, @"AppData\\LocalDbPath.txt");
                 string localConnectionString = settingsViewModel.GetConnectionString(fileLocation1);

                try
                {
                    if (string.IsNullOrEmpty(connectionString) || (!settingsViewModel.TestDBConnection(connectionString))|| 
                        string.IsNullOrEmpty(localConnectionString) || (!settingsViewModel.TestDBConnection(localConnectionString) ))
                    {
                        this.ShowSystemControlDialog(string.Empty);
                    }

                    else
                    {
                        UserLoginWindow loginWindow = new UserLoginWindow();
                        loginWindow.Show();
                    }
                }
                catch (Exception ex)
                {
                    ErrorLogging.LogException("Error: " + ex.Message.ToString() + " {0}");
                    this.ShowSystemControlDialog(ex.Message);
                }

            }
            catch (Exception ex)
            {
                ErrorLogging.LogException("Error: " + ex.Message.ToString() + " {0}");
               // ModernDialog.ShowMessage("There is some Error while Starting the application , please contact your system Administrator!", "Error!", MessageBoxButton.OK);
              
            }
        }

        /// <summary>
        /// Finds the MAC address of the first operation NIC found.
        /// </summary>
        /// <returns>The MAC address.</returns>
        private List<string> GetMacAddress()
        {
            List<string> macAddresses = new List<string>();// string.Empty;

            foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
            {

               macAddresses.Add(BitConverter.ToString(nic.GetPhysicalAddress().GetAddressBytes()));
                
              
                //Chnged by Komal to store all MacAddress..08/08/2017


                //if (nic.NetworkInterfaceType == NetworkInterfaceType.Ethernet)
                //{
                //    var ddd= ;
                //     var dd = nic.GetPhysicalAddress();
                //    macAddresses.Add(BitConverter.ToString(nic.GetPhysicalAddress().GetAddressBytes()));
                //    //break;
                //}
            }

            return macAddresses;
        }

        /// <summary>
        /// Method to check the License
        /// </summary>
        private void CheckLicense()
        {
            try
            {
                LicenseOperations.Operations operation = new LicenseOperations.Operations();
                //Constants.AccessibleModules = operation.GetAccessibleModulesOfLicense(LicenseOperations.Utilities.IoHelper.BasePath + @"\License.licx");
                Constants.LicenseDetail = operation.GetLicenseDetails(LicenseOperations.Utilities.IoHelper.BasePath + @"\License.licx");
                bool isValidMachine = false;
                if (!string.IsNullOrEmpty(Constants.LicenseDetail["machineid"]))
                {

                    //Komal Change the Machine Name instead  of MAC ID--03/05/2018

                        string[] listMachineId = Constants.LicenseDetail["machineid"].Split(',');
                        foreach (string machineId in listMachineId)
                        {
                            if (Environment.MachineName.Equals(machineId))
                            {
                                isValidMachine = true;
                                break;
                            }
                        }


                    //foreach (var macAddress in GetMacAddress())
                    //{
                    //    string[] listMachineId = Constants.LicenseDetail["machineid"].Split(',');
                    //    foreach (string machineId in listMachineId)
                    //    {
                    //        if (macAddress.Equals(machineId))
                    //        {
                    //            isValidMachine = true;
                    //            break;
                    //        }
                    //    }
                    //    if (isValidMachine)
                    //        break;
                    //}
                }
                if (!isValidMachine)
                {
                    ModernDialog.ShowMessage("This is not a valid License!", "Not valid License!", MessageBoxButton.OK);
                    LicenseDialog dialog = new LicenseDialog();
                    dialog.ShowDialog();
                    App.Current.Shutdown();
                }
            }
            catch (FileNotFoundException ex)
            {
                LicenseDialog dialog = new LicenseDialog();
                dialog.ShowDialog();
                App.Current.Shutdown();
            }
        }

        /// <summary>
        /// Method to show the system control dialog
        /// </summary>
        public void ShowSystemControlDialog(string Message)
        {
            SettingsViewModel settingsViewModel = new SettingsViewModel();
            var content = new UCSharedAndLocalDbPath(true);
            try
            {
                content.statusMessage.Text = Message;
                content.statusMessage.Visibility = Visibility.Visible;
                ModernDialog dialog = new ModernDialog();
                dialog.Title = "System Control Settings";
                dialog.Content = content;
                dialog.Buttons = new Button[] { };
                dialog.ShowDialog();
                if (content.IsSharedDbPathSetSuccess  && content.IsLocalDbPathSetSuccess)
                {
                    System.Windows.Forms.Application.Restart();
                }
            }
            catch (Exception ex)
            {
                ErrorLogging.LogException("Error: " + ex.Message.ToString() + " {0}");
                ModernDialog.ShowMessage("There is some Error while Starting the application , please contact your system Administrator!", "Error!", MessageBoxButton.OK);
            }
        }
    }
}
