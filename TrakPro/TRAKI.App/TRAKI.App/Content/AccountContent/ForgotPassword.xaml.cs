﻿using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TRAKI.App.Common;
using TRAKI.App.Model;
using TRAKI.App.Utilities;
using TRAKI.App.ViewModel.Account;

namespace TRAKI.App.Content.AccountContent
{
    /// <summary>
    /// Interaction logic for ForgotPassword.xaml
    /// </summary>
    public partial class ForgotPassword : UserControl
    {
        ForgotPasswordViewModel forgotPasswordViewModel ;
        public ForgotPassword()
        {
            InitializeComponent();
            forgotPasswordViewModel = new ForgotPasswordViewModel();
            this.DataContext = forgotPasswordViewModel;
        }

        #region Private

        private void btnSendEMail_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                
                var result = forgotPasswordViewModel.ForgotPassword();
                if (result > 0)
                {
                    ModernDialog.ShowMessage("A temporary password has been sent to you email!", "Success!", MessageBoxButton.OK);
                    ((Button)sender).IsCancel = true;
                }
            }
            catch (Exception ex)
            {
                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        private void TextEmail_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = IsMatching.Check(e.Text);
        }

        #endregion
    }
}
