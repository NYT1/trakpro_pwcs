﻿using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TRAKI.App.Common;
using TRAKI.App.Model.Account;
using TRAKI.App.ViewModel;
using TRAKI.App.ViewModel.Account;


namespace TRAKI.App.Content.AccountContent
{
    /// <summary>
    /// Interaction logic for ChangePassword.xaml
    /// </summary>
    public partial class ChangePasswordFrm : UserControl
    {
        ChangePasswordViewModel changePasswordViewModel;

        public ChangePasswordFrm()
        {
            InitializeComponent();
            changePasswordViewModel = new ChangePasswordViewModel();
            this.DataContext = changePasswordViewModel.BindPassword();
        }


        #region Private

        private void btnChange_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Password passwordInfo = ((ObservableCollection<Password>)(sender as Button).DataContext)[0];
                if (changePasswordViewModel.ChangePassword(passwordInfo) > 0)
                {
                    ModernDialog.ShowMessage("Password successfully changed!", "Success!", MessageBoxButton.OK);
                    this.DataContext = changePasswordViewModel.BindPassword();

                }
                else
                {
                    ModernDialog.ShowMessage("Unable to change the password!", "Error!", MessageBoxButton.OK);
                }
            }
            catch (Exception ex)
            {
                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);

            }
        }

        private void TextConfirmPassword_PreviewExecuted(object sender, ExecutedRoutedEventArgs e)
        {

            if (e.Command == ApplicationCommands.Copy ||
                          e.Command == ApplicationCommands.Cut ||
                          e.Command == ApplicationCommands.Paste)
            {
                e.Handled = true;
            }
        }

        private void TextOldPassword_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = IsMatching.Check(e.Text);
        }

        private void TextNewPassword_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = IsMatching.Check(e.Text);
        }

        private void TextConfirmNewPassword_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = IsMatching.Check(e.Text);
        }

        #endregion

    }
}
