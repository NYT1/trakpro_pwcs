﻿using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TRAKI.App.Common;
using TRAKI.App.Content.CommonContent;
using TRAKI.App.Model;
using TRAKI.App.ViewModel;
using TRAKI.App.ViewModel.Common;

namespace TRAKI.App.Content.AssetContent
{
    /// <summary>
    /// Interaction logic for UserDefinedFieldFrm.xaml
    /// </summary>
    public partial class UserDefinedFieldsFrm : UserControl
    {
        private static UserDefinedFieldViewModel userDefinedFieldViewModel;
        private static ObservableCollection<CustomField> _userDefinedFieldList;//= new ObservableCollection<CustomField>();

        public UserDefinedFieldsFrm()
        {
            InitializeComponent();
           // this.LoadUserDefinedFields();
            Loaded += UserDefinedFieldsFrm_Loaded;
        }

        void UserDefinedFieldsFrm_Loaded(object sender, RoutedEventArgs e)
        {
            this.LoadUserDefinedFields();
        }

        #region custom section

        private void LoadUserDefinedFields()
        {
            try
            {
                userDefinedFieldViewModel = new UserDefinedFieldViewModel();
                _userDefinedFieldList = userDefinedFieldViewModel.GetUserDefinedFieldValues(string.Empty, -1, Constants.AssetNumber);

                this.CustomFieldPanel.Children.Clear();
                foreach (CustomField customField in _userDefinedFieldList)
                {
                    if (_userDefinedFieldList != null && _userDefinedFieldList.Count > 0)
                    {
                        if (customField.FieldType.Equals("textbox"))
                            this.AddCustomTextBoxField(customField);
                        else if (customField.FieldType.Equals("datepicker"))
                            this.AddCustomDateField(customField);
                        else if (customField.FieldType.Equals("radiobutton"))
                            this.AddCustomRadioButtonField(customField);
                        else if (customField.FieldType.Equals("checkbox"))
                            this.AddCustomCheckBoxField(customField);
                        else if (customField.FieldType.Equals("dropdownlist"))
                            this.AddCustomDropdownListField(customField);
                    }
                }
                this.DataContext = userDefinedFieldViewModel;
            }
            catch (Exception ex)
            {
                throw ex;

            }
        }

        #region text Box Field

        private void AddCustomTextBoxField(CustomField customField)
        {
            StackPanel stPanel = new StackPanel();
            stPanel.Margin = new Thickness(-5, 10, 0, 0);
            stPanel.Orientation = Orientation.Horizontal;
            stPanel.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            stPanel.VerticalAlignment = System.Windows.VerticalAlignment.Center;
            Label LabelCaption = new Label();
            LabelCaption.Content = customField.FieldCaption;
            LabelCaption.Width = 165;

            TextBox textField = new TextBox();
            textField.Width = 250;

            Binding binding = new Binding();
            binding.Source = customField; // set the source object instead of ElementName
            binding.Mode = BindingMode.TwoWay;
            binding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            binding.Path = new PropertyPath("FieldValue");
            if (customField.Required.Equals(BooleanValue.True))
                binding.ValidatesOnDataErrors = true;

            BindingOperations.SetBinding(textField, TextBox.TextProperty, binding);

            if (customField.DataType.Equals("string"))
            {
                textField.Name = "TextField" + customField.UserDefinedFieldId + "_" + customField.MaxStringLength + "_" + customField.MinStringLength;
                textField.MaxLength = customField.MaxStringLength;
                textField.LostKeyboardFocus += textField_LostKeyboardFocus;
                textField.ToolTip = customField.UserDefinedField + " with minimum string length of " + customField.MinStringLength + " max string length of " + customField.MaxStringLength;
            }

            if (customField.DataType.Equals("number"))
            {
                textField.Name = "TextField" + customField.UserDefinedFieldId + "_" + customField.MaxNumber + "_" + customField.MinNumber;
                textField.PreviewTextInput += TextField_PreviewTextInput;
                textField.MaxLength = 9;
                textField.ToolTip = customField.UserDefinedField + " with minimum Number " + customField.MinNumber + " and max Number " + customField.MaxNumber;

            }

            stPanel.Children.Add(LabelCaption);
            stPanel.Children.Add(textField);
            this.CustomFieldPanel.Children.Add(stPanel);
        }

        void textField_LostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            TextBox textFieldBox = (sender as TextBox);
            string[] txtBoxName = textFieldBox.Name.Split('_');
            string txtBoxValue = textFieldBox.Text;
            if (txtBoxValue.Length > Convert.ToInt32(txtBoxName[1]) || txtBoxValue.Length < Convert.ToInt32(txtBoxName[2]))
            {
                ModernDialog.ShowMessage("Field Values Exceed the Min or Max Limit!", "Error!", MessageBoxButton.OK);
                userDefinedFieldViewModel.IsValid = false;
            }
            else
            {
                userDefinedFieldViewModel.IsValid = true;
            }
        }

        private void TextField_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = IsMatching.CheckForNumber(e.Text);
        }


        #endregion

        #region radio button

        private void AddCustomRadioButtonField(CustomField customField)
        {
            if (!string.IsNullOrEmpty(customField.PickListValues))
            {
                string[] content = customField.PickListValues.Split(';');
                StackPanel stPanel = new StackPanel();
                stPanel.Margin = new Thickness(-5, 10, 0, 0);
                stPanel.Orientation = Orientation.Horizontal;
                stPanel.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
                stPanel.VerticalAlignment = System.Windows.VerticalAlignment.Center;
                Label LabelCaption = new Label();
                LabelCaption.Content = customField.FieldCaption;
                LabelCaption.Width = 165;

                stPanel.Children.Add(LabelCaption);
                foreach (var item in content)
                {
                    if (!string.IsNullOrWhiteSpace(item))
                    {
                        RadioButton radioBtnField = new RadioButton();
                        radioBtnField.GroupName = "RadioButtonField_" + customField.UserDefinedFieldId;
                        radioBtnField.Content = item.Trim();
                        radioBtnField.Margin = new Thickness(0, 0, 10, 0);
                        radioBtnField.Name = "TextDateField_" + customField.UserDefinedFieldId;
                        radioBtnField.Checked += AadioBtnField_Checked;
                        radioBtnField.Unchecked += radioBtnField_Unchecked;
                        if (!string.IsNullOrEmpty(customField.FieldValue))
                            if (customField.FieldValue.Trim().Contains(radioBtnField.Content.ToString().Trim()))
                                radioBtnField.IsChecked = true;
                        stPanel.Children.Add(radioBtnField);
                    }

                }

                this.CustomFieldPanel.Children.Add(stPanel);
            }

        }

        private void radioBtnField_Unchecked(object sender, RoutedEventArgs e)
        {
            RadioButton rBtn = (sender as RadioButton);
            string[] rBtnName = rBtn.Name.Split('_');
            CustomField result = _userDefinedFieldList.Where(x => x.UserDefinedFieldId == Convert.ToInt32(rBtnName[1])).FirstOrDefault();
            if (!string.IsNullOrEmpty(result.FieldValue))
            {
                string[] res = result.FieldValue.Split(';');
                res = res.Where(x => x.Trim() != rBtn.Content.ToString().Trim()).ToArray();
                result.FieldValue = String.Join(";", res);
            }
        }

        private void AadioBtnField_Checked(object sender, RoutedEventArgs e)
        {
            RadioButton rBtn = (sender as RadioButton);
            string[] rBtnName = rBtn.Name.Split('_');

            // result.FieldValue = rBtn.Content.ToString();
            CustomField result = _userDefinedFieldList.Where(x => x.UserDefinedFieldId == Convert.ToInt32(rBtnName[1])).FirstOrDefault();

            if (string.IsNullOrEmpty(result.FieldValue))
                result.FieldValue = rBtn.Content.ToString();
            else if (result.FieldValue.Contains(rBtn.Content.ToString()))
            {
                return;
            }
            else
                result.FieldValue += ";" + rBtn.Content.ToString();
        }

        #endregion

        #region Check Box

        private void AddCustomCheckBoxField(CustomField customField)
        {
            if (!string.IsNullOrEmpty(customField.PickListValues))
            {
                string[] content = customField.PickListValues.Split(';');
                StackPanel stPanel = new StackPanel();
                stPanel.Margin = new Thickness(-5, 10, 0, 0);
                stPanel.Orientation = Orientation.Horizontal;
                stPanel.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
                stPanel.VerticalAlignment = System.Windows.VerticalAlignment.Center;
                Label LabelCaption = new Label();
                LabelCaption.Content = customField.FieldCaption;
                LabelCaption.Width = 165;

                stPanel.Children.Add(LabelCaption);
                foreach (var item in content)
                {
                    if (!string.IsNullOrWhiteSpace(item))
                    {
                        CheckBox chkBoxField = new CheckBox();
                        chkBoxField.Content = item.Trim();
                        chkBoxField.Margin = new Thickness(0, 0, 10, 0);
                        chkBoxField.Name = "TextDateField_" + customField.UserDefinedFieldId;
                        chkBoxField.Checked += ChkBoxField_Checked;
                        chkBoxField.Unchecked += ChkBoxField_Unchecked;
                        if (!string.IsNullOrEmpty(customField.FieldValue))
                            if (customField.FieldValue.Trim().Contains(chkBoxField.Content.ToString().Trim()))
                                chkBoxField.IsChecked = true;
                        stPanel.Children.Add(chkBoxField);
                    }
                }
                this.CustomFieldPanel.Children.Add(stPanel);
            }

        }

        private void ChkBoxField_Unchecked(object sender, RoutedEventArgs e)
        {
            CheckBox chkBox = (sender as CheckBox);
            string[] chkBoxName = chkBox.Name.Split('_');
            CustomField result = _userDefinedFieldList.Where(x => x.UserDefinedFieldId == Convert.ToInt32(chkBoxName[1])).FirstOrDefault();
            if (!string.IsNullOrEmpty(result.FieldValue))
            {
                string[] res = result.FieldValue.Split(';');
                res = res.Where(x => x.Trim() != chkBox.Content.ToString().Trim()).ToArray();
                result.FieldValue = String.Join(";", res);
            }
        }

        private void ChkBoxField_Checked(object sender, RoutedEventArgs e)
        {
            CheckBox chkBox = (sender as CheckBox);
            string[] chkBoxName = chkBox.Name.Split('_');
            CustomField result = _userDefinedFieldList.Where(x => x.UserDefinedFieldId == Convert.ToInt32(chkBoxName[1])).FirstOrDefault();

            if (string.IsNullOrEmpty(result.FieldValue))
                result.FieldValue = chkBox.Content.ToString();
            else if (result.FieldValue.Contains(chkBox.Content.ToString()))
            {
                return;
            }
            else
                result.FieldValue += ";" + chkBox.Content.ToString();


        }

        #endregion

        #region DateField

        private void AddCustomDateField(CustomField customField)
        {
            StackPanel stPanel = new StackPanel();
            stPanel.Margin = new Thickness(-5, 10, 0, 0);
            stPanel.Orientation = Orientation.Horizontal;
            stPanel.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            stPanel.VerticalAlignment = System.Windows.VerticalAlignment.Center;
            Label LabelCaption = new Label();
            LabelCaption.Content = customField.FieldCaption;
            LabelCaption.Width = 165;
            TextBox tFieldName = new TextBox();
            tFieldName.Width = 225;
            Binding binding = new Binding();
            binding.Source = customField; // set the source object instead of ElementName
            binding.Mode = BindingMode.TwoWay;
            binding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            binding.Path = new PropertyPath("FieldValue");
            BindingOperations.SetBinding(tFieldName, TextBox.TextProperty, binding);
            DatePicker datepicker = new DatePicker();
            datepicker.Width = 24;
            datepicker.Height = 24;
            datepicker.Margin = new Thickness(0, 0, 0, 0);
            datepicker.Padding = new Thickness(0, 0, 0, 0);
            datepicker.DisplayDateStart = customField.MinDate;
            datepicker.DisplayDateEnd = customField.MaxDate;
            datepicker.SelectedDateChanged += Datepicker_SelectedDateChanged;
            datepicker.Name = "TextDateField_" + customField.UserDefinedFieldId;
            stPanel.Children.Add(LabelCaption);
            stPanel.Children.Add(tFieldName);
            stPanel.Children.Add(datepicker);
            this.CustomFieldPanel.Children.Add(stPanel);

        }

        private void Datepicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            DatePicker picker = (sender as DatePicker);
            string[] pickerName = picker.Name.Split('_');

            CustomField result = _userDefinedFieldList.Where(x => x.UserDefinedFieldId == Convert.ToInt32(pickerName[1])).FirstOrDefault();
            result.FieldValue = String.Format("{0:G}", picker.SelectedDate.Value.Date.Add(DateTime.Now.TimeOfDay));//.ToString();
        }

        #endregion

        #region Dropdown List

        private void AddCustomDropdownListField(CustomField customField)
        {
            if (!string.IsNullOrEmpty(customField.PickListValues))
            {
                string[] content = customField.PickListValues.Split(';');
                StackPanel stPanel = new StackPanel();
                stPanel.Margin = new Thickness(-5, 10, 0, 0);
                stPanel.Orientation = Orientation.Horizontal;
                stPanel.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
                stPanel.VerticalAlignment = System.Windows.VerticalAlignment.Center;

                Label LabelCaption = new Label();
                LabelCaption.Content = customField.FieldCaption;
                LabelCaption.Width = 165;
                stPanel.Children.Add(LabelCaption);

                ComboBox comboBoxField = new ComboBox();
                comboBoxField.SelectionChanged += ComboBoxField_SelectionChanged;
                comboBoxField.Name = "ComboBoxField_" + customField.UserDefinedFieldId;
                comboBoxField.Width = 250;


                foreach (var item in content)
                {
                    if (!string.IsNullOrWhiteSpace(item))
                    {
                        ComboBoxItem cmboBoxFieldItem = new ComboBoxItem();
                        cmboBoxFieldItem.Content = item.Trim();
                        cmboBoxFieldItem.Name = "ComboItemField_" + customField.UserDefinedFieldId;
                        if (!string.IsNullOrEmpty(customField.FieldValue))
                            if (customField.FieldValue.Trim().Contains(cmboBoxFieldItem.Content.ToString().Trim()))
                                cmboBoxFieldItem.IsSelected = true;

                        comboBoxField.Items.Add(cmboBoxFieldItem);

                    }
                }

                stPanel.Children.Add(comboBoxField);
                this.CustomFieldPanel.Children.Add(stPanel);
            }
        }

        private void ComboBoxField_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            ComboBox combobox = (sender as ComboBox);
            ComboBoxItem chkBox = (ComboBoxItem)combobox.SelectedItem;
            if (chkBox != null)
            {
                string[] chkBoxName = chkBox.Name.Split('_');
                CustomField result = _userDefinedFieldList.Where(x => x.UserDefinedFieldId == Convert.ToInt32(chkBoxName[1])).FirstOrDefault();
                if (!string.IsNullOrEmpty(result.PickListValues))
                {
                    string[] res = result.PickListValues.Split(';');
                    res = res.Where(x => x.Trim() == chkBox.Content.ToString().Trim()).ToArray();
                    result.FieldValue = String.Join(";", res);
                }
            }

        }

        #endregion

        #endregion

        private void btnSubmitButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                userDefinedFieldViewModel = new UserDefinedFieldViewModel();
                var result = userDefinedFieldViewModel.InsertUpdateUserDefinedFieldValues(_userDefinedFieldList.ToList(), Constants.AssetNumber);
                if (result > 0)
                    ModernDialog.ShowMessage("Custom fields Successfully Saved!", "Success", MessageBoxButton.OK);

            }
            catch (Exception ex)
            {
                if (ex.HResult == -2147467259)
                    ModernDialog.ShowMessage("Asset Number already exists, Please try another Number!", "Error!", MessageBoxButton.OK);
                else
                {
                    ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
                }
            }
        }


        internal static int InsertUpdateUserDefinedFieldValues(int AssetID)
        {
            try
            {
                userDefinedFieldViewModel = new UserDefinedFieldViewModel();
                return userDefinedFieldViewModel.InsertUpdateUserDefinedFieldValues(_userDefinedFieldList.ToList(), AssetID);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }
}
