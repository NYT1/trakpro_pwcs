﻿using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using TRAKI.App.Common;
using TRAKI.App.Model;
using TRAKI.App.ViewModel;
using TRAKI.App.ViewModel.Common;

namespace TRAKI.App.Content.AssetContent
{
    /// <summary>
    /// Interaction logic for AssetDetailsFrm.xaml
    /// </summary>
    public partial class ArchivedAssetsFrm : UserControl, IContent
    {
        private long recordCounter = 0;//1;
        private long recordCount = 0;
        private long searchedRecordCount = 0;
        private long minIdValue = 0;
        private long maxIdValue = 0;
        private AssetViewModel assetViewModel;
        List<Asset> _assetList = new List<Asset>();

        public ArchivedAssetsFrm()
        {
            InitializeComponent();
            this.DataContext = new AssetViewModel();
            //  this.Loaded += ArchivedAssetsFrm_Loaded;
        }
 
        private void LoadAssetList(string FilterValue, string SearchText)
        {
            try
            {
                assetViewModel = new AssetViewModel();
                Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                 {
                     this.BusyBar.IsBusy = true;
                 }));

                ThreadStart work = () =>
                 {
                     _assetList = new List<Asset>(assetViewModel.GetAsset(0, null, SqlOperatorValue.GreaterThan, FilterValue, SearchText, Constants.NumberOfRecordsPerPage, -1).ToList());
                     recordCount = assetViewModel.GetAssetRecordCount(string.Empty, FilterValue, -1);
                     Application.Current.Dispatcher.BeginInvoke(new Action(() => CommandManager.InvalidateRequerySuggested()));
                     Application.Current.Dispatcher.Invoke(DispatcherPriority.ContextIdle, new Action(() =>
                  {
                      assetViewModel.RecordCount = recordCount;
                      this.recordCounter = Constants.NumberOfRecordsPerPage;
                      this.SetMaxAndMinIdValue(_assetList);
                      if (_assetList.Count > 0 && _assetList != null)
                      {
                          this.minIdValue = _assetList.Min(asset => asset.AssetId);
                          this.maxIdValue = _assetList.Max(asset => asset.AssetId);
                      }
                      this.DataContext = assetViewModel;
                      this.AssetDataGrid.ItemsSource = CollectionViewSource.GetDefaultView(_assetList);
                      this.AssetDataGrid.Items.Refresh();
                      this.BusyBar.IsBusy = false;
                      if (assetViewModel.RecordCount == 0)
                          this.TextBlockPages.Text = "0";
                      else
                          this.TextBlockPages.Text = "1";
                      //"komal"
                  }));
                 };
                new Thread(work).Start();
            }
            catch (Exception ex)
            {
                
                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        private void LoadFilterList()
        {
            this.ComboFilterBy.ItemsSource = null;
            this.ComboFilterBy.ItemsSource = Constants.AppSettings.Where(setting => setting.SettingItem == "AssetFilterBy").OrderBy(p => p.DisplayOrder);// new SettingsViewModel().GetFilterByList();
            this.ComboFilterBy.Items.Refresh();
            this.ComboFilterBy.SelectedIndex = 0;
        }

        private void ComboFilterBy_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (this.ComboFilterBy.SelectedValue != null)
                {
                    this.recordCount = 0;
                    this.LoadAssetList((string)this.ComboFilterBy.SelectedValue, this.SearchTextBox.Text.Trim());
                }
            }
            catch (Exception ex)
            {
                
                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }

        }

        private void btnViewDetail_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Asset asset = AssetDataGrid.SelectedItem as Asset;
                Constants.AssetNumber = asset.AssetId;
             //   Constants.IsAssetView = true;
                NavigationCommands.GoToPage.Execute("/Content/AssetContent/ViewAssetDetailFrm.xaml#" + asset.AssetId, this);
            }
            catch (Exception ex)
            {
                
                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        private void btnRefreshArchivedAssets_Click(object sender, RoutedEventArgs e)
        {
            this.LoadAssetList((string)this.ComboFilterBy.SelectedValue, string.Empty);
        }

        private void SearchTextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = IsMatching.Check(e.Text);
        }


        #region Search Region

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            string searchText = this.SearchTextBox.Text.Trim();
            if (string.IsNullOrEmpty(searchText))
                return;
            try
            {
                this.LoadSearchData(searchText, (string)this.ComboFilterBy.SelectedValue);
            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage("Some error occured, please try again! " + ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        private void SearchTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            string searchText = ((TextBox)sender).Text.Trim();
            if (!string.IsNullOrEmpty(searchText))
                return;
            try
            {
                this.LoadSearchData(searchText, (string)this.ComboFilterBy.SelectedValue);
            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage("Some error occured, please try again! " + ex.Message, "Error!", MessageBoxButton.OK);
            }

        }

        private void SearchTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                // if user didn't press Enter, do nothing
                if (!e.Key.Equals(Key.Enter)) return;
                else
                {
                    string searchText = ((TextBox)sender).Text.Trim();
                    if (string.IsNullOrEmpty(searchText))
                        return;

                    this.LoadSearchData(searchText, (string)this.ComboFilterBy.SelectedValue);
                }
            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage("Some error occured, please try again! " + ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        private void LoadSearchData(string SearchText, string FilterValue)
        {
            try
            {
                assetViewModel = new AssetViewModel();
                Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                  {
                      this.BusyBar.IsBusy = true;
                  }));

                ThreadStart work = () =>
                {
                    _assetList = new List<Asset>(assetViewModel.GetAsset(0, null, SqlOperatorValue.GreaterThan, FilterValue, SearchText, Constants.NumberOfRecordsPerPage, -1)); // new List<User>(userViewModel.GetUser().ToList());
                    //if (_assetList != null && _assetList.Count == Constants.NumberOfRecordsPerPage)
                     recordCount=  searchedRecordCount = assetViewModel.GetAssetRecordCount(SearchText, FilterValue, -1);
                    Application.Current.Dispatcher.BeginInvoke(new Action(() => CommandManager.InvalidateRequerySuggested()));
                    Application.Current.Dispatcher.Invoke(DispatcherPriority.ContextIdle, new Action(() =>
                     {
                         // else
                         //    assetViewModel.RecordCount = searchedRecordCount = _assetList.Count;
                         assetViewModel.RecordCount = searchedRecordCount;
                         this.recordCounter = Constants.NumberOfRecordsPerPage;
                         this.SetMaxAndMinIdValue(_assetList);
                         this.AssetDataGrid.ItemsSource = CollectionViewSource.GetDefaultView(_assetList);
                         this.DataContext = assetViewModel;
                         this.AssetDataGrid.Items.Refresh();
                         if (assetViewModel.RecordCount == 0)
                             this.TextBlockPages.Text = "0";
                         else
                             this.TextBlockPages.Text = "1";
                         this.BusyBar.IsBusy = false;
                     }));
                };
                new Thread(work).Start();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Pagination

        private void btnNext_Click(object sender, System.EventArgs e)
        {
            this.recordCounter += Constants.NumberOfRecordsPerPage;
            this.LoadAssetData(this.maxIdValue, null, SqlOperatorValue.GreaterThan, (string)this.ComboFilterBy.SelectedValue, this.SearchTextBox.Text.Trim(), false, Constants.NumberOfRecordsPerPage, true);
            //if (_assetList.Count > 0 && _assetList != null)
            //{
            //    this.minIdValue = _assetList.Min(asset => asset.AssetId);
            //    this.maxIdValue = _assetList.Max(asset => asset.AssetId);
            //}
        }

        private void btnPrev_Click(object sender, System.EventArgs e)
        {
            this.recordCounter -= Constants.NumberOfRecordsPerPage;
            assetViewModel = new AssetViewModel();
            List<Asset> minMaxIdValuesForPager = (List<Asset>)assetViewModel.GetMinAndMaxIdValuesForPager(this.minIdValue, (string)this.ComboFilterBy.SelectedValue, this.SearchTextBox.Text.Trim(), false, Constants.NumberOfRecordsPerPage, -1);
            if (minMaxIdValuesForPager.Count > 0 && minMaxIdValuesForPager != null)
            {
                this.minIdValue = minMaxIdValuesForPager.Min(asset => asset.AssetId);
                this.maxIdValue = minMaxIdValuesForPager.Max(asset => asset.AssetId);
            }
            this.LoadAssetData(this.minIdValue, this.maxIdValue, string.Empty, (string)this.ComboFilterBy.SelectedValue, this.SearchTextBox.Text.Trim(), false, Constants.NumberOfRecordsPerPage, false);
        }

        private void btnFirst_Click(object sender, RoutedEventArgs e)
        {
            this.TextBlockPages.Text = "1";
            this.LoadAssetList((string)this.ComboFilterBy.SelectedValue, this.SearchTextBox.Text.Trim());
        }

        private void btnLast_Click(object sender, RoutedEventArgs e)
        {

            var _numberofRecords = Convert.ToInt32(recordCount % Constants.NumberOfRecordsPerPage);

            List<Asset> minMaxIdValuesForPager = (List<Asset>)assetViewModel.GetMinAndMaxIdValuesForPager(this.minIdValue, (string)this.ComboFilterBy.SelectedValue, this.SearchTextBox.Text.Trim(), true, (_numberofRecords > 0) ? _numberofRecords : Constants.NumberOfRecordsPerPage, -1);
            if (minMaxIdValuesForPager.Count > 0 && minMaxIdValuesForPager != null)
            {
                this.minIdValue = minMaxIdValuesForPager.Min(asset => asset.AssetId);
                this.maxIdValue = minMaxIdValuesForPager.Max(asset => asset.AssetId);
            }

            this.LoadAssetData(this.minIdValue, this.maxIdValue, SqlOperatorValue.LessThan, (string)this.ComboFilterBy.SelectedValue, this.SearchTextBox.Text.Trim(), true, (_numberofRecords > 0) ? _numberofRecords : Constants.NumberOfRecordsPerPage, false);
            this.TextBlockPages.Text = Math.Ceiling((this.SearchTextBox.Text.Trim().Length > 0) ? (Convert.ToDecimal(this.searchedRecordCount) / Constants.NumberOfRecordsPerPage) : (this.recordCount / Constants.NumberOfRecordsPerPage)).ToString();
        }

        private void LoadAssetData(long? MinIdValue, long? MaxIdValue, string OperatorValue, string Status, string SearchText, bool GetLastRecords, int NumberOfRecords, bool IsNext)
        {
            assetViewModel = new AssetViewModel();
            Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
          {
              this.BusyBar.IsBusy = true;
          }));

            ThreadStart work = () =>
            {
                _assetList = new List<Asset>(assetViewModel.GetAsset(MinIdValue, MaxIdValue, OperatorValue, Status, SearchText, NumberOfRecords, -1).ToList());
                Application.Current.Dispatcher.BeginInvoke(new Action(() => CommandManager.InvalidateRequerySuggested()));
                Application.Current.Dispatcher.Invoke(DispatcherPriority.ContextIdle, new Action(() =>
              {
                  if (GetLastRecords)
                      this.recordCounter = this.recordCount;
                  if (IsNext)
                  {
                      if (_assetList.Count > 0 && _assetList != null)
                      {
                          this.minIdValue = _assetList.Min(asset => asset.AssetId);
                          this.maxIdValue = _assetList.Max(asset => asset.AssetId);
                      }
                  }
                  this.SetMaxAndMinIdValue(_assetList);
                  this.AssetDataGrid.ItemsSource = CollectionViewSource.GetDefaultView(_assetList);
                  this.AssetDataGrid.Items.Refresh();
                  this.BusyBar.IsBusy = false;
              }));
            };
            new Thread(work).Start();
        }

        private void SetMaxAndMinIdValue(List<Asset> AssetList)
        {
            long _recordCount = 0;
            if (this.SearchTextBox.Text.Trim().Length > 0)
            {
                _recordCount = searchedRecordCount;
            }
            else
            {
                _recordCount = recordCount;
            }
            if (this.recordCounter >= _recordCount)
            {
                this.btnNext.IsEnabled = false;
                this.btnLast.IsEnabled = false;
                this.btnFirst.IsEnabled = true;
                this.btnPrev.IsEnabled = true;
            }

            if (_recordCount <= Constants.NumberOfRecordsPerPage)
            {
                this.btnNext.IsEnabled = false;
                this.btnPrev.IsEnabled = false;
                this.btnFirst.IsEnabled = false;
                this.btnLast.IsEnabled = false;
            }

            if (this.recordCounter <= Constants.NumberOfRecordsPerPage)
            {
                this.btnPrev.IsEnabled = false;
                this.btnFirst.IsEnabled = false;
            }

            if (this.recordCounter > Constants.NumberOfRecordsPerPage)
            {
                this.btnPrev.IsEnabled = true;
                this.btnFirst.IsEnabled = true;
            }

            if (_recordCount > this.recordCounter)
            {
                this.btnNext.IsEnabled = true;
                this.btnLast.IsEnabled = true;
            }
            this.TextBlockPages.Text = Math.Ceiling(Convert.ToDecimal(this.recordCounter) / Constants.NumberOfRecordsPerPage).ToString();
        }

        #endregion

        #region IContent

        public void OnFragmentNavigation(FirstFloor.ModernUI.Windows.Navigation.FragmentNavigationEventArgs e)
        {
        }

        public void OnNavigatedFrom(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
        }

        public void OnNavigatedTo(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
            // if (e.NavigationType == FirstFloor.ModernUI.Windows.Navigation.NavigationType.New)
            //{
            this.recordCounter = 0;//1;
            this.recordCount = 0;
            this.searchedRecordCount = 0;
            this.minIdValue = 0;
            this.maxIdValue = 0;
            this.LoadFilterList();
            //  }
        }

        public void OnNavigatingFrom(FirstFloor.ModernUI.Windows.Navigation.NavigatingCancelEventArgs e)
        {
        }

        #endregion

       
    }
}

