﻿
using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TRAKI.App.Common;
using TRAKI.App.Content.CommonContent;
using TRAKI.App.Model;
using TRAKI.App.ViewModel;
using TRAKI.App.ViewModel.Common;
using System.Printing;
//using ReachFramework;


namespace TRAKI.App.Content.AssetContent
{
    /// <summary>
    /// Interaction logic for CreateEditAssetFrm.xaml
    /// </summary>
    public partial class CreateEditAssetFrm : UserControl, IContent
    {
        private int buildingId = -1;

        private AssetViewModel assetViewModel;
        private int assetId = -1;
        // private string old_BuildingNumber = string.Empty;
        // private string old_RoomNumber = string.Empty;
        private ObservableCollection<Asset> asset;
        private bool _showConfirmationMsg = true;
        //Steve
        bool _changeInventoryDate = false;
        private DateTime? _inventoryDate;




        public CreateEditAssetFrm()
        {
            InitializeComponent();

            Constants.isforDispcode = "DispAsset";
        }

        #region Private

        private void LoadAssetDetail()
        {
            try
            {
                //104
                DateTime tm1 = DateTime.Now;
                ErrorLogging.LogException("104 ->" + tm1.ToLongTimeString());
                assetViewModel = new AssetViewModel();
                asset = new ObservableCollection<Asset>();
                asset = assetViewModel.GetAssetForEdit(this.assetId);
                //105
                DateTime tm2 = DateTime.Now;
                ErrorLogging.LogException("105 ->" + tm2.ToLongTimeString());
                if (asset[0].AssetId > 0)
                {
                    this.BuilkAddPanel.Visibility = Visibility.Collapsed;
                    this.TextAssetNumber.IsEnabled = false;
                    _inventoryDate = asset[0].InventoryDate;
                }
                else
                {
                    this.BuilkAddPanel.Visibility = Visibility.Visible;
                    this.TextAssetNumber.IsEnabled = true;
                }
                //  this.old_BuildingNumber = asset[0].BuildingNumber;
                // this.old_RoomNumber = asset[0].RoomNumber;
                this.buildingId = asset[0].BuildingId;
                this.DataContext = asset;// assetViewModel; 
                //106
                DateTime tm3 = DateTime.Now;
                ErrorLogging.LogException("106 ->" + tm3.ToLongTimeString());

            }
            catch (Exception ex)
            {

                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        private void btnSelectCatalog_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                CatalogPopUpDialog catalogPopUpDialog = new CatalogPopUpDialog();
                catalogPopUpDialog.Buttons = new Button[] { catalogPopUpDialog.OkButton, catalogPopUpDialog.CancelButton };
                catalogPopUpDialog.ShowDialog();
                if (catalogPopUpDialog.DialogResult.Value)
                {
                    if (!string.IsNullOrEmpty(catalogPopUpDialog.CatalogDialogResult))
                    {
                        string[] result = catalogPopUpDialog.CatalogDialogResult.Split('•');
                        this.TextCatalogNumber.Text = result[0];
                        this.TextCatalogDescription.Text = result[1];
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                throw ex;
            }
        }

        private void btnSelectBuilding_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                BuildingPopUpDialog buildingPopUpDialog = new BuildingPopUpDialog();
                buildingPopUpDialog.Buttons = new Button[] { buildingPopUpDialog.OkButton, buildingPopUpDialog.CancelButton };
                buildingPopUpDialog.ShowDialog();
                if (buildingPopUpDialog.DialogResult.Value)
                {
                    if (!string.IsNullOrEmpty(buildingPopUpDialog.BuildingDialogResult))
                    {
                        string[] result = buildingPopUpDialog.BuildingDialogResult.Split('•');
                        this.TextBuildingNumber.Text = result[0];
                        this.TextBuildingDescription.Text = result[1];
                        this.buildingId = Convert.ToInt32(result[2]);
                        this.TextRoomNumber.Clear();
                        this.TextRoomDescription.Clear();
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                throw ex;
            }
        }

        private void btnSelectRoom_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(this.TextBuildingNumber.Text.Trim()))
                {
                    ModernDialog.ShowMessage("Please select building first!", "Information", MessageBoxButton.OK);
                    return;
                }
                RoomPopUpDialog roomPopUpDialog = new RoomPopUpDialog(this.buildingId);
                roomPopUpDialog.Buttons = new Button[] { roomPopUpDialog.OkButton, roomPopUpDialog.CancelButton };
                roomPopUpDialog.ShowDialog();
                if (roomPopUpDialog.DialogResult.Value)
                {
                    if (!string.IsNullOrEmpty(roomPopUpDialog.RoomDialogResult))
                    {
                        string[] result = roomPopUpDialog.RoomDialogResult.Split('•');
                        this.TextRoomNumber.Text = result[0];
                        this.TextRoomDescription.Text = result[1];
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                throw ex;
            }

        }

        private void btnSelectManufacturer_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ManufacturerPopUpDialog manufacturerPopUpDialog = new ManufacturerPopUpDialog();
                manufacturerPopUpDialog.Buttons = new Button[] { manufacturerPopUpDialog.OkButton, manufacturerPopUpDialog.CancelButton };
                manufacturerPopUpDialog.ShowDialog();
                if (manufacturerPopUpDialog.DialogResult.Value)
                {
                    if (!string.IsNullOrEmpty(manufacturerPopUpDialog.ManufacturerDialogResult))
                    {
                        string[] result = manufacturerPopUpDialog.ManufacturerDialogResult.Split('•');
                        this.TextManufacturerCode.Text = result[0];
                        this.TextManufacturerName.Text = result[1];
                    }
                }
            }
            catch (Exception ex)
            {

                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                throw ex;
            }
        }

        private void btnPreviousLocation_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Asset asset = ((ObservableCollection<Asset>)(sender as Button).DataContext)[0];
                PreviousLocationPopUpDialog previousLocationPopUpDialog = new PreviousLocationPopUpDialog(asset.AssetId);
                previousLocationPopUpDialog.MinWidth = 900;
                previousLocationPopUpDialog.Buttons = new Button[] { previousLocationPopUpDialog.CloseButton };
                previousLocationPopUpDialog.ShowDialog();
            }
            catch (Exception ex)
            {

                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                throw ex;
            }
        }

        private void btnSelectDepartment_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DepartmentPopUpDialog departmentPopUpDialog = new DepartmentPopUpDialog();
                departmentPopUpDialog.Buttons = new Button[] { departmentPopUpDialog.OkButton, departmentPopUpDialog.CancelButton };
                departmentPopUpDialog.ShowDialog();
                if (departmentPopUpDialog.DialogResult.Value)
                {
                    string[] result = departmentPopUpDialog.DepartmentDialogResult.Split('•');
                    this.TextDepartmentNumber.Text = result[0];
                    this.TextDepartmentDescription.Text = result[1];
                }
            }
            catch (Exception ex)
            {

                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                throw ex;
            }
        }

        private void btnSelectDisposition_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Constants.isforDispcode = "DispAsset";
                DispositionPopUpDialog dispositionPopUpDialog = new DispositionPopUpDialog();
                dispositionPopUpDialog.Buttons = new Button[] { dispositionPopUpDialog.OkButton, dispositionPopUpDialog.CancelButton };
                //Steve
                //don't make ok button as default. Otherwise, exit out screen press enter 
                dispositionPopUpDialog.OkButton.IsDefault = false;
                // dispositionPopUpDialog.
                //End here
                dispositionPopUpDialog.ShowDialog();
                if (dispositionPopUpDialog.DialogResult.Value)
                {
                    if (dispositionPopUpDialog.DispositionDialogResult != null)
                    {
                        string[] result = dispositionPopUpDialog.DispositionDialogResult.Split('•');
                        if (result[0] == "CLEAR DISPOSITION CODE")
                        {

                            this.TextDispositionCode.Text = "";
                            this.TextDispositionDescription.Text = "";
                        }
                        else
                        {
                            this.TextDispositionCode.Text = result[0];
                            this.TextDispositionDescription.Text = result[1];
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                throw ex;
            }
        }

        private void TextAssetNumber_LostFocus(object sender, RoutedEventArgs e)
        {
            Asset _asset = ((ObservableCollection<Asset>)(sender as TextBox).DataContext)[0];
            if (string.IsNullOrEmpty(_asset.AssetNumber) || _asset.AssetId > 0 || this.assetId == -1)
                return;
            try
            {
                assetViewModel = new AssetViewModel();
                if (assetViewModel.CheckAssetNumber(_asset.AssetNumber) > 0)
                {
                    var result = ModernDialog.ShowMessage("Asset Number already exists, Please try another Number!", "Error!", MessageBoxButton.OK);
                    return;
                }
            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage("Some error occured, please try again! " + ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        private void btnSubmitButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //107
                DateTime tm1 = DateTime.Now;
                ErrorLogging.LogException("107 ->" + tm1.ToLongTimeString());

                bool isLocationChanged = false;
                Asset _asset = ((ObservableCollection<Asset>)(sender as Button).DataContext)[0];

                //108
                DateTime tm2 = DateTime.Now;
                ErrorLogging.LogException("108 ->" + tm2.ToLongTimeString());

                //Steve
                if (_changeInventoryDate)
                {
                    _asset.IsMissing = BooleanValue.False;
                    _asset.Status = AssetStatus.Active.ToString();
                }
                else
                {
                    if (_asset.IsMissing == BooleanValue.True)
                        _asset.Status = AssetStatus.Missing.ToString();
                }

                //if (_asset.PrevRoomNumber != null && this.old_BuildingNumber != null)
                if ((_asset.BuildingNumber != _asset.PrevBuildingNumber) || (_asset.RoomNumber != _asset.PrevRoomNumber))
                {
                    _asset.AssetRelocated = BooleanValue.True;
                    isLocationChanged = true;
                }

                string message = "some error occured please try again!"; ;
                int result = 0;
                if (_asset.AssetId == 0)
                {
                    string temp = this.TextAssetNumber.Text.Trim();
                    //108a
                    DateTime tm21 = DateTime.Now;
                    ErrorLogging.LogException("108a ->" + tm21.ToLongTimeString());
                    _asset.AssetId = result = assetViewModel.InsertAsset(_asset);
                    //108b
                    DateTime tm22 = DateTime.Now;
                    ErrorLogging.LogException("108b ->" + tm22.ToLongTimeString());
                    message = "Asset Successfully Added";
                }
                else
                {
                    //108c
                    DateTime tm21 = DateTime.Now;
                    ErrorLogging.LogException("108c ->" + tm21.ToLongTimeString());
                    result = assetViewModel.UpdateAsset(_asset, isLocationChanged, _changeInventoryDate);
                    //108d
                    DateTime tm22 = DateTime.Now;
                    ErrorLogging.LogException("108d ->" + tm22.ToLongTimeString());
                    message = "Asset Successfully updated";
                }

                //109
                DateTime tm3 = DateTime.Now;
                ErrorLogging.LogException("109 ->" + tm3.ToLongTimeString());

                UserDefinedFieldsFrm.InsertUpdateUserDefinedFieldValues(_asset.AssetId);

                //110
                DateTime tm4 = DateTime.Now;
                ErrorLogging.LogException("110 ->" + tm4.ToLongTimeString());

                ModernDialog.ShowMessage(message, "Success", MessageBoxButton.OK);
                if (_asset.IsBulkAdd == BooleanValue.True)
                {
                    asset[0].AssetId = 0;
                    asset[0].AssetNumber = string.Empty;
                    asset[0].SerialNumber = string.Empty;
                    //asset[0].DateAcquired = null;
                    asset[0].WarrantyExpiryDate = null;
                    asset[0].DepartmentNumber = string.Empty;
                    asset[0].DepartmentDescription = string.Empty;
                    asset[0].Invoice = string.Empty;
                    asset[0].DispositionCode = string.Empty;
                    asset[0].DispositionDescription = string.Empty;
                    asset[0].AssetRelocated = BooleanValue.False;
                    //asset[0].ModelNumber = string.Empty;
                    asset[0].IsMissing = BooleanValue.False;
                    asset[0].IsActive = BooleanValue.True;
                    this.TextAssetNumber.Focus();
                    //111
                    DateTime tm5 = DateTime.Now;
                    ErrorLogging.LogException("111 ->" + tm5.ToLongTimeString());
                }
                else
                {
                    //112
                    DateTime tm6 = DateTime.Now;
                    ErrorLogging.LogException("112 ->" + tm6.ToLongTimeString());
                    this._showConfirmationMsg = false;
                    NavigationCommands.GoToPage.Execute("/Content/AssetContent/AssetDetailsFrm.xaml#" + null, this);
                    //112b
                    DateTime tm7 = DateTime.Now;
                    ErrorLogging.LogException("112b ->" + tm7.ToLongTimeString());
                }
            }
            catch (Exception ex)
            {
                if (ex.HResult == -2147467259)
                    ModernDialog.ShowMessage("Asset Number already exists, Please try another Number!", "Error!", MessageBoxButton.OK);
                else
                {
                    ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
                }
            }

        }

        private void dtPickerDateAcquired_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {

            string dateAcquider = String.Format("{0:G}", this.dtPickerDateAcquired.SelectedDate.Value.Date.Add(DateTime.Now.TimeOfDay));//.ToString();
            asset[0].DateAcquired = DateTime.Parse(dateAcquider);
        }

        private void dtPickerWarrantyExpiryDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            string dateAcquider = String.Format("{0:G}", this.dtPickerWarrantyExpiryDate.SelectedDate.Value.Date.Add(DateTime.Now.TimeOfDay));//.ToString();
            asset[0].WarrantyExpiryDate = DateTime.Parse(dateAcquider);
        }

        private void btnChangeBarcode_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var content = new AssetBarcodeDialog(this.assetId);
                ModernDialog dialog = new ModernDialog();
                dialog.Title = "Change Barcode";
                dialog.CloseButton.Content = "Close";
                dialog.Content = content;
                dialog.Buttons = new Button[] { dialog.CloseButton };
                dialog.ShowDialog();

                if (dialog.DialogResult.Value == false)
                {
                    this.LoadAssetDetail();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void TextAssetNumber_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = IsMatching.Check(e.Text);

        }

        private void TextModelNumber_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            // e.Handled = IsMatching.Check(e.Text);

        }

        private void TextInvoice_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = IsMatching.Check(e.Text);
        }

        #endregion


        #region IContent

        public void OnFragmentNavigation(FirstFloor.ModernUI.Windows.Navigation.FragmentNavigationEventArgs e)
        {
            this.assetId = 0;
            if (!string.IsNullOrEmpty(e.Fragment))
            {
                this.assetId = Convert.ToInt32(e.Fragment);
            }
            // this.assetId = Constants.AssetId;
            Constants.AssetNumber = this.assetId;
            this.LoadAssetDetail();
        }

        public void OnNavigatedFrom(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
            //  throw new NotImplementedException();
        }

        public void OnNavigatedTo(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
            this._showConfirmationMsg = true;
            // this.assetId = Constants.AssetId;
            // this.LoadAssetDetail();
        }

        public void OnNavigatingFrom(FirstFloor.ModernUI.Windows.Navigation.NavigatingCancelEventArgs e)
        {
            if (this._showConfirmationMsg)
            {
                string message = "Are you sure you want to leave?";
                if (e.NavigationType == FirstFloor.ModernUI.Windows.Navigation.NavigationType.Refresh)
                {
                    message = "Are you sure you want to refresh the page?";
                }
                if (e.NavigationType == FirstFloor.ModernUI.Windows.Navigation.NavigationType.Back)
                {
                    this.assetId = -1;
                }
                var confirm = ModernDialog.ShowMessage(message, "Warning!", MessageBoxButton.YesNo);
                if (confirm == MessageBoxResult.No)
                    e.Cancel = true;
            }
        }

        #endregion

        private void TextModelName_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {

        }

        //Steve
        private void dtPickerInventiryDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {



            bool doit = false;
            string dateAcquider = String.Format("{0:G}", this.dtPickerInventiryDate.SelectedDate.Value.Date.Add(DateTime.Now.TimeOfDay));//.ToString();

            //Steve
            //only for edit asset
            //if (asset[0].AssetId > 0)
            //{
            //    //Steve
            //    //only when field is populated with date info
            //    if (_inventoryDate != null)
            //    {
            //        //Steve
            //        //if picked date is eariler than current inventor date
            //        //should ask user
            if (DateTime.Parse(dateAcquider) < _inventoryDate)
            {
                var result = ModernDialog.ShowMessage("The date you selected is eariler than current inventory date, do you want to continue?", "Info!", MessageBoxButton.YesNo);
                if (result == MessageBoxResult.Yes)
                {
                    doit = true;
                }

            }
            else
            {
                doit = true;
            }

            //}
            //else
            //{
            //    //Steve
            //    //do it anyway if current inventory is null
            //    doit = true;
            //}

            if (doit)
            {
                asset[0].InventoryDate = DateTime.Parse(dateAcquider);
                asset[0].Status = "Active";
                _changeInventoryDate = true;
            }

            //bool doit = false;
            //string dateAcquider = String.Format("{0:G}", this.dtPickerInventiryDate.SelectedDate.Value.Date.Add(DateTime.Now.TimeOfDay));//.ToString();

            ////Steve
            ////only for edit asset
            //if (asset[0].AssetId > 0)
            //{
            //    //Steve
            //    //only when field is populated with date info
            //    if (_inventoryDate != null)
            //    {
            //        //Steve
            //        //if picked date is eariler than current inventor date
            //        //should ask user
            //        if (DateTime.Parse(dateAcquider) < _inventoryDate)
            //        {
            //            var result = ModernDialog.ShowMessage("The date you selected is eariler than current inventory date, do you want to continue?", "Info!", MessageBoxButton.YesNo);
            //            if (result == MessageBoxResult.Yes)
            //            {
            //                doit = true;
            //            }

            //        }

            //    }
            //    else
            //    {
            //        //Steve
            //        //do it anyway if current inventory is null
            //        doit = true;
            //    }
            //    if (doit)
            //    {
            //        asset[0].InventoryDate = DateTime.Parse(dateAcquider);
            //        asset[0].Status = "Active";
            //        _changeInventoryDate = true;
            //    }
            //}
        }
        private void btnPrint_Click(object sender, RoutedEventArgs e)
        {
            PrintDialog printDlg = new PrintDialog();
            var result = printDlg.ShowDialog();
            if (result == true)
            {
                printDlg.PrintTicket.PageOrientation = PageOrientation.Landscape;
                printDlg.PrintVisual(this, "Asset Details");
            }



        }








    }
}
