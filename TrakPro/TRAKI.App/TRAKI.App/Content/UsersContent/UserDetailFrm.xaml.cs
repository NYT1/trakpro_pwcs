﻿using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TRAKI.App.Common;
using TRAKI.App.DapperQueries.UserQueries;
using TRAKI.App.Model;
using TRAKI.App.Pages;
using TRAKI.App.ViewModel;
using TRAKI.App.ViewModel.Common;

namespace TRAKI.App.Content.UsersContent
{
    /// <summary>
    /// Interaction logic for GlossaryDetail.xaml
    /// </summary>
    /// 
    //[PrincipalPermission(SecurityAction.Demand, Role = "Administrator")]
    public partial class UserDetailFrm : UserControl, IContent
    {
        int pageIndex = 1;
        private UserViewModel userViewModel;
        List<User> _userList = new List<User>();

        public UserDetailFrm()
        {
            InitializeComponent();
        }

        #region Private

        private void LoadUserList()
        {
            try
            {
                userViewModel = new UserViewModel();
                _userList = new List<User>(userViewModel.GetUser(null, Convert.ToInt32(this.ComboRole.SelectedValue)).ToList());
                this.UserGrid.ItemsSource = CollectionViewSource.GetDefaultView(_userList.Take(Constants.NumberOfRecordsPerPage));
                this.UserGrid.Items.Refresh();
                if (userViewModel.RecordCount == 0)
                    this.TextBlockPages.Text = "0";
                else
                    this.TextBlockPages.Text = "1";
                this.Navigate((int)PagingMode.First);
                this.DataContext = userViewModel;
            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage("Some error occured, please try again! " + ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        private void LoadFilterList()
        {
            UserRole newRole = new UserRole();
            newRole.Name = "All";
            newRole.RoleId = 0;
            var roleList = new RoleViewModel();
            roleList.RoleList.Insert(0, newRole);
            this.ComboRole.DataContext = roleList;
            this.ComboRole.SelectedIndex = 0;
        }

        private void btnEditUser_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                User user = UserGrid.SelectedItem as User;
                NavigationCommands.GoToPage.Execute("/Content/UsersContent/CreateEditUserFrm.xaml#" + user.UserId, this);
            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        private void btnDeleteUser_Click(object sender, RoutedEventArgs e)
        {

            try
            {
                var confirm = ModernDialog.ShowMessage("Are you sure you want to delete the record?", "Delete", MessageBoxButton.YesNo);
                if (confirm == MessageBoxResult.Yes)
                {
                    User user = UserGrid.SelectedItem as User;
                    var result = userViewModel.DeleteExistingUser(user.UserId);
                    if (result > 0)
                    {
                        ModernDialog.ShowMessage("User successfully Deleted", "Success", MessageBoxButton.OK);
                        userViewModel = new UserViewModel();
                        _userList = new List<User>(userViewModel.GetUser(null, Convert.ToInt32(this.ComboRole.SelectedValue)).ToList());
                        this.UserGrid.ItemsSource = _userList.Take(Constants.NumberOfRecordsPerPage);
                        UserGrid.Items.Refresh();
                        this.Navigate((int)PagingMode.First);
                    }
                    else
                    {
                        ModernDialog.ShowMessage("Error while deleting the record!", "Error!", MessageBoxButton.OK);
                    }
                }
            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }


        }

        private void btnCreateNewUser_Click(object sender, RoutedEventArgs e)
        {
            NavigationCommands.GoToPage.Execute("/Content/UsersContent/CreateEditUserFrm.xaml#" + -1, this);
        }

        private void ComboRole_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (this.ComboRole.SelectedValue != null)
                {
                    this.LoadSearchData(this.SearchTextBox.Text.Trim());
                }
            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage("Some error occured, please try again! " + ex.Message, "Error!", MessageBoxButton.OK);
            }

        }

        private void SearchTextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = IsMatching.Check(e.Text);
        }
        #endregion

        #region Search Region

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string searchText = this.SearchTextBox.Text.Trim();
                if (string.IsNullOrEmpty(searchText))
                    return;
                this.LoadSearchData(searchText);
            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        private void SearchTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                string searchText = ((TextBox)sender).Text.Trim();
                if (!string.IsNullOrEmpty(searchText))
                    return;
                this.LoadSearchData(searchText);
            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage("Some error occured, please try again! " + ex.Message, "Error!", MessageBoxButton.OK);
            }

        }

        private void SearchTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                string searchText = ((TextBox)sender).Text.Trim();
                if (string.IsNullOrEmpty(searchText))
                    return;
                if (e.Key == Key.Enter)
                    this.LoadSearchData(searchText);
            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage("Some error occured, please try again! " + ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        private void LoadSearchData(string SearchText)
        {
            try
            {
                userViewModel = new UserViewModel();
                _userList = new List<User>(userViewModel.SearchUser(SearchText, Convert.ToInt32(this.ComboRole.SelectedValue))); // new List<User>(userViewModel.GetUser().ToList());
                this.UserGrid.ItemsSource = CollectionViewSource.GetDefaultView(_userList.Take(Constants.NumberOfRecordsPerPage));
                this.UserGrid.Items.Refresh();
                if (userViewModel.RecordCount == 0)
                    this.TextBlockPages.Text = "0";
                else
                    this.TextBlockPages.Text = "1";
                this.Navigate((int)PagingMode.First);
                this.DataContext = userViewModel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Pagination

        private void btnFirst_Click(object sender, System.EventArgs e)
        {

            Navigate((int)PagingMode.First);
            this.TextBlockPages.Text = "1";
        }

        private void btnNext_Click(object sender, System.EventArgs e)
        {
            Navigate((int)PagingMode.Next);
            this.TextBlockPages.Text = this.pageIndex.ToString();
        }

        private void btnPrev_Click(object sender, System.EventArgs e)
        {
            Navigate((int)PagingMode.Previous);
            this.TextBlockPages.Text = this.pageIndex.ToString();
        }

        private void btnLast_Click(object sender, System.EventArgs e)
        {
            Navigate((int)PagingMode.Last);
            this.TextBlockPages.Text = this.pageIndex.ToString();
        }

        //private void cbNumberOfRecords_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    Navigate((int)PagingMode.PageCountChange);
        //}

        private void Navigate(int mode)
        {
            int count;
            switch (mode)
            {
                case (int)PagingMode.Next:
                    btnPrev.IsEnabled = true;
                    btnFirst.IsEnabled = true;
                    if (_userList.Count >= (pageIndex * Constants.NumberOfRecordsPerPage))
                    {

                        if (_userList.Skip(pageIndex * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage).Count() == 0)
                        {
                            this.UserGrid.ItemsSource = null;
                            this.UserGrid.ItemsSource = _userList.Skip((pageIndex * Constants.NumberOfRecordsPerPage) - Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage);
                            count = (pageIndex * Constants.NumberOfRecordsPerPage) + (_userList.Skip(pageIndex * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage)).Count();
                        }
                        else
                        {
                            this.UserGrid.ItemsSource = null;
                            this.UserGrid.ItemsSource = _userList.Skip(pageIndex * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage);
                            count = (pageIndex * Constants.NumberOfRecordsPerPage) + (_userList.Skip(pageIndex * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage)).Count();
                            pageIndex++;
                        }

                        if ((pageIndex * Constants.NumberOfRecordsPerPage) >= _userList.Count)
                        {
                            btnNext.IsEnabled = false;
                            btnLast.IsEnabled = false;
                        }

                    }
                    else
                    {
                        btnNext.IsEnabled = false;
                        btnLast.IsEnabled = false;
                    }

                    break;
                case (int)PagingMode.Previous:
                    btnNext.IsEnabled = true;
                    btnLast.IsEnabled = true;
                    pageIndex -= 1;
                    if (pageIndex >= 1)
                    {
                        this.UserGrid.ItemsSource = null;
                        if (pageIndex == 1)
                        {
                            btnPrev.IsEnabled = false;
                            btnFirst.IsEnabled = false;
                            this.UserGrid.ItemsSource = _userList.Take(Constants.NumberOfRecordsPerPage);
                            count = _userList.Take(Constants.NumberOfRecordsPerPage).Count();
                        }
                        else
                        {
                            this.UserGrid.ItemsSource = _userList.Skip((pageIndex - 1) * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage);
                            count = Math.Min((pageIndex - 1) * Constants.NumberOfRecordsPerPage, _userList.Count);
                        }
                    }
                    else
                    {
                        btnPrev.IsEnabled = false;
                        btnFirst.IsEnabled = false;

                    }

                    if (_userList.Count <= Constants.NumberOfRecordsPerPage)
                    {
                        btnNext.IsEnabled = false;
                        btnLast.IsEnabled = false;
                        btnPrev.IsEnabled = false;
                        btnFirst.IsEnabled = false;
                    }
                    break;

                case (int)PagingMode.First:
                    pageIndex = 2;
                    Navigate((int)PagingMode.Previous);
                    break;
                case (int)PagingMode.Last:
                    pageIndex = (_userList.Count / Constants.NumberOfRecordsPerPage);
                    Navigate((int)PagingMode.Next);
                    break;

                case (int)PagingMode.PageCountChange:
                    pageIndex = 1;
                    this.UserGrid.ItemsSource = null;
                    this.UserGrid.ItemsSource = _userList.Take(Constants.NumberOfRecordsPerPage);
                    count = (_userList.Take(Constants.NumberOfRecordsPerPage)).Count();
                    btnNext.IsEnabled = true;
                    btnPrev.IsEnabled = true;
                    break;
            }


        }

        #endregion

        #region IContent

        public void OnFragmentNavigation(FirstFloor.ModernUI.Windows.Navigation.FragmentNavigationEventArgs e)
        {
            //  throw new NotImplementedException();
        }

        public void OnNavigatedFrom(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
            // throw new NotImplementedException();
        }

        public void OnNavigatedTo(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
            this.LoadFilterList();
            // this.LoadUserList();

        }

        public void OnNavigatingFrom(FirstFloor.ModernUI.Windows.Navigation.NavigatingCancelEventArgs e)
        {
            // throw new NotImplementedException();
        }

        #endregion

    }
}
