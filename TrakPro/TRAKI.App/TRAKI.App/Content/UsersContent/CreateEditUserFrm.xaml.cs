﻿using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TRAKI.App.Common;
using TRAKI.App.DapperQueries.UserQueries;
using TRAKI.App.DapperQueries.Common;
using TRAKI.App.Model;
using TRAKI.App.ViewModel;
using System.ComponentModel;
using TRAKI.App.ViewModel.Common;
using FirstFloor.ModernUI.Windows;

namespace TRAKI.App.Content.UsersContent
{
    /// <summary>
    /// Interaction logic for ControlsStylesSampleForm.xaml
    /// </summary>
    public partial class CreateEditUserFrm : UserControl, IContent
    {

        private UserViewModel userViewModel;
        private int UserID = -1;
        private ObservableCollection<User> user;
        private bool _showConfirmationMsg = true;

        public CreateEditUserFrm()
        {

            InitializeComponent();
            this.ComboRole.DataContext = new RoleViewModel();
        }

        #region Private

        private void LoadUserInfo()
        {

            userViewModel = new UserViewModel();
            user = userViewModel.GetUser(this.UserID, null);
            if (this.UserID == -1)
                user[0].RoleId = 2;
            this.DataContext = user;
            ComboRole.SelectedValue = ((ObservableCollection<User>)this.DataContext)[0].RoleId.ToString();
        }

        private void Submit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                User user = ((ObservableCollection<User>)(sender as Button).DataContext)[0];
                user.RoleId = Convert.ToInt32(this.ComboRole.SelectedValue);
                string message = string.Empty;
                int numberOfAllowedUsers = Convert.ToInt32(Constants.LicenseDetail["numberofusers"]);
                if (user.UserId == 0 && numberOfAllowedUsers != -1)
                {
                    int userCount = userViewModel.GetUserCount();
                    if (userCount >= numberOfAllowedUsers)
                    {
                        ModernDialog.ShowMessage("Maximum limit of Allowed users reached, please upgrade you license to be able to add more users", "Error", MessageBoxButton.OK);
                        return;
                    }
                }
                var result = userViewModel.InsertUpdateUser(user);
                if (result == 1)
                {
                    message = "User Successfully Created";
                }
                else if (result == 2)
                {
                    message = "User Successfully updated";
                }
                else
                {
                    message = "some error occured please try again!";
                }
                ModernDialog.ShowMessage(message, "Success", MessageBoxButton.OK);
                this._showConfirmationMsg = false;
                NavigationCommands.GoToPage.Execute("/Content/UsersContent/UserDetailFrm.xaml#" + null, this);

            }
            catch (Exception ex)
            {

                if (ex.HResult == -2147467259)
                {
                    this.TextUserName.Focus();
                    ModernDialog.ShowMessage("Username or EmailID already exists, Please try another username or EmailId!", "Error!", MessageBoxButton.OK);
                }
                else
                {
                    ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);

                }
            }
        }

        private void TextConfirmPassword_PreviewExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            if (e.Command == ApplicationCommands.Copy ||
                          e.Command == ApplicationCommands.Cut ||
                          e.Command == ApplicationCommands.Paste)
            {
                e.Handled = true;
            }
        }

        private void TextFirstName_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = IsMatching.Check(e.Text);
        }

        private void TextLastName_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = IsMatching.Check(e.Text);
        }

        private void TextEmail_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = IsMatching.Check(e.Text);
        }

        private void TextUserName_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = IsMatching.Check(e.Text);
        }

        private void TextPassword_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = IsMatching.Check(e.Text);
        }

        private void TextConfirmPassword_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = IsMatching.Check(e.Text);
        }

        #endregion

        #region IContent

        public void OnFragmentNavigation(FirstFloor.ModernUI.Windows.Navigation.FragmentNavigationEventArgs e)
        {
            this.UserID = -1;
            if (!string.IsNullOrEmpty(e.Fragment))
            {
                this.UserID = Convert.ToInt32(e.Fragment);
            }
            this.LoadUserInfo();
        }

        public void OnNavigatedFrom(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
            //  throw new NotImplementedException();
        }

        public void OnNavigatedTo(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
            this._showConfirmationMsg = true;
        }

        public void OnNavigatingFrom(FirstFloor.ModernUI.Windows.Navigation.NavigatingCancelEventArgs e)
        {

            if (this._showConfirmationMsg)
            {
                string message = "Are you sure you want to leave?";
                if (e.NavigationType == FirstFloor.ModernUI.Windows.Navigation.NavigationType.Refresh)
                {
                    message = "Are you sure you want to refresh the page?";
                }
                var confirm = ModernDialog.ShowMessage(message, "Warning!", MessageBoxButton.YesNo);
                if (confirm == MessageBoxResult.No)
                    e.Cancel = true;
            }

        }

        #endregion
    }
}
