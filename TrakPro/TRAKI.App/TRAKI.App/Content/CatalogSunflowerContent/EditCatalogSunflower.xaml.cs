﻿using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TRAKI.App.Common;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;
using TRAKI.App.ViewModel;


namespace TRAKI.App.Content.CatalogSunflowerContent
{
    /// <summary>
    /// Interaction logic for EditCatalogSunflower.xaml
    /// </summary>
    public partial class EditCatalogSunflower : UserControl, IContent
    {

        private CatalogSunflowerViewModel catalogSunViewModel;
        private ObservableCollection<CatalogSunflower> CatalogSunflowerList;
        private int LineNo;
        private bool _showConfirmationMsg = true;


        public EditCatalogSunflower()
        {
            InitializeComponent();
        }


        #region Private

        private void LoadCatalogInfo()
        {

            CatalogSunflowerList = new ObservableCollection<CatalogSunflower>();
            catalogSunViewModel = new CatalogSunflowerViewModel();
            //this.DataContext = catalogSunViewModel.GetCatalogSunflower(this.LineNo);

            CatalogSunflowerList = catalogSunViewModel.GetCatalogSunflower(this.LineNo); ;
            this.DataContext = CatalogSunflowerList;
        }
        #endregion
        #region IContent

        public void OnFragmentNavigation(FirstFloor.ModernUI.Windows.Navigation.FragmentNavigationEventArgs e)
        {
            this.LineNo = -1;
            if (!string.IsNullOrEmpty(e.Fragment))
            {
                this.LineNo = Convert.ToUInt16(e.Fragment);
            }
            this.LoadCatalogInfo();
        }

        public void OnNavigatedFrom(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
        }

        public void OnNavigatedTo(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
            this._showConfirmationMsg = true;
        }

        public void OnNavigatingFrom(FirstFloor.ModernUI.Windows.Navigation.NavigatingCancelEventArgs e)
        {
            if (this._showConfirmationMsg)
            {
                string message = "Are you sure you want to leave?";
                if (e.NavigationType == FirstFloor.ModernUI.Windows.Navigation.NavigationType.Refresh)
                {
                    message = "Are you sure you want to refresh the page?";
                }
                var confirm = ModernDialog.ShowMessage(message, "Warning!", MessageBoxButton.YesNo);
                if (confirm == MessageBoxResult.No)
                    e.Cancel = true;
            }
        }

        #endregion

        private void TextOrgMfgCode_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {

        }

        private void Submit_Click(object sender, RoutedEventArgs e)
        {

            try
            {
                CatalogSunflower CatalogSunflower = ((ObservableCollection<CatalogSunflower>)(sender as Button).DataContext)[0];
                string message = string.Empty;
                var result = catalogSunViewModel.InsertUpdateCatlog(CatalogSunflower);
                if (result == 1)
                {
                    message = "CatalogSunflower Successfully Created";
                }
                if (result == 2)
                {
                    message = "CatalogSunflower Successfully Updated";
                }
                ModernDialog.ShowMessage(message, "Success", MessageBoxButton.OK);
                this._showConfirmationMsg = false;
                NavigationCommands.GoToPage.Execute("/Content/CatalogSunflowerContent/CatalogSunflowDetailFrm.xaml#" + null, this);
            }

            catch (Exception ex)
            {
                if (ex.HResult == -2147467259)
                {
                    this.TextCatalogKey.Focus();
                    ModernDialog.ShowMessage("Catalog key already exists, Please try another Code!", "Error!", MessageBoxButton.OK);
                }
                else
                {
                    ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
                }
            }
        }

        private void Textmodelname_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {

        }

        private void dtSPickerDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            string SDate = String.Format("{0:G}", this.dtSPickerDate.SelectedDate.Value.Date.Add(DateTime.Now.TimeOfDay));//.ToString();
            CatalogSunflowerList[0].SDate = DateTime.Parse(SDate);
        }

        private void Textoffname_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {

        }
        private void Textmodel_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {

        }
        private void TextCatalogKey_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {

        }
        private void dtCPicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            string dateC = String.Format("{0:G}", this.dtCPicker.SelectedDate.Value.Date.Add(DateTime.Now.TimeOfDay));//.ToString();
            CatalogSunflowerList[0].CDate = DateTime.Parse(dateC);
        }
        private void Textmfgname_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {

        }
        private void TextCatalogKey_LostFocus(object sender, RoutedEventArgs e)
        {

        }
        private void dtMPickerDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            string dateM = String.Format("{0:G}", this.dtMPickerDate.SelectedDate.Value.Date.Add(DateTime.Now.TimeOfDay));//.ToString();
            CatalogSunflowerList[0].MDate = DateTime.Parse(dateM);
        }
        private void dtEPickerDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            string dateE = String.Format("{0:G}", this.dtEPickerDate.SelectedDate.Value.Date.Add(DateTime.Now.TimeOfDay));//.ToString();
            CatalogSunflowerList[0].EDate = DateTime.Parse(dateE);
        }
    }
}
