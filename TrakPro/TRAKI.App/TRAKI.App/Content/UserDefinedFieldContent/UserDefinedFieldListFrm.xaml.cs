﻿using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TRAKI.App.Common;
using TRAKI.App.Model;
using TRAKI.App.ViewModel;
using TRAKI.App.ViewModel.Common;


namespace TRAKI.App.Content.UserDefinedFieldContent
{
    /// <summary>
    /// Interaction logic for UserDefinedFieldListFrm.xaml
    /// </summary>
    public partial class UserDefinedFieldListFrm : UserControl, IContent
    {
        int pageIndex = 1;
        private UserDefinedFieldViewModel userDefinedFieldViewModel;
        ObservableCollection<CustomField> _userDefinedFieldList = new ObservableCollection<CustomField>();
        public UserDefinedFieldListFrm()
        {
            InitializeComponent();
        }

        #region Private

        private void LoadFilterList()
        {
            this.ComboFilterBy.ItemsSource = null;
            this.ComboFilterBy.ItemsSource = Constants.AppSettings.Where(setting => setting.SettingItem == "FilterBy").OrderBy(p => p.DisplayOrder);// new SettingsViewModel().GetFilterByList();
            this.ComboFilterBy.SelectedIndex = 0;
        }

        private void ComboFilterBy_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (this.ComboFilterBy.SelectedValue != null)
                {
                    this.LoadSearchData(this.SearchTextBox.Text.Trim());
                }
            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage("Some error occured, please try again! " + ex.Message, "Error!", MessageBoxButton.OK);
            }

        }


        private void btnEditUserDefinedField_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                CustomField customFIeld = this.UserDefinedFieldGrid.SelectedItem as CustomField;
                NavigationCommands.GoToPage.Execute("/Content/UserDefinedFieldContent/CreateUserDefinedFieldFrm.xaml#" + customFIeld.UserDefinedFieldId, this);
            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }

        }

        private void btnCreateNewCustomField_Click(object sender, RoutedEventArgs e)
        {
            NavigationCommands.GoToPage.Execute("/Content/UserDefinedFieldContent/CreateUserDefinedFieldFrm.xaml#" + 0, this);
        }

        private void SearchTextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = IsMatching.Check(e.Text);

        }
        #endregion

        #region search

        private void SearchTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                string searchText = ((TextBox)sender).Text.Trim();
                if (!string.IsNullOrEmpty(searchText))
                    return;
                this.LoadSearchData(searchText);
            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage("Some error occured, please try again! " + ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            string searchText = this.SearchTextBox.Text.Trim();
            if (string.IsNullOrEmpty(searchText))
                return;
            try
            {
                this.LoadSearchData(searchText);
            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage("Some error occured, please try again! " + ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        private void SearchTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                string searchText = ((TextBox)sender).Text.Trim();
                if (e.Key == Key.Enter)
                    this.LoadSearchData(searchText);
            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage("Some error occured, please try again! " + ex.Message, "Error!", MessageBoxButton.OK);
            }
        }


        private void LoadSearchData(string SearchText)
        {
            try
            {
                userDefinedFieldViewModel = new UserDefinedFieldViewModel();
                _userDefinedFieldList = userDefinedFieldViewModel.SearchCustomField(SearchText, Convert.ToInt32(this.ComboFilterBy.SelectedValue));
                this.UserDefinedFieldGrid.ItemsSource = CollectionViewSource.GetDefaultView(_userDefinedFieldList.Take(Constants.NumberOfRecordsPerPage));
                if (userDefinedFieldViewModel.RecordCount == 0)
                    this.TextBlockPages.Text = "0";
                else
                    this.TextBlockPages.Text = "1";
                this.UserDefinedFieldGrid.Items.Refresh();
                this.Navigate((int)PagingMode.First);
                this.DataContext = userDefinedFieldViewModel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Pagination

        private void btnFirst_Click(object sender, System.EventArgs e)
        {

            Navigate((int)PagingMode.First);
            this.TextBlockPages.Text = "1";
        }

        private void btnNext_Click(object sender, System.EventArgs e)
        {
            Navigate((int)PagingMode.Next);
            this.TextBlockPages.Text = this.pageIndex.ToString();
        }

        private void btnPrev_Click(object sender, System.EventArgs e)
        {
            Navigate((int)PagingMode.Previous);
            this.TextBlockPages.Text = this.pageIndex.ToString();
        }

        private void btnLast_Click(object sender, System.EventArgs e)
        {
            Navigate((int)PagingMode.Last);
            this.TextBlockPages.Text = this.pageIndex.ToString();
        }

        //private void cbNumberOfRecords_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    Navigate((int)PagingMode.PageCountChange);
        //}

        private void Navigate(int mode)
        {
            int count;
            switch (mode)
            {
                case (int)PagingMode.Next:
                    btnPrev.IsEnabled = true;
                    btnFirst.IsEnabled = true;
                    if (_userDefinedFieldList.Count >= (pageIndex * Constants.NumberOfRecordsPerPage))
                    {

                        if (_userDefinedFieldList.Skip(pageIndex * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage).Count() == 0)
                        {
                            this.UserDefinedFieldGrid.ItemsSource = null;
                            this.UserDefinedFieldGrid.ItemsSource = _userDefinedFieldList.Skip((pageIndex * Constants.NumberOfRecordsPerPage) - Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage);
                            count = (pageIndex * Constants.NumberOfRecordsPerPage) + (_userDefinedFieldList.Skip(pageIndex * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage)).Count();
                        }
                        else
                        {
                            this.UserDefinedFieldGrid.ItemsSource = null;
                            this.UserDefinedFieldGrid.ItemsSource = _userDefinedFieldList.Skip(pageIndex * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage);
                            count = (pageIndex * Constants.NumberOfRecordsPerPage) + (_userDefinedFieldList.Skip(pageIndex * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage)).Count();
                            pageIndex++;
                        }

                        if ((pageIndex * Constants.NumberOfRecordsPerPage) >= _userDefinedFieldList.Count)
                        {
                            btnNext.IsEnabled = false;
                            btnLast.IsEnabled = false;
                        }

                    }
                    else
                    {
                        btnNext.IsEnabled = false;
                        btnLast.IsEnabled = false;
                    }

                    break;
                case (int)PagingMode.Previous:
                    btnNext.IsEnabled = true;
                    btnLast.IsEnabled = true;
                    pageIndex -= 1;

                    if (pageIndex >= 1)
                    {
                        this.UserDefinedFieldGrid.ItemsSource = null;
                        if (pageIndex == 1)
                        {
                            btnPrev.IsEnabled = false;
                            btnFirst.IsEnabled = false;
                            this.UserDefinedFieldGrid.ItemsSource = _userDefinedFieldList.Take(Constants.NumberOfRecordsPerPage);
                            count = _userDefinedFieldList.Take(Constants.NumberOfRecordsPerPage).Count();
                        }
                        else
                        {
                            this.UserDefinedFieldGrid.ItemsSource = _userDefinedFieldList.Skip((pageIndex - 1) * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage);
                            count = Math.Min((pageIndex - 1) * Constants.NumberOfRecordsPerPage, _userDefinedFieldList.Count);
                        }
                    }
                    else
                    {
                        btnPrev.IsEnabled = false;
                        btnFirst.IsEnabled = false;
                    }

                    if (_userDefinedFieldList.Count <= Constants.NumberOfRecordsPerPage)
                    {
                        btnNext.IsEnabled = false;
                        btnLast.IsEnabled = false;
                        btnPrev.IsEnabled = false;
                        btnFirst.IsEnabled = false;
                    }



                    break;

                case (int)PagingMode.First:
                    pageIndex = 2;
                    Navigate((int)PagingMode.Previous);
                    break;
                case (int)PagingMode.Last:
                    pageIndex = (_userDefinedFieldList.Count / Constants.NumberOfRecordsPerPage);
                    Navigate((int)PagingMode.Next);
                    break;

                case (int)PagingMode.PageCountChange:
                    pageIndex = 1;
                    this.UserDefinedFieldGrid.ItemsSource = null;
                    this.UserDefinedFieldGrid.ItemsSource = _userDefinedFieldList.Take(Constants.NumberOfRecordsPerPage);
                    count = (_userDefinedFieldList.Take(Constants.NumberOfRecordsPerPage)).Count();
                    btnNext.IsEnabled = true;
                    btnPrev.IsEnabled = true;
                    break;
            }
        }

        #endregion

        #region IContent

        public void OnFragmentNavigation(FirstFloor.ModernUI.Windows.Navigation.FragmentNavigationEventArgs e)
        {
            // throw new NotImplementedException();
        }

        public void OnNavigatedFrom(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
            // throw new NotImplementedException();
        }

        public void OnNavigatedTo(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
            this.LoadFilterList();
        }

        public void OnNavigatingFrom(FirstFloor.ModernUI.Windows.Navigation.NavigatingCancelEventArgs e)
        {
            // throw new NotImplementedException();
        }

        #endregion

        private void btnDeleteUserDefinedField_Click(object sender, RoutedEventArgs e)
        {

            try
            {
                var confirm = ModernDialog.ShowMessage("Are you sure you want to Archive the Custom Field?", "Archive", MessageBoxButton.YesNo);
                if (confirm == MessageBoxResult.Yes)
                {
                    CustomField customField = this.UserDefinedFieldGrid.SelectedItem as CustomField;
                    var result = userDefinedFieldViewModel.DeleteExistingCustomField(customField.UserDefinedFieldId);
                    if (result > 0)
                    {
                        ModernDialog.ShowMessage("User successfully Archived", "Success", MessageBoxButton.OK);
                        this.LoadSearchData(string.Empty);
                    }
                    else
                    {
                        ModernDialog.ShowMessage("Error while Archiving the Custom Field!", "Error!", MessageBoxButton.OK);
                    }
                }
            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

    }
}
