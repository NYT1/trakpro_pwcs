﻿using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TRAKI.App.Common;
using TRAKI.App.Model;
using TRAKI.App.ViewModel;
using TRAKI.App.ViewModel.Common;

namespace TRAKI.App.Content.RoomContent
{
    /// <summary>
    /// Interaction logic for RoomDetailFrm.xaml
    /// </summary>
    public partial class RoomDetailFrm : UserControl, IContent
    {

        int pageIndex = 1;
        private RoomViewModel roomViewModel;
        private string buildingNumber = string.Empty;
        private string BuildingDescription = string.Empty;
        private bool BuildingStatus = true;
        private bool IsActive = true;
        private int BuildingId = 0;
        List<Room> _roomList = new List<Room>();

        public RoomDetailFrm()
        {
            InitializeComponent();

        }
        private void RoomHeader()
        {
            this.lblNumber.Content = this.buildingNumber;
            this.lbldescription.Content = this.BuildingDescription;
            this.lblStatus.Content = this.BuildingStatus;
            this.lblStatus.Content = (!this.BuildingStatus) ? "Closed" : "Open";
            //this.lblStatus.Content = (!this.BuildingStatus) ? "Closed & InActive" : "Open";
            //if (this.lblStatus.Content.ToString().Equals("Closed & InActive") || this.IsActive == false)
            if (this.lblStatus.Content.ToString().Equals("Closed") || this.IsActive == false)
            //{
            //    btnCreateNewRoom.IsEnabled = false;
            //    RoomGrid.IsEnabled = false;
            //}
            //else
            {
                btnCreateNewRoom.IsEnabled = true;
                RoomGrid.IsEnabled = true;
            }
        }

        private void LoadRoomList()
        {
            try
            {
                roomViewModel = new RoomViewModel();
                _roomList = new List<Room>(roomViewModel.GetRoomsByBuilding(this.BuildingId, Convert.ToInt32(this.ComboFilterBy.SelectedValue)).ToList());
                RoomHeader();

                this.DataContext = _roomList;
                this.RoomGrid.ItemsSource = CollectionViewSource.GetDefaultView(_roomList);
                RoomGrid.Items.Refresh();
                if (roomViewModel.RecordCount == 0)
                    this.TextBlockPages.Text = "0";
                else
                    this.TextBlockPages.Text = "1";
                this.Navigate((int)PagingMode.First);
                this.DataContext = roomViewModel;
            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage("Some error occured, please try again! " + ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        private void btnEditRoom_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Room room = RoomGrid.SelectedItem as Room;
                string roomInfo = room.RoomId + "•" + room.BuildingId + "•" + this.buildingNumber + "•" + room.BuildingDescription;
                NavigationCommands.GoToPage.Execute("/Content/RoomContent/CreateEditRoomFrm.xaml#" + roomInfo, this);
            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        //private void btnDeleteRoom_Click(object sender, RoutedEventArgs e)
        //{
        //    try
        //    {
        //        var confirm = ModernDialog.ShowMessage("Are you sure you want to delete the record?", "Delete", MessageBoxButton.YesNo);
        //        if (confirm == MessageBoxResult.Yes)
        //        {
        //            Room room = RoomGrid.SelectedItem as Room;
        //            room.IsActive = BooleanValue.False;
        //            var result = roomViewModel.InsertNewRoom(room);
        //            if (result > 0)
        //            {
        //                ModernDialog.ShowMessage("Room successfully Deleted", "Success", MessageBoxButton.OK);
        //                roomViewModel = new RoomViewModel();
        //                _roomList = new List<Room>(roomViewModel.GetRoomsByBuilding(this.BuildingId).ToList());
        //                this.RoomGrid.ItemsSource = _roomList.Take(Constants.NumberOfRecordsPerPage);
        //                RoomGrid.Items.Refresh();

        //            }
        //            else
        //            {
        //                ModernDialog.ShowMessage("Error while deleting the record!", "Error!", MessageBoxButton.OK);
        //            }
        //        }
        //    }
        //    catch (Exception Ex)
        //    {
        //        ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
        //    }
        //}

        private void btnCreateNewRoom_Click(object sender, RoutedEventArgs e)
        {

            Room RoomRow = ((RoomViewModel)this.DataContext).RoomList.FirstOrDefault();
            string buildingInf = "-1" + "•" + BuildingId + "•" + buildingNumber + "•" + BuildingDescription;
            NavigationCommands.GoToPage.Execute("/Content/RoomContent/CreateEditRoomFrm.xaml#" + buildingInf, this);
        }


        private void LoadFilterList()
        {
            RoomHeader();
            this.ComboFilterBy.ItemsSource = null;
            this.ComboFilterBy.ItemsSource = Constants.AppSettings.Where(setting => setting.SettingItem == "FilterBy").OrderBy(p => p.DisplayOrder); //new SettingsViewModel().GetFilterByList();
            this.ComboFilterBy.SelectedIndex = 0;
        }

        private void ComboFilterBy_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (this.ComboFilterBy.SelectedValue != null)
                {
                    this.LoadSearchData(this.SearchTextBox.Text.Trim());
                }
            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage("Some error occured, please try again! " + ex.Message, "Error!", MessageBoxButton.OK);
            }

        }


        #region Pagination

        private void btnFirst_Click(object sender, System.EventArgs e)
        {

            Navigate((int)PagingMode.First);
            this.TextBlockPages.Text = "1";
        }

        private void btnNext_Click(object sender, System.EventArgs e)
        {
            Navigate((int)PagingMode.Next);
            this.TextBlockPages.Text = this.pageIndex.ToString();
        }

        private void btnPrev_Click(object sender, System.EventArgs e)
        {
            Navigate((int)PagingMode.Previous);
            this.TextBlockPages.Text = this.pageIndex.ToString();
        }

        private void btnLast_Click(object sender, System.EventArgs e)
        {
            Navigate((int)PagingMode.Last);
            this.TextBlockPages.Text = this.pageIndex.ToString();
        }

        //private void cbNumberOfRecords_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    Navigate((int)PagingMode.PageCountChange);
        //}

        private void Navigate(int mode)
        {
            int count;
            switch (mode)
            {
                case (int)PagingMode.Next:
                    btnPrev.IsEnabled = true;
                    btnFirst.IsEnabled = true;
                    if (_roomList.Count >= (pageIndex * Constants.NumberOfRecordsPerPage))
                    {

                        if (_roomList.Skip(pageIndex * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage).Count() == 0)
                        {
                            this.RoomGrid.ItemsSource = null;
                            this.RoomGrid.ItemsSource = _roomList.Skip((pageIndex * Constants.NumberOfRecordsPerPage) - Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage);
                            count = (pageIndex * Constants.NumberOfRecordsPerPage) + (_roomList.Skip(pageIndex * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage)).Count();
                        }
                        else
                        {
                            this.RoomGrid.ItemsSource = null;
                            this.RoomGrid.ItemsSource = _roomList.Skip(pageIndex * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage);
                            count = (pageIndex * Constants.NumberOfRecordsPerPage) + (_roomList.Skip(pageIndex * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage)).Count();
                            pageIndex++;
                        }

                        if ((pageIndex * Constants.NumberOfRecordsPerPage) >= _roomList.Count)
                        {
                            btnNext.IsEnabled = false;
                            btnLast.IsEnabled = false;
                        }

                    }
                    else
                    {
                        btnNext.IsEnabled = false;
                        btnLast.IsEnabled = false;
                    }

                    break;
                case (int)PagingMode.Previous:
                    btnNext.IsEnabled = true;
                    btnLast.IsEnabled = true;
                    pageIndex -= 1;

                    if (pageIndex >= 1)
                    {
                        this.RoomGrid.ItemsSource = null;
                        if (pageIndex == 1)
                        {
                            btnPrev.IsEnabled = false;
                            btnFirst.IsEnabled = false;
                            this.RoomGrid.ItemsSource = _roomList.Take(Constants.NumberOfRecordsPerPage);
                            count = _roomList.Take(Constants.NumberOfRecordsPerPage).Count();
                        }
                        else
                        {
                            this.RoomGrid.ItemsSource = _roomList.Skip((pageIndex - 1) * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage);
                            count = Math.Min((pageIndex - 1) * Constants.NumberOfRecordsPerPage, _roomList.Count);
                        }
                    }
                    else
                    {
                        btnPrev.IsEnabled = false;
                        btnFirst.IsEnabled = false;
                    }

                    if (_roomList.Count <= Constants.NumberOfRecordsPerPage)
                    {
                        btnNext.IsEnabled = false;
                        btnLast.IsEnabled = false;
                        btnPrev.IsEnabled = false;
                        btnFirst.IsEnabled = false;
                    }
                    break;

                case (int)PagingMode.First:
                    pageIndex = 2;
                    Navigate((int)PagingMode.Previous);
                    break;
                case (int)PagingMode.Last:
                    pageIndex = (_roomList.Count / Constants.NumberOfRecordsPerPage);
                    Navigate((int)PagingMode.Next);
                    break;

                case (int)PagingMode.PageCountChange:
                    pageIndex = 1;
                    this.RoomGrid.ItemsSource = null;
                    this.RoomGrid.ItemsSource = _roomList.Take(Constants.NumberOfRecordsPerPage);
                    count = (_roomList.Take(Constants.NumberOfRecordsPerPage)).Count();
                    btnNext.IsEnabled = true;
                    btnPrev.IsEnabled = true;
                    break;
            }
        }

        #endregion

        #region Search Region

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            string searchText = this.SearchTextBox.Text.Trim();
            if (string.IsNullOrEmpty(searchText))
                return;
            try
            {
                this.LoadSearchData(searchText);
            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage("Some error occured, please try again! " + ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        private void SearchTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            string searchText = ((TextBox)sender).Text.Trim();
            if (!string.IsNullOrEmpty(searchText))
                return;
            try
            {
                this.LoadSearchData(searchText);
            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage("Some error occured, please try again! " + ex.Message, "Error!", MessageBoxButton.OK);
            }

        }

        private void SearchTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                string searchText = ((TextBox)sender).Text.Trim();
                if (string.IsNullOrEmpty(searchText))
                    return;
                if (e.Key == Key.Enter)
                    this.LoadSearchData(searchText);
            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage("Some error occured, please try again! " + ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        private void LoadSearchData(string SearchText)
        {
            try
            {
                roomViewModel = new RoomViewModel();
                _roomList = new List<Room>(roomViewModel.SearchRoom(SearchText, this.BuildingId, Convert.ToInt32(this.ComboFilterBy.SelectedValue)));
                this.RoomGrid.ItemsSource = CollectionViewSource.GetDefaultView(_roomList.Take(Constants.NumberOfRecordsPerPage));
                this.RoomGrid.Items.Refresh();
                this.Navigate((int)PagingMode.First);
                if (roomViewModel.RecordCount == 0)
                    this.TextBlockPages.Text = "0";
                else
                    this.TextBlockPages.Text = "1";
                this.DataContext = roomViewModel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        public void OnFragmentNavigation(FirstFloor.ModernUI.Windows.Navigation.FragmentNavigationEventArgs e)
        {
            if (!string.IsNullOrEmpty(e.Fragment))
            {
                var roomInfo = e.Fragment.Split('•');

                this.BuildingId = Convert.ToInt32(roomInfo[0]);
                this.buildingNumber = roomInfo[1];
                this.BuildingDescription = roomInfo[2];
                this.BuildingStatus = Convert.ToBoolean(roomInfo[3]);
            }
            this.LoadFilterList();
        }

        public void OnNavigatedFrom(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
            //  throw new NotImplementedException();
        }

        public void OnNavigatedTo(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
            //this.LoadFilterList();
        }

        public void OnNavigatingFrom(FirstFloor.ModernUI.Windows.Navigation.NavigatingCancelEventArgs e)
        {
            //  throw new NotImplementedException();
        }
    }
}





