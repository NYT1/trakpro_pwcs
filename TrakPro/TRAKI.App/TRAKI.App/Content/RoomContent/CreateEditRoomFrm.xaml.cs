﻿using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TRAKI.App.Common;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;
using TRAKI.App.ViewModel;

namespace TRAKI.App.Content.RoomContent
{
    /// <summary>
    /// Interaction logic for CreateEditRoomFrm.xaml
    /// </summary>
    public partial class CreateEditRoomFrm : UserControl, IContent
    {

        private RoomViewModel roomViewModel;
        private int RoomID = -1;
        private string buildingNumber = string.Empty;
        private string BuildingDescription = string.Empty;
        private int buildingId = 0;
        private bool _showConfirmationMsg = true;

        public CreateEditRoomFrm()
        {
            InitializeComponent();

        }

        #region Private

        private void loadRoomInfo()
        {
            roomViewModel = new RoomViewModel();
            ObservableCollection<Room> room = roomViewModel.GetRooms(this.RoomID);
            room[0].BuildingId = this.buildingId;
            room[0].BuildingNumber = this.buildingNumber;
            room[0].BuildingDescription = this.BuildingDescription;
            this.DataContext = room;
        }

        private void Submit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Room room = ((ObservableCollection<Room>)(sender as Button).DataContext)[0];
                string message = string.Empty;
                var result = roomViewModel.InsertNewRoom(room,this.buildingId.ToString());  //Steve
                if (result == -1)
                {
                    ModernDialog.ShowMessage("Room Number already exists, Please try another number!", "Error!", MessageBoxButton.OK);
                    return;
                }
                if (result == 1)
                {
                    message = "Room Successfully Created";
                }
                if (result == 2)
                {
                    message = "Room Successfully updated";
                }
                ModernDialog.ShowMessage(message, "Success", MessageBoxButton.OK);
                this._showConfirmationMsg = false;
                NavigationCommands.GoToPage.Execute("/Content/RoomContent/RoomDetailFrm.xaml#" + null, this);
            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        private void TextRoomNumber_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = IsMatching.Check(e.Text);
        }

        private void TextRoomDescription_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = IsMatching.Check(e.Text);
        }
        #endregion

        #region IContent

        public void OnFragmentNavigation(FirstFloor.ModernUI.Windows.Navigation.FragmentNavigationEventArgs e)
        {
            this.RoomID = -1;
            if (!string.IsNullOrEmpty(e.Fragment))
            {
                var roomInfo = e.Fragment.Split('•');

                this.RoomID = Convert.ToInt32(roomInfo[0]);
                this.buildingId = Convert.ToInt32(roomInfo[1]);
                this.buildingNumber = roomInfo[2];
                this.BuildingDescription = roomInfo[3];
            }
            this.loadRoomInfo();
        }

        public void OnNavigatedFrom(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
        }

        public void OnNavigatedTo(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
            this._showConfirmationMsg = true;
        }

        public void OnNavigatingFrom(FirstFloor.ModernUI.Windows.Navigation.NavigatingCancelEventArgs e)
        {
            if (this._showConfirmationMsg)
            {
                string message = "Are you sure you want to leave?";
                if (e.NavigationType == FirstFloor.ModernUI.Windows.Navigation.NavigationType.Refresh)
                {
                    message = "Are you sure you want to refresh the page?";
                }
                var confirm = ModernDialog.ShowMessage(message, "Warning!", MessageBoxButton.YesNo);
                if (confirm == MessageBoxResult.No)
                    e.Cancel = true;
            }
        }

        #endregion

    }
}
