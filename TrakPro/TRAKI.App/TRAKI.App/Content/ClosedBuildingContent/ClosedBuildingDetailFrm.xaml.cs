﻿using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TRAKI.App.Common;
using TRAKI.App.DapperQueries.BuildingQueries;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;
using TRAKI.App.ViewModel;

namespace TRAKI.App.Content.ClosedBuildingContent
{
    /// <summary>
    /// Interaction logic for ClosedBuildingDetailFrm.xaml
    /// </summary>
    public partial class ClosedBuildingDetailFrm : UserControl
    {
        List<Building> OpenBuilding;
        List<Building> ClosedBuilding;

        ArrayList OpenIdCollector = new ArrayList();
        ArrayList ClosedIdCollector = new ArrayList();

        //Steve
        //use for reset button
        //Once save, reset doesn't take action
        bool _saved = false;

      //  protected IDbContext dbContext;
      //  public ObservableCollection<Building> BuildingList { get; private set; }
       // List<Building> _buildingList = new List<Building>();

        public ClosedBuildingDetailFrm()
        {
            InitializeComponent();
            Loaded += ClosedBuildingDetailFrm_Loaded;
        }


        #region Private

        void ClosedBuildingDetailFrm_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                LoadOpenList();
                LoadClosedList();
            }
            catch (Exception ex)
            {

                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        private void LoadOpenList()
        {
            ClosedBuildingViewModel closedBuildingViewModel = new ClosedBuildingViewModel();
            this.OpenBuilding = closedBuildingViewModel.GetBuildingbyBuildingStatus(true); // new ObservableCollection<Building>(dbContext.Execute(BuildingQuery)).ToList();
            this.OpenBuildingList.ItemsSource = OpenBuilding;
            //Steve
            foreach (var item in OpenBuilding)
            {
                OpenIdCollector.Add(item.BuildingId);
            }

        }

        private void LoadClosedList()
        {
            ClosedBuildingViewModel closedBuildingViewModel = new ClosedBuildingViewModel();
            this.ClosedBuilding = closedBuildingViewModel.GetBuildingbyBuildingStatus(false); // new ObservableCollection<Building>(dbContext.Execute(BuildingQuery)).ToList();
            this.ClosedBuildingList.ItemsSource = ClosedBuilding;

        }

        private void BtnAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (OpenBuildingList.SelectedIndex == -1)
                {
                    ModernDialog.ShowMessage("Please select an open building to close", "Information", MessageBoxButton.OK);
                }
                else
                {

                
                    foreach (Building item in OpenBuildingList.SelectedItems)
                    {
                        if (ClosedIdCollector.Count > 0)
                        {
                             
                            if (!ClosedIdCollector.Contains(item.BuildingId ))
                            {
                                ClosedIdCollector.Add(item.BuildingId );
                                //Steve
                                _saved = false;
                            }

                        }
                        else
                        {
                            //Steve
                            // data type affect compare
                            ClosedIdCollector.Add(item.BuildingId );
                            //Steve
                            _saved = false;
                        }

                        
                        if (OpenIdCollector.Count > 0)
                        {
                            if (OpenIdCollector.Contains(item.BuildingId))
                            {
                                OpenIdCollector.Remove(item.BuildingId);
                                //Steve
                                _saved = false;
                            }
                        }

                        ClosedBuilding.Add(item);
                        OpenBuilding.RemoveAll(building => building.BuildingId == item.BuildingId);

                    }
                    //var it = OpenBuilding;
                    OpenBuildingList.ItemsSource = null;
                    OpenBuildingList.ItemsSource = OpenBuilding;
                    OpenBuildingList.Items.SortDescriptions.Remove(new System.ComponentModel.SortDescription("BuildingTypeDescription", System.ComponentModel.ListSortDirection.Ascending));
                    OpenBuildingList.Items.Refresh();

                    ClosedBuildingList.ItemsSource = ClosedBuilding;
                    ClosedBuildingList.Items.SortDescriptions.Add(new System.ComponentModel.SortDescription("BuildingTypeDescription", System.ComponentModel.ListSortDirection.Ascending));
                    ClosedBuildingList.Items.Refresh();

                }
            }
            catch (Exception ex)
            {

                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        private void bindnewlist()
        {
            OpenBuildingList.ItemsSource = null;
            OpenBuildingList.ItemsSource = OpenBuilding ;
            //Steve
            OpenBuildingList.Items.SortDescriptions.Add(new System.ComponentModel.SortDescription("BuildingTypeDescription", System.ComponentModel.ListSortDirection.Ascending));
            OpenBuildingList.Items.Refresh();
            ClosedBuildingList.ItemsSource = null;
            ClosedBuildingList.ItemsSource = ClosedBuilding;
            ClosedBuildingList.Items.SortDescriptions.Add(new System.ComponentModel.SortDescription("BuildingTypeDescription", System.ComponentModel.ListSortDirection.Ascending));
            ClosedBuildingList.Items.Refresh();
        }

        private void BtnRemove_Click(object sender, RoutedEventArgs e)
        {

            try
            {
                if (ClosedBuildingList.SelectedIndex == -1)
                {
                    ModernDialog.ShowMessage("Please select a closed building to open", "Information", MessageBoxButton.OK);

                }
                else
                {

                    //Steve
                    _saved = false;
                    foreach (Building item in ClosedBuildingList.SelectedItems)
                    {
                        if (OpenIdCollector.Count > 0)
                        {
                           
                            if (!OpenIdCollector.Contains(item.BuildingId))
                                OpenIdCollector.Add(item.BuildingId);

                        }
                        else
                        {
                            
                            OpenIdCollector.Add(item.BuildingId);
                        }

                        if (ClosedIdCollector.Count > 0)
                        {
                            
                            if (ClosedIdCollector.Contains(item.BuildingId))
                                ClosedIdCollector.Remove(item.BuildingId);

                        }

                        OpenBuilding.Add(item);
                        ClosedBuilding.RemoveAll(building => building.BuildingId == item.BuildingId);

                    }
                    //var it = OpenBuilding;
                    //ClosedBuildingList.ItemsSource = ClosedBuilding;
                    //ClosedBuildingList.Items.SortDescriptions.Remove(new System.ComponentModel.SortDescription("BuildingDescription", System.ComponentModel.ListSortDirection.Ascending));
                    //ClosedBuildingList.Items.Refresh();


                    //OpenBuildingList.ItemsSource = OpenBuilding;
                    //OpenBuildingList.Items.SortDescriptions.Add(new System.ComponentModel.SortDescription("BuildingDescription", System.ComponentModel.ListSortDirection.Ascending));
                    //OpenBuildingList.Items.Refresh();


                    //--Komal Made changes--07/18/2018

                    var it = OpenBuilding;
                    ClosedBuildingList.ItemsSource = ClosedBuilding;
                    ClosedBuildingList.Items.SortDescriptions.Remove(new System.ComponentModel.SortDescription("BuildingDescription", System.ComponentModel.ListSortDirection.Ascending));
                    ClosedBuildingList.Items.Refresh();
                    ClosedBuildingList.Items.SortDescriptions.Remove(new System.ComponentModel.SortDescription("BuildingTypeDescription", System.ComponentModel.ListSortDirection.Ascending));
                    ClosedBuildingList.Items.Refresh();


                    OpenBuildingList.ItemsSource = OpenBuilding;
                    OpenBuildingList.Items.SortDescriptions.Add(new System.ComponentModel.SortDescription("BuildingTypeDescription", System.ComponentModel.ListSortDirection.Ascending));
                    OpenBuildingList.Items.Refresh();

                }
            }
            catch (Exception ex)
            {

                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }

        }

        private void BtnSelectAll_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (OpenBuildingList.Items.Count <= 0)
                {
                    ModernDialog.ShowMessage("No Building found", "Information", MessageBoxButton.OK);

                }
                foreach (Building item in OpenBuildingList.Items)
                {
                    if (ClosedIdCollector.Count > 0)
                    {
                        if (!ClosedIdCollector.Contains(item.BuildingId))
                        { 
                            //Steve
                            _saved = false;
                            ClosedIdCollector.Add(item.BuildingId);
                        }
                    }
                    else
                    {
                        //Steve
                        _saved = false;
                        ClosedIdCollector.Add(item.BuildingId);
                    }

                    if (OpenIdCollector.Count > 0)
                    {
                        if (OpenIdCollector.Contains(item.BuildingId))
                        {
                            OpenIdCollector.Remove(item.BuildingId); //Steve
                            _saved = false;
                        }

                    }
                    ClosedBuilding.Add(item);
                   
                }
                OpenBuilding.Clear();
                bindnewlist();
            }
            catch (Exception ex)
            {

                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }

        }

        private void BtnRemoveAll_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ClosedBuildingList.Items.Count <= 0)
                {
                    ModernDialog.ShowMessage("No Building found", "Sorry", MessageBoxButton.OK);

                }
                foreach (Building item in ClosedBuildingList.Items)
                {
                    if (OpenIdCollector.Count > 0)
                    {
                        if (!OpenIdCollector.Contains(item.BuildingId))
                        {
                            OpenIdCollector.Add(item.BuildingId);
                            //Steve
                            _saved = false;
                        }
                    }
                    else
                    {
                        OpenIdCollector.Add(item.BuildingId);
                        //Steve
                        _saved = false;
                    }

                    if (ClosedIdCollector.Count > 0)
                    {
                        if (ClosedIdCollector.Contains(item.BuildingId))
                        {
                            ClosedIdCollector.Remove(item.BuildingId);
                            //Steve
                            _saved = false;
                        }

                    }
                    OpenBuilding.Add(item);
                    //Steve
                    _saved = false;
                }
                ClosedBuilding.Clear();
                bindnewlist();
            }
            catch (Exception ex)
            {

                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            //OpenBuilding = ClosedBuilding = null;         
            //LoadClosedList();
            //LoadOpenList();
            NavigationCommands.GoToPage.Execute("/Content/BuildingContent/BuildingDetailFrm.xaml#" + null, this);
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //  GetConnection getConnection = new GetConnection();
                //  dbContext = getConnection.InitializeConnection();
                // int result = 0;
                //dynamic buildingQuery = null;
                // string message = string.Empty;

                // buildingQuery = new UpdateBuildingbyBuildingStatus(OpenIdCollector, ClosedIdCollector);
                //result = dbContext.Execute(buildingQuery);
                ClosedBuildingViewModel closedBuildingViewModel = new ClosedBuildingViewModel();
                // result = closedBuildingViewModel.UpdateBuildingbyBuildingStatus(OpenIdCollector, ClosedIdCollector);
                if (closedBuildingViewModel.UpdateBuildingbyBuildingStatus(OpenIdCollector, ClosedIdCollector) > 0)
                {
                    _saved = true;
                    ModernDialog.ShowMessage("Successfully Updated the Buildings", "Success", MessageBoxButton.OK);// success update operation
                }

                //return result;

            }
            catch (Exception ex)
            {

                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }

        }

        private void BtnReset_Click(object sender, RoutedEventArgs e)
        {
            //Steve
            if (_saved != true)
            {
                try
                {
                    OpenBuilding = ClosedBuilding = null;
                    LoadClosedList();
                    LoadOpenList();
                }
                catch (Exception ex)
                {

                    ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                    ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
                }
            }
        }

        #endregion

    }
}
