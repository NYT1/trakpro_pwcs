﻿using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TRAKI.App.Common;
using TRAKI.App.DapperQueries.BuildingQueries;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;
using TRAKI.App.Pages.BuildingPage;
using TRAKI.App.ViewModel;
using TRAKI.App.ViewModel.Common;

namespace TRAKI.App.Content.BuildingContent
{
    /// <summary>
    /// Interaction logic for ControlsStylesSampleForm.xaml
    /// </summary>
    public partial class CreateEditBuildingFrm : UserControl, IContent
    {
        private CreateUpdateBuildingViewModel createUpdateBuildingViewModel;
        protected IDbContext dbContext;
        private ObservableCollection<Building> buildingList;
        private int BuildingId = -1;
        private bool _showConfirmationMsg = true;

        public CreateEditBuildingFrm()
        {

            InitializeComponent();
            this.ComboBuildingType.DataContext = new BuildingTypeViewModel();
            // this.Loaded += OnLoaded;
        }

        #region Private

        private void LoadBuildingInfo()
        {
            createUpdateBuildingViewModel = new CreateUpdateBuildingViewModel();
            buildingList = createUpdateBuildingViewModel.GetBuilding(this.BuildingId);
            this.DataContext = buildingList;
            ComboBuildingType.SelectedValue = ((ObservableCollection<Building>)this.DataContext)[0].BuildingTypeId.ToString();

            //if (Constants.BuildingId == 0)
            //{
            //    RadioBTOpen.IsEnabled = false;
            //    RadioBTClose.IsEnabled = false;
            //    createUpdateBuildingViewModel = new CreateUpdateBuildingViewModel();

            //    buildingList = createUpdateBuildingViewModel.GetBuilding(0);

            //    this.DataContext = buildingList; // newBuilding;

            //}
            //else
            //{
            //    RadioBTOpen.IsEnabled = true;
            //    RadioBTClose.IsEnabled = true;
            //    //TextBuildingCode.IsReadOnly = true;
            //    createUpdateBuildingViewModel = new CreateUpdateBuildingViewModel();
            //    buildingList = createUpdateBuildingViewModel.GetBuilding(Constants.BuildingId);
            //    this.DataContext = buildingList;
            //    ComboBuildingType.SelectedValue = ((ObservableCollection<Building>)this.DataContext)[0].BuildingTypeId.ToString();
            //}


        }


        private void Submit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Building building = ((ObservableCollection<Building>)(sender as Button).DataContext)[0];
                building.BuildingTypeId = Convert.ToInt32(this.ComboBuildingType.SelectedValue);
                string message = string.Empty;
                //Steve
                //Must select building type
                if (String.IsNullOrEmpty(this.ComboBuildingType.Text.Trim()))
                {
                    ModernDialog.ShowMessage("Please select the building type!", "Error!", MessageBoxButton.OK);
                    return;
                };
                var result = createUpdateBuildingViewModel.InsertNewBuilding(building);
                if (result == -1)
                {
                    ModernDialog.ShowMessage("Building already exists, Please try another ID/Code!", "Error!", MessageBoxButton.OK);
                    return;
                }
                if (result == 1)
                {
                    message = "Building Successfully Created";
                }
                if (result == 2)
                {
                    message = "Building Successfully updated";
                }
                ModernDialog.ShowMessage(message, "Success", MessageBoxButton.OK);
                this._showConfirmationMsg = false;
                NavigationCommands.GoToPage.Execute("/Content/BuildingContent/BuildingDetailFrm.xaml#" + null, this);

            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }


        }

        private void rbClose_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var confirm = ModernDialog.ShowMessage("Are you sure you want to Close the Building?", "Close", MessageBoxButton.YesNo);
                if (confirm == MessageBoxResult.Yes)
                {
                    var result = createUpdateBuildingViewModel.CloseBuilding(this.BuildingId);
                    if (result > 0)
                    {
                        createUpdateBuildingViewModel = new CreateUpdateBuildingViewModel();

                        ModernDialog.ShowMessage("Building successfully Closed", "Success", MessageBoxButton.OK);
                        //NavigationCommands.GoToPage.Execute("/Content/BuildingContent/BuildingDetailFrm.xaml#" + null, this);
                    }
                    else
                    {

                        ModernDialog.ShowMessage("Error while deleting the record!", "Error!", MessageBoxButton.OK);
                    }
                }
                else
                {
                    RadioBTClose.IsChecked = false;
                    RadioBTOpen.IsChecked = true;
                }
            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        private void dtPickerDateSigned_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {

            string dateSigned = String.Format("{0:G}", this.dtPickerDateSigned.SelectedDate.Value.Date.Add(DateTime.Now.TimeOfDay));//.ToString();
            buildingList[0].DateSigned = DateTime.Parse(dateSigned);
        }

        private void dtPickerDateReceived_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            string dateRecieved = String.Format("{0:G}", this.dtPickerDateReceived.SelectedDate.Value.Date.Add(DateTime.Now.TimeOfDay));//.ToString();
            buildingList[0].DateReceived = DateTime.Parse(dateRecieved);
        }

        private void TextBuildingCode_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = IsMatching.Check(e.Text);
        }

        private void TextBuildingDescription_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = IsMatching.Check(e.Text);
        }

        private void AddressLine1_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = IsMatching.Check(e.Text);
        }

        private void TextAddressLine2_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = IsMatching.Check(e.Text);
        }

        private void TextContact_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = IsMatching.Check(e.Text);
        }

        private void TextTitle_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = IsMatching.Check(e.Text);
        }

        private void TextCurrentStatus_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = IsMatching.Check(e.Text);
        }

#endregion

        #region IContent

        public void OnFragmentNavigation(FirstFloor.ModernUI.Windows.Navigation.FragmentNavigationEventArgs e)
        {
            this.BuildingId = 0;
            if (!string.IsNullOrEmpty(e.Fragment))
            {
                this.BuildingId = Convert.ToInt32(e.Fragment);
            }
            this.LoadBuildingInfo();
        }

        public void OnNavigatedFrom(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
            //  throw new NotImplementedException();
        }

        public void OnNavigatedTo(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
            this._showConfirmationMsg = true;
        }

        public void OnNavigatingFrom(FirstFloor.ModernUI.Windows.Navigation.NavigatingCancelEventArgs e)
        {
            if (this._showConfirmationMsg)
            {
                string message = "Are you sure you want to leave?";
                if (e.NavigationType == FirstFloor.ModernUI.Windows.Navigation.NavigationType.Refresh)
                {
                    message = "Are you sure you want to refresh the page?";
                }
                var confirm = ModernDialog.ShowMessage(message, "Warning!", MessageBoxButton.YesNo);
                if (confirm == MessageBoxResult.No)
                    e.Cancel = true;
            }
        }

        #endregion

        private void RadioBTClose_Checked(object sender, RoutedEventArgs e)
        {

        }

        

       
    }
}
