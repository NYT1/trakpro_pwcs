﻿using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TRAKI.App.Common;
using TRAKI.App.DapperQueries.BuildingQueries;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;
using TRAKI.App.Pages.BuildingPage;
using TRAKI.App.ViewModel;
using TRAKI.App.ViewModel.Common;

namespace TRAKI.App.Content.BuildingContent
{
    /// <summary>
    /// Interaction logic for GlossaryDetail.xaml
    /// </summary>
    public partial class BuildingDetailFrm : UserControl, IContent
    {
        protected IDbContext dbContext;
        private BuildingViewModel buildingViewModel;
        int pageIndex = 1;
        List<Building> _buildingList = new List<Building>();

        public BuildingDetailFrm()
        {
            InitializeComponent();
            Loaded += BuildingDetailFrm_Loaded;
        }

        void BuildingDetailFrm_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                this.LoadSearchData(this.SearchTextBox.Text.Trim());
            }
            catch (Exception ex)
            {

                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }

        }


        private void LoadFilterList()
        {
            try
            {
                this.ComboFilterBy.ItemsSource = null;
                this.ComboFilterBy.ItemsSource = Constants.AppSettings.Where(setting => setting.SettingItem == "FilterBy").OrderBy(p => p.DisplayOrder); //new SettingsViewModel().GetFilterByList();
            }
            catch (Exception ex)
            {

                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }
            this.ComboFilterBy.SelectedIndex = 0;
        }

        private void ComboFilterBy_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (this.ComboFilterBy.SelectedValue != null)
                {
                    this.LoadSearchData(this.SearchTextBox.Text.Trim());
                }
            }

            catch (Exception ex)
            {

                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }

        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Building BuildingRow = BuildingGrid.SelectedItem as Building;

                NavigationCommands.GoToPage.Execute("/Content/BuildingContent/CreateEditBuildingFrm.xaml#" + BuildingRow.BuildingId, this);

            }
            catch (Exception ex)
            {

                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        private void btnCreateNewBuilding_Click(object sender, RoutedEventArgs e)
        {
            NavigationCommands.GoToPage.Execute("/Content/BuildingContent/CreateEditBuildingFrm.xaml#" + null, this);
        }

        private void btnRooms_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Building building = BuildingGrid.SelectedItem as Building;
                string buildingInf = building.BuildingId + "•" + building.BuildingNumber + "•" + building.BuildingDescription + "•" + building.BuildingStatus
                    + "•" + building.IsActive;
                NavigationCommands.GoToPage.Execute("/Content/RoomContent/RoomDetailFrm.xaml#" + buildingInf, this);

            }
            catch (Exception ex)
            {

                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }
        }



        #region Pagination

        private void btnFirst_Click(object sender, System.EventArgs e)
        {

            Navigate((int)PagingMode.First);
            this.TextBlockPages.Text = "1";
        }

        private void btnNext_Click(object sender, System.EventArgs e)
        {
            Navigate((int)PagingMode.Next);
            this.TextBlockPages.Text = this.pageIndex.ToString();
        }

        private void btnPrev_Click(object sender, System.EventArgs e)
        {
            Navigate((int)PagingMode.Previous);
            this.TextBlockPages.Text = this.pageIndex.ToString();
        }

        private void btnLast_Click(object sender, System.EventArgs e)
        {
            Navigate((int)PagingMode.Last);
            this.TextBlockPages.Text = this.pageIndex.ToString();
        }

        //private void cbNumberOfRecords_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    Navigate((int)PagingMode.PageCountChange);
        //}

        private void Navigate(int mode)
        {
            int count;
            switch (mode)
            {
                case (int)PagingMode.Next:
                    btnPrev.IsEnabled = true;
                    btnFirst.IsEnabled = true;
                    if (_buildingList.Count >= (pageIndex * Constants.NumberOfRecordsPerPage))
                    {

                        if (_buildingList.Skip(pageIndex * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage).Count() == 0)
                        {
                            this.BuildingGrid.ItemsSource = null;
                            this.BuildingGrid.ItemsSource = _buildingList.Skip((pageIndex * Constants.NumberOfRecordsPerPage) - Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage);
                            count = (pageIndex * Constants.NumberOfRecordsPerPage) + (_buildingList.Skip(pageIndex * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage)).Count();
                        }
                        else
                        {
                            this.BuildingGrid.ItemsSource = null;
                            this.BuildingGrid.ItemsSource = _buildingList.Skip(pageIndex * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage);
                            count = (pageIndex * Constants.NumberOfRecordsPerPage) + (_buildingList.Skip(pageIndex * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage)).Count();
                            pageIndex++;
                        }

                        if ((pageIndex * Constants.NumberOfRecordsPerPage) >= _buildingList.Count)
                        {
                            btnNext.IsEnabled = false;
                            btnLast.IsEnabled = false;
                        }

                    }
                    else
                    {
                        btnNext.IsEnabled = false;
                        btnLast.IsEnabled = false;
                    }

                    break;
                case (int)PagingMode.Previous:
                    btnNext.IsEnabled = true;
                    btnLast.IsEnabled = true;
                    pageIndex -= 1;

                    if (pageIndex >= 1)
                    {
                        this.BuildingGrid.ItemsSource = null;
                        if (pageIndex == 1)
                        {
                            btnPrev.IsEnabled = false;
                            btnFirst.IsEnabled = false;
                            this.BuildingGrid.ItemsSource = _buildingList.Take(Constants.NumberOfRecordsPerPage);
                            count = _buildingList.Take(Constants.NumberOfRecordsPerPage).Count();
                        }
                        else
                        {
                            this.BuildingGrid.ItemsSource = _buildingList.Skip((pageIndex - 1) * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage);
                            count = Math.Min((pageIndex - 1) * Constants.NumberOfRecordsPerPage, _buildingList.Count);
                        }
                    }
                    else
                    {
                        btnPrev.IsEnabled = false;
                        btnFirst.IsEnabled = false;
                    }

                    if (_buildingList.Count <= Constants.NumberOfRecordsPerPage)
                    {
                        btnNext.IsEnabled = false;
                        btnLast.IsEnabled = false;
                        btnPrev.IsEnabled = false;
                        btnFirst.IsEnabled = false;
                    }
                    break;

                case (int)PagingMode.First:
                    pageIndex = 2;
                    Navigate((int)PagingMode.Previous);
                    break;
                case (int)PagingMode.Last:
                    pageIndex = (_buildingList.Count / Constants.NumberOfRecordsPerPage);
                    Navigate((int)PagingMode.Next);
                    break;

                case (int)PagingMode.PageCountChange:
                    pageIndex = 1;
                    this.BuildingGrid.ItemsSource = null;
                    this.BuildingGrid.ItemsSource = _buildingList.Take(Constants.NumberOfRecordsPerPage);
                    count = (_buildingList.Take(Constants.NumberOfRecordsPerPage)).Count();
                    btnNext.IsEnabled = true;
                    btnPrev.IsEnabled = true;
                    break;
            }
        }

        #endregion

        #region Search Region

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            string searchText = this.SearchTextBox.Text.Trim();
            if (string.IsNullOrEmpty(searchText))
                return;
            try
            {
                this.LoadSearchData(searchText);
            }
            catch (Exception ex)
            {

                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        private void SearchTextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = IsMatching.Check(e.Text);
        }

        private void SearchTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            string searchText = ((TextBox)sender).Text.Trim();
            if (!string.IsNullOrEmpty(searchText))
                return;
            try
            {
                this.LoadSearchData(searchText);

            }
            catch (Exception ex)
            {

                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }

        }

        private void SearchTextBox_PreviewExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            //     e.Handled =;
            if (IsMatching.Check(((TextBox)sender).Text.Trim()))
                e.Handled = false;
        }




        private void SearchTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                string searchText = ((TextBox)sender).Text.Trim();
                if (string.IsNullOrEmpty(searchText))
                    return;
                if (e.Key == Key.Enter)
                    this.LoadSearchData(searchText);
            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage("Some error occured, please try again! " + ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        private void LoadSearchData(string SearchText)
        {
            try
            {
                buildingViewModel = new BuildingViewModel();
                _buildingList = new List<Building>(buildingViewModel.SearchBuilding(SearchText, Convert.ToInt32(this.ComboFilterBy.SelectedValue))).ToList();
                this.BuildingGrid.ItemsSource = CollectionViewSource.GetDefaultView(_buildingList.Take(Constants.NumberOfRecordsPerPage));
                this.BuildingGrid.Items.Refresh();
                if (buildingViewModel.RecordCount == 0)
                    this.TextBlockPages.Text = "0";
                else
                    this.TextBlockPages.Text = "1";
                this.Navigate((int)PagingMode.First);
                this.DataContext = buildingViewModel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Navigation

        public void OnFragmentNavigation(FirstFloor.ModernUI.Windows.Navigation.FragmentNavigationEventArgs e)
        {
            //throw new NotImplementedException();
        }

        public void OnNavigatedFrom(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
            // throw new NotImplementedException();
        }

        public void OnNavigatedTo(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
            //  throw new NotImplementedException();
            this.LoadFilterList();
        }

        public void OnNavigatingFrom(FirstFloor.ModernUI.Windows.Navigation.NavigatingCancelEventArgs e)
        {
            // throw new NotImplementedException();
        }

        #endregion




    }
}

