﻿using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using TRAKI.App.Common;
using TRAKI.App.Content.CommonContent;
using TRAKI.App.ViewModel;

namespace TRAKI.App.Content.InventoryContent
{
    /// <summary>
    /// Interaction logic for UpdateDBAfterInventoryFrm.xaml
    /// </summary>
    public partial class UpdateDBAfterInventoryFrm : UserControl, IContent
    {
        private bool isUpdationinProgress = false;


        public UpdateDBAfterInventoryFrm()
        {
            InitializeComponent();
            //Steve
            //Ensure it is only for PWCS for this project
           
            this.btnUpdateDatabasePWCPS.Visibility = Visibility.Visible;
        }

        private void btnUpdateDatabase_Click(object sender, RoutedEventArgs e)
        {

            //Steve
            //enture no action below
            /*
            try
            {

                var confirm = ModernDialog.ShowMessage("Are you sure you want to Update the Database after Inventory, the action can not be Undone?", "Warning!", MessageBoxButton.YesNo);
                if (confirm == MessageBoxResult.No)
                    return;


                InventoryViewModel inventoryViewModel = new InventoryViewModel();
                Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                {
                    this.BusyBar.IsBusy = true;
                    isUpdationinProgress = true;
                }));

                ThreadStart work = () =>
                {
                    int result = inventoryViewModel.UpdateDatabaseAfterInventory();

                    Application.Current.Dispatcher.BeginInvoke(new Action(() => CommandManager.InvalidateRequerySuggested()));
                    Application.Current.Dispatcher.Invoke(DispatcherPriority.ContextIdle, new Action(() =>
                    {
                        if (result > 0)
                        {
                            this.BusyBar.IsBusy = false;
                            isUpdationinProgress = false;
                            if (result == 1)
                                ModernDialog.ShowMessage("Nothing to Update!, No Record found in Inventory Table. ", "Information!", MessageBoxButton.OK);
                            if (result == 2)
                                ModernDialog.ShowMessage("Database updated successfully after Inventory", "Success", MessageBoxButton.OK);
                        }
                    }));
                };
                new Thread(work).Start();

            }
            catch (Exception ex)
            {
                ErrorLogging.LogException("Error:" + ex.Message);
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }
              */
        }

        private void btnUpdateDatabasePWCPS_Click(object sender, RoutedEventArgs e)
        {

            try
            {
                    List<string> selectedBuildingNumbers = new List<string>();
                 
                var confirm = ModernDialog.ShowMessage("Are you sure you want to Update the Database after PWCPS Inventory?, the action can not be Undone.", "Warning!", MessageBoxButton.YesNo);
                if (confirm == MessageBoxResult.No)
                    return;

                    //Steve
                    Constants.needBldgSelect = true;
                    Constants.isForPWCS = true;
                    BuildingSelectionDialog buildingSelectionDialog = new BuildingSelectionDialog(null);
                    buildingSelectionDialog.Title = "Select Building";
                    buildingSelectionDialog.CloseButton.Content = "Close";
                    buildingSelectionDialog.OkButton.Content = "Select";
                    buildingSelectionDialog.Buttons = new Button[] { buildingSelectionDialog.CloseButton, buildingSelectionDialog.OkButton };
                    buildingSelectionDialog.ShowDialog();

                    if (buildingSelectionDialog.DialogResult.Value)
                    {
                        selectedBuildingNumbers = buildingSelectionDialog.SelectedBuildingNumbers.ToList();
                    }

                    if (selectedBuildingNumbers!= null && selectedBuildingNumbers.Count > 0)
                    {
                        InventoryViewModel inventoryViewModel = new InventoryViewModel();
                        Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                        {
                            this.BusyBar.IsBusy = true;
                            isUpdationinProgress = true;
                        }));

                        ThreadStart work = () =>
                        {
                            int result = inventoryViewModel.UpdateDatabaseAfterInventoryPWCPS(selectedBuildingNumbers);

                            Application.Current.Dispatcher.BeginInvoke(new Action(() => CommandManager.InvalidateRequerySuggested()));
                            Application.Current.Dispatcher.Invoke(DispatcherPriority.ContextIdle, new Action(() =>
                            {
                                if (result > 0)
                                {
                                    this.BusyBar.IsBusy = false;
                                    isUpdationinProgress = false;
                                    if (result == 1)
                                        ModernDialog.ShowMessage("Nothing to Update!, No Record found in Inventory Table. ", "Information!", MessageBoxButton.OK);
                                    if (result == 2)
                                        ModernDialog.ShowMessage("Database updated successfully after Inventory PWCPS", "Success", MessageBoxButton.OK);

                                }
                            }));
                        };
                        new Thread(work).Start();
                    }

            }
            catch (Exception ex)
            {
                ErrorLogging.LogException("Error:" + ex.Message);
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);

            }

        }


        #region Navigation

        public void OnFragmentNavigation(FirstFloor.ModernUI.Windows.Navigation.FragmentNavigationEventArgs e)
        {
            //throw new NotImplementedException();
            //Steve
            //No action
           
            if (this.isUpdationinProgress)
            {
                ModernDialog.ShowMessage("Can not navigate because the database updation is in progress!", "Warning!", MessageBoxButton.OK);
            }
            else
            {
               // this.btnUpdateDatabase.Visibility = Convert.ToBoolean(e.Fragment) ? Visibility.Visible : Visibility.Collapsed;
              //  this.btnUpdateDatabasePWCPS.Visibility = Convert.ToBoolean(e.Fragment) ? Visibility.Collapsed : Visibility.Visible;
            }
           
        }

        public void OnNavigatedFrom(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
            // throw new NotImplementedException();
        }

        public void OnNavigatedTo(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
            //throw new NotImplementedException();
        }

        public void OnNavigatingFrom(FirstFloor.ModernUI.Windows.Navigation.NavigatingCancelEventArgs e)
        {
            // throw new NotImplementedException();
        }

        #endregion


    }
}
