﻿using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using TRAKI.App.Common;
using TRAKI.App.Model;
using TRAKI.App.ViewModel;

namespace TRAKI.App.Content.InventoryContent
{
    /// <summary>
    /// Interaction logic for EditInventoryFrm.xaml
    /// </summary>
    public partial class EditInventoryFrm : UserControl
    {

        string result = "Cancel";
        private InventoryViewModel inventoryViewModel;
        List<Inventory> _inventoryList = new List<Inventory>();
        List<Inventory> _updatedInventoryList;
        Inventory _orginalInventory;
        int pageIndex = 1;

        public EditInventoryFrm()
        {
            InitializeComponent();
            this.Loaded += EditInventoryFrm_Loaded;
        }

        #region Private


        private void EditInventoryFrm_Loaded(object sender, RoutedEventArgs e)
        {
            this.LoadInventoryData();
            _updatedInventoryList = new List<Inventory>();
        }

        private void LoadInventoryData()
        {
            try
            {
                this.panelButtons.IsEnabled = false;
                inventoryViewModel = new InventoryViewModel();
                Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                 {
                     this.BusyBar.IsBusy = true;
                 }));

                ThreadStart work = () =>
                 {
                     _inventoryList = new List<Inventory>(inventoryViewModel.GetUploadData());
                     if (_inventoryList != null && _inventoryList.Count > 0)
                         foreach (Inventory _inv in _inventoryList)
                         {
                             Inventory InItem = _updatedInventoryList.Find(item => item.UploadDataId == _inv.UploadDataId);
                             if (InItem == null) // check item isn't null
                             {
                                 _updatedInventoryList.Add(_inv);
                             }

                         }
                     Application.Current.Dispatcher.BeginInvoke(new Action(() => CommandManager.InvalidateRequerySuggested()));
                     Application.Current.Dispatcher.Invoke(DispatcherPriority.ContextIdle, new Action(() =>
                  {

                      this.DataContext = inventoryViewModel;
                      this.InventoryGrid.ItemsSource = CollectionViewSource.GetDefaultView(_inventoryList.Take(Constants.NumberOfRecordsPerPage).ToList());
                      this.InventoryGrid.Items.Refresh();
                      this.BusyBar.IsBusy = false;
                      if (inventoryViewModel.RecordCount == 0)
                          this.TextBlockPages.Text = "0";
                      else
                          this.TextBlockPages.Text = "1";

                      this.panelButtons.IsEnabled = true;

                      //Steve
                      //code in navigate method.
                      //force the navigate.

                      this.Navigate((int)PagingMode.First);

                  }));
                 };
                new Thread(work).Start();
                this.Navigate((int)PagingMode.First);
                this.DataContext = inventoryViewModel;

            }
            catch (Exception ex)
            {
                ErrorLogging.LogException("Error:" + ex.Message);
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
                this.panelButtons.IsEnabled = true;
            }

        }

        private void InventoryGrid_SelectionChanged(object sender, RoutedEventArgs e)
        {
            try
            {
                var datagrid = sender as DataGrid;
                if (datagrid.SelectedItems.Count > 1)
                {
                    ModernDialog dialog = new ModernDialog();
                    dialog.SizeToContent = SizeToContent.Manual;
                    dialog.YesButton.Content = "Accept";
                    dialog.NoButton.Content = "Reject";
                    dialog.CancelButton.Content = "Cancel";
                    dialog.YesButton.Click += YesButton_Click;
                    dialog.NoButton.Click += NoButton_Click;
                    dialog.Content = "\n\nIf Accepted QA of all the selected records will be set to TRUE and if Rejected the QA will be set to FALSE!\n You can cancel the Action by pressing Cancel!";
                    dialog.Title = "Confirmation!";
                    dialog.Buttons = new Button[] { dialog.CancelButton, dialog.YesButton, dialog.NoButton };
                    dialog.MaxHeight = 50;
                    dialog.ShowDialog();
                    if (result.Equals("Yes"))
                    {
                        for (int i = 0; i < datagrid.SelectedItems.Count; i++)
                        {
                            Inventory _inventory = (Inventory)datagrid.SelectedItems[i];
                            if (_updatedInventoryList.Contains(_inventory))
                                _updatedInventoryList.Remove(_inventory);

                            _inventory.QA = BooleanValue.True;
                            _updatedInventoryList.Add(_inventory);
                        }
                    }
                    else if (result.Equals("No"))
                    {
                        for (int i = 0; i < datagrid.SelectedItems.Count; i++)
                        {
                            Inventory _inventory = (Inventory)datagrid.SelectedItems[i];
                            _updatedInventoryList.Remove(_inventory);
                            _inventory.QA = BooleanValue.False;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void NoButton_Click(object sender, RoutedEventArgs e)
        {
            result = "No";
        }

        void YesButton_Click(object sender, RoutedEventArgs e)
        {
            result = "Yes";
        }

        private void btnSaveAndClose_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.panelButtons.IsEnabled = false;
                inventoryViewModel = new InventoryViewModel();
                Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                 {
                     this.BusyBar.IsBusy = true;
                 }));

                ThreadStart work = () =>
                 {
                     int result = inventoryViewModel.SaveInventoryData(this._updatedInventoryList.Where(inventory => inventory.QA == BooleanValue.True).ToList());

                     Application.Current.Dispatcher.BeginInvoke(new Action(() => CommandManager.InvalidateRequerySuggested()));
                     Application.Current.Dispatcher.Invoke(DispatcherPriority.ContextIdle, new Action(() =>
                  {
                      if (result > 0)
                      {
                         
                          this.InventoryGrid.ItemsSource = null;
                          this.InventoryGrid.Items.Refresh();
                          if (inventoryViewModel.RecordCount == 0)
                              this.TextBlockPages.Text = "0";
                          else
                              this.TextBlockPages.Text = "1";
                        //  this.DataContext = inventoryViewModel;
                          this.BusyBar.IsBusy = false;
                          ModernDialog.ShowMessage("Inventory Data sucessfully Saved", "Success", MessageBoxButton.OK);
                          this.LoadInventoryData();
                          _updatedInventoryList = new List<Inventory>();
                      }
                      this.panelButtons.IsEnabled = false;
                  }));
                 };
                new Thread(work).Start();
                this.Navigate((int)PagingMode.First);
                this.DataContext = inventoryViewModel;

            }
            catch (Exception ex)
            {
                ErrorLogging.LogException("Error:" + ex.Message);
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);

            }

        }

        private void InventoryGrid_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            try
            {
                DataGrid dataGrid = sender as DataGrid;
                bool errors = false;
                errors = (from c in
                              (from object i in dataGrid.ItemsSource
                               select dataGrid.ItemContainerGenerator.ContainerFromItem(i))
                          where c != null
                          select Validation.GetHasError(c)).FirstOrDefault(x => x);

                Inventory _inventory = e.Row.Item as Inventory;
                var InItem = _updatedInventoryList.Where(item => item.AssetNumber == _inventory.AssetNumber).ToList();
                if (InItem.Count > 1) // check item isn't null
                {
                    ModernDialog.ShowMessage("AssetNumber already in the List, Please entere differnt one!", "Error!", MessageBoxButton.OK);
                    errors = true;
                }

                if (errors)
                {
                    (e.Row.Item as Inventory).AssetNumber = _orginalInventory.AssetNumber;
                    (e.Row.Item as Inventory).RoomNumber = _orginalInventory.RoomNumber;
                    (e.Row.Item as Inventory).BuildingNumber = _orginalInventory.BuildingNumber;
                    return;
                }
                _inventory.QA = BooleanValue.True;
            }
            catch (Exception ex)
            {
                ErrorLogging.LogException("Error:" + ex.Message);
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        private void InventoryGrid_BeginningEdit(object sender, DataGridBeginningEditEventArgs e)
        {
            try
            {
                _orginalInventory = (Inventory)(e.Row.Item as Inventory).Clone();
            }
            catch (Exception ex)
            {
                ErrorLogging.LogException("Error:" + ex.Message);
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        private void btnDeleteAll_Click(object sender, RoutedEventArgs e)
        {

            try
            {
                if (ModernDialog.ShowMessage("Are you sure you want to Delete all the data from local storage? This action cannot be undone!", "Warning!", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    this.panelButtons.IsEnabled = false;
                    InventoryViewModel inventoryViewModel = new InventoryViewModel();
                    int result = inventoryViewModel.DeleteAllUploadData();

                    if (result > 0)
                    {
                        ModernDialog.ShowMessage("Upload Data sucessfully deleted from local storage", "Success", MessageBoxButton.OK);
                        this.InventoryGrid.ItemsSource = null;
                        this.InventoryGrid.Items.Refresh();
                        this.panelButtons.IsEnabled = true;
                    }
                    this.DataContext = inventoryViewModel;
                }

            }
            catch (Exception ex)
            {
                ErrorLogging.LogException("Error:" + ex.Message);
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
                this.panelButtons.IsEnabled = true;

            }

        }

        #endregion

        #region Pagination

        private void btnFirst_Click(object sender, System.EventArgs e)
        {

            Navigate((int)PagingMode.First);
            this.TextBlockPages.Text = "1";
        }

        private void btnNext_Click(object sender, System.EventArgs e)
        {
            Navigate((int)PagingMode.Next);
            this.TextBlockPages.Text = this.pageIndex.ToString();
        }

        private void btnPrev_Click(object sender, System.EventArgs e)
        {
            Navigate((int)PagingMode.Previous);
            this.TextBlockPages.Text = this.pageIndex.ToString();
        }

        private void btnLast_Click(object sender, System.EventArgs e)
        {
            Navigate((int)PagingMode.Last);
            this.TextBlockPages.Text = this.pageIndex.ToString();
        }

        //private void cbNumberOfRecords_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    Navigate((int)PagingMode.PageCountChange);
        //}

        private void Navigate(int mode)
        {
            int count;
            switch (mode)
            {
                case (int)PagingMode.Next:
                    btnPrev.IsEnabled = true;
                    btnFirst.IsEnabled = true;
                    if (_inventoryList.Count >= (pageIndex * Constants.NumberOfRecordsPerPage))
                    {

                        if (_inventoryList.Skip(pageIndex * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage).Count() == 0)
                        {
                            this.InventoryGrid.ItemsSource = null;
                            this.InventoryGrid.ItemsSource = _inventoryList.Skip((pageIndex * Constants.NumberOfRecordsPerPage) - Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage).ToList();
                            count = (pageIndex * Constants.NumberOfRecordsPerPage) + (_inventoryList.Skip(pageIndex * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage)).Count();
                        }
                        else
                        {
                            this.InventoryGrid.ItemsSource = null;
                            this.InventoryGrid.ItemsSource = _inventoryList.Skip(pageIndex * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage).ToList();
                            count = (pageIndex * Constants.NumberOfRecordsPerPage) + (_inventoryList.Skip(pageIndex * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage)).Count();
                            pageIndex++;
                        }

                        if ((pageIndex * Constants.NumberOfRecordsPerPage) >= _inventoryList.Count)
                        {
                            btnNext.IsEnabled = false;
                            btnLast.IsEnabled = false;
                        }

                    }
                    else
                    {
                        btnNext.IsEnabled = false;
                        btnLast.IsEnabled = false;
                    }

                    break;
                case (int)PagingMode.Previous:
                    btnNext.IsEnabled = true;
                    btnLast.IsEnabled = true;
                    pageIndex -= 1;
                    if (pageIndex >= 1)
                    {
                        this.InventoryGrid.ItemsSource = null;
                        if (pageIndex == 1)
                        {
                            btnPrev.IsEnabled = false;
                            btnFirst.IsEnabled = false;
                            this.InventoryGrid.ItemsSource = _inventoryList.Take(Constants.NumberOfRecordsPerPage).ToList();
                            count = _inventoryList.Take(Constants.NumberOfRecordsPerPage).Count();
                        }
                        else
                        {
                            this.InventoryGrid.ItemsSource = _inventoryList.Skip((pageIndex - 1) * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage).ToList();
                            count = Math.Min((pageIndex - 1) * Constants.NumberOfRecordsPerPage, _inventoryList.Count);
                        }
                    }
                    else
                    {
                        btnPrev.IsEnabled = false;
                        btnFirst.IsEnabled = false;
                    }
                    if (_inventoryList.Count <= Constants.NumberOfRecordsPerPage)
                    {
                        btnNext.IsEnabled = false;
                        btnLast.IsEnabled = false;
                        btnPrev.IsEnabled = false;
                        btnFirst.IsEnabled = false;
                    }
                    break;

                case (int)PagingMode.First:
                    pageIndex = 2;
                    Navigate((int)PagingMode.Previous);
                    break;
                case (int)PagingMode.Last:
                    pageIndex = (_inventoryList.Count / Constants.NumberOfRecordsPerPage);
                    Navigate((int)PagingMode.Next);
                    break;

                case (int)PagingMode.PageCountChange:
                    pageIndex = 1;
                    this.InventoryGrid.ItemsSource = null;
                    this.InventoryGrid.ItemsSource = _inventoryList.Take(Constants.NumberOfRecordsPerPage).ToList();
                    count = (_inventoryList.Take(Constants.NumberOfRecordsPerPage)).Count();
                    btnNext.IsEnabled = true;
                    btnPrev.IsEnabled = true;
                    break;
            }
        }

        #endregion


    }
}
