﻿using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using TRAKI.App.Common;
using TRAKI.App.ViewModel;

namespace TRAKI.App.Content.InventoryContent
{
    /// <summary>
    /// Interaction logic for UpdateDBAfterInventoryFrm.xaml
    /// </summary>
    public partial class ResetBaseLineFrm : UserControl
    {
        public ResetBaseLineFrm()
        {
            InitializeComponent();
        }

        private void btnOpenBuildings_Click(object sender, RoutedEventArgs e)
        {


            try
            {
                var confirm = ModernDialog.ShowMessage("Are you sure you want to open all the buildings?, the action can not be Undone.", "Warning!", MessageBoxButton.YesNo);
                if (confirm == MessageBoxResult.No)
                    return;

                InventoryViewModel inventoryViewModel = new InventoryViewModel();

                if (inventoryViewModel.OpenAllBuilding() > 0)
                    ModernDialog.ShowMessage("All buildings successfully set to open", "Success", MessageBoxButton.OK);
            }
            catch (Exception ex)
            {
                ErrorLogging.LogException("Error:" + ex.Message);
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        private void btnEmptyInventoryTable_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var confirm = ModernDialog.ShowMessage("Are you sure you want to clear Inventory Table?, the action can not be Undone.", "Warning!", MessageBoxButton.YesNo);
                if (confirm == MessageBoxResult.No)
                    return;

                InventoryViewModel inventoryViewModel = new InventoryViewModel();

                if (inventoryViewModel.ClearInventoryData() > 0)
                    ModernDialog.ShowMessage("Inventory data successfully cleared", "Success", MessageBoxButton.OK);
            }
            catch (Exception ex)
            {
                ErrorLogging.LogException("Error:" + ex.Message);
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }


        }


    }
}
