﻿using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TRAKI.App.Common;
using TRAKI.App.Content.CommonContent;
using TRAKI.App.Model;
using TRAKI.App.ViewModel;


namespace TRAKI.App.Content.InventoryContent
{
    /// <summary>
    /// Interaction logic for UploadData.xaml
    /// </summary>
    public partial class UploadInventoryFrm : UserControl
    {

        int pageIndex = 1;
        string SrcFilePath = string.Empty;
        List<Inventory> _inventoryList = new List<Inventory>();
        public UploadInventoryFrm()
        {
            InitializeComponent();
            this.DataContext = new InventoryViewModel();
            this.Navigate((int)PagingMode.First);
            this.TextBlockPages.Text = "0";
            Loaded += UploadInventoryFrm_Loaded;
        }

        void UploadInventoryFrm_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                InventoryViewModel inventoryViewModel = new InventoryViewModel();
                _inventoryList = new List<Inventory>(inventoryViewModel.GetUploadData());
                this.InventoryGrid.ItemsSource = CollectionViewSource.GetDefaultView(_inventoryList.Take(Constants.NumberOfRecordsPerPage));
                this.InventoryGrid.Items.Refresh();
                if (inventoryViewModel.RecordCount == 0)
                    this.TextBlockPages.Text = "0";
                else
                    this.TextBlockPages.Text = "1";
                this.Navigate((int)PagingMode.First);
                this.DataContext = inventoryViewModel;

            }
            catch (Exception ex)
            {
                ErrorLogging.LogException("Error:" + ex.Message);
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }
        }




        #region Private



        private void btnBrowse_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Create OpenFileDialog
                Microsoft.Win32.OpenFileDialog dialog = new Microsoft.Win32.OpenFileDialog();
                dialog.Filter = "TXT File (*.TXT)|*.TXT";
                dialog.InitialDirectory = @"C:\";
                dialog.Title = "Please select a file to upload.";
                dialog.Multiselect = false;
                if (dialog.ShowDialog() != null)
                {
                    SrcFilePath = this.TextFilePath.Text = dialog.FileName;
                }
            }
            catch (Exception ex)
            {
                ErrorLogging.LogException("Error:" + ex.Message);
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }

        }


        private void btnUpload_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                InventoryViewModel inventoryViewModel = new InventoryViewModel();
                _inventoryList = new List<Inventory>(inventoryViewModel.UploadAndGetFileData(this.TextFilePath.Text.Trim()).ToList());
                this.InventoryGrid.ItemsSource = CollectionViewSource.GetDefaultView(_inventoryList.Take(Constants.NumberOfRecordsPerPage));
                this.InventoryGrid.Items.Refresh();
                if (inventoryViewModel.RecordCount == 0)
                    this.TextBlockPages.Text = "0";
                else
                    this.TextBlockPages.Text = "1";
                this.Navigate((int)PagingMode.First);
                this.DataContext = inventoryViewModel;
                string message = "Data sucessfully Uploaded";
                if (!inventoryViewModel.MoveFile(SrcFilePath))
                    message = message + ", But there is an error while moving the UploadData File!";
                ModernDialog.ShowMessage(message, "Success", MessageBoxButton.OK);

            }
            catch (Exception ex)
            {
                ErrorLogging.LogException("Error:" + ex.Message);
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }
        }
      

        // Commented out not in use
        private void btnSubmitButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                InventoryViewModel inventoryViewModel = new InventoryViewModel();
                //  int result = inventoryViewModel.SaveUploadData(this._inventoryList.ToList());
                // if (result > 0)
                // {
                string message = "Data sucessfully Uploaded";
                if (!inventoryViewModel.MoveFile(SrcFilePath))
                    message = message + ", But there is an error while moving the UploadData File!";
                ModernDialog.ShowMessage(message, "Success", MessageBoxButton.OK);
                this.InventoryGrid.ItemsSource = null;
                this.InventoryGrid.Items.Refresh();
                //  SrcFilePath = string.Empty;
                // }
                this.DataContext = inventoryViewModel;
            }
            catch (Exception ex)
            {
                ErrorLogging.LogException("Error:" + ex.Message);
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        
        private void TextFilePath_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = IsMatching.Check(e.Text);
        }

        #endregion

        #region Pagination

        private void btnFirst_Click(object sender, System.EventArgs e)
        {

            Navigate((int)PagingMode.First);
            this.TextBlockPages.Text = "1";
        }

        private void btnNext_Click(object sender, System.EventArgs e)
        {
            Navigate((int)PagingMode.Next);
            this.TextBlockPages.Text = this.pageIndex.ToString();
        }

        private void btnPrev_Click(object sender, System.EventArgs e)
        {
            Navigate((int)PagingMode.Previous);
            this.TextBlockPages.Text = this.pageIndex.ToString();
        }

        private void btnLast_Click(object sender, System.EventArgs e)
        {
            Navigate((int)PagingMode.Last);
            this.TextBlockPages.Text = this.pageIndex.ToString();
        }

        //private void cbNumberOfRecords_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    Navigate((int)PagingMode.PageCountChange);
        //}

        private void Navigate(int mode)
        {
            int count;
            switch (mode)
            {
                case (int)PagingMode.Next:
                    btnPrev.IsEnabled = true;
                    btnFirst.IsEnabled = true;
                    if (_inventoryList.Count >= (pageIndex * Constants.NumberOfRecordsPerPage))
                    {

                        if (_inventoryList.Skip(pageIndex * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage).Count() == 0)
                        {
                            this.InventoryGrid.ItemsSource = null;
                            this.InventoryGrid.ItemsSource = _inventoryList.Skip((pageIndex * Constants.NumberOfRecordsPerPage) - Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage);
                            count = (pageIndex * Constants.NumberOfRecordsPerPage) + (_inventoryList.Skip(pageIndex * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage)).Count();
                        }
                        else
                        {
                            this.InventoryGrid.ItemsSource = null;
                            this.InventoryGrid.ItemsSource = _inventoryList.Skip(pageIndex * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage);
                            count = (pageIndex * Constants.NumberOfRecordsPerPage) + (_inventoryList.Skip(pageIndex * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage)).Count();
                            pageIndex++;
                        }

                        if ((pageIndex * Constants.NumberOfRecordsPerPage) >= _inventoryList.Count)
                        {
                            btnNext.IsEnabled = false;
                            btnLast.IsEnabled = false;
                        }

                    }
                    else
                    {
                        btnNext.IsEnabled = false;
                        btnLast.IsEnabled = false;
                    }

                    break;
                case (int)PagingMode.Previous:
                    btnNext.IsEnabled = true;
                    btnLast.IsEnabled = true;
                    pageIndex -= 1;
                    if (pageIndex >= 1)
                    {
                        this.InventoryGrid.ItemsSource = null;
                        if (pageIndex == 1)
                        {
                            btnPrev.IsEnabled = false;
                            btnFirst.IsEnabled = false;
                            this.InventoryGrid.ItemsSource = _inventoryList.Take(Constants.NumberOfRecordsPerPage);
                            count = _inventoryList.Take(Constants.NumberOfRecordsPerPage).Count();
                        }
                        else
                        {
                            this.InventoryGrid.ItemsSource = _inventoryList.Skip((pageIndex - 1) * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage);
                            count = Math.Min((pageIndex - 1) * Constants.NumberOfRecordsPerPage, _inventoryList.Count);
                        }
                    }
                    else
                    {
                        btnPrev.IsEnabled = false;
                        btnFirst.IsEnabled = false;
                    }
                    if (_inventoryList.Count <= Constants.NumberOfRecordsPerPage)
                    {
                        btnNext.IsEnabled = false;
                        btnLast.IsEnabled = false;
                        btnPrev.IsEnabled = false;
                        btnFirst.IsEnabled = false;
                    }
                    break;

                case (int)PagingMode.First:
                    pageIndex = 2;
                    Navigate((int)PagingMode.Previous);
                    break;
                case (int)PagingMode.Last:
                    pageIndex = (_inventoryList.Count / Constants.NumberOfRecordsPerPage);
                    Navigate((int)PagingMode.Next);
                    break;

                case (int)PagingMode.PageCountChange:
                    pageIndex = 1;
                    this.InventoryGrid.ItemsSource = null;
                    this.InventoryGrid.ItemsSource = _inventoryList.Take(Constants.NumberOfRecordsPerPage);
                    count = (_inventoryList.Take(Constants.NumberOfRecordsPerPage)).Count();
                    btnNext.IsEnabled = true;
                    btnPrev.IsEnabled = true;
                    break;
            }
        }

        #endregion

    }
}
