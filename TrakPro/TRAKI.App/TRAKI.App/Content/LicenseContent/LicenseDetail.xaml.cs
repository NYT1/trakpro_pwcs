﻿using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TRAKI.App.Common;
using TRAKI.App.DapperQueries.UserQueries;
using TRAKI.App.Model;
using TRAKI.App.Pages;
using TRAKI.App.ViewModel;
using TRAKI.App.ViewModel.Common;

namespace TRAKI.App.Content.LicenseContent
{
    /// <summary>
    /// Interaction logic for GlossaryDetail.xaml
    /// </summary>
    /// 
    //[PrincipalPermission(SecurityAction.Demand, Role = "Administrator")]
    public partial class LicenseDetail : UserControl, IContent
    {
       
        public LicenseDetail()
        {
            InitializeComponent();
            this.Loaded += LicenseDetail_Loaded;
        }

        void LicenseDetail_Loaded(object sender, RoutedEventArgs e)
        {
            this.LoadLicenseDetail();
        }

        #region Private
        private void LoadLicenseDetail()
        {
            try
            {

                this.TextBlockLicenseNumber.Text = Constants.LicenseDetail["licenseid"];
                this.TextBlockClientName.Text = Constants.LicenseDetail["clientname"];
                this.TextBlockName.Text = Constants.LicenseDetail["firstname"] + " " + Constants.LicenseDetail["lastname"];
                this.TextBlockExpirationDate.Text = Constants.LicenseDetail["expirationdate"];
                //if (Constants.LicenseDetail["numberofclients"].Equals("-1"))
                //    this.TextBlockNumberOfUsers.Text = "Unlimited";
                //else
                //    this.TextNumberofClients.Text = Constants.LicenseDetail["numberofclients"];
                if (Constants.LicenseDetail["numberofusers"].Equals("-1"))
                    this.TextBlockNumberOfUsers.Text = "Unlimited";
                else
                    this.TextBlockNumberOfUsers.Text = Constants.LicenseDetail["numberofusers"];

                this.TextBlockEmail.Text = Constants.LicenseDetail["email"];
                this.TextBlockPhone.Text = Constants.LicenseDetail["phone"];

                if (Convert.ToBoolean(Constants.LicenseDetail["istrial"]))
                    this.TextIsTrail.Text = "Trial";
                // else
                //   this.rdbNo.IsChecked = true;
            }
            catch (Exception ex)
            {
                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }
        }
        #endregion


        #region IContent

        public void OnFragmentNavigation(FirstFloor.ModernUI.Windows.Navigation.FragmentNavigationEventArgs e)
        {
            //  throw new NotImplementedException();
        }

        public void OnNavigatedFrom(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
            // throw new NotImplementedException();
        }

        public void OnNavigatedTo(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
            //  this.LoadFilterList();
            // this.LodadUserList();

        }

        public void OnNavigatingFrom(FirstFloor.ModernUI.Windows.Navigation.NavigatingCancelEventArgs e)
        {
            // throw new NotImplementedException();
        }

        #endregion

        private void btnUpgradeLicense_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var confirm = ModernDialog.ShowMessage("Upgrading the License will restart the applciation, please save all changes before proceeding. Are you sure you want to continue?", "Warning!", MessageBoxButton.YesNo);
                if (confirm == MessageBoxResult.No)
                    return;
                LicenseDialog dialog = new LicenseDialog();
                dialog.ShowDialog();

                if (dialog.IsSuccess)
                    App.Current.Shutdown();
            }
            catch (Exception ex)
            {
                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }
        }


    }
}
