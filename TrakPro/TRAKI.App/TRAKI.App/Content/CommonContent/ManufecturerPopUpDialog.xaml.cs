﻿using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TRAKI.App.Common;
using TRAKI.App.Model;
using TRAKI.App.ViewModel;

namespace TRAKI.App.Content.CommonContent
{
    /// <summary>
    /// Interaction logic for ManufacturerPopUpDialog.xaml
    /// </summary>
    public partial class ManufacturerPopUpDialog : ModernDialog
    {
        private string _ManufacturerDialogResult;
        private int pageIndex = 1;
        private ManufacturerViewModel manufacturerViewModel;
        private List<Manufacturer> _manufacturerList = new List<Manufacturer>();

        public ManufacturerPopUpDialog()
        {
            InitializeComponent();
            this.Loaded += OnLoaded;
        }

        #region Private

        void OnLoaded(object sender, RoutedEventArgs e)
        {
            this.LoadManufacturerList();
        }
        private void LoadManufacturerList()
        {
            try
            {
                manufacturerViewModel = new ManufacturerViewModel();
                _manufacturerList = new List<Manufacturer>(manufacturerViewModel.GetManufacturer(null, null).ToList());
                foreach (var Manufacturer in _manufacturerList)
                {
                    Manufacturer.IsActive = Common.BooleanValue.False;
                }
                this.ManufacturerDataGrid.ItemsSource = CollectionViewSource.GetDefaultView(_manufacturerList.Take(Constants.NumberOfRecordsPerPage));
                this.ManufacturerDataGrid.Items.Refresh();
                if (manufacturerViewModel.RecordCount == 0)
                    this.TextBlockPages.Text = "0";
                else
                    this.TextBlockPages.Text = "1";
                this.Navigate((int)PagingMode.First);
                this.DataContext = manufacturerViewModel;
            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        private void ManufacturerDataGrid_SelectionChanged(object sender, RoutedEventArgs e)
        {
            try
            {
                var datagrid = sender as DataGrid;
                ((Manufacturer)datagrid.SelectedItem).IsActive = BooleanValue.True;
                this.ManufacturerDialogResult = ((Manufacturer)datagrid.SelectedItem).ManufacturerCode + "•" + ((Manufacturer)datagrid.SelectedItem).ManufacturerName;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void SearchTextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = IsMatching.Check(e.Text);
        }
        #endregion

        #region Pagination

        private void btnFirst_Click(object sender, System.EventArgs e)
        {

            Navigate((int)PagingMode.First);
            this.TextBlockPages.Text = "1";
        }

        private void btnNext_Click(object sender, System.EventArgs e)
        {
            Navigate((int)PagingMode.Next);
            this.TextBlockPages.Text = this.pageIndex.ToString();
        }

        private void btnPrev_Click(object sender, System.EventArgs e)
        {
            Navigate((int)PagingMode.Previous);
            this.TextBlockPages.Text = this.pageIndex.ToString();
        }

        private void btnLast_Click(object sender, System.EventArgs e)
        {
            Navigate((int)PagingMode.Last);
            this.TextBlockPages.Text = this.pageIndex.ToString();
        }

        //private void cbNumberOfRecords_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    Navigate((int)PagingMode.PageCountChange);
        //}

        private void Navigate(int mode)
        {
            int count;
            switch (mode)
            {
                case (int)PagingMode.Next:
                    btnPrev.IsEnabled = true;
                    btnFirst.IsEnabled = true;
                    if (_manufacturerList.Count >= (pageIndex * Constants.NumberOfRecordsPerPage))
                    {

                        if (_manufacturerList.Skip(pageIndex * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage).Count() == 0)
                        {
                            this.ManufacturerDataGrid.ItemsSource = null;
                            this.ManufacturerDataGrid.ItemsSource = _manufacturerList.Skip((pageIndex * Constants.NumberOfRecordsPerPage) - Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage);
                            count = (pageIndex * Constants.NumberOfRecordsPerPage) + (_manufacturerList.Skip(pageIndex * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage)).Count();
                        }
                        else
                        {
                            this.ManufacturerDataGrid.ItemsSource = null;
                            this.ManufacturerDataGrid.ItemsSource = _manufacturerList.Skip(pageIndex * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage);
                            count = (pageIndex * Constants.NumberOfRecordsPerPage) + (_manufacturerList.Skip(pageIndex * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage)).Count();
                            pageIndex++;
                        }

                        if ((pageIndex * Constants.NumberOfRecordsPerPage) >= _manufacturerList.Count)
                        {
                            btnNext.IsEnabled = false;
                            btnLast.IsEnabled = false;
                        }

                    }
                    else
                    {
                        btnNext.IsEnabled = false;
                        btnLast.IsEnabled = false;
                    }

                    break;
                case (int)PagingMode.Previous:
                    btnNext.IsEnabled = true;
                    btnLast.IsEnabled = true;
                    pageIndex -= 1;

                    if (pageIndex >= 1)
                    {
                        this.ManufacturerDataGrid.ItemsSource = null;
                        if (pageIndex == 1)
                        {
                            btnPrev.IsEnabled = false;
                            btnFirst.IsEnabled = false;
                            this.ManufacturerDataGrid.ItemsSource = _manufacturerList.Take(Constants.NumberOfRecordsPerPage);
                            count = _manufacturerList.Take(Constants.NumberOfRecordsPerPage).Count();
                        }
                        else
                        {
                            this.ManufacturerDataGrid.ItemsSource = _manufacturerList.Skip((pageIndex - 1) * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage);
                            count = Math.Min((pageIndex - 1) * Constants.NumberOfRecordsPerPage, _manufacturerList.Count);
                        }
                    }
                    else
                    {
                        btnPrev.IsEnabled = false;
                        btnFirst.IsEnabled = false;
                    }
                    if (_manufacturerList.Count <= Constants.NumberOfRecordsPerPage)
                    {
                        btnNext.IsEnabled = false;
                        btnLast.IsEnabled = false;
                        btnPrev.IsEnabled = false;
                        btnFirst.IsEnabled = false;
                    }
                    break;

                case (int)PagingMode.First:
                    pageIndex = 2;
                    Navigate((int)PagingMode.Previous);
                    break;
                case (int)PagingMode.Last:
                    pageIndex = (_manufacturerList.Count / Constants.NumberOfRecordsPerPage);
                    Navigate((int)PagingMode.Next);
                    break;

                case (int)PagingMode.PageCountChange:
                    pageIndex = 1;
                    this.ManufacturerDataGrid.ItemsSource = null;
                    this.ManufacturerDataGrid.ItemsSource = _manufacturerList.Take(Constants.NumberOfRecordsPerPage);
                    count = (_manufacturerList.Take(Constants.NumberOfRecordsPerPage)).Count();
                    btnNext.IsEnabled = true;
                    btnPrev.IsEnabled = true;
                    break;
            }
        }

        #endregion

        #region Search Region

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            string searchText = this.SearchTextBox.Text.Trim();
            if (string.IsNullOrEmpty(searchText))
                return;
            try
            {
                this.LoadSearchData(searchText);
            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage("Some error occured, please try again! " + ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        private void SearchTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                string searchText = ((TextBox)sender).Text.Trim();
                if (!string.IsNullOrEmpty(searchText))
                    return;
                this.LoadManufacturerList();
            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage("Some error occured, please try again! " + ex.Message, "Error!", MessageBoxButton.OK);
            }

        }

        private void SearchTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                string searchText = ((TextBox)sender).Text.Trim();
                if (string.IsNullOrEmpty(searchText))
                    return;
                if (e.Key == Key.Enter)
                    this.LoadSearchData(searchText);
            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage("Some error occured, please try again! " + ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        private void LoadSearchData(string SearchText)
        {
            try
            {
                manufacturerViewModel = new ManufacturerViewModel();
                _manufacturerList = new List<Manufacturer>(manufacturerViewModel.SearchManufacturer(SearchText, null).ToList());
                foreach (var Manufacturer in _manufacturerList)
                {
                    Manufacturer.IsActive = Common.BooleanValue.False;
                }
                this.ManufacturerDataGrid.ItemsSource = CollectionViewSource.GetDefaultView(_manufacturerList.Take(Constants.NumberOfRecordsPerPage));
                this.ManufacturerDataGrid.Items.Refresh();
                if (manufacturerViewModel.RecordCount == 0)
                    this.TextBlockPages.Text = "0";
                else
                    this.TextBlockPages.Text = "1";
                this.Navigate((int)PagingMode.First);
                this.DataContext = manufacturerViewModel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        public string ManufacturerDialogResult
        {
            get { return _ManufacturerDialogResult; }
            set { _ManufacturerDialogResult = value; }
        }
    }
}
