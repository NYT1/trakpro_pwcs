﻿using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TRAKI.App.Common;
using TRAKI.App.Model;
using TRAKI.App.Utilities;
using TRAKI.App.ViewModel;

namespace TRAKI.App.Content.CommonContent
{
    /// <summary>
    /// Interaction logic for MissingBuildingDialog.xaml
    /// </summary>
    public partial class MissingBuildingDialog : ModernDialog
    {
        private SelectionList<Building> buildingList;
        private IEnumerable<string> _selectedBuildingNumbers;

        private BuildingViewModel buildingViewModel;

        public MissingBuildingDialog()
        {
            InitializeComponent();
            if (Constants.needBldgSelect == false)
                this.GrpMain.Visibility = Visibility.Collapsed;
            else
            {
                this.GrpMain.Visibility = Visibility.Visible;
                this.BindData();
            }
        }

        #region Private

        private void BindData()
        {
            buildingViewModel = new BuildingViewModel();

            buildingList = new SelectionList<Building>(buildingViewModel.GetOpenBuildings().ToList());
            buildingCheckBoxList.ItemsSource = buildingList;// buildingViewModel.GetOpenBuildings();
            buildingList.PropertyChanged += buildingList_PropertyChanged;

            this.noBuildingMessage.Visibility = (buildingList.Count > 0) ? Visibility.Collapsed : Visibility.Visible;
            this.checkBoxAllBuildings.Visibility = (buildingList.Count > 0) ? Visibility.Visible : Visibility.Collapsed;
            this.OkButton.IsEnabled = buildingList.SelectionCount > 0;
        }

        private void buildingList_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            // the save button is enabled if at least one item is checked
            this.OkButton.IsEnabled = buildingList.SelectionCount > 0;
            SelectedBuildingNumbers = buildingList.GetSelection(elt => elt.Element.BuildingNumber);
            
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            foreach (var item in buildingList)
            {
                item.IsSelected = true;
            }
            buildingCheckBoxList.ItemsSource = buildingList;
            buildingCheckBoxList.Items.Refresh();
        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            foreach (var item in buildingList)
            {
                item.IsSelected = false;
            }
            buildingCheckBoxList.ItemsSource = buildingList;
            buildingCheckBoxList.Items.Refresh();
        }
        #endregion

        #region Properties
        
        public IEnumerable<string> SelectedBuildingNumbers
        {
            get { return _selectedBuildingNumbers; }
            set { _selectedBuildingNumbers = value; }
        }

        #endregion

        private void buildingCheckBoxList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            foreach (SelectionItem<Building> item in this.buildingCheckBoxList.SelectedItems)
            {
                item.IsSelected = true;// !item.IsSelected;
            }
        }

    }
}
