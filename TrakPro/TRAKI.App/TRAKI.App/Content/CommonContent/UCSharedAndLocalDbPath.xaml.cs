﻿using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TRAKI.App.Common;
using TRAKI.App.Model;
using TRAKI.App.Utilities;
using TRAKI.App.ViewModel;
using TRAKI.App.ViewModel.Common;
using System.Collections.ObjectModel;
using TRAKI.App.Common;
using TRAKI.App.Content.CommonContent;
using TRAKI.App.Content.SystemControlContent;

namespace TRAKI.App.Content.CommonContent
{
    /// <summary>
    /// Interaction logic for PreviousLocationPopUpDialog.xaml
    /// </summary>
    public partial class UCSharedAndLocalDbPath : UserControl
    {
        string connectionString = string.Empty;
        bool isSharedDbPathSetSuccess;
        protected DbContext dbContext;
        private ObservableCollection<SystemControl> SCList;
        private bool isStartupDialog;

        public bool IsSharedDbPathSetSuccess
        {
            get { return isSharedDbPathSetSuccess; }
            set { isSharedDbPathSetSuccess = value; }
        }

        bool isLocalDbPathSetSuccess;
        public bool IsLocalDbPathSetSuccess
        {
            get { return isLocalDbPathSetSuccess; }
            set { isLocalDbPathSetSuccess = value; }
        }

        public UCSharedAndLocalDbPath(bool IsStartUpDialog)
        {
            try
            {
                InitializeComponent();
                this.isStartupDialog = IsStartUpDialog;
                this.btnClose.Visibility = Visibility.Collapsed;
                this.DataContext = new SettingsViewModel();
                this.lblLocalPathHeading.Text = "Set Local Database Path";
                this.lblSharedPathHeading.Text = "Set Main Shared Database Path";
                this.SetDbPaths();
            }
            catch (Exception ex)
            {
                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                ModernDialog.ShowMessage("Some error occured, please try again!", "Error!", MessageBoxButton.OK);
            }
        }

        public UCSharedAndLocalDbPath()
        {
            try
            {
                InitializeComponent();
                this.btnCloseDialog.Visibility = Visibility.Collapsed;
                this.isStartupDialog = this.IsSharedDbPathSetSuccess = this.IsLocalDbPathSetSuccess = false;
                this.DataContext = new SettingsViewModel();
                this.messageInfo.Visibility = Visibility.Collapsed;// this.Visibility="{Binding isStartupDialog}" 
                this.SetDbPaths();

            }
            catch (Exception ex)
            {
                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                ModernDialog.ShowMessage("Some error occured, please try again!", "Error!", MessageBoxButton.OK);

            }
        }



        #region Private

      

        private void SetDbPaths()
        {
            try
            {
                // Local Db File Path
                SettingsViewModel settingViewModel = new SettingsViewModel();
                //string fileLocation = System.IO.Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, @"AppData\\LocalDbPath.txt");
                string fileLocation = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + @"\NYT Inc\Asset TRAK Pro\LocalDbPath.txt";


                string fileLocation1 = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + @"\NYT Inc\Asset TRAK Pro\AppInfo.txt";

                if (File.Exists(fileLocation) && File.Exists(fileLocation1))
                {
                    System.Data.OleDb.OleDbConnectionStringBuilder builder = new System.Data.OleDb.OleDbConnectionStringBuilder(settingViewModel.GetConnectionString(fileLocation));
                    this.TextLocalDBFilePath.Text = builder.DataSource;

                    //fileLocation = System.IO.Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, @"AppData\\AppInfo.txt");

                    builder = new System.Data.OleDb.OleDbConnectionStringBuilder(settingViewModel.GetConnectionString(fileLocation1));
                    //IsMatching.writeLog("AppData\\AppInfo.txt" + builder);



                    //
                    ///GetFolder Details

                    if (builder.DataSource != "")
                    {

                        SCList = settingViewModel.GetSystemControl(1);

                        if (SCList != null)
                        {
                            this.DataContext = SCList;
                            this.TextArchiveFolder.Text = ((ObservableCollection<SystemControl>)this.DataContext)[0].Path.ToString();


                            //Get  Delete folder Details
                            SCList = settingViewModel.GetSystemControl(2);

                            this.DataContext = SCList;
                            var iVal = ((ObservableCollection<SystemControl>)this.DataContext)[0].cValue.ToString();
                            if (iVal == "1")
                            {
                                this.CheckboxDelete.IsChecked = true;
                            }
                            else
                            {
                                this.CheckboxDelete.IsChecked = false;
                            }

                        }
                    }
                    //-------------------------------------------------------
                    this.TextDBFilePath.Text = builder.DataSource;
                    if (builder.Count > 0)
                        this.TextDBPassword.Password = (string)builder["Jet OLEDB:Database Password"];

                    if (string.IsNullOrEmpty(this.TextDBFilePath.Text))
                    {
                        this.statusMessage.Text = "Shared Database path Not found";
                        this.statusMessage.Foreground = Brushes.Red;
                        this.IsSharedDbPathSetSuccess = false;
                    }
                    if (string.IsNullOrEmpty(this.TextLocalDBFilePath.Text))
                    {
                        if (this.statusMessage.Text.Length > 0)
                            this.statusMessage.Text = "Both Shared and Local Database paths not found!";
                        else
                            this.statusMessage.Text = "local Database path Not found";

                        this.statusMessage.Foreground = Brushes.Red;
                        this.IsLocalDbPathSetSuccess = false;
                    }

                    this.statusMessage.Visibility = Visibility.Visible;
                    this.DataContext = settingViewModel;
                }
            }
            catch (Exception ex)
            {
                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                //IsMatching.writeLog(ex.Message);
                ModernDialog.ShowMessage("Some error occured, please try again!", "Error!", MessageBoxButton.OK);

            }
        }

        private void btnBrowse_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Create OpenFileDialog
                //  System.Windows.Forms.OpenFileDialog dialog = new System.Windows.Forms.OpenFileDialog();
                Microsoft.Win32.OpenFileDialog dialog = new Microsoft.Win32.OpenFileDialog();
                dialog.Filter = "Access DB File (*.mdb;*.accdb;)|*.mdb;*.accdb";
                // dialog.Filter = "Access DB File (*.mdb)|*.mdb";
                dialog.InitialDirectory = @"C:\";
                dialog.Title = "Please select a Database file.";
                dialog.Multiselect = false;
                if (dialog.ShowDialog() != null)// == System.Windows.Forms.DialogResult.OK)
                {
                    if (!string.IsNullOrEmpty(dialog.FileName))
                        this.TextDBFilePath.Text = dialog.FileName;
                }

            }
            catch (Exception ex)
            {
                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                ModernDialog.ShowMessage("Some error occured, please try again!", "Error!", MessageBoxButton.OK);

            }
        }



        private void btnChangeUpdateDBPath_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!this.TestDBConnection(this.TextDBFilePath.Text.Trim(), this.TextDBPassword.Password, "shared"))
                    return;


                if (!this.isStartupDialog)
                {

                    if (!this.ShowMessageDialog("You are attempting to change the database Path, the Action will be saved and can not be undone." + Environment.NewLine + "You will be logged out of the application and any pending workig will be lost." + Environment.NewLine + "Are you sure you want to Continue?", "Warning!"))
                        return;
                }
                SettingsViewModel settingViewModel = new SettingsViewModel();

                //IsMatching.writeLog("btnChangeUpdateDBPath_Click  " + connectionString);
                if (settingViewModel.UpdateDBConnectionFile(connectionString))
                {
                    this.statusMessage.Text = "Database Path Updated Successfully";
                    this.statusMessage.Foreground = Brushes.Green;
                    this.IsSharedDbPathSetSuccess = true;
                    if (!this.isStartupDialog)
                    {
                        ModernDialog.ShowMessage("The Database path has been Successfully Changed, You need to loggin again to continue working!", "Success!", MessageBoxButton.OK);
                        //IsMatching.writeLog("The Database path has been Successfully Changed, You need to loggin again to continue working!");
                        NavigationCommands.GoToPage.Execute(" /Pages/LogoutPage.xaml", this);
                    }
                }
                else
                {
                    this.statusMessage.Text = "Database Path Updation Failed!";
                }

                this.statusMessage.Visibility = Visibility.Visible;
                // }
            }

            catch (Exception ex)
            {
                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                //IsMatching.writeLog(ex.Message);
                this.statusMessage.Text = "Error while Updating the Database Path!" + ex.Message;
            }

        }

        private bool ShowMessageDialog(string Message, string Title)
        {
            ModernDialog dialog = new ModernDialog();
            dialog.Title = Title;
            dialog.OkButton.Content = "Yes";
            dialog.CancelButton.Content = "No";
            dialog.Content = Message;
            dialog.Buttons = new Button[] { dialog.OkButton, dialog.CancelButton };
            dialog.ShowDialog();
            return dialog.DialogResult.Value;
        }

        // TO DO REmove this 
        //private void btnTestConnection_Click(object sender, RoutedEventArgs e)
        //{
        //    this.TestDBConnection(this.TextDBFilePath.Text.Trim(), this.TextDBPassword.Password);

        //}

        private bool TestDBConnection(string DBFilePath, string DBPassword, string type)
        {
            bool result = false;
            SettingsViewModel settingViewModel = new SettingsViewModel();
            try
            {
                if (string.IsNullOrEmpty(DBFilePath))
                {

                    switch (type)
                    {
                        case "shared":
                            this.statusMessage.Text = "Empty Path!, Shared database path not found.";
                            break;
                        //case "local":
                        //     this.statusMessage.Text = "Empty Path!, local database path not found.";
                        //   break;
                        default:
                            this.statusMessage.Text = "Empty Path!, local database path not found.";
                            break;
                    }

                    this.statusMessage.Foreground = Brushes.Red;
                    return false;
                }


                result = settingViewModel.TestDBConnection(DBFilePath, DBPassword, ref connectionString);
                //IsMatching.writeLog("Path successfully Saved");

            }
            catch (Exception ex)
            {
                if (ex.HResult == -2147467259)
                    this.statusMessage.Text = "Error: " + this.TextDBFilePath.Text + " is Invalid File Name or Path ";
                else
                    this.statusMessage.Text = "Error: " + ex.Message;
                //IsMatching.writeLog(ex.Message);
                this.statusMessage.Foreground = Brushes.Red;
                result = false;
            }
            this.statusMessage.Visibility = Visibility.Visible;
            this.DataContext = settingViewModel;
            return result;
        }

        private void btnCloseDialog_Click(object sender, RoutedEventArgs e)
        {
            (sender as Button).IsCancel = true;
        }

        private void TextDBFilePath_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = IsMatching.Check(e.Text);
        }

        private void Label_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = IsMatching.Check(e.Text);
        }


        private void btnLocalPathBrowse_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Create OpenFileDialog
                Microsoft.Win32.OpenFileDialog dialog = new Microsoft.Win32.OpenFileDialog();
                //System.Windows.Forms.OpenFileDialog dialog = new System.Windows.Forms.OpenFileDialog();
                dialog.Filter = "Access DB File (*.mdb;*.accdb)|*.mdb; *.accdb";// dialog.Filter = "Access DB File (*.mdb)|*.mdb";
                dialog.InitialDirectory = @"C:\";
                dialog.Title = "Please select a Database file.";
                dialog.Multiselect = false;
                if (dialog.ShowDialog() != null) // == System.Windows.Forms.DialogResult.OK)
                {
                    if (!string.IsNullOrEmpty(dialog.FileName))
                        this.TextLocalDBFilePath.Text = dialog.FileName;
                }
            }
            catch (Exception ex)
            {
                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                ModernDialog.ShowMessage("Some error occured, please try again!", "Error!", MessageBoxButton.OK);
            }
        }

        private void btnLocalChangeUpdateDBPath_Click(object sender, RoutedEventArgs e)
        {

            try
            {

                if (!this.TestDBConnection(this.TextLocalDBFilePath.Text.Trim(), string.Empty, "local"))
                    return;


                SettingsViewModel settingViewModel = new SettingsViewModel();
                if (settingViewModel.UpdateLocalDBConnectionFile(connectionString))
                {
                    this.statusMessage.Text = "Local Database Path Updated Successfully";
                    this.statusMessage.Foreground = Brushes.Green;
                    this.isLocalDbPathSetSuccess = true;
                    if (!this.isStartupDialog)
                    {
                        ModernDialog.ShowMessage("The Local Database path has been Successfully Changed!", "Success!", MessageBoxButton.OK);
                        //  NavigationCommands.GoToPage.Execute(" /Pages/LogoutPage.xaml", this);
                    }
                }
                else
                {
                    this.statusMessage.Text = "Local Database Path Updation Failed!";
                    //IsMatching.writeLog("Local Database Path Updation Failed");
                }

                this.statusMessage.Visibility = Visibility.Visible;
                // }
            }

            catch (Exception ex)
            {
                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                //IsMatching.writeLog(ex.Message);
                this.statusMessage.Text = "Error while Updating the Local Database Path!" + ex.Message;
            }


        }




        #endregion

        private void btnArchivePathBrowse_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Create FolderBrowserDialog
                System.Windows.Forms.FolderBrowserDialog dialog = new System.Windows.Forms.FolderBrowserDialog();

                if (dialog.ShowDialog() != null)// == System.Windows.Forms.DialogResult.OK)
                {
                    if (!string.IsNullOrEmpty(dialog.SelectedPath))
                        this.TextArchiveFolder.Text = dialog.SelectedPath;
                }
            }
            catch (Exception ex)
            {
                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                ModernDialog.ShowMessage("Some error occured, please try again!", "Error!", MessageBoxButton.OK);

            }
        }

        private void TextArchiveFolder_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {

        }

        private void btnSaveFolder_Click(object sender, RoutedEventArgs e)
        {

            //Update the folder path
            SettingsViewModel settingViewModel = new SettingsViewModel();
            SystemControl systemControl = new SystemControl();
            systemControl.SystemControlId = 1;
            systemControl.ControlName = "ArchiveFolderPath";

            if (TextArchiveFolder.Text == null)
            {
                systemControl.Path = "C:\\Users\\Default\\AppData\\Local\\NYT\\Asset Trak Pro\\InventoryDataArchive";
            }
            else
            {
                systemControl.Path = TextArchiveFolder.Text;
            }
            systemControl.Password = "";
            systemControl.cValue = "0";
            if (Constants.ConnectionString == "")
            {

                ModernDialog.ShowMessage("Inventory Data Processing Settings will be set after database path has been set", "Message!", MessageBoxButton.OK);
            }
            else
            {
                var result = settingViewModel.UpdateSystemControl(systemControl);  //Steve

                //Update the Checkboxvalue
                // SystemControl systemControl = new SystemControl();
                systemControl.SystemControlId = 2;
                systemControl.ControlName = "DeleteUploadFile";
                systemControl.Path = "";
                systemControl.Password = "";
                if (CheckboxDelete.IsChecked == true)
                {
                    systemControl.cValue = "1";
                }
                else
                {
                    systemControl.cValue = "0";
                }
                var iresult = settingViewModel.UpdateSystemControl(systemControl);
                this.statusMessage.Text = "The Archive folder path and Delete file after Archive settings has been set Successfully";
                this.statusMessage.Foreground = Brushes.Green;
                ModernDialog.ShowMessage("The Archive folder path and Delete file after Archive settings has been set Successfully", "Success!", MessageBoxButton.OK);
            }
        }

    }
  
}
