﻿using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TRAKI.App.Common;
using TRAKI.App.Model;
using TRAKI.App.ViewModel;

namespace TRAKI.App.Content.CommonContent
{
    /// <summary>
    /// Interaction logic for RoomPopUpDialog.xaml
    /// </summary>
    public partial class RoomPopUpDialog : ModernDialog
    {

        private string _roomDialogResult;
        private int pageIndex = 1;
        private RoomViewModel roomViewModel;
        private List<Room> _roomList = new List<Room>();
        private int buildingId = 0;

        public RoomPopUpDialog(int BuildingID)
        {
            InitializeComponent();
            this.buildingId = BuildingID;
            this.Loaded += OnLoaded;
        }

        #region Private

        void OnLoaded(object sender, RoutedEventArgs e)
        {
            this.LoadRoomList();
        }

        private void LoadRoomList()
        {
            try
            {
                roomViewModel = new RoomViewModel();
                _roomList = new List<Room>(roomViewModel.GetRoomsByBuilding(this.buildingId, -1).ToList());
                foreach (var Room in _roomList)
                {
                    Room.IsActive = Common.BooleanValue.False;
                }
                this.RoomDialogDataGrid.ItemsSource = CollectionViewSource.GetDefaultView(_roomList.Take(Constants.NumberOfRecordsPerPage));
                this.RoomDialogDataGrid.Items.Refresh();
                if (roomViewModel.RecordCount == 0)
                    this.TextBlockPages.Text = "0";
                else
                    this.TextBlockPages.Text = "1";
                this.Navigate((int)PagingMode.First);
                this.DataContext = roomViewModel;
            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        private void RoomDialogDataGrid_SelectionChanged(object sender, RoutedEventArgs e)
        {
            try
            {
                var datagrid = sender as DataGrid;
                ((Room)datagrid.SelectedItem).IsActive = BooleanValue.True;
                this.RoomDialogResult = ((Room)datagrid.SelectedItem).RoomNumber + "•" + ((Room)datagrid.SelectedItem).RoomDescription;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void SearchTextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = IsMatching.Check(e.Text);
        }

        #endregion

        #region Pagination

        private void btnFirst_Click(object sender, System.EventArgs e)
        {

            Navigate((int)PagingMode.First);
            this.TextBlockPages.Text = "1";
        }

        private void btnNext_Click(object sender, System.EventArgs e)
        {
            Navigate((int)PagingMode.Next);
            this.TextBlockPages.Text = this.pageIndex.ToString();
        }

        private void btnPrev_Click(object sender, System.EventArgs e)
        {
            Navigate((int)PagingMode.Previous);
            this.TextBlockPages.Text = this.pageIndex.ToString();
        }

        private void btnLast_Click(object sender, System.EventArgs e)
        {
            Navigate((int)PagingMode.Last);
            this.TextBlockPages.Text = this.pageIndex.ToString();
        }

        //private void cbNumberOfRecords_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    Navigate((int)PagingMode.PageCountChange);
        //}

        private void Navigate(int mode)
        {
            int count;
            switch (mode)
            {
                case (int)PagingMode.Next:
                    btnPrev.IsEnabled = true;
                    btnFirst.IsEnabled = true;
                    if (_roomList.Count >= (pageIndex * Constants.NumberOfRecordsPerPage))
                    {

                        if (_roomList.Skip(pageIndex * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage).Count() == 0)
                        {
                            this.RoomDialogDataGrid.ItemsSource = null;
                            this.RoomDialogDataGrid.ItemsSource = _roomList.Skip((pageIndex * Constants.NumberOfRecordsPerPage) - Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage);
                            count = (pageIndex * Constants.NumberOfRecordsPerPage) + (_roomList.Skip(pageIndex * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage)).Count();
                        }
                        else
                        {
                            this.RoomDialogDataGrid.ItemsSource = null;
                            this.RoomDialogDataGrid.ItemsSource = _roomList.Skip(pageIndex * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage);
                            count = (pageIndex * Constants.NumberOfRecordsPerPage) + (_roomList.Skip(pageIndex * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage)).Count();
                            pageIndex++;
                        }

                        if ((pageIndex * Constants.NumberOfRecordsPerPage) >= _roomList.Count)
                        {
                            btnNext.IsEnabled = false;
                            btnLast.IsEnabled = false;
                        }

                    }
                    else
                    {
                        btnNext.IsEnabled = false;
                        btnLast.IsEnabled = false;
                    }

                    break;
                case (int)PagingMode.Previous:
                    btnNext.IsEnabled = true;
                    btnLast.IsEnabled = true;
                    pageIndex -= 1;
                    if (pageIndex >= 1)
                    {
                        this.RoomDialogDataGrid.ItemsSource = null;
                        if (pageIndex == 1)
                        {
                            btnPrev.IsEnabled = false;
                            btnFirst.IsEnabled = false;
                            this.RoomDialogDataGrid.ItemsSource = _roomList.Take(Constants.NumberOfRecordsPerPage);
                            count = _roomList.Take(Constants.NumberOfRecordsPerPage).Count();
                        }
                        else
                        {
                            this.RoomDialogDataGrid.ItemsSource = _roomList.Skip((pageIndex - 1) * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage);
                            count = Math.Min((pageIndex - 1) * Constants.NumberOfRecordsPerPage, _roomList.Count);
                        }
                    }
                    else
                    {
                        btnPrev.IsEnabled = false;
                        btnFirst.IsEnabled = false;
                    }
                    if (_roomList.Count <= Constants.NumberOfRecordsPerPage)
                    {
                        btnNext.IsEnabled = false;
                        btnLast.IsEnabled = false;
                        btnPrev.IsEnabled = false;
                        btnFirst.IsEnabled = false;
                    }
                    break;

                case (int)PagingMode.First:
                    pageIndex = 2;
                    Navigate((int)PagingMode.Previous);
                    break;
                case (int)PagingMode.Last:
                    pageIndex = (_roomList.Count / Constants.NumberOfRecordsPerPage);
                    Navigate((int)PagingMode.Next);
                    break;

                case (int)PagingMode.PageCountChange:
                    pageIndex = 1;
                    this.RoomDialogDataGrid.ItemsSource = null;
                    this.RoomDialogDataGrid.ItemsSource = _roomList.Take(Constants.NumberOfRecordsPerPage);
                    count = (_roomList.Take(Constants.NumberOfRecordsPerPage)).Count();
                    btnNext.IsEnabled = true;
                    btnPrev.IsEnabled = true;
                    break;
            }
        }

        #endregion

        #region Search Region

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            string searchText = this.SearchTextBox.Text.Trim();
            if (string.IsNullOrEmpty(searchText))
                return;
            try
            {
                this.LoadSearchData(searchText);
            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage("Some error occured, please try again! " + ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        private void SearchTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            string searchText = ((TextBox)sender).Text.Trim();
            if (!string.IsNullOrEmpty(searchText))
                return;
            try
            {
                this.LoadRoomList();

            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage("Some error occured, please try again! " + ex.Message, "Error!", MessageBoxButton.OK);
            }

        }

        private void LoadSearchData(string SearchText)
        {
            try
            {
                roomViewModel = new RoomViewModel();
                _roomList = new List<Room>(roomViewModel.SearchRoom(SearchText, this.buildingId, null));
                foreach (var Room in _roomList)
                {
                    Room.IsActive = Common.BooleanValue.False;
                }
                this.RoomDialogDataGrid.ItemsSource = CollectionViewSource.GetDefaultView(_roomList.Take(Constants.NumberOfRecordsPerPage));
                this.RoomDialogDataGrid.Items.Refresh();
                if (roomViewModel.RecordCount == 0)
                    this.TextBlockPages.Text = "0";
                else
                    this.TextBlockPages.Text = "1";
                this.Navigate((int)PagingMode.First);
                this.DataContext = roomViewModel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #endregion

        public string RoomDialogResult
        {
            get { return _roomDialogResult; }
            set { _roomDialogResult = value; }
        }


    }
}
