﻿using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TRAKI.App.Common;
using TRAKI.App.Model;
using TRAKI.App.ViewModel;

namespace TRAKI.App.Content.CommonContent
{
    /// <summary>
    /// Interaction logic for BarcodeHistoryDialog.xaml
    /// </summary>
    public partial class BarcodeHistoryDialog  : UserControl
    {

        private string _barcodeDialogResult;
        private int pageIndex = 1;
        private int _assetId = 0;
        private AssetBarcodeViewModel assetBarcodeViewModel;
        private List<AssetBarcode> _assetBarcodeList = new List<AssetBarcode>();


        public BarcodeHistoryDialog(int AssetId)
        {
            InitializeComponent();
            this._assetId = AssetId;
            this.Loaded += OnLoaded;
        }

        void OnLoaded(object sender, RoutedEventArgs e)
        {
            this.GetBarcodeList();
        }


        private void GetBarcodeList()
        {
            try
            {
                assetBarcodeViewModel = new AssetBarcodeViewModel();
                _assetBarcodeList = new List<AssetBarcode>(assetBarcodeViewModel.GetAssetBarcodeDetail(this._assetId));
                this.DataContext = assetBarcodeViewModel.BarcodeList;
                this.AssetBarcodeDataGrid.ItemsSource = CollectionViewSource.GetDefaultView(_assetBarcodeList.Take(Constants.NumberOfRecordsPerPage));
                this.AssetBarcodeDataGrid.Items.Refresh();
                if (assetBarcodeViewModel.RecordCount == 0)
                    this.TextBlockPages.Text = "0";
                else
                    this.TextBlockPages.Text = "1";
                Navigate((int)PagingMode.First);
                this.EmptyMessageBlock.DataContext = assetBarcodeViewModel;
             
            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }
        }


        #region Pagination

        private void btnFirst_Click(object sender, System.EventArgs e)
        {

            Navigate((int)PagingMode.First);
            this.TextBlockPages.Text = "1";
        }

        private void btnNext_Click(object sender, System.EventArgs e)
        {
            Navigate((int)PagingMode.Next);
            this.TextBlockPages.Text = this.pageIndex.ToString();
        }

        private void btnPrev_Click(object sender, System.EventArgs e)
        {
            Navigate((int)PagingMode.Previous);
            this.TextBlockPages.Text = this.pageIndex.ToString();
        }

        private void btnLast_Click(object sender, System.EventArgs e)
        {
            Navigate((int)PagingMode.Last);
            this.TextBlockPages.Text = this.pageIndex.ToString();
        }

        //private void cbNumberOfRecords_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    Navigate((int)PagingMode.PageCountChange);
        //}

        private void Navigate(int mode)
        {
            int count;
            switch (mode)
            {
                case (int)PagingMode.Next:
                    btnPrev.IsEnabled = true;
                    btnFirst.IsEnabled = true;
                    if (_assetBarcodeList.Count >= (pageIndex * Constants.NumberOfRecordsPerPage))
                    {

                        if (_assetBarcodeList.Skip(pageIndex * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage).Count() == 0)
                        {
                            this.AssetBarcodeDataGrid.ItemsSource = null;
                            this.AssetBarcodeDataGrid.ItemsSource = _assetBarcodeList.Skip((pageIndex * Constants.NumberOfRecordsPerPage) - Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage);
                            count = (pageIndex * Constants.NumberOfRecordsPerPage) + (_assetBarcodeList.Skip(pageIndex * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage)).Count();
                        }
                        else
                        {
                            this.AssetBarcodeDataGrid.ItemsSource = null;
                            this.AssetBarcodeDataGrid.ItemsSource = _assetBarcodeList.Skip(pageIndex * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage);
                            count = (pageIndex * Constants.NumberOfRecordsPerPage) + (_assetBarcodeList.Skip(pageIndex * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage)).Count();
                            pageIndex++;
                        }

                        if ((pageIndex * Constants.NumberOfRecordsPerPage) >= _assetBarcodeList.Count)
                        {
                            btnNext.IsEnabled = false;
                            btnLast.IsEnabled = false;
                        }

                    }
                    else
                    {
                        btnNext.IsEnabled = false;
                        btnLast.IsEnabled = false;
                    }

                    break;
                case (int)PagingMode.Previous:
                    btnNext.IsEnabled = true;
                    btnLast.IsEnabled = true;
                    pageIndex -= 1;

                    if (pageIndex >= 1)
                    {
                        this.AssetBarcodeDataGrid.ItemsSource = null;
                        if (pageIndex == 1)
                        {
                            btnPrev.IsEnabled = false;
                            btnFirst.IsEnabled = false;
                            this.AssetBarcodeDataGrid.ItemsSource = _assetBarcodeList.Take(Constants.NumberOfRecordsPerPage);
                            count = _assetBarcodeList.Take(Constants.NumberOfRecordsPerPage).Count();
                        }
                        else
                        {
                            this.AssetBarcodeDataGrid.ItemsSource = _assetBarcodeList.Skip((pageIndex - 1) * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage);
                            count = Math.Min((pageIndex - 1) * Constants.NumberOfRecordsPerPage, _assetBarcodeList.Count);
                        }
                    }
                    else
                    {
                        btnPrev.IsEnabled = false;
                        btnFirst.IsEnabled = false;
                    }

                    if (_assetBarcodeList.Count <= Constants.NumberOfRecordsPerPage)
                    {
                        btnNext.IsEnabled = false;
                        btnLast.IsEnabled = false;
                        btnPrev.IsEnabled = false;
                        btnFirst.IsEnabled = false;
                    }
                    break;

                case (int)PagingMode.First:
                    pageIndex = 2;
                    Navigate((int)PagingMode.Previous);
                    break;
                case (int)PagingMode.Last:
                    pageIndex = (_assetBarcodeList.Count / Constants.NumberOfRecordsPerPage);
                    Navigate((int)PagingMode.Next);
                    break;

                case (int)PagingMode.PageCountChange:
                    pageIndex = 1;
                    this.AssetBarcodeDataGrid.ItemsSource = null;
                    this.AssetBarcodeDataGrid.ItemsSource = _assetBarcodeList.Take(Constants.NumberOfRecordsPerPage);
                    count = (_assetBarcodeList.Take(Constants.NumberOfRecordsPerPage)).Count();
                    btnNext.IsEnabled = true;
                    btnPrev.IsEnabled = true;
                    break;
            }
        }

        #endregion


        public string BarcodeDialogResult
        {
            get { return _barcodeDialogResult; }
            set { _barcodeDialogResult = value; }
        }

    }
}
