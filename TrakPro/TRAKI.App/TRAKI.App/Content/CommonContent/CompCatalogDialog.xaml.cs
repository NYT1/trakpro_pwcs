﻿using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TRAKI.App.Common;
using TRAKI.App.Model;
using TRAKI.App.Utilities;
using TRAKI.App.ViewModel;

namespace TRAKI.App.Content.CommonContent
{
    /// <summary>
    /// Interaction logic for CompCatalogDialog.xaml
    /// </summary>
    public partial class CompCatalogDialog : ModernDialog
    {

        private SelectionList<Catalog> catalogList;
        private IEnumerable<string> _selectedcatalogNumbers;

        private CatalogViewModel catalogViewModel;

        public CompCatalogDialog()
        {
            InitializeComponent();
            if (Constants.needCatalogSelect == false)
                this.GrpMain.Visibility = Visibility.Collapsed;
            else
            {
                this.GrpMain.Visibility = Visibility.Visible;
                this.BindData();
            }
        }


        #region Private

        private void BindData()
        {

            Constants.isComp = 0;
            catalogViewModel = new CatalogViewModel();

            catalogList = new SelectionList<Catalog>(catalogViewModel.GetCompCatalogCheckboxList(null, -1, Constants.isComp).ToList());
            catalogCheckBoxList.ItemsSource = catalogList;// buildingViewModel.GetOpenBuildings();
            catalogList.PropertyChanged += catalogList_PropertyChanged;

            this.noCatalogMessage.Visibility = (catalogList.Count > 0) ? Visibility.Collapsed : Visibility.Visible;
            this.checkBoxAllcatalog.Visibility = (catalogList.Count > 0) ? Visibility.Visible : Visibility.Collapsed;
            this.OkButton.IsEnabled = catalogList.SelectionCount > 0;
        }

        private void catalogList_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            // the save button is enabled if at least one item is checked
            this.OkButton.IsEnabled = catalogList.SelectionCount > 0;
            SelectedCatalogNumbers = catalogList.GetSelection(elt => elt.Element.CatalogNumber);

        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            foreach (var item in catalogList)
            {
                item.IsSelected = true;
            }
            catalogCheckBoxList.ItemsSource = catalogList;
            catalogCheckBoxList.Items.Refresh();
        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            foreach (var item in catalogList)
            {
                item.IsSelected = false;
            }
            catalogCheckBoxList.ItemsSource = catalogList;
            catalogCheckBoxList.Items.Refresh();
        }
        #endregion

        #region Properties

        public IEnumerable<string> SelectedCatalogNumbers
        {
            get { return _selectedcatalogNumbers; }
            set { _selectedcatalogNumbers = value; }
        }

        #endregion

       
        private void catalogCheckBoxList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
           
            foreach (SelectionItem<Catalog> item in this.catalogCheckBoxList.SelectedItems)
            {
                item.IsSelected = true;// !item.IsSelected;
            }
        }

    }
}
