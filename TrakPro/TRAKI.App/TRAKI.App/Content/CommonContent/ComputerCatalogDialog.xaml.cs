﻿using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TRAKI.App.Common;
using TRAKI.App.Model;
using TRAKI.App.ViewModel;

namespace TRAKI.App.Content.CommonContent
{
    /// <summary>
    /// Interaction logic for ComputerCatalogDialog.xaml
    /// </summary>
    public partial class ComputerCatalogDialog :  ModernDialog
    {
        private string _catalogDialogResult;
        private int pageIndex = 1;
        private CatalogViewModel catalogViewModel;
        private List<Catalog> _compcatalogList = new List<Catalog>();

        public ComputerCatalogDialog()
        {
            InitializeComponent();
            this.Loaded += OnLoaded;
        }


        #region Private

        void OnLoaded(object sender, RoutedEventArgs e)
        {
            this.LoadCatalogList();
        }

        private void LoadCatalogList()
        {
            Constants.isComp = -1;
            try
            {
                catalogViewModel = new CatalogViewModel();
                _compcatalogList = new List<Catalog>(catalogViewModel.GetCompCatalog(null, -1, Constants.isComp).ToList());
                foreach (var catalog in _compcatalogList)
                {
                    catalog.IsActive = Common.BooleanValue.False;
                }
                this.CatalogDataGrid.ItemsSource = CollectionViewSource.GetDefaultView(_compcatalogList.Take(Constants.NumberOfRecordsPerPage));
                this.EmptyMessageBlock.DataContext = catalogViewModel;
                this.CatalogDataGrid.Items.Refresh();
                if (catalogViewModel.RecordCount == 0)
                    this.TextBlockPages.Text = "0";
                else
                    this.TextBlockPages.Text = "1";
                this.Navigate((int)PagingMode.First);
                this.DataContext = catalogViewModel;
            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        private void CatalogDataGrid_SelectionChanged(object sender, RoutedEventArgs e)
        {
            try
            {
                var datagrid = sender as DataGrid;
                ((Catalog)datagrid.SelectedItem).IsActive = BooleanValue.True;
                this.CatalogDialogResult = ((Catalog)datagrid.SelectedItem).CatalogNumber + "•" + ((Catalog)datagrid.SelectedItem).CatalogDescription;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void SearchTextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = IsMatching.Check(e.Text);
        }

        #endregion

        #region Pagination

        private void btnFirst_Click(object sender, System.EventArgs e)
        {

            Navigate((int)PagingMode.First);
            this.TextBlockPages.Text = "1";
        }

        private void btnNext_Click(object sender, System.EventArgs e)
        {
            Navigate((int)PagingMode.Next);
            this.TextBlockPages.Text = this.pageIndex.ToString();
        }

        private void btnPrev_Click(object sender, System.EventArgs e)
        {
            Navigate((int)PagingMode.Previous);
            this.TextBlockPages.Text = this.pageIndex.ToString();
        }

        private void btnLast_Click(object sender, System.EventArgs e)
        {
            Navigate((int)PagingMode.Last);
            this.TextBlockPages.Text = this.pageIndex.ToString();
        }

        //private void cbNumberOfRecords_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    Navigate((int)PagingMode.PageCountChange);
        //}

        private void Navigate(int mode)
        {
            int count;
            switch (mode)
            {
                case (int)PagingMode.Next:
                    btnPrev.IsEnabled = true;
                    btnFirst.IsEnabled = true;
                    if (_compcatalogList.Count >= (pageIndex * Constants.NumberOfRecordsPerPage))
                    {

                        if (_compcatalogList.Skip(pageIndex * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage).Count() == 0)
                        {
                            this.CatalogDataGrid.ItemsSource = null;
                            this.CatalogDataGrid.ItemsSource = _compcatalogList.Skip((pageIndex * Constants.NumberOfRecordsPerPage) - Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage);
                            count = (pageIndex * Constants.NumberOfRecordsPerPage) + (_compcatalogList.Skip(pageIndex * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage)).Count();
                        }
                        else
                        {
                            this.CatalogDataGrid.ItemsSource = null;
                            this.CatalogDataGrid.ItemsSource = _compcatalogList.Skip(pageIndex * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage);
                            count = (pageIndex * Constants.NumberOfRecordsPerPage) + (_compcatalogList.Skip(pageIndex * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage)).Count();
                            pageIndex++;
                        }

                        if ((pageIndex * Constants.NumberOfRecordsPerPage) >= _compcatalogList.Count)
                        {
                            btnNext.IsEnabled = false;
                            btnLast.IsEnabled = false;
                        }

                    }
                    else
                    {
                        btnNext.IsEnabled = false;
                        btnLast.IsEnabled = false;
                    }

                    break;
                case (int)PagingMode.Previous:
                    btnNext.IsEnabled = true;
                    btnLast.IsEnabled = true;
                    pageIndex -= 1;

                    if (pageIndex >= 1)
                    {
                        this.CatalogDataGrid.ItemsSource = null;
                        if (pageIndex == 1)
                        {
                            btnPrev.IsEnabled = false;
                            btnFirst.IsEnabled = false;
                            this.CatalogDataGrid.ItemsSource = _compcatalogList.Take(Constants.NumberOfRecordsPerPage);
                            count = _compcatalogList.Take(Constants.NumberOfRecordsPerPage).Count();
                        }
                        else
                        {
                            this.CatalogDataGrid.ItemsSource = _compcatalogList.Skip((pageIndex - 1) * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage);
                            count = Math.Min((pageIndex - 1) * Constants.NumberOfRecordsPerPage, _compcatalogList.Count);
                        }
                    }
                    else
                    {
                        btnPrev.IsEnabled = false;
                        btnFirst.IsEnabled = false;
                    }

                    if (_compcatalogList.Count <= Constants.NumberOfRecordsPerPage)
                    {
                        btnNext.IsEnabled = false;
                        btnLast.IsEnabled = false;
                        btnPrev.IsEnabled = false;
                        btnFirst.IsEnabled = false;
                    }
                    break;

                case (int)PagingMode.First:
                    pageIndex = 2;
                    Navigate((int)PagingMode.Previous);
                    break;
                case (int)PagingMode.Last:
                    pageIndex = (_compcatalogList.Count / Constants.NumberOfRecordsPerPage);
                    Navigate((int)PagingMode.Next);
                    break;

                case (int)PagingMode.PageCountChange:
                    pageIndex = 1;
                    this.CatalogDataGrid.ItemsSource = null;
                    this.CatalogDataGrid.ItemsSource = _compcatalogList.Take(Constants.NumberOfRecordsPerPage);
                    count = (_compcatalogList.Take(Constants.NumberOfRecordsPerPage)).Count();
                    btnNext.IsEnabled = true;
                    btnPrev.IsEnabled = true;
                    break;
            }
        }

        #endregion

        #region Search Region

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            string searchText = this.SearchTextBox.Text.Trim();
            if (string.IsNullOrEmpty(searchText))
                return;
            try
            {
                this.LoadSearchData(searchText);
            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage("Some error occured, please try again! " + ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        private void SearchTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            string searchText = ((TextBox)sender).Text.Trim();
            if (!string.IsNullOrEmpty(searchText))
                return;
            try
            {
                this.LoadCatalogList();
            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage("Some error occured, please try again! " + ex.Message, "Error!", MessageBoxButton.OK);
            }

        }

        private void LoadSearchData(string SearchText)
        {
            try
            {
                catalogViewModel = new CatalogViewModel();
                _compcatalogList = new List<Catalog>(catalogViewModel.SearchCatalog(SearchText, null).ToList());
                foreach (var catalog in _compcatalogList)
                {
                    catalog.IsActive = Common.BooleanValue.False;
                }
                this.CatalogDataGrid.ItemsSource = CollectionViewSource.GetDefaultView(_compcatalogList.Take(Constants.NumberOfRecordsPerPage));
                //  this.EmptyMessageBlock.DataContext = catalogViewModel;
                this.CatalogDataGrid.Items.Refresh();
                if (catalogViewModel.RecordCount == 0)
                    this.TextBlockPages.Text = "0";
                else
                    this.TextBlockPages.Text = "1";
                this.Navigate((int)PagingMode.First);
                this.DataContext = catalogViewModel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        public string CatalogDialogResult
        {
            get { return _catalogDialogResult; }
            set { _catalogDialogResult = value; }
        }



    }
}
