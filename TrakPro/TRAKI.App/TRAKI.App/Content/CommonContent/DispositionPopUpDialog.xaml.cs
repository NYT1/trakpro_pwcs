﻿using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TRAKI.App.Common;
using TRAKI.App.Model;
using TRAKI.App.ViewModel;

namespace TRAKI.App.Content.CommonContent
{
    /// <summary>
    /// Interaction logic for DispositionPopUpDialog.xaml
    /// </summary>
    public partial class DispositionPopUpDialog : ModernDialog
    {
        private string _dispositionDialogResult;
        private int pageIndex = 1;
        private DispositionViewModel dispositionViewModel;
        private List<Disposition> _dispositionList = new List<Disposition>();

        public DispositionPopUpDialog()
        {
            InitializeComponent();
            this.Loaded += OnLoaded;
        }

        #region Private

        void OnLoaded(object sender, RoutedEventArgs e)
        {
            this.LoadDispositionList();
        }

        private void LoadDispositionList()
        {
            try
            {
                dispositionViewModel = new DispositionViewModel();
                _dispositionList = new List<Disposition>(dispositionViewModel.GetDisposition(null, -1).ToList());
                
                foreach (var Disposition in _dispositionList)
                {
                    Disposition.IsActive = Common.BooleanValue.False;
                }
                this.DispositionDataGrid.ItemsSource = CollectionViewSource.GetDefaultView(_dispositionList.Take(Constants.NumberOfRecordsPerPage));
                this.DispositionDataGrid.Items.Refresh();
                if (dispositionViewModel.RecordCount == 0)
                    this.TextBlockPages.Text = "0";
                else
                    this.TextBlockPages.Text = "1";
                this.Navigate((int)PagingMode.First);
                this.DataContext = dispositionViewModel;
            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        private void DispositionDataGrid_SelectionChanged(object sender, RoutedEventArgs e)
        {
            try
            {
                var datagrid = sender as DataGrid;
                ((Disposition)datagrid.SelectedItem).IsActive = BooleanValue.True;
                this.DispositionDialogResult = ((Disposition)datagrid.SelectedItem).DispositionCode + "•" + ((Disposition)datagrid.SelectedItem).DispositionDescription;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void SearchTextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = IsMatching.Check(e.Text);
        }

        #endregion

        #region Pagination

        private void btnFirst_Click(object sender, System.EventArgs e)
        {

            Navigate((int)PagingMode.First);
            this.TextBlockPages.Text = "1";
        }

        private void btnNext_Click(object sender, System.EventArgs e)
        {
            Navigate((int)PagingMode.Next);
            this.TextBlockPages.Text = this.pageIndex.ToString();
        }

        private void btnPrev_Click(object sender, System.EventArgs e)
        {
            Navigate((int)PagingMode.Previous);
            this.TextBlockPages.Text = this.pageIndex.ToString();
        }

        private void btnLast_Click(object sender, System.EventArgs e)
        {
            Navigate((int)PagingMode.Last);
            this.TextBlockPages.Text = this.pageIndex.ToString();
        }

        //private void cbNumberOfRecords_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    Navigate((int)PagingMode.PageCountChange);
        //}

        private void Navigate(int mode)
        {
            int count;
            switch (mode)
            {
                case (int)PagingMode.Next:
                    btnPrev.IsEnabled = true;
                    btnFirst.IsEnabled = true;
                    if (_dispositionList.Count >= (pageIndex * Constants.NumberOfRecordsPerPage))
                    {

                        if (_dispositionList.Skip(pageIndex * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage).Count() == 0)
                        {
                            this.DispositionDataGrid.ItemsSource = null;
                            this.DispositionDataGrid.ItemsSource = _dispositionList.Skip((pageIndex * Constants.NumberOfRecordsPerPage) - Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage);
                            count = (pageIndex * Constants.NumberOfRecordsPerPage) + (_dispositionList.Skip(pageIndex * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage)).Count();
                        }
                        else
                        {
                            this.DispositionDataGrid.ItemsSource = null;
                            this.DispositionDataGrid.ItemsSource = _dispositionList.Skip(pageIndex * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage);
                            count = (pageIndex * Constants.NumberOfRecordsPerPage) + (_dispositionList.Skip(pageIndex * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage)).Count();
                            pageIndex++;
                        }

                        if ((pageIndex * Constants.NumberOfRecordsPerPage) >= _dispositionList.Count)
                        {
                            btnNext.IsEnabled = false;
                            btnLast.IsEnabled = false;
                        }

                    }
                    else
                    {
                        btnNext.IsEnabled = false;
                        btnLast.IsEnabled = false;
                    }

                    break;
                case (int)PagingMode.Previous:
                    btnNext.IsEnabled = true;
                    btnLast.IsEnabled = true;
                    pageIndex -= 1;
                    if (pageIndex >= 1)
                    {
                        this.DispositionDataGrid.ItemsSource = null;
                        if (pageIndex == 1)
                        {
                            btnPrev.IsEnabled = false;
                            btnFirst.IsEnabled = false;
                            this.DispositionDataGrid.ItemsSource = _dispositionList.Take(Constants.NumberOfRecordsPerPage);
                            count = _dispositionList.Take(Constants.NumberOfRecordsPerPage).Count();
                        }
                        else
                        {
                            this.DispositionDataGrid.ItemsSource = _dispositionList.Skip((pageIndex - 1) * Constants.NumberOfRecordsPerPage).Take(Constants.NumberOfRecordsPerPage);
                            count = Math.Min((pageIndex - 1) * Constants.NumberOfRecordsPerPage, _dispositionList.Count);
                        }
                    }
                    else
                    {
                        btnPrev.IsEnabled = false;
                        btnFirst.IsEnabled = false;
                    }
                    if (_dispositionList.Count <= Constants.NumberOfRecordsPerPage)
                    {
                        btnNext.IsEnabled = false;
                        btnLast.IsEnabled = false;
                        btnPrev.IsEnabled = false;
                        btnFirst.IsEnabled = false;
                    }
                    break;

                case (int)PagingMode.First:
                    pageIndex = 2;
                    Navigate((int)PagingMode.Previous);
                    break;
                case (int)PagingMode.Last:
                    pageIndex = (_dispositionList.Count / Constants.NumberOfRecordsPerPage);
                    Navigate((int)PagingMode.Next);
                    break;

                case (int)PagingMode.PageCountChange:
                    pageIndex = 1;
                    this.DispositionDataGrid.ItemsSource = null;
                    this.DispositionDataGrid.ItemsSource = _dispositionList.Take(Constants.NumberOfRecordsPerPage);
                    count = (_dispositionList.Take(Constants.NumberOfRecordsPerPage)).Count();
                    btnNext.IsEnabled = true;
                    btnPrev.IsEnabled = true;
                    break;
            }
        }

        #endregion

        #region Search Region

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            string searchText = this.SearchTextBox.Text.Trim();
            if (string.IsNullOrEmpty(searchText))
                return;
            try
            {
                this.LoadSearchData(searchText);
            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage("Some error occured, please try again! " + ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        private void SearchTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            string searchText = ((TextBox)sender).Text.Trim();
            if (!string.IsNullOrEmpty(searchText))
                return;
            try
            {
                this.LoadDispositionList();

            }
            catch (Exception ex)
            {
                ModernDialog.ShowMessage("Some error occured, please try again! " + ex.Message, "Error!", MessageBoxButton.OK);
            }

        }

        private void LoadSearchData(string SearchText)
        {
            try
            {
                dispositionViewModel = new DispositionViewModel();
                _dispositionList = new List<Disposition>(dispositionViewModel.SearchDisposition(SearchText, null)).ToList();
                foreach (var Disposition in _dispositionList)
                {
                    Disposition.IsActive = Common.BooleanValue.False;
                }
                this.DispositionDataGrid.ItemsSource = CollectionViewSource.GetDefaultView(_dispositionList.Take(Constants.NumberOfRecordsPerPage));
                this.EmptyMessageBlock.DataContext = dispositionViewModel;
                DispositionDataGrid.Items.Refresh();
                if (dispositionViewModel.RecordCount == 0)
                    this.TextBlockPages.Text = "0";
                else
                    this.TextBlockPages.Text = "1";
                this.Navigate((int)PagingMode.First);
                this.DataContext = dispositionViewModel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #endregion

        public string DispositionDialogResult
        {
            get { return _dispositionDialogResult; }
            set { _dispositionDialogResult = value; }
        }
    }
}
