﻿using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TRAKI.App.Common;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;
using TRAKI.App.ViewModel;

namespace TRAKI.App.Content.CatalogContent
{
    /// <summary>
    /// Interaction logic for CreateEditCatalogFrm.xaml
    /// </summary>
    public partial class CreateEditCatalogFrm : UserControl, IContent
    {

        private CatalogViewModel catalogViewModel;
        private int CatalogID = -1;
        private bool _showConfirmationMsg = true;


        public CreateEditCatalogFrm()
        {
            InitializeComponent();

        }

        #region Private

        private void LoadCatalogInfo()
        {
            catalogViewModel = new CatalogViewModel();
            this.DataContext = catalogViewModel.GetCatalog(this.CatalogID, null);
        }

        private void Submit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Catalog catalog = ((ObservableCollection<Catalog>)(sender as Button).DataContext)[0];
                string message = string.Empty;
                var result = catalogViewModel.InsertUpdateCatalog(catalog);
                if (result == 1)
                {
                    message = "Catalog Successfully Created";
                }
                if (result == 2)
                {
                    message = "Catalog Successfully updated";
                }
                ModernDialog.ShowMessage(message, "Success", MessageBoxButton.OK);
                this._showConfirmationMsg = false;
                NavigationCommands.GoToPage.Execute("/Content/CatalogContent/CatalogDetailFrm.xaml#" + null, this);
            }
            catch (Exception ex)
            {
                if (ex.HResult == -2147467259)
                {
                    this.TextCatalogNumber.Focus();
                    ModernDialog.ShowMessage("Catalog Number already exists, Please try another number!", "Error!", MessageBoxButton.OK);
                }
                else
                {
                    ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);

                }
            }
        }

        private void TextCatalogNumber_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = IsMatching.Check(e.Text);
        }

        private void TextCatalogDescription_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = IsMatching.Check(e.Text);
        }
        #endregion

        #region IContent

        public void OnFragmentNavigation(FirstFloor.ModernUI.Windows.Navigation.FragmentNavigationEventArgs e)
        {
            this.CatalogID = -1;
            if (!string.IsNullOrEmpty(e.Fragment))
            {
                this.CatalogID = Convert.ToInt32(e.Fragment);
            }
            this.LoadCatalogInfo();
        }

        public void OnNavigatedFrom(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
        }

        public void OnNavigatedTo(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
            this._showConfirmationMsg = true;
        }

        public void OnNavigatingFrom(FirstFloor.ModernUI.Windows.Navigation.NavigatingCancelEventArgs e)
        {
            if (this._showConfirmationMsg)
            {
                string message = "Are you sure you want to leave?";
                if (e.NavigationType == FirstFloor.ModernUI.Windows.Navigation.NavigationType.Refresh)
                {
                    message = "Are you sure you want to refresh the page?";
                }
                var confirm = ModernDialog.ShowMessage(message, "Warning!", MessageBoxButton.YesNo);
                if (confirm == MessageBoxResult.No)
                    e.Cancel = true;
            }
        }

        #endregion

       
    }
}
