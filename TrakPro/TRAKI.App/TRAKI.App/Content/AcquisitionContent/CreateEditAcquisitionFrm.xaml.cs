﻿using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TRAKI.App.Common;
using TRAKI.App.Interfaces;
using TRAKI.App.Model;
using TRAKI.App.ViewModel;

namespace TRAKI.App.Content.AcquisitionContent
{
    /// <summary>
    /// Interaction logic for CreateEditAcquisitionFrm.xaml
    /// </summary>
    public partial class CreateEditAcquisitionFrm : UserControl, IContent
    {

        private AcquisitionViewModel acquisitionViewModel;
        private int AcquisitionId = -1;
        private bool _showConfirmationMsg = true;

        public CreateEditAcquisitionFrm()
        {
            InitializeComponent();
        }

        private void loadAcquisitionInfo()
        {
            acquisitionViewModel = new AcquisitionViewModel();
            ObservableCollection<Acquisition> acquisition = acquisitionViewModel.GetAcquisition(this.AcquisitionId, null);
            this.DataContext = acquisition;
        }

        private void Submit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Acquisition acquisition = ((ObservableCollection<Acquisition>)(sender as Button).DataContext)[0];
                string message = string.Empty;
                var result = acquisitionViewModel.InsertUpdateAcquisition(acquisition); 
                if (result == 1)
                {
                    message = "Acquisition Successfully Created";
                }
                if (result == 2)
                {
                    message = "Acquisition Successfully updated";
                }
                ModernDialog.ShowMessage(message, "Success", MessageBoxButton.OK);
                this._showConfirmationMsg = false;
                NavigationCommands.GoToPage.Execute("/Content/AcquisitionContent/AcquisitionDetailFrm.xaml#" + null, this);
            }
            catch (Exception ex)
            {
                
                if (ex.HResult == -2147467259)
                {
                    this.TextAcquisitionCode.Focus();
                    ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                    ModernDialog.ShowMessage("Acquisition Code already exists, Please try another Code!", "Error!", MessageBoxButton.OK);
                }
                else
                {
                   ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                    ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
                }
            }
        }

        private void TextAcquisitionCode_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = IsMatching.Check(e.Text);

        }

        private void TextAcquisitionDescription_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = IsMatching.Check(e.Text); 
        }

        #region IContent

        public void OnFragmentNavigation(FirstFloor.ModernUI.Windows.Navigation.FragmentNavigationEventArgs e)
        {
            this.AcquisitionId = -1;
            if (!string.IsNullOrEmpty(e.Fragment))
            {
                this.AcquisitionId = Convert.ToInt32(e.Fragment);
            }
            this.loadAcquisitionInfo();
        }

        public void OnNavigatedFrom(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
        }

        public void OnNavigatedTo(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
            this._showConfirmationMsg = true;
        }

        public void OnNavigatingFrom(FirstFloor.ModernUI.Windows.Navigation.NavigatingCancelEventArgs e)
        {
            if (this._showConfirmationMsg)
            {
                string message = "Are you sure you want to leave?";
                if (e.NavigationType == FirstFloor.ModernUI.Windows.Navigation.NavigationType.Refresh)
                {
                    message = "Are you sure you want to refresh the page?";
                }
                var confirm = ModernDialog.ShowMessage(message, "Warning!", MessageBoxButton.YesNo);
                if (confirm == MessageBoxResult.No)
                    e.Cancel = true;
            }
        }

        #endregion

     
    }
}
