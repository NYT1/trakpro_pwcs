﻿using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TRAKI.App.Common;
using TRAKI.App.Content.CommonContent;
using TRAKI.App.Model;
using TRAKI.App.ViewModel;
using TRAKI.App.ViewModel.Common;

namespace TRAKI.App.Content.ComputerCatalogContent
{
    /// <summary>
    /// Interaction logic for ComputerCatalogList.xaml
    /// </summary>
    public partial class ComputerCatalogList : UserControl  
    {

        ArrayList ComputerCatalogId = new ArrayList();

        private IEnumerable<string> selectedCatalogNumbers;
        List<ComputerCatalog> ShowCompCatalogList;

        public ComputerCatalogList()
        {
            InitializeComponent();

            //changeToStateA();
            this.Loaded += OnLoaded;
            //LoadComputerList();
        }
        void OnLoaded(object sender, RoutedEventArgs e)
        {
            this.LoadComputerList();
            RadioNumber.IsChecked = true;
        }
       
       

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //Steve
                Constants.needCatalogSelect = true;
                string message = string.Empty;
                CompCatalogDialog compcatalogDialog = new CompCatalogDialog();
                compcatalogDialog.Title = "Select Catalog";
                compcatalogDialog.CloseButton.Content = "Close";
                compcatalogDialog.OkButton.Content = "Select";
                compcatalogDialog.Buttons = new Button[] { compcatalogDialog.CloseButton, compcatalogDialog.OkButton };
                compcatalogDialog.ShowDialog();

                if (compcatalogDialog.DialogResult.Value)
                {
                    this.selectedCatalogNumbers = compcatalogDialog.SelectedCatalogNumbers;
                    ComputerCatalogViewModel computercatalogViewModel = new ComputerCatalogViewModel();
                    var resultQ = computercatalogViewModel.SaveCompCatalog(this.selectedCatalogNumbers);
                    if (resultQ == -1)
                    {
                        ModernDialog.ShowMessage("Computer Catalog Number already exists, Please try another Catalog Number!", "Error!", MessageBoxButton.OK);
                        return;
                    }
                    if (resultQ == 1)
                    {

                        message = "Computer Catalog Successfully Created";
                        ModernDialog.ShowMessage(message, "Success", MessageBoxButton.OK);
                        LoadComputerList();
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                ModernDialog.ShowMessage("Some error occured, please try again!", "Error!", MessageBoxButton.OK);
            }
        }
        //private void btnAdd_Click(object sender, RoutedEventArgs e)
        //{        

        //        try
        //        {
        //            ComputerCatalogDialog computercatalogDialog = new ComputerCatalogDialog();

        //            Constants.isComp = 0;
        //            computercatalogDialog.Buttons = new Button[] { computercatalogDialog.OkButton, computercatalogDialog.CancelButton };
        //            computercatalogDialog.ShowDialog();
        //            if (computercatalogDialog.DialogResult.Value)
        //            {
        //                if (!string.IsNullOrEmpty(computercatalogDialog.CatalogDialogResult))
        //                {
        //                    string[] result = computercatalogDialog.CatalogDialogResult.Split('•');
        //                    ComputerCatalog computercatalog = new ComputerCatalog();
        //                    string message = string.Empty;

        //                    computercatalog.CatalogNumber = result[0];
        //                    computercatalog.CatalogDescription = result[1];

        //                    if (result[0] == "")
        //                    {
        //                        ModernDialog.ShowMessage("Please Select Computer Catalog Number!", "Warning!", MessageBoxButton.OK);
        //                        return;
        //                    }

        //                    ComputerCatalogViewModel computercatalogViewModel = new ComputerCatalogViewModel();
        //                    var resultQ = computercatalogViewModel.SaveCompCatalog(computercatalog);
        //                    if (resultQ == -1)
        //                    {
        //                        ModernDialog.ShowMessage("Computer Catalog Number already exists, Please try another Catalog Number!", "Error!", MessageBoxButton.OK);
        //                        return;
        //                    }
        //                    if (resultQ == 1)
        //                    {
                                
        //                        message = "Computer Catalog Successfully Created";
        //                        ModernDialog.ShowMessage(message, "Success", MessageBoxButton.OK);
        //                        LoadComputerList();
        //                    }
        //                }
        //            }                   

        //        }
        //        catch (Exception ex)
        //        {
        //            if (ex.HResult == -2147467259)
        //            {
        //                //this.TextCatalogNumber.Focus();
        //                ModernDialog.ShowMessage("Catalog Number already exists, Please try another number!", "Error!", MessageBoxButton.OK);
        //            }
        //            else
        //            {
        //                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);

        //            }
        //        }

        //    }
            
        private void LoadComputerList()
        {
            
            ComputerCatalogViewModel CompCatalogViewModel = new ComputerCatalogViewModel();
            this.ShowCompCatalogList = CompCatalogViewModel.GetComputerCatalog(0);

            this.CatalogList.ItemsSource = null;
            this.CatalogList.ItemsSource = ShowCompCatalogList;
            RadioNumber.IsChecked = true;
        }

        private void BtnRemove_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (CatalogList.SelectedIndex == -1)
                {
                    ModernDialog.ShowMessage("Please Select Computer Catalog to Delete...", "Information", MessageBoxButton.OK);
                }
                else
                {
                    foreach (ComputerCatalog item in CatalogList.SelectedItems)
                    {
                       ComputerCatalogId.Add(item.CatalogNumber.ToString());
                    }
                    var confirm = ModernDialog.ShowMessage("Are you sure you want to delete the selected Computer Catalog?", "Delete", MessageBoxButton.YesNo);
                    if (confirm == MessageBoxResult.Yes)
                    {

                        ComputerCatalogViewModel computercatalogViewModel = new ComputerCatalogViewModel();
                        if (computercatalogViewModel.DeleteCompCatalogList(ComputerCatalogId) > 0)
                        {
                            LoadComputerList();
                            ModernDialog.ShowMessage("Successfully Deleted the selected Computer Catalog", "Success", MessageBoxButton.OK);// success update operation
                            
                        }
                    }
                
                }
            }
                    catch (Exception ex)
            {
                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                throw ex;
            }

        }

      

        private void RadioNumber_Checked(object sender, RoutedEventArgs e)
        {

            CatalogList.Items.SortDescriptions.Clear();
            CatalogList.Items.SortDescriptions.Remove(new System.ComponentModel.SortDescription("CatalogDescription", System.ComponentModel.ListSortDirection.Ascending));
            CatalogList.Items.SortDescriptions.Add(new System.ComponentModel.SortDescription("CatalogNumber", System.ComponentModel.ListSortDirection.Ascending));
            CatalogList.Items.Refresh();
        }

        
        private void RadioDescription_Checked(object sender, RoutedEventArgs e)
        {
            CatalogList.Items.SortDescriptions.Clear();
            CatalogList.Items.SortDescriptions.Remove(new System.ComponentModel.SortDescription("CatalogNumber", System.ComponentModel.ListSortDirection.Ascending));
            CatalogList.Items.SortDescriptions.Add(new System.ComponentModel.SortDescription("CatalogDescription", System.ComponentModel.ListSortDirection.Ascending));
            CatalogList.Items.Refresh();
        }

        private void btnAll_Click(object sender, RoutedEventArgs e)
        {
            CatalogList.SelectAll();           
         }

        private void btnDAll_Click(object sender, RoutedEventArgs e)
        {
             CatalogList.SelectedItem = null;
        }

        
      
    }
}