﻿using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Controls;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Document;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using TRAKI.App.Common;
using TRAKI.App.Content.CommonContent;
using TRAKI.App.Model;
using TRAKI.App.ViewModel;
using TRAKI.App.ViewModel.Common;

namespace TRAKI.App.Reports
{
    /// <summary>
    /// Interaction logic for AssetValueSummaryReport.xaml
    /// </summary>
    public partial class DispositionReport : UserControl, IContent
    {
        FileInfo rptPath;
        FileInfo rptPath2;
        //PageReport pageReport;
        // GrapeCity.ActiveReports.Document.PageDocument pageDocument;
        private IEnumerable<string> selectedBuildingNumbers;
        private string dispositionCode = string.Empty;
        private string dispositionDesc = string.Empty;
        private bool _showConfirmationMsg = false;


        public DispositionReport()
        {
            InitializeComponent();
            Constants.isforDispcode = "DispReport";
            this.GetDispolistionList();
            Unloaded += DispositionReport_Unloaded;
        }

        void DispositionReport_Unloaded(object sender, RoutedEventArgs e)
        {
            try
            {
                this.clearResources();
            }
            catch (Exception ex) { }
        }

        private void GetDispolistionList()
        {
            try
            {
                DispositionViewModel dispositionViewModel = new DispositionViewModel();
                this.ComboDispositionList.ItemsSource = null;
                Disposition newDispositon = new Disposition();
                newDispositon.DispositionDescription = "Select Disposition";
                newDispositon.DispositionCode = null;
                List<Disposition> dispositionList = dispositionViewModel.GetDisposition(null, null).ToList();
                dispositionList.Insert(0, newDispositon);
                this.ComboDispositionList.ItemsSource = dispositionList;
            }
            catch (Exception ex)
            {
                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                ModernDialog.ShowMessage("Some error occured, please try again!", "Error!", MessageBoxButton.OK);
            }
        }



        /// <summary>
        /// Loads and shows the report.
        /// </summary>
        private void LoadReport()
        {
            try
            {
                if (Constants.reportViewer != null)
                {
                    this.reportPanel.Children.Remove(Constants.reportViewer);
                    Constants.reportViewer.Dispose();
                }

                //rptPath = new FileInfo(System.AppDomain.CurrentDomain.BaseDirectory + @"Reports/DispositionReport.rdlx");
                //pageReport = new PageReport(rptPath);
                //pageDocument = new PageDocument(pageReport);
                //pageDocument.LocateDataSource += new LocateDataSourceEventHandler(OnLocateDataSource);
                //this.AssetReportViewer.LoadDocument(pageDocument);

                Constants.reportViewer = new GrapeCity.ActiveReports.Viewer.Wpf.Viewer();
                
                rptPath = new FileInfo(System.AppDomain.CurrentDomain.BaseDirectory + @"Reports\DispositionReport.rdlx");
                string newRptName = this.dispositionDesc.Substring(0,1) + this.dispositionDesc.Substring(1, this.dispositionDesc.Length - 1).ToLower();

                // Steve
                // Take advanatge of Globals report name which can be changed.
                // Create a temp report rdlx file with customized name and load
                //
                // Start here
                rptPath2 = new FileInfo(System.AppDomain.CurrentDomain.BaseDirectory + @"Reports\Disposition Report (" + @newRptName +").rdlx");
                if (File.Exists(rptPath2.ToString()))
                {
                    File.Delete(rptPath2.ToString());
                }
                else
                {
                    File.Copy(rptPath.ToString(), rptPath2.ToString());
                }

                // end here

                Constants.pageReport = new PageReport(rptPath2);


                Constants.pageDocument = new PageDocument(Constants.pageReport);
                Constants.pageDocument.Printer.PrinterName = "";
                
                Constants.pageDocument.LocateDataSource += new LocateDataSourceEventHandler(OnLocateDataSource);
              
                //this.AssetReportViewer.LoadDocument(Constants.pageDocument1);
                Constants.reportViewer.LoadDocument(Constants.pageDocument);
               

                this.reportPanel.Children.Add(Constants.reportViewer);
                

            }
            catch (Exception ex)
            {
                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                ModernDialog.ShowMessage("Some error occured, please try again!", "Error!", MessageBoxButton.OK);
            }
        }
        private void OnLocateDataSource(object sender, LocateDataSourceEventArgs LocateDataSourceArgs)
        {
            try
            {
                ReportViewModel model = new ReportViewModel();
                LocateDataSourceArgs.Data = model.GetDispositionReport(this.selectedBuildingNumbers,dispositionCode);
                
            }
            catch (Exception ex)
            {
                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                ModernDialog.ShowMessage("Some error occured, please try again! ", "Error!", MessageBoxButton.OK);
            }
        }

        //private void btnGenerateReport_Click(object sender, RoutedEventArgs e)
        //{
        //    try
        //    {
        //        if (this.ComboDispositionList.SelectedValue != null)
        //        {
        //            this.dispositionCode = this.ComboDispositionList.SelectedValue.ToString();
        //            this.dispositionDesc = this.ComboDispositionList.Text.Trim();
        //            this._showConfirmationMsg = true;
        //            this.LoadReport();

        //            // Steve
        //            // Remove the temp report rdlx file 
        //            if (File.Exists(rptPath2.ToString()))
        //            {
        //                File.Delete(rptPath2.ToString());
        //            }
        //        }
        //        else
        //        {
        //            ModernDialog.ShowMessage("Please Select Disposition Type! ", "Warning!", MessageBoxButton.OK);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorLogging.LogException("Error: " + ex.Message + " {0}");
        //        ModernDialog.ShowMessage("Some error occured, please try again!", "Error!", MessageBoxButton.OK);
        //    }
        //}
        #region Export Section

        private void btnExportReport_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ReportViewModel reportViewModel = new ReportViewModel();
                var contextMenu = reportViewModel.PrepareContextMenu(Constants.pageDocument);
                foreach (var item in contextMenu.Items)
                {
                    if (!item.GetType().Name.Equals("Separator"))
                    {
                        MenuItem menuItem = item as MenuItem;
                        if (menuItem.IsEnabled)
                        {
                            if (menuItem.Name.Equals(ExportMenuItemName.ExportToPDF.ToString()))
                                menuItem.Click += ExportToPDFItem_Click;
                            if (menuItem.Name.Equals(ExportMenuItemName.ExportToExcel.ToString()))
                                menuItem.Click += ExportToExcelItem_Click;
                            if (menuItem.Name.Equals(ExportMenuItemName.ExportToFile.ToString()))
                                menuItem.Click += ExportToTextFileItem_Click;
                        }
                    }
                }
                contextMenu.IsOpen = true;
            }
            catch (Exception ex)
            {
                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        private void ExportToTextFileItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ThreadStart work = () =>
                {
                    try
                    {
                        ReportViewModel reportViewModel = new ReportViewModel();
                        GrapeCity.ActiveReports.Export.Xml.Section.TextExport textExport = new GrapeCity.ActiveReports.Export.Xml.Section.TextExport();

                        System.Windows.Forms.SaveFileDialog saveFileDialog = new System.Windows.Forms.SaveFileDialog();
                        saveFileDialog.Filter = "TXT file (*.txt)|*.txt";
                        saveFileDialog.FilterIndex = 2;
                        saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                        saveFileDialog.RestoreDirectory = true;
                        saveFileDialog.Title = "Save Asset Value Summary Report";
                        saveFileDialog.FileName = reportViewModel.GetReportName("txt", rptPath.FullName);
                        if (saveFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                        {

                            textExport.Export(Constants.pageDocument, saveFileDialog.FileName);
                            Application.Current.Dispatcher.BeginInvoke(new Action(() => CommandManager.InvalidateRequerySuggested()));
                            Application.Current.Dispatcher.Invoke(DispatcherPriority.ContextIdle, new Action(() =>
                            {
                                ModernDialog.ShowMessage("Report Successfully Exported ", "Success!", MessageBoxButton.OK);
                                textExport.Dispose();

                            }));
                        }
                    }
                    catch (Exception ex)
                    {
                        if (ex.HResult == -2147024893)
                            ModernDialog.ShowMessage("Export Directory path not found, Please contact your System Administrator!", "Error!", MessageBoxButton.OK);
                        else
                            ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
                        ErrorLogging.LogException("Error: " + ex.Message + " {0}");

                    }
                };
                new Thread(work).SetApartmentState(ApartmentState.STA);
                work.Invoke();
            }
            catch (Exception ex)
            {
                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }

        }

        private void ExportToPDFItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ThreadStart work = () =>
                {
                    try
                    {
                        ReportViewModel reportViewModel = new ReportViewModel();
                        GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport PDFExport = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                        System.Windows.Forms.SaveFileDialog saveFileDialog = new System.Windows.Forms.SaveFileDialog();
                        saveFileDialog.Filter = "PDF file (*.pdf)|*.pdf";
                        saveFileDialog.FilterIndex = 2;
                        saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                        saveFileDialog.RestoreDirectory = true;
                        saveFileDialog.Title = "Save Asset Value Summary Report";
                        saveFileDialog.FileName = reportViewModel.GetReportName("pdf", rptPath.FullName);
                        if (saveFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                        {
                            PDFExport.Export(Constants.pageDocument, saveFileDialog.FileName);
                            Application.Current.Dispatcher.BeginInvoke(new Action(() => CommandManager.InvalidateRequerySuggested()));
                            Application.Current.Dispatcher.Invoke(DispatcherPriority.ContextIdle, new Action(() =>
                            {
                                ModernDialog.ShowMessage("Report Successfully Exported ", "Success!", MessageBoxButton.OK);
                                PDFExport.Dispose();
                            }));
                        }
                    }
                    catch (Exception ex)
                    {
                        if (ex.HResult == -2147024893)
                            ModernDialog.ShowMessage("Export Directory path not found, Please contact your System Administrator!", "Error!", MessageBoxButton.OK);
                        else
                            ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
                        ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                    }

                };
                new Thread(work).SetApartmentState(ApartmentState.STA);
                work.Invoke();
            }
            catch (Exception ex)
            {
                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        void ExportToExcelItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ThreadStart work = () =>
                {

                    try
                    {
                        ReportViewModel reportViewModel = new ReportViewModel();
                        // Export the report in XLSX format.
                        GrapeCity.ActiveReports.Export.Excel.Section.XlsExport XLSExport = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();
                        // Set a file format of the exported excel file to Xlsx to support Microsoft Excel 2007 and newer versions.
                        XLSExport.FileFormat = GrapeCity.ActiveReports.Export.Excel.Section.FileFormat.Xlsx;
                        System.Windows.Forms.SaveFileDialog saveFileDialog = new System.Windows.Forms.SaveFileDialog();
                        saveFileDialog.Filter = "Excel file (*.xlsx)|*.xlsx";
                        saveFileDialog.FilterIndex = 2;
                        saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                        saveFileDialog.RestoreDirectory = true;
                        saveFileDialog.Title = "Save Asset Value Summary Report";
                        saveFileDialog.FileName = reportViewModel.GetReportName("xlsx", rptPath.FullName);
                        if (saveFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                        {
                            XLSExport.Export(Constants.pageDocument, saveFileDialog.FileName);
                            // XLSExport.Export(pageDocument, reportViewModel.GetReportName("xlsx", rptPath.FullName));
                            Application.Current.Dispatcher.BeginInvoke(new Action(() => CommandManager.InvalidateRequerySuggested()));
                            Application.Current.Dispatcher.Invoke(DispatcherPriority.ContextIdle, new Action(() =>
                            {
                                ModernDialog.ShowMessage("Report Successfully Exported ", "Success!", MessageBoxButton.OK);
                                XLSExport.Dispose();
                            }));
                        }
                    }
                    catch (Exception ex)
                    {
                        if (ex.HResult == -2147024893)
                            ModernDialog.ShowMessage("Export Directory path not found, Please contact your System Administrator!", "Error!", MessageBoxButton.OK);
                        else
                            ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
                        ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                    }
                };
                new Thread(work).SetApartmentState(ApartmentState.STA);
                work.Invoke();
            }
            catch (Exception ex)
            {
                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }

        }

        #endregion

        private void clearResources()
        {
            this.rptPath = null;
            if (Constants.pageDocument != null)
                Constants.pageDocument.Dispose();//= null;
            if (Constants.pageReport != null)
                Constants.pageReport.Dispose();// = null;
            Constants.pageReport = null;
            if (Constants.reportViewer != null)
            {
                this.reportPanel.Children.Remove(Constants.reportViewer);
                Constants.reportViewer.Dispose();
            }
            GC.Collect();
        }


        #region Navigation

        public void OnFragmentNavigation(FirstFloor.ModernUI.Windows.Navigation.FragmentNavigationEventArgs e)
        {

        }

        public void OnNavigatedFrom(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {

        }

        public void OnNavigatedTo(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
            this._showConfirmationMsg=false;
        }

        public void OnNavigatingFrom(FirstFloor.ModernUI.Windows.Navigation.NavigatingCancelEventArgs e)
        {
            if (this._showConfirmationMsg)
            {
                string message = "Are you sure you want to leave?";
                if (e.NavigationType == FirstFloor.ModernUI.Windows.Navigation.NavigationType.Refresh)
                {
                    message = "Are you sure you want to refresh the page?";
                }
                var confirm = ModernDialog.ShowMessage(message, "Warning!", MessageBoxButton.YesNo);
                if (confirm == MessageBoxResult.No)
                    e.Cancel = true;
            }
        }

        #endregion

        private void btnSelectBuilding_Click(object sender, RoutedEventArgs e)
        {
            try
            {                        
          
               if (this.ComboDispositionList.SelectedValue != null)
                    {
                        this.dispositionCode = this.ComboDispositionList.SelectedValue.ToString();
                        this.dispositionDesc = this.ComboDispositionList.Text.Trim();
                        this._showConfirmationMsg = true;                         
                    }
                    else
                    {
                        ModernDialog.ShowMessage("Please Select Disposition Type! ", "Warning!", MessageBoxButton.OK);
                        return;
                    }

               //Steve
               Constants.needBldgSelect = true;
               Constants.isFromMissingReport = true;
               Constants.isForPWCS = false;
               MissingBuildingDialog missingBuildingDialog = new MissingBuildingDialog();
               missingBuildingDialog.Title = "Select Building";
               missingBuildingDialog.CloseButton.Content = "Close";
               missingBuildingDialog.OkButton.Content = "Select";
               missingBuildingDialog.Buttons = new Button[] { missingBuildingDialog.CloseButton, missingBuildingDialog.OkButton };
               missingBuildingDialog.ShowDialog();

               if (missingBuildingDialog.DialogResult.Value)
               {
                   this.selectedBuildingNumbers = missingBuildingDialog.SelectedBuildingNumbers;
                   this._showConfirmationMsg = true;
                   //this.LoadReportList();// 
                   this.LoadReport();

                   //Steve
                   //Remove the temp report rdlx file 
                   if (File.Exists(rptPath2.ToString()))
                   {
                       File.Delete(rptPath2.ToString());
                   }

               }
              
                }
            
            catch (Exception ex)
            {
                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                ModernDialog.ShowMessage("Some error occured, please try again!", "Error!", MessageBoxButton.OK);
            }
        }

      

        
        
    }
}

