﻿using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Controls;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Document;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using TRAKI.App.Common;
using TRAKI.App.Content.CommonContent;
using TRAKI.App.Model;
using TRAKI.App.ViewModel;
using TRAKI.App.ViewModel.Common;

namespace TRAKI.App.Reports
{
    /// <summary>
    /// Interaction logic for ComputerCatalogReport.xaml
    /// </summary>
    public partial class ComputerCatalogReport : UserControl, IContent
    {

        FileInfo rptPath;
        private List<Report> reportDataList;
        private List<Report> _pagerReportList;
        private bool _showConfirmationMsg = false;
        private string _catalognumber;
        private string _catalogDesc;

        int pageIndex = 1;
        
        public ComputerCatalogReport()
        {
            InitializeComponent();
            Unloaded += ComputerCatalogReport_Unloaded;
            Loaded += ComputerCatalogReport_Loaded;
        }

        void ComputerCatalogReport_Loaded(object sender, RoutedEventArgs e)
        {
            this.pagerPanel.Visibility = Visibility.Collapsed;
        }

        void ComputerCatalogReport_Unloaded(object sender, RoutedEventArgs e)
        {
            try
            {
                this.clearResources();
            }
            catch (Exception ex) { }
        }

        private async void LoadReportList()
        {
            try
            {
                ReportViewModel reportViewModel = new ReportViewModel();

                Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                {
                    this.BusyBar.IsBusy = true;
                }));


                this.reportDataList = await Task.Run(() => reportViewModel.GetComputerCatalogReport(this._catalognumber));
                if (this.reportDataList.Count > Constants.NumberOfRecordsPerReport)
                    this.pagerPanel.Visibility = Visibility.Visible;
                else
                    this.pagerPanel.Visibility = Visibility.Collapsed;

                this.BusyBar.IsBusy = false;
                if (reportViewModel.RecordCount == 0)
                    this.TextBlockPages.Text = "0";
                else
                    this.TextBlockPages.Text = "1";
                
                LoadReport();
                this.DataContext = reportViewModel;
            }
            catch (Exception ex)
            {
                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                ModernDialog.ShowMessage("Some error occured, please try again! " + ex.Message, "Error!", MessageBoxButton.OK);
            }
        }
        private void LoadReport()
        {
            try
            {

                if (Constants.reportViewer != null)
                {
                    this.reportPanel.Children.Remove(Constants.reportViewer);
                    Constants.reportViewer.Dispose();
                }

                
                Constants.reportViewer = new GrapeCity.ActiveReports.Viewer.Wpf.Viewer();

                rptPath = new FileInfo(System.AppDomain.CurrentDomain.BaseDirectory + @"Reports\ComputerCatalogReport.rdlx");
                //string newRptName = this._catalogDesc.Substring(0, 1) + this._catalogDesc.Substring(1, this._catalogDesc.Length - 1).ToLower();

                //string newRptName = this._catalogDesc.ToString();


                // Steve
                // Take advanatge of Globals report name which can be changed.
                // Create a temp report rdlx file with customized name and load
                //
                // Start here
                //rptPath2 = new FileInfo(System.AppDomain.CurrentDomain.BaseDirectory + @"Reports\Computer Catalog Report (" + @newRptName + ").rdlx");
                //if (File.Exists(rptPath2.ToString()))
                //{
                //    File.Delete(rptPath2.ToString());
                //}
                //else
                //{
                //    File.Copy(rptPath.ToString(), rptPath2.ToString());
                //}


                Constants.pageReport = new PageReport(rptPath);
                Constants.pageDocument = new PageDocument(Constants.pageReport);
                Constants.pageDocument.Printer.PrinterName = "";
                Constants.pageDocument.LocateDataSource += new LocateDataSourceEventHandler(OnLocateDataSource);

                //this.AssetReportViewer.LoadDocument(Constants.pageDocument1);
                Constants.reportViewer.LoadDocument(Constants.pageDocument);

                this.reportPanel.Children.Add(Constants.reportViewer);

            }
            catch (Exception ex)
            {
                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                ModernDialog.ShowMessage("Some error occured, please try again!", "Error!", MessageBoxButton.OK);
            }
        }
        private void OnLocateDataSource(object sender, LocateDataSourceEventArgs LocateDataSourceArgs)
        {
            try
            {
                LocateDataSourceArgs.Data = this.reportDataList;
            }
            catch (Exception ex)
            {
                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                ModernDialog.ShowMessage("Some error occured, please try again!", "Error!", MessageBoxButton.OK);
            }
        }


        private void btnRunAllReport_Click(object sender, RoutedEventArgs e)
        {

            
            string message = string.Empty;
            this._catalognumber = null;
            this.LoadReportList();
            this._showConfirmationMsg = true;
        }

        private void btnGenerateReport_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ComputerCatalogDialog computercatalogDialog = new ComputerCatalogDialog();
                Constants.isComp = -1;
                computercatalogDialog.Buttons = new Button[] { computercatalogDialog.OkButton, computercatalogDialog.CancelButton };
                computercatalogDialog.ShowDialog();
                if (computercatalogDialog.DialogResult.Value)
                {
                    if (!string.IsNullOrEmpty(computercatalogDialog.CatalogDialogResult))
                    {
                        string[] result = computercatalogDialog.CatalogDialogResult.Split('•');
                        ComputerCatalog computercatalog = new ComputerCatalog();
                        string message = string.Empty;

                        computercatalog.CatalogNumber = result[0];
                        computercatalog.CatalogDescription = result[1];

                        if (computercatalogDialog.DialogResult.Value)
                        {

                            this._catalognumber = computercatalog.CatalogNumber;
                            this._catalogDesc = computercatalog.CatalogDescription;
                            this.LoadReportList();
                            this._showConfirmationMsg = true;

                            
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                ModernDialog.ShowMessage("Some error occured, please try again!", "Error!", MessageBoxButton.OK);
            }

                            }

        #region Pagination

        #region Export Section

        private void btnExportReport_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ReportViewModel reportViewModel = new ReportViewModel();
                var contextMenu = reportViewModel.PrepareContextMenu(Constants.pageDocument);
                foreach (var item in contextMenu.Items)
                {
                    if (!item.GetType().Name.Equals("Separator"))
                    {
                        MenuItem menuItem = item as MenuItem;
                        if (menuItem.IsEnabled)
                        {
                            if (menuItem.Name.Equals(ExportMenuItemName.ExportToPDF.ToString()))
                                menuItem.Click += ExportToPDFItem_Click;
                            if (menuItem.Name.Equals(ExportMenuItemName.ExportToExcel.ToString()))
                                menuItem.Click += ExportToExcelItem_Click;
                            if (menuItem.Name.Equals(ExportMenuItemName.ExportToFile.ToString()))
                                menuItem.Click += ExportToTextFileItem_Click;
                        }
                    }
                }
                contextMenu.IsOpen = true;
            }
            catch (Exception ex)
            {
                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        private void ExportToTextFileItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ThreadStart work = () =>
                {
                    try
                    {
                        ReportViewModel reportViewModel = new ReportViewModel();
                        GrapeCity.ActiveReports.Export.Xml.Section.TextExport textExport = new GrapeCity.ActiveReports.Export.Xml.Section.TextExport();

                        System.Windows.Forms.SaveFileDialog saveFileDialog = new System.Windows.Forms.SaveFileDialog();
                        saveFileDialog.Filter = "TXT file (*.txt)|*.txt";
                        saveFileDialog.FilterIndex = 2;
                        saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                        saveFileDialog.RestoreDirectory = true;
                        saveFileDialog.Title = "Save Computer Catalog Summary Report";
                        saveFileDialog.FileName = reportViewModel.GetReportName("txt", rptPath.FullName);
                        if (saveFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                        {

                            textExport.Export(Constants.pageDocument, saveFileDialog.FileName);
                            Application.Current.Dispatcher.BeginInvoke(new Action(() => CommandManager.InvalidateRequerySuggested()));
                            Application.Current.Dispatcher.Invoke(DispatcherPriority.ContextIdle, new Action(() =>
                            {
                                ModernDialog.ShowMessage("Report Successfully Exported ", "Success!", MessageBoxButton.OK);
                                textExport.Dispose();
                            }));
                        }
                    }
                    catch (Exception ex)
                    {
                        if (ex.HResult == -2147024893)
                            ModernDialog.ShowMessage("Export Directory path not found, Please contact your System Administrator!", "Error!", MessageBoxButton.OK);
                        else
                            ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
                        ErrorLogging.LogException("Error: " + ex.Message + " {0}");

                    }
                };
                new Thread(work).SetApartmentState(ApartmentState.STA);
                work.Invoke();
            }
            catch (Exception ex)
            {
                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }

        }

        private void ExportToPDFItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ThreadStart work = () =>
                {
                    try
                    {

                        ReportViewModel reportViewModel = new ReportViewModel();
                        GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport PDFExport = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                        System.Windows.Forms.SaveFileDialog saveFileDialog = new System.Windows.Forms.SaveFileDialog();
                        saveFileDialog.Filter = "PDF file (*.pdf)|*.pdf";
                        saveFileDialog.FilterIndex = 2;
                        saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                        saveFileDialog.RestoreDirectory = true;
                        saveFileDialog.Title = "Save Computer Catalog Summary Report";
                        saveFileDialog.FileName = reportViewModel.GetReportName("pdf", rptPath.FullName);

                        if (saveFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                        {
                            PDFExport.Export(Constants.pageDocument, saveFileDialog.FileName);
                            Application.Current.Dispatcher.BeginInvoke(new Action(() => CommandManager.InvalidateRequerySuggested()));
                            Application.Current.Dispatcher.Invoke(DispatcherPriority.ContextIdle, new Action(() =>
                            {
                                ModernDialog.ShowMessage("Report Successfully Exported ", "Success!", MessageBoxButton.OK);
                                PDFExport.Dispose();
                            }));
                        }
                    }
                    catch (Exception ex)
                    {
                        if (ex.HResult == -2147024893)
                            ModernDialog.ShowMessage("Export Directory path not found, Please contact your System Administrator!", "Error!", MessageBoxButton.OK);
                        else
                            ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
                        ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                    }

                };
                new Thread(work).SetApartmentState(ApartmentState.STA);
                work.Invoke();
            }
            catch (Exception ex)
            {
                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }
        }

        void ExportToExcelItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ThreadStart work = () =>
                {

                    try
                    {
                        ReportViewModel reportViewModel = new ReportViewModel();
                        // Export the report in XLSX format.
                        GrapeCity.ActiveReports.Export.Excel.Section.XlsExport XLSExport = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();
                        // Set a file format of the exported excel file to Xlsx to support Microsoft Excel 2007 and newer versions.
                        XLSExport.FileFormat = GrapeCity.ActiveReports.Export.Excel.Section.FileFormat.Xlsx;
                        System.Windows.Forms.SaveFileDialog saveFileDialog = new System.Windows.Forms.SaveFileDialog();
                        saveFileDialog.Filter = "Excel file (*.xlsx)|*.xlsx";
                        saveFileDialog.FilterIndex = 2;
                        saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                        saveFileDialog.RestoreDirectory = true;
                        saveFileDialog.Title = "Save Computer Catalog Summary Report";
                        saveFileDialog.FileName = reportViewModel.GetReportName("xlsx", rptPath.FullName);
                        if (saveFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                        {
                            XLSExport.Export(Constants.pageDocument, saveFileDialog.FileName);
                            Application.Current.Dispatcher.BeginInvoke(new Action(() => CommandManager.InvalidateRequerySuggested()));
                            Application.Current.Dispatcher.Invoke(DispatcherPriority.ContextIdle, new Action(() =>
                            {
                                ModernDialog.ShowMessage("Report Successfully Exported ", "Success!", MessageBoxButton.OK);
                                XLSExport.Dispose();

                            }));
                        }
                    }
                    catch (Exception ex)
                    {
                        if (ex.HResult == -2147024893)
                            ModernDialog.ShowMessage("Export Directory path not found, Please contact your System Administrator!", "Error!", MessageBoxButton.OK);
                        else
                            ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
                        ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                    }
                };
                new Thread(work).SetApartmentState(ApartmentState.STA);
                work.Invoke();
            }
            catch (Exception ex)
            {
                ErrorLogging.LogException("Error: " + ex.Message + " {0}");
                ModernDialog.ShowMessage(ex.Message, "Error!", MessageBoxButton.OK);
            }

        }

        #endregion
        private void clearResources()
        {
            this.rptPath = null;
            if (Constants.pageDocument != null)
                Constants.pageDocument.Dispose();//= null;
            if (Constants.pageReport != null)
                Constants.pageReport.Dispose();// = null;
            Constants.pageReport = null;
            if (Constants.reportViewer != null)
            {
                this.reportPanel.Children.Remove(Constants.reportViewer);
                Constants.reportViewer.Dispose();
            }

            // this.AssetReportViewer.LoadDocument(new GrapeCity.ActiveReports.Document.SectionDocument());
            //this.StartingBuildingNumber = string.Empty;
            //this.EndingBuildingNumber = string.Empty;
            GC.Collect();
        }

        private void btnFirst_Click(object sender, System.EventArgs e)
        {

            Navigate((int)PagingMode.First);
            this.TextBlockPages.Text = "1";
            this.DisplayRecordCount.Text = "of " + this._pagerReportList.Count().ToString() + " out of ";
            //LoadReport();

        }

        private void btnNext_Click(object sender, System.EventArgs e)
        {
            Navigate((int)PagingMode.Next);
            this.TextBlockPages.Text = this.pageIndex.ToString();
            this.DisplayRecordCount.Text = "of " + this._pagerReportList.Count().ToString() + " out of ";

            //LoadReport();

        }

        private void btnPrev_Click(object sender, System.EventArgs e)
        {
            Navigate((int)PagingMode.Previous);
            this.TextBlockPages.Text = this.pageIndex.ToString();
            this.DisplayRecordCount.Text = "of " + this._pagerReportList.Count().ToString() + " out of ";

            //LoadReport();

        }

        private void btnLast_Click(object sender, System.EventArgs e)
        {
            Navigate((int)PagingMode.Last);
            this.TextBlockPages.Text = this.pageIndex.ToString();
            this.DisplayRecordCount.Text = "of " + this._pagerReportList.Count().ToString() + " out of ";

            //LoadReport();

        }

        //private void cbNumberOfRecords_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    Navigate((int)PagingMode.PageCountChange);
        //}

        private void Navigate(int mode)
        {
            int count;
            switch (mode)
            {
                case (int)PagingMode.Next:
                    btnPrev.IsEnabled = true;
                    btnFirst.IsEnabled = true;
                    if (reportDataList.Count >= (pageIndex * Constants.NumberOfRecordsPerReport))
                    {

                        if (reportDataList.Skip(pageIndex * Constants.NumberOfRecordsPerReport).Take(Constants.NumberOfRecordsPerReport).Count() == 0)
                        {
                            // _pagerReportList = null;
                            _pagerReportList = reportDataList.Skip((pageIndex * Constants.NumberOfRecordsPerReport) - Constants.NumberOfRecordsPerReport).Take(Constants.NumberOfRecordsPerReport).ToList();
                            count = (pageIndex * Constants.NumberOfRecordsPerReport) + (reportDataList.Skip(pageIndex * Constants.NumberOfRecordsPerReport).Take(Constants.NumberOfRecordsPerReport)).Count();
                        }
                        else
                        {
                            _pagerReportList = reportDataList.Skip(pageIndex * Constants.NumberOfRecordsPerReport).Take(Constants.NumberOfRecordsPerReport).ToList();
                            count = (pageIndex * Constants.NumberOfRecordsPerReport) + (reportDataList.Skip(pageIndex * Constants.NumberOfRecordsPerReport).Take(Constants.NumberOfRecordsPerReport)).Count();
                            pageIndex++;
                        }

                        if ((pageIndex * Constants.NumberOfRecordsPerReport) >= reportDataList.Count)
                        {
                            btnNext.IsEnabled = false;
                            btnLast.IsEnabled = false;
                        }

                    }
                    else
                    {
                        btnNext.IsEnabled = false;
                        btnLast.IsEnabled = false;
                    }

                    break;
                case (int)PagingMode.Previous:
                    btnNext.IsEnabled = true;
                    btnLast.IsEnabled = true;
                    pageIndex -= 1;
                    if (pageIndex >= 1)
                    {

                        if (pageIndex == 1)
                        {
                            btnPrev.IsEnabled = false;
                            btnFirst.IsEnabled = false;
                            _pagerReportList = reportDataList.Take(Constants.NumberOfRecordsPerReport).ToList();
                            count = reportDataList.Take(Constants.NumberOfRecordsPerReport).Count();
                        }
                        else
                        {
                            _pagerReportList = reportDataList.Skip((pageIndex - 1) * Constants.NumberOfRecordsPerReport).Take(Constants.NumberOfRecordsPerReport).ToList();
                            count = Math.Min((pageIndex - 1) * Constants.NumberOfRecordsPerReport, reportDataList.Count);
                        }
                    }
                    else
                    {
                        btnPrev.IsEnabled = false;
                        btnFirst.IsEnabled = false;

                    }

                    if (reportDataList.Count <= Constants.NumberOfRecordsPerReport)
                    {
                        btnNext.IsEnabled = false;
                        btnLast.IsEnabled = false;
                        btnPrev.IsEnabled = false;
                        btnFirst.IsEnabled = false;
                    }
                    break;

                case (int)PagingMode.First:
                    pageIndex = 2;
                    Navigate((int)PagingMode.Previous);
                    break;
                case (int)PagingMode.Last:
                    pageIndex = (reportDataList.Count / Constants.NumberOfRecordsPerReport);
                    Navigate((int)PagingMode.Next);
                    break;

                case (int)PagingMode.PageCountChange:
                    pageIndex = 1;
                    _pagerReportList = reportDataList.Take(Constants.NumberOfRecordsPerReport).ToList();
                    count = (reportDataList.Take(Constants.NumberOfRecordsPerReport)).Count();
                    btnNext.IsEnabled = true;
                    btnPrev.IsEnabled = true;
                    break;
            }
        }

        #endregion
        #region Navigation

        public void OnFragmentNavigation(FirstFloor.ModernUI.Windows.Navigation.FragmentNavigationEventArgs e)
        {

        }

        public void OnNavigatedFrom(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {

        }

        public void OnNavigatedTo(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
            this._showConfirmationMsg = false;
        }

        public void OnNavigatingFrom(FirstFloor.ModernUI.Windows.Navigation.NavigatingCancelEventArgs e)
        {
            if (this._showConfirmationMsg)
            {
                string message = "Are you sure you want to leave?";
                if (e.NavigationType == FirstFloor.ModernUI.Windows.Navigation.NavigationType.Refresh)
                {
                    message = "Are you sure you want to refresh the page?";
                }
                var confirm = ModernDialog.ShowMessage(message, "Warning!", MessageBoxButton.YesNo);
                if (confirm == MessageBoxResult.No)
                    e.Cancel = true;
            }
        }

        #endregion

        

    }
}
