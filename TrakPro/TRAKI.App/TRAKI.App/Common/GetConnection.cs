﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Interfaces;
using System.Configuration;

namespace TRAKI.App.Common
{
    public class GetConnection
    {
        private IDbContext dbContext;
        private IDbConnection dbConnection;


        /// <summary>
        /// Method to Intialize the New DB conneciton Object and Data Context.
        /// </summary>
        /// <returns>DB Context Object</returns>
        /// 
        public IDbContext InitializeConnection()
        {
            if (this.CheckPathOrNetworkAvailability(Constants.ConnectionString))
            {
                using (dbConnection = new OleDbConnection(Constants.ConnectionString))
                {
                    dbConnection = new OleDbConnection(Constants.ConnectionString);//(ConfigurationManager.ConnectionStrings["TRAKI"].ConnectionString);
                    dbConnection.Open();
                    dbContext = new DbContext(dbConnection);
                }
            }
            return dbContext;

        }


        /// <summary>
        /// Method to Intialize the New DB conneciton Object and Data Context.
        /// </summary>
        /// <returns>DB Context Object</returns>
        /// 
        public IDbContext InitializeLocalDbConnection()
        {
            dbConnection = new OleDbConnection(Constants.LocalConnectionString);//(ConfigurationManager.ConnectionStrings["TRAKI"].ConnectionString);
            dbConnection.Open();
            dbContext = new DbContext(dbConnection);
            return dbContext;

        }



        /// <summary>
        /// Method to Test the Connection of New DB File Path
        /// </summary>
        /// <returns>True or Flase</returns>
        /// 
        public bool TestConnection(string Path, string Password, ref string ConnectionString)
        {

            OleDbConnectionStringBuilder builder = new OleDbConnectionStringBuilder();
            bool result = false;
          //  builder.Add("Provider", "Microsoft.Jet.OLEDB.4.0");
            builder.Add("Provider", "Microsoft.ACE.OLEDB.12.0");
            builder.Add("Data Source", Path);
            builder.Add("Jet OLEDB:Database Password", Password);
            builder.Add("Persist Security Info", "True");
          
            if (this.CheckPathOrNetworkAvailability(builder.ConnectionString))
            {
                using (dbConnection = new OleDbConnection(builder.ConnectionString))
                {
                    dbConnection.Open(); // throws if invalid
                    dbConnection.Close();
                    ConnectionString = builder.ConnectionString;
                    result = true;
                }
            }
            return result;
        }


        /// <summary>
        /// Overlaod Method to Test the Connection
        /// </summary>
        /// <returns>True or Flase</returns>
        /// 
        public bool TestConnection(string ConnectionString)
        {

            bool result = false;
            try
            {
              
                if (this.CheckPathOrNetworkAvailability(ConnectionString))
                {
                    using (dbConnection = new OleDbConnection(ConnectionString))
                    {
                        dbConnection.Open(); // throws if invalid
                        dbConnection.Close();
                        result = true;
                    }
                }
            }
            catch (Exception ex) { throw ex; }
            return result;
        }


        /// <summary>
        /// Overlaod Teset whether db path is available
        /// </summary>
        /// <returns>True or Flase</returns>
        /// 
        private bool CheckPathOrNetworkAvailability(string ConnectionString)
        {

            bool isChecked = false;
            try
            {
                if (ConnectionString != "")
                {
                    var conStrArray = ConnectionString.Split('=');

                    if ((!@conStrArray[2].Split(';')[0].StartsWith(@"\\")) || (@conStrArray[2].Split(';')[0].StartsWith(@"\\") && System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable()))
                    {
                        isChecked = true;
                    }
                    else
                        throw new Exception("Error: Invalid path or Network not available");
                    return isChecked;
                }
                return isChecked;
            }
            catch (Exception ex) { throw ex; }
        }


    }
}