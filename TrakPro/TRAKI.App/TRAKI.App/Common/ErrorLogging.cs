﻿using System;
using System.Configuration;
using System.IO;
using System.Security.AccessControl;
using System.Security.Principal;


namespace TRAKI.App.Common
{
    public static class ErrorLogging
    {
        /// <summary>
        /// Method to Log the Exception
        /// </summary>
        static internal void LogException(string ErrorMessage)
        {
            try
            {
                if (Convert.ToBoolean(ConfigurationManager.AppSettings["GenerateLog"]))
                {
                    string fileDirPath = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + @"\NYT Inc\Asset TRAK Pro\";
                    //string fileDirPath = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "ErrorLog");
                    if (!Directory.Exists(fileDirPath))  // if it doesn't exist, create
                    Directory.CreateDirectory(fileDirPath);
                    GiveAccessToFile(fileDirPath);
                    string filePath = Path.Combine(fileDirPath, "AppErrorLog.txt");
                    using (StreamWriter writer = new StreamWriter(filePath.ToString(), true))
                    {
                        writer.WriteLine(string.Format(ErrorMessage, DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss tt")));
                        writer.Close();
                    }
                }
            }
            catch (Exception ex) { throw ex; }
        }
         
        /// <summary>
        /// Method to Give access permission to the File
        /// </summary>
        /// 
        private static void GiveAccessToFile(string FilePath)
        {
            DirectorySecurity sec = Directory.GetAccessControl(FilePath);
            SecurityIdentifier everyone = new SecurityIdentifier(WellKnownSidType.WorldSid, null);
            sec.AddAccessRule(new FileSystemAccessRule(everyone, FileSystemRights.Modify | FileSystemRights.Synchronize, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
            Directory.SetAccessControl(FilePath, sec);


        }

    }
}
