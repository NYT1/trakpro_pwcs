﻿using System;
using System.Windows;
using System.Windows.Data;


namespace TRAKI.App.Common.Converters
{
    public class VisibilityConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool flag = false;
            switch (value.ToString().ToLower())
            {
                case "true":
                    {
                        flag = true;
                        break;
                    }
                case "false":
                    {
                        flag = false;
                        break;
                    }
                case "0":
                    {
                        flag = false;
                        break;
                    }
                default:
                    {
                        flag = true;
                        break;
                    }
            }
            return (flag ? Visibility.Visible : Visibility.Collapsed);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
