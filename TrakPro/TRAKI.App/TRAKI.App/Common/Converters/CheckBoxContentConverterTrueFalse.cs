﻿using System;
using System.Windows.Data;


namespace TRAKI.App.Common.Converters
{
    class CheckBoxContentConverterTrueFalse : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            switch (value.ToString().ToLower())
            {
                case "true":
                    return "True";
                case "false":
                    return "False";
            }
            return "No";
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

