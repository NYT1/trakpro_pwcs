﻿using System;
using System.Threading;
using System.Windows;
using System.Windows.Data;
using TRAKI.App.Account;


namespace TRAKI.App.Common.Converters
{
    class ToggleControlVisibilityBasedOnRoleConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool flag = false;
            if (parameter == null)
                parameter = string.Empty;
            if ((Thread.CurrentPrincipal as CustomPrincipal).Identity.CurrentUser.RoleId == 1)
                flag = true;
            else if ((Thread.CurrentPrincipal as CustomPrincipal).Identity.CurrentUser.RoleId != 1 && string.IsNullOrEmpty(parameter.ToString()))
                flag = false;
            else if ((Thread.CurrentPrincipal as CustomPrincipal).Identity.CurrentUser.RoleId == 2 && (parameter.ToString().Equals("True")))
                flag = true;
            else if ((Thread.CurrentPrincipal as CustomPrincipal).Identity.CurrentUser.RoleId == 3 && (parameter.ToString().Equals("False")))
                flag = true;
            else if (parameter.ToString().Equals("0"))
                flag = true;
            else
                flag = false;
            return (flag ? Visibility.Visible : Visibility.Collapsed);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
