﻿using System;
using System.Windows;
using System.Windows.Data;


namespace TRAKI.App.Common.Converters
{
    class ClosedBuildingConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            switch (value.ToString().ToLower())
            {
                case "true":
                    return "Open";
                case "false":
                    return "Open";
                    //return "Closed";
            }
            return "Closed";
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
       
