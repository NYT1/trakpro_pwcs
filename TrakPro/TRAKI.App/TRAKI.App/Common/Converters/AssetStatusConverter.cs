﻿using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Data;
using TRAKI.App.Model;
using TRAKI.App.ViewModel;


namespace TRAKI.App.Common.Converters
{
    class AssetStatusConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Asset asset;
           // AssetViewModel assvm = (AssetViewModel)values[0];
           // asset = assvm.AssetList[0];
            if (null == values) return "New";
            if (values[0].GetType().IsGenericType &&
                values[0].GetType().GetGenericTypeDefinition() == typeof(ObservableCollection<>))
            {
                var _assetList = (ObservableCollection<Asset>)values[0];
                asset = _assetList[0];
            }
            else
                asset = (Asset)values[0];

            string isMissing = asset.IsMissing.ToString();
            if (asset.IsDeleted == BooleanValue.True)
            {
                return "Deleted";
            }
            if (isMissing.Equals("False") && asset.InventoryDate == null)
            {
                return "New";
            }
            
            if (isMissing.Equals("True"))
            {
                return "Missing";
            }
            else
            {
                return "Active";
            }
        }


        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
