﻿using LicenseOperations.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Xml.Linq;

namespace TRAKI.App
{
    public partial class LicenseDialog : Form
    {
        private bool isSuccess;
        public bool IsSuccess
        {
            get { return isSuccess; }
            set { isSuccess = value; }
        }

        public LicenseDialog()
        {

            InitializeComponent();
            this.IsSuccess = true;

        }

        #region Private

        private void btnSaveDBPath_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(this.TextDBFilePath.Text.Trim()))
                {
                    System.Windows.Forms.MessageBox.Show("Please select a license file", "Error!", MessageBoxButtons.OK, MessageBoxIcon.None);
                    return;
                }

                if (!this.CheckLicenseForCurrentMachine(this.TextDBFilePath.Text.Trim()))
                {
                    System.Windows.Forms.MessageBox.Show("This is not a valid License for this particular machine!", "Not valid License!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else if (!this.CheckLicenseExpiration(this.TextDBFilePath.Text.Trim()))
                {
                    System.Windows.Forms.MessageBox.Show("This copy of Trak pro license has expired, please select a different license!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                else
                {
                    LicenseOperations.Operations opr = new LicenseOperations.Operations();
                    if (IoHelper.CopyFile(this.TextDBFilePath.Text.Trim(), IoHelper.BasePath + @"\License.licx"))
                    {
                        System.Windows.Forms.MessageBox.Show("License Successfully Installed", "Success", MessageBoxButtons.OK, MessageBoxIcon.None);
                        this.Close();
                    }
                    else
                    {
                        System.Windows.Forms.MessageBox.Show("License installation Failed!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }

            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show("Error:" + ex.Message, "Trak Pro Report Service Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        /// <summary>
        /// File to check whether the license is valid for this machine
        /// </summary>
        /// <param name="FilePath"></param>
        /// <returns></returns>
        private bool CheckLicenseForCurrentMachine(string FilePath)
        {
            bool isValidMachine = false;
            try
            {
                LicenseOperations.Operations operation = new LicenseOperations.Operations();
                Dictionary<string, string> LicenseDetail = operation.GetLicenseDetails(FilePath);
                if (!string.IsNullOrEmpty(LicenseDetail["machineid"]))
                {


                    //Komal---Check the Machine Name instead  of MAC ID--03/05/2018

                    string[] listMachineId = LicenseDetail["machineid"].Split(',');
                    foreach (string machineId in listMachineId)
                    {
                        if (Environment.MachineName.Equals(machineId))
                        {
                            isValidMachine = true;
                            break;
                        }
                    }

                    //foreach (var macAddress in GetMacAddress())
                    //{
                    //    string[] listMachineId = LicenseDetail["machineid"].Split(',');
                    //    foreach (string machineId in listMachineId)
                    //    {
                    //        if (macAddress.Equals(machineId))
                    //        {
                    //            isValidMachine = true;
                    //            break;
                    //        }
                    //    }
                    //    if (isValidMachine)
                    //        break;
                    //}
                }
            }
            catch (FileNotFoundException ex)
            {
                ;
            }
            return isValidMachine;
        }


        /// <summary>
        /// File to check whether the license is valid for this machine
        /// </summary>
        /// <param name="FilePath"></param>
        /// <returns></returns>
        private bool CheckLicenseExpiration(string FilePath)
        {
            bool isValid = false;
            try
            {
                LicenseOperations.Operations operation = new LicenseOperations.Operations();
                Dictionary<string, string> LicenseDetail = operation.GetLicenseDetails(FilePath);
                //DateTime ExpirationDate = DateTime.ParseExact(LicenseDetail["expirationdate"], "dd/MM/yyyy", null);
                DateTime ExpirationDate = DateTime.ParseExact(LicenseDetail["expirationdate"], "dd/MM/yyyy", null);
                if (ExpirationDate < DateTime.Now.Date)
                {
                    isValid = false;
                }
                else
                {
                    isValid = true;
                }
            }
            catch (FileNotFoundException ex)
            {
                ;
            }
            return isValid;
        }


        /// <summary>
        /// Finds the MAC address of the first operation NIC found.
        /// </summary>
        /// <returns>The MAC address.</returns>
        private List<string> GetMacAddress()
        {
            List<string> macAddresses = new List<string>();// string.Empty;

            foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
            {
                if (nic.NetworkInterfaceType == NetworkInterfaceType.Ethernet)
                {
                    macAddresses.Add(BitConverter.ToString(nic.GetPhysicalAddress().GetAddressBytes()));
                }
            }

            return macAddresses;
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.FileDialog dialog = new OpenFileDialog();
            dialog.Filter = "License File (*.licx)|*.licx";
            dialog.InitialDirectory = @"C:\";
            dialog.Title = "Please select a License file.";
            dialog.SupportMultiDottedExtensions = false;
            DialogResult dgResult = this.STAShowDialog(dialog);

            if (dgResult == DialogResult.OK)
            {
                this.TextDBFilePath.Text = dialog.FileName;
            }
        }

        #endregion

        #region Dialog Thread

        private DialogResult STAShowDialog(FileDialog dialog)
        {
            DialogState state = new DialogState();
            state.dialog = dialog;
            System.Threading.Thread t = new System.Threading.Thread(state.ThreadProcShowDialog);
            t.SetApartmentState(System.Threading.ApartmentState.STA);
            t.Start();
            t.Join();
            return state.result;
        }

        public class DialogState
        {
            public DialogResult result;
            public FileDialog dialog;
            public void ThreadProcShowDialog()
            {
                result = dialog.ShowDialog();
            }
        }

        #endregion

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.IsSuccess = false;
            this.Close();
        }


    }
}
