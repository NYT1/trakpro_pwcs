﻿namespace TRAKI.App.Account
{
    class AnonymousIdentity: CustomIdentity
    {
        public AnonymousIdentity()
            : base(new Model.User())
        { }
    }
}