﻿using System.Linq;
using System.Text;
using System.Security.Cryptography;
using TRAKI.App.Model;
using System;
using TRAKI.App.Common;
using TRAKI.App.Interfaces;

namespace TRAKI.App.Account
{
    public interface IAuthenticationService
    {
        User AuthenticateUser(User CurrentUser);
    }

    public class AuthenticationService : IAuthenticationService
    {
      
        //private string CalculateHash(string clearTextPassword, string salt)
        //{
        //    // Convert the salted password to a byte array
        //    byte[] saltedHashBytes = Encoding.UTF8.GetBytes(clearTextPassword + salt);
        //    // Use the hash algorithm to calculate the hash
        //    HashAlgorithm algorithm = new SHA256Managed();
        //    byte[] hash = algorithm.ComputeHash(saltedHashBytes);
        //    // Return the hash as a base64 encoded string to be compared to the stored password
        //    return Convert.ToBase64String(hash);
        //}


        /// <summary>
        /// Method to Authenticate the user
        /// </summary>
        /// <param name="User Object with loging Credentials"></param>
        /// <returns>Valid user detail object or access denied exception if Invalid users</returns>
        /// 
        public User AuthenticateUser(User CurrentUser)
        {
            GetConnection getConnection = new GetConnection();
            IDbContext dbContext = getConnection.InitializeConnection();
            User _currentUser = dbContext.Execute(new DapperQueries.UserQueries.Account.AuthenticateUser(CurrentUser));

            //Steve
            //0-- origanl  1--else
            Constants.InnerErrorType = 0;

            if (_currentUser == null)
            {
                Constants.InnerErrorType=0;
                throw new UnauthorizedAccessException("Access denied. Please provide some valid credentials.");
            }
                else
            {
                if (_currentUser.IsActive == BooleanValue.False)
                {
                    //Steve
                    //0-- origanl  1--else

                    Constants.InnerErrorType=1;
                    throw new UnauthorizedAccessException( "This user account is inactive.  Please contact your administrator to re-activate your account.");
                };

            };

            return _currentUser;
        }
    }
    
}