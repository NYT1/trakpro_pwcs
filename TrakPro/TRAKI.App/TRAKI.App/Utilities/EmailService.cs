﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using TRAKI.App.Common;
using TRAKI.App.Model;

namespace TRAKI.App.Utilities
{
    class EmailService
    {

        internal void SendEmail(string ToEmailAddress, string subject, string body)
        {
            try
            {
                //  string SenderEmailId = Constants.AppSettings.Where(setting => setting.SettingName == "SenderEmailId").SingleOrDefault().SettingValue;
                string EmailHost = Constants.AppSettings.Where(setting => setting.SettingName == "EmailHost").SingleOrDefault().SettingValue;
                int EmailPort = Convert.ToInt32(Constants.AppSettings.Where(setting => setting.SettingName == "EmailPort").SingleOrDefault().SettingValue);
                string SenderPassword = Constants.AppSettings.Where(setting => setting.SettingName == "SenderPassword").SingleOrDefault().SettingValue;
                bool EnableSsl = Convert.ToBoolean(Constants.AppSettings.Where(setting => setting.SettingName == "EnableSsl").SingleOrDefault().SettingValue);
                string SenderName = Constants.AppSettings.Where(setting => setting.SettingName == "SenderName").SingleOrDefault().SettingValue;
                var SenderEmailId = new MailAddress(Constants.AppSettings.Where(setting => setting.SettingName == "SenderEmailId").SingleOrDefault().SettingValue.ToString(), SenderName);
                var toEmailAddress = new MailAddress(ToEmailAddress);



                SmtpClient smtp = new SmtpClient
                {
                    Host = EmailHost, // smtp server address here…
                    Port = EmailPort,
                    EnableSsl = EnableSsl,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    Credentials = new System.Net.NetworkCredential(SenderEmailId.Address, SenderPassword),
                    Timeout = 30000,

                };
                MailMessage message = new MailMessage(SenderEmailId, toEmailAddress);
                message.IsBodyHtml = true;
                message.Body = body;
                message.Subject = subject;
                smtp.Send(message);
            }
            catch (Exception ex)
            {
                ErrorLogging.LogException("Error Email : " + ex.Message);
            }
            //return result;
        }
    }
}


