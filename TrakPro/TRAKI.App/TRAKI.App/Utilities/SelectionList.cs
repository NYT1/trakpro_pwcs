﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TRAKI.App.Utilities
{
    /// <summary>
    /// a list that will include the elements that will be selected
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class SelectionList<T> : List<SelectionItem<T>>, INotifyPropertyChanged
    {
        #region private fields
        /// <summary>
        /// the number of selected elements
        /// </summary>
        private int _selectionCount;
        #endregion

        #region private methods
        /// <summary>
        /// this events responds to the "IsSelectedEvent"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void item_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            var item = sender as SelectionItem<T>;
            if ((item != null) && e.PropertyName == "IsSelected")
            {
                if (item.IsSelected)
                    SelectionCount = SelectionCount + 1;
                else
                    SelectionCount = SelectionCount - 1;
            }
        }

        #endregion

        public SelectionList()
        { }

        /// <summary>
        /// creates the selection list from an existing simple list
        /// </summary>
        /// <param name="elements"></param>
        public SelectionList(IEnumerable<T> elements)
        {
            foreach (T element in elements)
                AddItem(element);
        }

        #region public methods
        /// <summary>
        /// adds an element to the element and listens to its "IsSelected" property to update the SelectionCount property
        /// use this method insteand of the "Add" one
        /// </summary>
        /// <param name="element"></param>
        public void AddItem(T element)
        {
            var item = new SelectionItem<T>(element);
            item.PropertyChanged += item_PropertyChanged;

            Add(item);
        }

        /// <summary>
        /// gets the selected elements
        /// </summary>
        /// <returns></returns>
        public IEnumerable<T> GetSelection()
        {
            return this.Where(e => e.IsSelected).Select(e => e.Element);
        }

        /// <summary>
        /// uses linq expression to select a part of an object (for example, only id)
        /// </summary>
        /// <typeparam name="U"></typeparam>
        /// <param name="expression"></param>
        /// <returns></returns>
        public IEnumerable<U> GetSelection<U>(Func<SelectionItem<T>, U> expression)
        {
           return this.Where(e => e.IsSelected).Select(expression);
        }

        #endregion


        #region public properties
        /// <summary>
        /// the selection count property is ui-bindable, returns the number of selected elements
        /// </summary>
        public int SelectionCount
        {
            get { return _selectionCount; }

            private set
            {
                _selectionCount = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("SelectionCount"));
            }
        }
        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion
    }
}
