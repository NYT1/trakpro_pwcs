﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Configuration.Install;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Security.Principal;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace TrackService
{
    public static class Program
    {

        /// <summary
        /// The main entry point for the application.
        /// </summary
        public static void Main(string[] args)
        {
            string connection = "";
            try
            {
                bool install = false, uninstall = false;
                 foreach (string arg in args) 
                 { 
                     switch (arg) 
                     { 
                         case "-i": 
                         case "-install": 
                             install = true; break; 
                         case "-u": 
                         case "-uninstall": 
                             uninstall = true; break; 
                         default: 
                             Console.Error.WriteLine("Argument not expected: " + arg); 
                             break; 
                     } 
                 }

                 if (uninstall) 
                 { 
                     Install(true, args); 
                 } 
                 if (install) 
                 { 
                     Install(false, args);
                     StartService();
                 } 
                 
                 if (!(install || uninstall))
                 {

                     connection = ConfigurationManager.ConnectionStrings["TRAKI"].ToString();
                     ServiceBase[] ServicesToRun;

                     ServicesToRun = new ServiceBase[] 
                    { 
                        new Service1(connection) 
                    };

                     ServiceBase.Run(ServicesToRun);
                 }
            }
            catch (Exception e)
            {
                throw;
            }
        }

        private static void StartService()
        {
            using (ServiceController controller =
                new ServiceController("TrakService"))
            {
                try
                {
                    if (controller.Status != ServiceControllerStatus.Running)
                    {
                        controller.Start();
                        controller.WaitForStatus(ServiceControllerStatus.Running,
                            TimeSpan.FromSeconds(10));
                    }
                }
                catch
                {
                    throw;
                }
            }
        }
       
        private static void fullacc(string path)
        {
            DirectorySecurity sec = Directory.GetAccessControl(path);
            SecurityIdentifier everyone = new SecurityIdentifier(WellKnownSidType.WorldSid, null);
            sec.AddAccessRule(new FileSystemAccessRule(everyone, FileSystemRights.Modify | FileSystemRights.Synchronize, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
            Directory.SetAccessControl(path, sec);
        }
     

        static void Install(bool undo, string[] args)
         {
             try
             {
                 Console.WriteLine(undo ? "uninstalling" : "installing");
                 using (AssemblyInstaller inst = new AssemblyInstaller(typeof(Program).Assembly, args))
                 {
                     IDictionary state = new Hashtable();
                     inst.UseNewContext = true;
                     try
                     {
                         if (undo)
                         {
                             inst.Uninstall(state);
                         }
                         else
                         {
                             inst.Install(state);
                             inst.Commit(state);
                         }
                     }
                     catch
                     {
                         try
                         {
                             inst.Rollback(state);
                         }
                         catch { }
                         throw;
                     }
                 }
             }
             catch (Exception ex)
             {
                 Console.Error.WriteLine(ex.Message);
             }
         }
         
    }
}
