﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrackService
{
    public static class Library
    {
        public static string connectionstring;
        public static void ListofReports()
        {
            OleDbConnection con = new OleDbConnection();
            con.ConnectionString = connectionstring;
            con.Open();
            using (OleDbCommand cmd1 = new OleDbCommand())
            {
                //Asset list by building Report              
                cmd1.Connection = con;
                cmd1.CommandText = @"Delete * from AssetListbyBuildingReport";
                cmd1.ExecuteNonQuery();
                cmd1.CommandText = @"INSERT INTO AssetListbyBuildingReport (
                                    AssetNumber,
                                    Cost,
                                    SerialNumber,                                   
                                    InventoryDate,
                                    BuildingNumber,
                                    BuildingDescription,
                                    CatalogNumber,
                                    CatalogDescription,
                                    RoomNumber
                                    )
                         SELECT  Asset.AssetNumber,  Asset.Cost,  Asset.SerialNumber ,  Asset.InventoryDate, Building.BuildingNumber,  Building.BuildingDescription, [Catalog].CatalogNumber,
                       [Catalog].CatalogDescription , AssetLocation.RoomNumber
                        from ((Asset 
                        INNER JOIN
                        [Catalog] on [Catalog].CatalogNumber =Asset.CatalogNumber)
                        INNER JOIN
                        AssetLocation on AssetLocation.AssetLocationId= Asset.AssetLocationId)
                        INNER JOIN
                        Building on Building.BuildingNumber =AssetLocation.BuildingNumber  Where (Asset.IsDeleted=0 and AssetLocation.IsActive=-1); ";
                cmd1.ExecuteNonQuery();

                //Asset Value Summary report
                cmd1.CommandText = @"Delete * from AssetValueSummaryReport";
                cmd1.ExecuteNonQuery();
                cmd1.CommandText = @"INSERT INTO AssetValueSummaryReport (
                                    Cost,
                                    BuildingNumber,
                                    BuildingDescription                                   
                                    )
                        SELECT   Sum(Asset.Cost) as Cost ,  Building.BuildingNumber,  Building.BuildingDescription 
                        from (Asset 
                        INNER JOIN 
                        AssetLocation on AssetLocation.AssetLocationId= Asset.AssetLocationId)
                        INNER JOIN
                        Building on Building.BuildingNumber =AssetLocation.BuildingNumber 
						Where( AssetLocation.IsActive= -1 and Asset.IsDeleted=0)
                        Group By Building.BuildingNumber,  Building.BuildingDescription; ";
                cmd1.ExecuteNonQuery();


                //CAFETERIA REPORT
                cmd1.CommandText = @"Delete * from CafeteriaReport";
                cmd1.ExecuteNonQuery();
                cmd1.CommandText = @"INSERT INTO CafeteriaReport (
                                    BuildingNumber,
                                    BuildingDescription,
                                    DateReceived,
                                    CurrentStatus,
                                    TotalCost                                   
                                    )
                        Select BuildingNumber, BuildingDescription , DateReceived, CurrentStatus, TotalCost  from 
						(
						SELECT    B.BuildingNumber,    B.BuildingDescription ,B.DateReceived,B.CurrentStatus,       
                                        ( 
                                        Select sum(AST.cost)
                                        from (Asset AST
												INNER JOIN 
												AssetLocation ASL on ASL.AssetLocationId=  AST.AssetLocationId)
												INNER JOIN
												Building BL on BL.BuildingNumber =ASL.BuildingNumber
                                                     Where ( ASL.IsActive=-1  and AST.IsDeleted=0 and ASL.BuildingNumber= B.BuildingNumber and AST.AssetNumber like 'C%')
                                                group by  BL.BuildingNumber,  BL.BuildingDescription , BL.DateSigned, BL.CurrentStatus

                                                  ) as TotalCost 
												from (Asset A
												INNER JOIN 
												AssetLocation AL on AL.AssetLocationId=  A.AssetLocationId)
												INNER JOIN
												Building B on B.BuildingNumber =AL.BuildingNumber Where (A.IsDeleted=0 and  A.AssetNumber like 'C%' ));";
                cmd1.ExecuteNonQuery();


                //DISPOSITION REPORT
                cmd1.CommandText = @"Delete * from DispositionReport";
                cmd1.ExecuteNonQuery();
                cmd1.CommandText = @"INSERT INTO DispositionReport (
                                    AssetNumber,
                                    Cost,
                                    SerialNumber,
                                    DateAcquired,
                                    CatalogDescription,
                                    CatalogNumber,
                                    BuildingNumber,
                                    Condition,
                                    ManufacturerName,
                                    ModelNumber
                                    )
                        SELECT Asset.AssetNumber, Asset.Cost, Asset.SerialNumber, Asset.DateAcquired, [Catalog].CatalogDescription,  
                                  [Asset].CatalogNumber,  Building.BuildingNumber, '' as [Condition], Manufacturer.ManufacturerName, Asset.ModelNumber 
                                from ((((Asset 
                                INNER JOIN
                                [Catalog] on [Catalog].CatalogNumber =Asset.CatalogNumber)
                                LEFT JOIN
                                 Manufacturer on Manufacturer.ManufacturerCode =Asset.ManufacturerCode)
                                INNER JOIN
                                 Disposition on Disposition.DispositionCode =Asset.DispositionCode)
                                INNER JOIN
                                AssetLocation on AssetLocation.AssetLocationId= Asset.AssetLocationId)
                                INNER JOIN
                                Building on Building.BuildingNumber =AssetLocation.BuildingNumber where (Asset.IsDeleted=0); ";
                cmd1.ExecuteNonQuery();


                //INVENTORY SUMMARY BY BUILDING REPORT
                cmd1.CommandText = @"Delete * from InventorySummarybyBuildingReport";
                cmd1.ExecuteNonQuery();
                cmd1.CommandText = @"INSERT INTO InventorySummarybyBuildingReport (
                                    BuildingNumber,
                                    BuildingDescription,
                                    CurrentStatus,
                                    DateSigned,
                                    MissingCost,
                                    TotalCost,
                                    BuildingType,
                                    MissingCostPercentage                                   
                                    )
                        Select BuildingNumber, BuildingDescription, CurrentStatus , DateSigned, MissingCost, TotalCost, BuildingType  ,  ((MissingCost /TotalCost) * 100) as MissingCostPercentage from 
						(
						SELECT    B.BuildingNumber, B.BuildingDescription , B.DateSigned, B.CurrentStatus, BT.BuildingTypeDescription as BuildingType, 
                                         sum(cost)  as TotalCost ,  
                                        ( 
                                        Select sum(AST.cost)
                                        from ((Asset AST
												INNER JOIN 
												AssetLocation ASL on ASL.AssetLocationId=  AST.AssetLocationId)
												INNER JOIN
												Building BL on BL.BuildingNumber =ASL.BuildingNumber)
                                                inner join BuildingType BLT on BLT.BuildingTypeId = BL.BuildingTypeId
                                                     Where (AST.IsDeleted=0 and ASL.BuildingNumber= B.BuildingNumber)
                                                group by  BL.BuildingNumber,  BL.BuildingDescription , BL.DateSigned, BL.CurrentStatus, BLT.BuildingTypeDescription

                                                  ) as MissingCost 
												from ((Asset A
												INNER JOIN 
												AssetLocation AL on AL.AssetLocationId=  A.AssetLocationId)
												INNER JOIN
												Building B on B.BuildingNumber =AL.BuildingNumber)
                                                inner join BuildingType BT on BT.BuildingTypeId = B.BuildingTypeId
                                                     Where (A.IsDeleted=0)            
                                                group by  B.BuildingNumber,  B.BuildingDescription , B.DateSigned, B.CurrentStatus, BT.BuildingTypeDescription); ";
                cmd1.ExecuteNonQuery();


                //MissingAssetsReport
                cmd1.CommandText = @"Delete * from MissingAssetsReport";
                cmd1.ExecuteNonQuery();
                cmd1.CommandText = @"INSERT INTO MissingAssetsReport (
                                    AssetNumber,
                                    Cost,
                                    SerialNumber,
                                    CatalogDescription,
                                    CatalogNumber,
                                    BuildingNumber,
                                    BuildingDescription,
                                    RoomNumber                                   
                                    )
                        SELECT Asset.AssetNumber, Asset.Cost, Asset.SerialNumber, [Catalog].CatalogDescription, [Catalog].CatalogNumber, Building.BuildingNumber, Building.BuildingDescription, Room.RoomNumber
                                    FROM 
									(((Asset 
									INNER JOIN [Catalog] ON [Catalog].CatalogNumber =Asset.CatalogNumber) 
									INNER JOIN AssetLocation ON AssetLocation.AssetLocationId= Asset.AssetLocationId) 
									INNER JOIN Building ON Building.BuildingNumber =AssetLocation.BuildingNumber) 
									INNER JOIN Room ON Room.RoomNumber =AssetLocation.RoomNumber
                                    Where(Asset.IsDeleted=0 and Asset.IsMissing=-1); ";
                cmd1.ExecuteNonQuery();



                //PerkinsReport
                cmd1.CommandText = @"Delete * from PerkinsReport";
                cmd1.ExecuteNonQuery();
                cmd1.CommandText = @"INSERT INTO PerkinsReport (
                                    AssetNumber,
                                    Cost,
                                    SerialNumber,
                                    InventoryDate,
                                    BuildingNumber,
                                    BuildingDescription,
                                    RoomNumber,
                                    PONumber                                   
                                    )
                        SELECT  Asset.AssetNumber, Asset.Cost, Asset.SerialNumber, Asset.InventoryDate, Building.BuildingNumber,  Building.BuildingDescription , AssetLocation.RoomNumber, '' as PONumber
                        from (Asset 
                        INNER JOIN
                        AssetLocation on AssetLocation.AssetLocationId= Asset.AssetLocationId)
                        INNER JOIN
                        Building on Building.BuildingNumber =AssetLocation.BuildingNumber  Where (Asset.AssetNumber like 'P%' and Asset.IsDeleted=0); ";
                cmd1.ExecuteNonQuery();
            }

            con.Close();
        }
    }
}
